<footer class="footer">
	<div class="container">
		<div class="row">
			<div data-wow-delay=".1s" data-wow-duration="2s" class="col-lg-4 col-sm-4 address wow fadeInUp animated" style="visibility: visible; animation-duration: 2s; animation-delay: 0.1s; animation-name: fadeInUp;" data-wow-animation-name="fadeInUp">
				<h1 align="left"> contact info </h1>
				<address>
					<h3 align="left" style="color:#FFF;">{$company_name}</h3>
					<p><i class="fa fa-home pr-10"></i>Address: {$company_address}</p>
					<!--<p><i class="fa fa-globe pr-10"></i>Mars city, Country </p>-->
					<p><i class="fa fa-mobile pr-10"></i>Mobile: {$mobile_no} </p>
					<p><i class="fa fa-phone pr-10"></i>Phone: {$phone} </p>
					<p><i class="fa fa-envelope pr-10"></i>Email: <a href="javascript:;">{$SITE_EMAIL}</a></p>
				</address>
			</div>
			<div class="col-lg-4 col-sm-4">
				<div data-wow-delay=".5s" data-wow-duration="2s" class="page-footer wow fadeInUp animated" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeInUp;" data-wow-animation-name="fadeInUp">
					<h1 align="left"> Our Company </h1>
					<ul style="text-align:left;" class="page-footer-list">
						<li> <i class="fa fa-angle-right"></i> <a href="about.html"> About Us</a></li>
						<li> <i class="fa fa-angle-right"></i> <a href="privacy.html"> Privacy Policy</a></li>
						<li> <i class="fa fa-angle-right"></i> <a href="terms.html"> Terms &amp; Conditions</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-4 col-sm-4">
				<div data-wow-delay=".7s" data-wow-duration="2s" class="text-footer wow fadeInUp animated" style="visibility: visible; animation-duration: 2s; animation-delay: 0.7s; animation-name: fadeInUp;" data-wow-animation-name="fadeInUp">
					<h1 align="left"> Why Choose Us </h1>
					<p> Load, freight and transport exchange service with a robust, instinctive load-matching and freight exchange calculator. Powerful real-time load tracking from departure to destination. Optimise your fleet’s utilisation quickly and efficiently. </p>
				</div>
			</div>
		</div>
	</div>
</footer>
<footer class="footer-small">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-sm-6 pull-right">
				<ul class="social-link-footer list-unstyled">
					<li data-wow-delay=".1s" data-wow-duration="2s" class="wow flipInX animated" style="visibility: visible; animation-duration: 2s; animation-delay: 0.1s; animation-name: flipInX;" data-wow-animation-name="flipInX"><a target="_new" href="https://www.facebook.com/eagletechnosys"><i class="fa fa-facebook"></i></a></li>
					<!--  <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".2s"><a href="https://www.facebook.com/eagletechnosys"><i class="fa fa-google-plus"></i></a></li>--> 
					<!--<li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".3s"><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".4s"><a href="#"><i class="fa fa-linkedin"></i></a></li>-->
					<li data-wow-delay=".5s" data-wow-duration="2s" class="wow flipInX animated" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: flipInX;" data-wow-animation-name="flipInX"><a target="_new" href="https://www.twitter.com/eagletechnosys"><i class="fa fa-twitter"></i></a></li>
					<!--     <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".6s"><a href="#"><i class="fa fa-skype"></i></a></li>
                        <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".7s"><a href="#"><i class="fa fa-github"></i></a></li>-->
					<li data-wow-delay=".8s" data-wow-duration="2s" class="wow flipInX animated" style="visibility: visible; animation-duration: 2s; animation-delay: 0.8s; animation-name: flipInX;" data-wow-animation-name="flipInX"><a target="_new" href="https://www.youtube.com/eagletechnosys"><i class="fa fa-youtube"></i></a></li>
				</ul>
			</div>
			<div class="col-md-4">
				<div class="copyright">
					<p>{$company_name} &nbsp;&copy; 2015</p>
				</div>
			</div>
		</div>
	</div>
</footer>
