<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>View Payment Detail</h2> 
                     
                    </div>
                </div>
              
                 <hr />  <a href="{$site_url}/admin.php?page=payment&action=show_payment" style="float:right; margin-bottom:8px;">  <button class="btn btn-default btn" onclick="goBack()">Go Back</button></a>  
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Customer {$functions->get_name($usersdata[0].user_id)} Payment Detail
                           
                            
                        </div>
                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered ">
                                    <tbody>
                                        <tr>
                                                      
                                        	<th>User Name</th>
                                            <td>{$functions->get_name($usersdata[0].user_id)}</td>
                                          <th>Email Address</th>
                                            <td>{$functions->get_email($usersdata[0].user_id)}</td>
                                            </tr>
                                            <tr>
                                              <th>Mobile</th>
                                            <td>{$functions->get_mobile($usersdata[0].user_id)}</td>
                                            <th>Entery Date</th>
                                            <td>{$usersdata[0].entrydate}</td>
                                        </tr>
                                        <tr>
                                              <th>State</th>
                                            <td>{$functions->get_State($usersdata[0].user_id)}</td>
                                            <th>City</th>
                                            <td>{$functions->get_city($usersdata[0].user_id)}</td>
                                        </tr>
                                        <tr>
                                             <th>Payment Option</th>
                                            <td>{$usersdata[0].payment_opt}</td>
                                            <th>Amount</th>
                                            <td>{$usersdata[0].amount}</td>
                                        </tr>
                                         <tr>
                                              <th>Pay Status</th>
                                            <td>{$usersdata[0].paystatus}</td>
                                            <th></th>
                                            <td></td>
                                        </tr>
                                        
                                    </tbody>
                                    
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->
            
                <!-- /. ROW  -->
            
             
        </div>
               
    </div>