{literal}
<style style="text/css">
	/* Define the hover highlight color for the table row */
    .hoverTable tr:hover {
          background-color: #ffff99;
    }
</style>
{/literal}
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
                        <h1 class="page-header">Location List
                     </h1>
                         
         </div>
    </div>
    <!-- /. ROW  -->

    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading">
                     <table class="table table-striped table-bordered table-hover">
                   <tr>
                                <th class="head1" colspan="5">
                                <form method="POST" id="bar_code_Frm" name="bar_code_Frm" >
                             {if $Country_name!=''} <a href="{$site_url}/admin.php?page=location_mange&action=add_state&country_name={$Country_name}" title="Add New State"><button type="button"  class="btn btn-default" >Add State</button></a>{/if} 
                              {if $State_name!=''}
                                    <a href="{$site_url}/admin.php?page=location_mange&action=add_city&state_name={$State_name}" title="Add New City"><button type="button"  class="btn btn-default" >Add City</button></a>{/if}
                              
							 {if $Country_name=='' && $State_name==''}  <a href="{$site_url}/admin.php?page=location_mange&action=add_country" title="Add New Country"> 	<button type="button"  class="btn btn-default" >Add Country </button></a>{/if}
		 						 </form>
                                </th>
                            </tr> 
                   </table>
                 <table class="table table-striped table-bordered table-hover">
                             
                            <thead>
                          
                                <tr>
                                    <th width="5%">Sr.no</th>
                                    <th width="20%">Country Name</th>
                                   {if $Country_name=='' || $State_name==''}    <th width="20%">Flag</th>{/if}
                                  {*  <th width="12%">Title</th>*}
                                     {if $Country_name!='' || $State_name!=''}
                                    <th width="20%">State</th>
                                    {/if}
                                    {if $State_name!=''}
                                    <th width="15%">City</th>
                                    {/if}
                                    <th width="20%">Actions</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                            {if $location_list}
                            	{section name=data loop=$location_list}
                                <tr>
                                    <td>{$smarty.section.data.index+$row_no+1}</td>
                                    <td>
                                    <a href="{$site_url}/admin.php?page=location_mange&action=show_location_list&country_name={$location_list[data].country}">
                                    {$location_list[data].country}
                                    </a>
                                    </td>
                                  {if $Country_name=='' || $State_name==''}   <td>{if $location_list[data].flage!=''}<img src="{$site_url}/location_flage/{$location_list[data].flage}" height="20px" width="30px" />{/if} </td>{/if}
                                    
                                     {if $Country_name!='' || $State_name!=''}
                                    <td>
                                    <a href="{$site_url}/admin.php?page=location_mange&action=show_location_list&state_name={$location_list[data].state}">
                                    {$location_list[data].state}
                                    </a></td>
                                    {/if}
                                    {if $State_name!=''}
                                    <td>{$location_list[data].city}</td>    
                                    {/if}
                                    <td>
                                    <!--=================================active and inactive=============================--->
                             <!-- {if $location_list[data].status=='Y'} 
                                     <a href="{$site_url}/admin.php?page=location_mange&action=add_country&active_id={$location_list[data].id}&active=N" title="Active">
                                    <img src="{$site_url}/templates/admin/img/icons/bs/ic_unlock.png" />
                                    </a>
                                    {else}
                                    <a href="{$site_url}/admin.php?page=location_mange&action=add_country&active_id={$location_list[data].id}&active=Y" title="Inactive">
                                    <img src="{$site_url}/templates/admin/img/icons/bs/ic_lock.png" />
                                    </a>
                                    {/if}-->
                                    <!--================================================================================-->
                                   {if $Country_name!=''}
                                    <a href="{$site_url}/admin.php?page=location_mange&action=add_state&edit_id={$location_list[data].id}" class="fa fa-pencil-square-o" title="{$lang_edit}"> {/if}
                                   {if $State_name!=''}
                                    <a href="{$site_url}/admin.php?page=location_mange&action=add_city&edit_id={$location_list[data].id}"class="fa fa-pencil-square-o" title="{$lang_edit}"> {/if}
                                   {if $Country_name=='' && $State_name==''} 
                                   <a href="{$site_url}/admin.php?page=location_mange&action=add_country&edit_id={$location_list[data].id}" class="fa fa-pencil-square-o" title="{$lang_edit}">
                                   {/if}
                                   
                                  
                                   <!-- <img src="{$site_url}/templates/admin/img/icons/bs/ic_edit.png" />-->
                                    </a>
                                      {if $Country_name!=''}
                                    <a href="{$site_url}/admin.php?page=location_mange&action=show_location_list&state_delete_id={$location_list[data].state}" title="{$lang_delete}" onclick="return confirm('{$lang_do_you_want_delete}');" class="fa fa-trash-o">{/if}
                                      {if $State_name!=''}
                                        <a href="{$site_url}/admin.php?page=location_mange&action=show_location_list&city_delete_id={$location_list[data].city}" title="{$lang_delete}" onclick="return confirm('{$lang_do_you_want_delete}');" class="fa fa-trash-o">{/if}
                                      {if $Country_name=='' && $State_name==''} 
                                          <a href="{$site_url}/admin.php?page=location_mange&action=show_location_list&country_delete_id={$location_list[data].country}" title="{$lang_delete}" onclick="return confirm('{$lang_do_you_want_delete}');" class="fa fa-trash-o">{/if}
                                   <!-- <img src="{$site_url}/templates/admin/img/icons/bs/ic_delete.png" />-->
                                    </a>
                                    </td>                            
                                </tr>
                                {/section}
                                 <tr>
                                	<td colspan="6" align="center">
                                    	  <form name="frm_pagi" action="" method="post">
                   			
                           <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />
                           
                           
                           

                      	{$show_pagi}
                        </form>
                                    </td>
                                </tr>
                            {else}
                            	<tr><td colspan="6">{$lang_record_not_found}</td></tr>
                            {/if}    
                            </tbody>
                        </table>
                    </table>
    </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>