
{if $type=='C'}

<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2></h2>
      </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading"><b> {if $edit_id==''}Add{else}Edit{/if} Customer</b>
           
       
         </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
                
                <!--===============customer form======================-->
                <form role="form" name="add_new_customer" id="add_new_customer" action="" method="post">
                  <input type="text" name="edit_id" id="edit_id" value="{$edit_id}" />
                  <input type="text" name="type" id="type"  value="{$type}" />
                     <h4>{$show_message}</h4> 
              
                  <div class="form-group">
                    <label>Customer/Transporter Name</label>
                     {$functions->get_customer_detail($usersdata[0].user_id)}
                    <input class="form-control" type="text" name="customer_name" readonly="readonly" id="user_name" value="{$customer_info[0].name }" placeholder="Enter Customer Name " />
                   </div>
                  <div class="form-group">
                    <label>Communication Rating</label>
                    <input class="form-control" type="text" name="com_rating" id="Communication" value="{$usersdata[0].com_rating}" placeholder="Enter communication rating " />
                    </div>
                  <div class="form-group">
                    <label>punctuality Rating</label>
                    <input class="form-control" type="text" name="pun_rating" id="punctuality" value="{$usersdata[0].pun_rating	}" placeholder="Enter punctuality rating " />
                   </div>
                  
                  <!--==========password=========-->
                  
                  <div class="form-group">
                    <label>Over All rating</label>
                    <input class="form-control" type="text" name="over_rating" id="rating" value="{$usersdata[0].rating}" placeholder="Enter over all rating " />
                    </div>
                 <!-- <div class="form-group">
                    <label>Confirm Password</label>
                    <input class="form-control" type="password" name="confirm_password" id="alert_confirm_password" value="" placeholder="Enter Confirm Password " />
                    </div>-->
                  
                  <!--==========password finish====-->
                  
                  <div class="form-group">
                    <label>Comment</label>
                       <input class="form-control" type="text" name="comment" id="Comment" value="{$usersdata[0].review}" placeholder="Enter comment " />
                  </div>
                  <div class="form-group">
                    
                
                
                  <button type="submit" class="btn btn-primary" name="user_btn" value="add_new_customer" > {if $edit_id==''}Add{else}Edit{/if} Customer</button>
                </form>
                <!--==================================================--> 
                
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>

{/if}
{if $type=='T'}
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2>{if $edit_id==''}Add{else}Edit{/if} Transporter</h2>
      </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading"><b> {if $edit_id==''}Add{else}Edit{/if} Customer/Transporter</b>
       
         </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
                
                <!--===============customer form======================-->
                <form role="form" name="add_new_customer" id="add_new_customer" action="" method="post">
                  <input type="text" name="edit_id" id="edit_id" value="{$edit_id}" />
                  <input type="text" name="type" id="type" value="{$type}" />
                     <h4>{$show_message}</h4> 
              
                  <div class="form-group">
                    <label>Customer/Transporter Name</label>
                    {$functions->get_Transpoter_detail($userdata[0].transpoter_id)}
                    <input class="form-control" type="text" name="transporter_name" readonly="readonly" id="user_name" value="{$Transpoter_info[0].name}" placeholder="Enter Customer Name " />
                   </div>
                  <div class="form-group">
                    <label>Communication Rating</label>
                    <input class="form-control" type="text" name="cum_rating" id="email" value="{$userdata[0].com_rating}" placeholder="Enter Communication rating " />
                    </div>
                  <div class="form-group">
                    <label>service rating</label>
                    <input class="form-control" type="text" name="ser_rating" id="mobile_no" value="{$userdata[0].ser_rating}" placeholder="Enter serice rating" />
                   </div>
                  
                  <!--==========password=========-->
                  
                  <div class="form-group">
                    <label>punctuality Rating</label>
                    <input class="form-control" type="text" name="pun_rating" id="password" value="{$userdata[0].pun_rating}" placeholder="Enter Punctuality rating " />
                    </div>
                    <div class="form-group">
                    <label>care of goods Rating</label>
                    <input class="form-control" type="text" name="car_rating" id="password" value="{$userdata[0].care_rating}" placeholder="Enter cars of goods rating " />
                    </div>
                    <div class="form-group">
                    <label>Over All rating</label>
                    <input class="form-control" type="text" name="rating" id="password" value="{$userdata[0].rating}" placeholder="Enter over all rating " />
                    </div>
                    <div class="form-group">
                    <label>Date</label>
                    <input class="form-control" type="text" name="date" id="password" value="{$userdata[0].date}" placeholder="Enter date " />
                    </div>
                 <!-- <div class="form-group">
                    <label>Confirm Password</label>
                    <input class="form-control" type="password" name="confirm_password" id="alert_confirm_password" value="" placeholder="Enter Confirm Password " />
                    </div>-->
                  
                  <!--==========password finish====-->
                  
                  <div class="form-group">
                    <label>Comment</label>
                     <input class="form-control" type="text" name="comment" id="password" value="{$userdata[0].review}" placeholder="Enter comment " />
                  </div>
                  <div class="form-group">
                    
                
                
                  <button type="submit" class="btn btn-primary" name="user_btn1" value="add_new_customer" > {if $edit_id==''}Add{else}Edit{/if} Transporter</button>
                </form>
                <!--==================================================--> 
                
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>
{/if}