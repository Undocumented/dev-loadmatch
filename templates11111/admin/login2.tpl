<!--========================login details=============================-->
	<table cellpadding="0" cellspacing="0" width="100%" border="1">
    	<tr>
        	<td>
            
            <!--=======login form============-->
            <div id="login">
			<h2><span class="fontawesome-lock"></span>Sign In</h2>

                <form action="" method="POST" name="login_form" id="login_form">
        
                    <fieldset>
        				<span id="login_info" class="alert_message"></span>
                        <p><label for="email">User Name</label></p>
                        <p><input type="text" id="user_name" name="user_name" value="" placeholder="Enter User Name"><br />
                        <span id="login_email_info" class="alert_message"></span>
                        </p> <!-- JS because of IE support; better: placeholder="mail@address.com" -->
        
                        <p><label for="password">Password</label></p>
                        <p><input type="password" id="password" value="" name="password" placeholder="Enter Password"><br />
                        <span id="login_password_info" class="alert_message"></span></p> <!-- JS because of IE support; better: placeholder="password" -->
        
                        <p><input type="submit" name="login_submit"  value="Sign In" onclick="return Valid_login_page(document.login_form);"></p>
        
                    </fieldset>
        
                </form>

			</div> <!-- end login -->

            <!--=============================-->
            
            </td>
        </tr>
    </table>
<!--==================================================================-->