<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Mail List</h2>
                    
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <h3>Mail List</h3>
                           
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
              
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					 <thead align="left" >
					<tr>
						<th width="50">Sr.No.</th>
						<th  width="600">Subject</th>
						<th width="80">Action</th>
					</tr>
				 </thead>
				  <tbody>
					{if $mailsdata}
						{section name=data loop=$mailsdata}
							<tr>
								<td align="center">{$smarty.section.data.index+$row_no+1}</td>
								<td>{$mailsdata[data].subject}</td>
								<td >
								
								  <a href="{$site_url}/admin.php?page=mails&action=add_mails&id={$mailsdata[data].id}" title="view"><span class="btn btn-success btn-sm">Edit</span></a>
							
								</td>
							</tr>
						{/section}
				<tr>
				<td colspan="7" align="center"></td>
				</tr>	
				
					{else}
						<tr><td colspan="7" align="center">Record Not Found</td></tr>
					{/if}
					 <tbody>			
				</table>
					
					
 </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>