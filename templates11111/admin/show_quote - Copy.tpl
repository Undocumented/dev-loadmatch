<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Placed Quote</h2>
                     {if $show_message!=''}
                       <h5 class=" btn-success btn-lg" align="center">{$show_message}</h5>  
                       {/if} 
                      
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Placed Quote
                            {if $show_message!=''}
                          
                            {/if}
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="8%">Sr. No.</th>
                                            <th>Order ID</th>
                                            <th>Customer Name</th>
                                            <th>Email</th>
                                            <th>New Quote</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if $usersdata}
                                        {section name=data loop=$usersdata}
                                        <tr class="gradeU">
                                            <td align="center">{$smarty.section.data.index+$row_no+1}</td>
                                            <td><a href="{$site_url}/admin.php?page=payment&action=show_quote_agent&order_id={$usersdata[data].order_id}">{$usersdata[data].order_id}</a></td>
                                            <td>{$functions->get_name($usersdata[data].user_id)}</td>
                                            <td>{$functions->get_email($usersdata[data].user_id)}</td>
                                             <td>New({$functions->new_quote_count($usersdata[data].order_id)}) Total({$functions->totel_quote_count($usersdata[data].order_id)})</td>
                                            
                                               <td>
<a href="{$site_url}/admin.php?page=payment&action=show_quote_agent&order_id={$usersdata[data].order_id}" title="view"><span class="btn btn-success btn-sm">view</span></a>
                                               
                                               {* {if $usersdata[data].status=='Y'}
                                               	<a href="{$site_url}/admin.php?page=payment&action=show_quote&status_id={$usersdata[data].id}&active=N" title="active"><img src="{$site_url}/templates/admin/images/active.gif" width="12" height="12" alt="active" title="active" /></a>
                                                {else}
                                                	<a href="{$site_url}/admin.php?page=payment&action=show_quote&status_id={$usersdata[data].id}&inactive=Y" title="inactive"><img src="{$site_url}/templates/admin/images/inactive.gif" width="12" height="12" alt="inactive" title="inactive" /></a>
                                                    {/if}
                                                <a href="{$site_url}/admin.php?page=payment&action=show_quote&del_id={$usersdata[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><i class="fa fa-trash-o"></i></a>*}
                                               </td>
                                        </tr>
                                        {/section}
                                        
                                         <tr>
            <td colspan="9"><form name="frm_pagi" action="" method="post">
         <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />
            {$show_pagi}
         </form>
              </td>
          </tr>
          {else}
          <tr>
            <td colspan="9"><span style="color:red;">Record Not Found</span></td>
          </tr>
          {/if}
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>

