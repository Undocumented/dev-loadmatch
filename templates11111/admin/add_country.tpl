<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
                        
            <h1 class="page-header">{if $edit_id!=''}Edit {else}Add {/if} Country
                     </h1>
       </div>
    </div>
    <!-- /. ROW  -->
   
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading">
        <b>{if $edit_id!=''}Edit {else}Add {/if} Country</b>
         
 </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
        {if $sucess!=''}
               
    				 <div class="notibar msgsuccess">
                        <a class="close"></a>
                        <p>Country Added Sucessfully</p>
                    </div><!-- notification msgsuccess -->
                  
                {/if} 
        <form class="stdform stdform2" name="frm_addcountry" id="frm_addcountry" method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="edit_id" value="{$edit_id}" />  
         <input type="hidden" name="old_country_name" value="{$edit_data[0].country}" />  
                    
                    	 <div class="form-group">
                        	<label>Country Name</label> <span  class="error_alert"></span>
                               {if edit_id!=''}
                             <input type="hidden" name="old_country_name" id="old_country_name" value="{$edit_data[0].country}" />{/if}
                             <input type="text" name="country_name" id="alert_name" value="{$edit_data[0].country}" class="form-control" />
                          </div> 
                    
                         <div class="form-group">
                        	<label>Image</label><span class="error_alert"></span> 
                          
                              <input type="file" name="country_flage" id="alert_image" class="form-control"  />
                             {if $edit_data[0].flage!=''}
                          <img src="{$site_url}/location_flage/{$edit_data[0].flage}" height="80" width="100" />
                            {/if}
                           <input type="hidden" name="country_flage_old"  value="{$edit_data[0].flage}" />
                           
                      </div>
                        
                       <div class="form-group">
                        	<label>Title</label> <span  class="error_alert"></span>
                            
                            <input type="text" name="title" id="alert_title" class="form-control"  value="{$edit_data[0].title}" />
                       </div>    
                    
                        <div class="form-group">
                        	<label>Description</label> <span  class="error_alert"></span>
                           
                            <textarea name="description" id="alert_description" class="form-control" >{$edit_data[0].descen}</textarea>
                           
                       </div>
                       
                        <div class="form-group">
                        	<label>keyword</label><span  class="error_alert"></span>
                            
                            <textarea name="keyword" id="alert_keyword" class="form-control" >{$edit_data[0].keyword}</textarea>
                            
                        </div>
                        
                        
                     
                        
                            <button type="submit"  name="submit" value="Submit" onClick="return add_country(document.frm_addcountry);" class="btn btn-default">Submit</button>
                           
                       
                    </form>
 			     
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>