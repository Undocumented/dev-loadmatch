<div class="breadcrumbs">
    <div class="container">
        <div class="row">

            <div class="col-lg-4 col-sm-4" style="margin-top:15px;">
                <h1>
                    About Us
                </h1>
            </div>
            <div class="col-lg-8 col-sm-8">
                <ol class="breadcrumb pull-right">
                    <li>
                        <a href="index.html">
                            Home
                        </a>
                    </li>

                    <li class="active">
                        About us
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--breadcrumbs end-->

<!--container start-->
<section id="body">
    
    <div class="about_us" id="About1">
    <div class="top_banner_add" id="top_banner1">
        
   <!--/*
  *
  * Revive Adserver Asynchronous JS Tag
  * - Generated with Revive Adserver v4.0.2
  *
  */-->
 
<ins data-revive-zoneid="83" data-revive-id="43422af75a9f9066297995789acc7f48"></ins>
<script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
    </div>
    </div>
        <div class="breadcrumbs">
    <div class="container">
        <div class="row">

            <div class="col-lg-4 col-sm-4" style="margin-top:15px;">
                <h1>
                   
                </h1>
            </div>
            <div class="col-lg-8 col-sm-8">
                <ol class="breadcrumb pull-right">
                    <li>
                        <a href="index.html">
                            
                        </a>
                    </li>

                    <li class="active">
                        
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
            
    <div class="about_us" id="About1">
        <div class="col-sm-3 home-adbar">
            <ins data-revive-zoneid="91" data-revive-id="43422af75a9f9066297995789acc7f48"></ins>
            <script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
        </div>
        <div class="container about-container" >
            <div class="col-sm-12">
                <h3><b>History of LoadMatch</h3></b>
                <p>LoadMatch was created due to the ever-increasing need for business efficiency. Another drive force for the system was to make modern logistics operations and applications available to the small to medium transport enterprise as well, right down to owner driver level.</p>
                <p>LoadMatch was designed by transport and supply chain experts and modelers, with the first planning for an all-inclusive service portal kicking off in January 2015. The base of the system is built around the ever-increasing challenges in the industry such as driver shortage, increasing compliance complexities, volatile costs and constantly evolving consumer demand, the tension between cost-cutting and maximizing profit</p>
                <p>In a sector as multifaceted as transportation, LoadMatch was therefore built around improvement and efficiency, incorporating existing complicated approaches and models with best known practices and innovations on the horizon by addressing the constraints -</p>
                <ul>
                    <li>how transportation and logistics headlines affect the industry;</li>
                    <li>how valuable knowledge and service models available in the smaller niche markets can be incorporated into the chain; and</li>
                    <li>how carriers and shippers of all sizes can join forces to work toward mutual goals.</li>
                </ul>
                <h3><b>Connecting People</b></h3>  
            </div>
        </div>
        <div class="col-sm-3 home-adbar" id="add_2">
            <ins data-revive-zoneid="92" data-revive-id="43422af75a9f9066297995789acc7f48"></ins>
            <script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
        </div>

    </div>
    <div class="about_us" id="About2">
        <div class="col-sm-3 home-adbar" id="add_3">
            <ins data-revive-zoneid="93" data-revive-id="43422af75a9f9066297995789acc7f48"></ins>
            <script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
        </div>
        <div class="container about-container">
            <div class="col-sm-12">
                
                    <p>LoadMatch is an online transport marketplace that connects customers with transport and delivery companies for all shipment collections and deliveries by means of loading shipments. Transporters place competing quotes to win a customer's business, which brings down delivery costs and makes transport more affordable and efficient.</p>
                    <p>After reviewing quoted prices, using the LoadMatch freight calculator™ (based on proprietary freight data built into the system) to compare shipping rates, Load Owners can enjoy significant savings when accepting quotes for their shipments. When Carriers have empty space in their trucks, they can use LoadMatch to fill it at discounted rates. Our advanced search tools and apps allow Truckers to find shipments nearby as well as along their routes.</p>
                    <p>LoadMatch is a privately owned company of transport, supply chain and distribution centre experts based in Johannesburg, Gauteng, South Africa, operating in South Africa and Africa.</p>
                <p> 
                </p>
                    
                <h3><b>LoadMatch for Carriers</b></h3>
                    <p>LoadMatch provides subscribed transport, distribution and delivery companies immediate access to transport jobs. LoadMatch charges a small admin fee on completed jobs, unlike similar companies who inflate prices substantially. The admin fee charged by LoadMatch is to cover resources, administrative, ongoing development, app updates and system management costs.  With thousands of deliveries listed and available on any given day, owner-operators and large freight companies alike have an equal opportunity to bid on jobs that suit their needs and routes. Through LoadMatch, service providers can fill excess capacity or find profitable backloads that help maximise their business and time on the road. LoadMatch also gives service providers free tools to help them establish and build their online business reputation through its feedback system and customer reviews.</p>

                    <h3><b>LoadMatch for Delivery Customers</b></h3>
                    <p>LoadMatch makes it easy to get in touch with thousands of transport, distribution and delivery companies, making it simple and affordable to deliver or transport anything. Customers decide on transporters based on feedback and unbiased customer reviews through a vetting and rating system.</p>
                    <p>Our rating and review system allows our Load Owners to make informed decisions when selecting a transporter. High customer satisfaction rates can be achieved as well as vast savings on rates through the LoadMatch service portal.</p>

                    <h3><b>LoadMatch Service Partners</b></h3>
                    <p>For more information on becoming a LoadMatch Service Partner for specialist services and products to the Transport Industry through our portal, please contact martin@loadmatch.co.za.</p>
                <!-- End container -->
            </div>
        </div>
        <div class="col-sm-3 home-adbar" id="add_4">
            <ins data-revive-zoneid="96" data-revive-id="43422af75a9f9066297995789acc7f48"></ins>
            <script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
        </div>
    </div>
    
    
    </div>
    <div class="about_us" id="About3">
        <div class="col-sm-3 home-adbar" id="add_3">
            <ins data-revive-zoneid="94" data-revive-id="43422af75a9f9066297995789acc7f48"></ins>
            <script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
        </div>
        <div class="container about-container">
            <div class="col-sm-12">
                   
                 The admin fee charged by LoadMatch is to cover resources, administrative, ongoing development, app updates and system management costs.  With thousands of deliveries listed and available on any given day, owner-operators and large freight companies alike have an equal opportunity to bid on jobs that suit their needs and routes. Through LoadMatch, service providers can fill excess capacity or find profitable backloads that help maximise their business and time on the road. LoadMatch also gives service providers free tools to help them establish and build their online business reputation through its feedback system and customer reviews.</p>

                    <h3><b>LoadMatch for Delivery Customers</b></h3>
                    <p>LoadMatch makes it easy to get in touch with thousands of transport, distribution and delivery companies, making it simple and affordable to deliver or transport anything. Customers decide on transporters based on feedback and unbiased customer reviews through a vetting and rating system.</p>
                    <p>Our rating and review system allows our Load Owners to make informed decisions when selecting a transporter. High customer satisfaction rates can be achieved as well as vast savings on rates through the LoadMatch service portal.</p>

                    <h3><b>LoadMatch Service Partners</b></h3>
                    <p>For more information on becoming a LoadMatch Service Partner for specialist services and products to the Transport Industry through our portal, please contact martin@loadmatch.co.za.</p>
                <!-- End container --> 
                
                
                
            </div>
        </div>
        <div class="col-sm-3 home-adbar" id="add_4">
            <ins data-revive-zoneid="97" data-revive-id="43422af75a9f9066297995789acc7f48"></ins>
            <script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
        </div>
    </div>
        <!--container end-->
</section>
  
