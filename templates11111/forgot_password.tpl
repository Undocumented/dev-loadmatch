
    <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Forgot Password</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        
                        <li class="active">Forgot Password</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="login-bg">
        <div class="container">
            <div class="form-wrapper">
            <form class="form-signin wow fadeInUp" action="" method="post" name="login" id="login">
            <h2 class="form-signin-heading">Forgot Password</h2>
            <div class="login-wrap">
            <label id="err">{$message_notR}</label>
             <label id="err">{$message}</label>
                <input type="text" class="form-control" name="email"  id="email" placeholder="User Email ID" autofocus>
               
               
               
                <button class="btn btn-lg btn-login btn-block" name="submit" type="submit" onclick="return Valid_add_login(document.login);">Submit</button>
               
               

            </div>

              <!-- Modal -->
            

          </form>
          </div>
        </div>
    </div>
    <!--container end-->
