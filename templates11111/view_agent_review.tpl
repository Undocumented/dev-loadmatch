
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Transporter Review</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Transporter Review</a></li>
                         <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
 <div class="component-bg">
    <div class="container">
            <!-- Forms
================================================== -->
    <div class="bs-docs-section mar-b-30">
      <h1 id="forms" class="page-header">Transporter Detail</h1>
      <div class="col-lg-8" style="margin-left:15%; width:70%;">
     <div class="row user_proflie1">
              <div class="col-lg-2  bg-navy1" align="center">
      <div class="form-group"> <img src="{$site_url}/images/profile_pic/{$agent_detail_search[0].image}" width="100px" height="100px" style="margin-top:15px;" /> </div>
       <div class="form-group"> <label for="inputEmail3" class="col-sm-30 control-label" >{$agent_detail_search[0].name}</label></div>
      </div>
    
         <div class="col-lg-6" align="center">
       <h4 align="left">  Total Shipment:{$functions->get_Agent_totel_job($agent_detail_search[0].id)} </h4>
       <h4 align="left"> Completed: {$functions->get_Agent_totel_job_complet($agent_detail_search[0].id)}</h4> 
      <h4 align="left"> User Rating:{$functions->get_Agent_rating_average($agent_detail_search[0].id)}</h4>
     </div>
      <div class="col-lg-4" align="left">
      <label for="inputEmail3" class="col-sm-30 control-label" >Available Vehicle</label>
      {$agent_detail_search[0].agent_id}
        {$functions->get_agent_vehicle_front($agent_detail_search[0].id)} 
        
      </div>
    </div> 
     </div>
    
       
        
        <table align="center" style="border:2px solid #999; padding:10px;" width="70%">
       
         {if $search}
                    {section name=data loop=$search}
        <tr style="border:2px solid #999;">
        <td align="left" style="color:#00F; background:#999;" >Posted By Customer:{$functions->get_name($search[data].user_id)}</td><td>Date{$search[data].date}</td>
        </tr>
        <td colspan="2" align="left" style=" padding:15px;">{$search[data].review}</td>
        </tr>
        <tr>
        <td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        {/section}
        {else}
        <tr><td colspan="2" align="center">Review Not Found</td></tr>
        {/if}
        </hr>
        
        </table>
    <br />
        <div class="col-lg-12 ">
    <form action="" method="post">
                    <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}
                    </form>
      </div><!-- /.bs-example -->
    
      
         
       
    
    </div>
    </div>
</div>
    <!--container end-->