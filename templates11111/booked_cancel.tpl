{if $smarty.session.user_type=='C'}
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Cancel Shipment </h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My Cancel Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          My Cancel Shipment
          </h4>
           <table align="right">
          <tr>
          <td align="left">
          
           <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
          <p><b style="color:#F00;">Active&nbsp;:</b>&nbsp;&nbsp;Change Shipment Status to Active</p>
          <p><b style="color:#F00;">Delete&nbsp;:</b>&nbsp;&nbsp;Delete Shipment </p>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
            <br/>
                  
             </td>
             </tr>
             </table>
             <br/><br/><br/>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="{$site_url}/edit_post_shipping" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                      Collection
                    </th>
                    <th>
                      Delivery
                    </th>
                    <th>
                     Category
                    </th>
                    <th>
                     Under Category
                    </th>
                     <th>
                     Status
                    </th>
                   <th>
                     Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                      {$search[data].order_id}
                    </td>
                    <td>
                     {$search[data].form_state}, {$search[data].form_city}
                    </td>
                    <td>
                     {$search[data].to_state}, {$search[data].to_city}
                    </td>
                    <td>
                      {$functions->get_category($search[data].category)}
                    </td>
                    <td>
                       {$functions->get_category($search[data].sub_category)}
                    </td>
                     <td>
                        <div class="btn btn-default btn-sm">{$search[data].transport_status} </div>
                    </td>
                  <td>
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a style="cursor:pointer;" onclick="submitbuy_book_cancel_complet('{$search[data].id}','c',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                    <a href="{$site_url}/eagleushp.php?page=myaccount&action=booked_cancel&active={$search[data].id}" class="btn btn-success btn-sm">Avtive</a>
                    <a href="{$site_url}/eagleushp.php?page=myaccount&action=booked_cancel&delete={$search[data].id}" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    {/if}
    
    {if $smarty.session.user_type=='T'}
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Cancel Bookings</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My Cancel Bookings</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
           My Cancel Bookings
          </h4>
          <table align="right">
          <tr>
          <td align="left">
           <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
          <p><b style="color:#F00;">Active&nbsp;:</b>&nbsp;&nbsp;Re-Active Shipment Quote </p>
          <p><b style="color:#F00;">Not Available&nbsp;:</b>&nbsp;&nbsp;Shipment is Booked by Other Transporter</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
            <br/>
              
                      </td>
             </tr>
             </table>
             <br/><br/><br/>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="{$site_url}/detail_shiping_booked" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                      Customer
                    </th>
                    <th>
                     Pick up Time
                    </th>
                    <th>
                     Delivery Time
                    </th>
                     <th>
                      Delivery Location
                    </th>
                     <th>
                     Status
                    </th>
                     <th>
                     Action
                    </th> 
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                      {$search[data].order_id}
                    </td>
                     <td>
                       {$functions->get_name($search[data].user_id)}
                    </td>
                    <td>
                     {$search[data].collect_start_date|date_format:"%d- %m -%Y"}, {$search[data].collect_end_date|date_format:"%d- %m -%Y"}
                    </td>
                    <td>
                     {$search[data].delivery_start_date|date_format:"%d- %m -%Y"}, {$search[data].delivery_end_date|date_format:"%d- %m -%Y"}
                    </td>
                     <td>
                     {$functions->get_destnation_state_city($search[data].order_id)}
                    </td>
                    <td>
                      {*{$search[data].transport_status} *}<div class="btn btn-default btn-sm"> Cancled</div>
                    </td>
                    <td>
                      {if $search[data].agent_quata_cencle=='Y'} <a href="{$site_url}/eagleushp.php?page=myaccount&action=booked_cancel&active={$search[data].id}" class="btn btn-success btn-sm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Avtive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>{else} <a href="" class="btn btn-success btn-sm">Not Available</a>{/if}
                    <a style="cursor:pointer;" onclick="submitbuy_agent_cencle_complet('{$search[data].id}','{$search[data].order_id}','c',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                  
                    <a href="{$site_url}/eagleushp.php?page=myaccount&action=booked_cancel&delete={$search[data].id}" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    {/if}