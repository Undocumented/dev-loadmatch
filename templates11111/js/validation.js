 /***************************/
//@Author: Vimlesh Verma
//@website: www.eagletechnosys.com
//@email: contact@eagletechnosys.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/


/************************************************************************************************************
			  /////////////////////////////////////////////////////////////////////////////////////
			 /////////////////////////START MAIN FUNCTION FOR USE AJAX	//////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////
************************************************************************************************************/

var xmlHttp

function showHint(str)
{
	if (str.length==0)
	{ 
		document.getElementById("txtHint").innerHTML="";
		return;
	}

	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null)
	{
		alert ("Your browser does not support AJAX!");
		return;
	}
	var url="get.php";
	url=url+"?q="+str;
	url=url+"&sid="+Math.random();
	xmlHttp.onreadystatechange=stateChanged;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
}

function ajax() {
	// Make a new XMLHttp object
	
	if (typeof window.ActiveXObject != 'undefined' )
	{ 
		doc =GetXmlHttpObject();
	}
	else
		doc = new XMLHttpRequest();
		

/*	
alert(typeof XMLHttpRequest === "function");
alert(Object.prototype.toString.call(XMLHttpRequest));
alert(typeof XMLHttpRequest.toString);	*/
		
}


function GetXmlHttpObject()
{
	var xmlHttp=null;
	try
	{
		
	   // If IE7, Firefox, Opera 8.0+, Safari, and so on: Use native object.
		xmlHttp=new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			// ...otherwise, use the ActiveX control for IE5.x and IE6.
    		 xmlHttp = new ActiveXObject('MSXML2.XMLHTTP.3.0');
			//xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	//alert(xmlHttp);
	return xmlHttp;
}
 




/************************************************************************************************************
			  ///////////////////////////////////////////////////////////////////////////////////// 
			 ////////////////////////////START MAIN FUNCTIONS/////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////
************************************************************************************************************/

function Valid_add_login(frm)
{
	 var checkFocus=0;
	 
	
		 
	 if(frm.user_name.value=='')
			{
			document.getElementById("user_name").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.user_name.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("user_name").style.borderColor='#CCC';
		             
		if(frm.password.value=='')
		{
			document.getElementById("password").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.password.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("password").style.borderColor='#CCC';
			 

		if(checkFocus==1)
			return false;
		else
			return true;
	
}



function Valid_contect_us(frm)
{
	 var checkFocus=0;
		 
	 if(frm.name.value=='')
			{
			document.getElementById("name").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.name.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("name").style.borderColor='';
		             
     if(frm.norobot.value=='')
			{
			document.getElementById("norobot").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.norobot.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("norobot").style.borderColor='';
		             

			 if(frm.email.value=='')
		{	
			document.getElementById("email").style.borderColor='#F00';
				if(checkFocus!=1)
				{
					frm.email.focus();
					var checkFocus=1;
				}
		}
			else
		{
				var frmEmail1 =frm.email.value.toLowerCase();	
				var frmEmailformat = /^[^@\s]+@([-a-z0-9]+\.)+([a-z]{2}|com|net|edu|org|gov|mil|int|biz|pro|info|arpa|aero|coop|name|museum)$/;
				if (!frmEmailformat.test(frmEmail1)) 
			{
				alert("Please enter valid email address.");
				document.getElementById("email").style.borderColor='#F00';
				if(checkFocus!=1)
					frm.email.focus();	
				var checkFocus=1;
			}
			else
			{
				document.getElementById("email").style.borderColor='';
			}
		}
			
			
			
		if(frm.phone.value=='')
		{
			document.getElementById("phone").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.phone.focus();				checkFocus=1;			};
			}
			else
 						
			if(isNaN(frm.phone.value)) {
			alert("Please Enter Numeric value.");
			document.getElementById("phone").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.phone.focus();				 var checkFocus=1;			} 
			}
		
		else 
		if(frm.phone.value.length< 10)
		{
		alert("Please Enter 10 Character Mobile no.");
		document.getElementById("phone").style.borderColor='#F00';
				if(checkFocus!=1)
				{	frm.phone.focus();				var checkFocus=1;			}
				}
			else
			document.getElementById("phone").innerHTML=""; 
			
			
			
			if(frm.Subject.value=='')
		{
			document.getElementById("Subject").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.Subject.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("Subject").style.borderColor='';
			if(frm.massage.value=='')
		{
			document.getElementById("massage").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.massage.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("massage").style.borderColor='';	 

		if(checkFocus==1)
			return false;
		else
			return true;
	
}





function Valid_add_customerfront(frm)
{
	 var checkFocus=0;
	
	 if (document.getElementById('checkbox1').checked) {
           
        }
		else
		{
			
			alert("Please Select I agree to the Terms of Service and Privacy Policy");
			
		}
		
		 
	 if(frm.user_name.value=='')
			{
			document.getElementById("user_name").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.user_name.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("user_name").style.borderColor='#CCC';
		             
		if(frm.address.value=='')
		{
			document.getElementById("address").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.address.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("address").style.borderColor='#CCC';
			 
			 if(frm.mobile_no.value=='')
		{
			document.getElementById("mobile_no").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.mobile_no.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("mobile_no").style.borderColor='#CCC';
			 
			 
			 
			
	 
 		if(isNaN(frm.mobile_no.value)) {
			alert("Please Enter Numeric value.");
			document.getElementById("mobile_no").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.mobile_no.focus();				 var checkFocus=1;			} 
			}
		
		else 
		if(frm.mobile_no.value.length< 10)
		{
		alert("Please Enter 10 Character Mobile no.");
		document.getElementById("mobile_no").style.borderColor='#F00';
				if(checkFocus!=1)
				{	frm.mobile_no.focus();				var checkFocus=1;			}
				}
			else
			document.getElementById("mobile_no").innerHTML=""; 
			 
			 
			 
			 
			 
			 if(frm.email.value=='')
		{	
			document.getElementById("email").style.borderColor='#F00';
				if(checkFocus!=1)
				{
					frm.email.focus();
					var checkFocus=1;
				}
		}
			else
		{
				var frmEmail1 =frm.email.value.toLowerCase();	
				var frmEmailformat = /^[^@\s]+@([-a-z0-9]+\.)+([a-z]{2}|com|net|edu|org|gov|mil|int|biz|pro|info|arpa|aero|coop|name|museum)$/;
				if (!frmEmailformat.test(frmEmail1)) 
			{
				alert("Please enter valid email address.");
				document.getElementById("email").style.borderColor='#F00';
				if(checkFocus!=1)
					frm.email.focus();	
				var checkFocus=1;
			}
			else
			{ 
				ajax();
			if (doc){
				   doc.open("GET", "ajax.php?section=check_customer_avilable&email="+frm.email.value, false);   
				   doc.send(null);
			}
	 	  	//alert(doc.responseText);//return false;
			   if(doc.responseText==1)
			   {	
					alert("Email Already Exists");
					document.getElementById("email").style.borderColor='#F00';
					frm.email.focus();
					var	 checkFocus=1;
			   }
			   else
				document.getElementById("email").innerHTML="";
			}
		
		}
			 
			 
						
					 
			 if(frm.state.value=='')
		{
			document.getElementById("state").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.state.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("state").style.borderColor='#CCC';	
  	
	  if(frm.form_change.value=='')
		{
			document.getElementById("form_change").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.form_change.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("form_change").style.borderColor='#CCC';	
			
			 if(frm.password.value=='')
		{
			document.getElementById("password").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.password.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("password").style.borderColor='#CCC';	
			
			
					
			if(frm.checkbox1.value=='')
		{
			document.getElementById("checkbox1").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.checkbox1.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("checkbox1").style.borderColor='#CCC';	
			 
		if(checkFocus==1)
			return false;
		else
			return true;
	
}

function Valid_add_quote(frm)
{
	 var checkFocus=0;
	 		 
	 if(frm.pickup_start_date.value=='')
			{
			document.getElementById("pickup_start_date").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.pickup_start_date.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("pickup_start_date").style.borderColor='#CCC';
		             
		if(frm.pickup_end_date.value=='')
		{
			document.getElementById("pickup_end_date").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.pickup_end_date.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("pickup_end_date").style.borderColor='#CCC';
			 
			 if(frm.delv_start_date.value=='')
		{
			document.getElementById("delv_start_date").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.delv_start_date.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("delv_start_date").style.borderColor='#CCC';
			 
						
			if(frm.delv_end_date.value=='')
		{	
			document.getElementById("delv_end_date").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.delv_end_date.focus();				checkFocus=1;			};
			}
		
			else
				document.getElementById("delv_end_date").style.borderColor='';
			
				
			 
			 if(frm.Price.value=='')
		{
			document.getElementById("Price").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.Price.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("Price").style.borderColor='#CCC';	
  	
	
			 
		if(checkFocus==1)
			return false;
		else
			return true;
	
}

  function Valid_add_customer(frm)
 {
  		 var checkFocus=0;
		// alert("ok");
  	
		//var type_id=frm.type_id.value;user_type
		
		 if(frm.user_type.value=='please Select')
			{
			document.getElementById("user_type").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.user_type.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("user_type").style.borderColor='#CCC';
		             
		if(frm.user_name.value=='')
		{
			document.getElementById("user_name").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.user_name.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("user_name").style.borderColor='#CCC';
			 
						
			if(frm.email.value=='')
		{	
			document.getElementById("email").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.email.focus();				checkFocus=1;			};
			}
			
		
		else
		{
			/*ajax();
			if (doc){
				   doc.open("GET", "ajax.php?section=check_customer_avilable&email="+frm.email.value, false);   
				   doc.send(null);
			}
			alert(doc.responseText);
			if(doc.responseText==1)
			{	
				alert("User Name Already Exits");
				frm.email.focus();
				var	 checkFocus=1;
			}
			else*/
				document.getElementById("email").style.borderColor='';
		}	
				
				
				
  	
	if(frm.mobile_no.value=='')
			{
		document.getElementById("mobile_no").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.mobile_no.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("mobile_no").style.borderColor='#CCC';
  	
	if(frm.password.value=='')
			{
			document.getElementById("password").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.password.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("password").style.borderColor='#CCC';
  	
	 if(frm.state.value=='please Select')
			{
			document.getElementById("state").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.state.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("state").style.borderColor='#CCC';
		             
	if(frm.city.value=='please Select')
			{
			document.getElementById("city").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.city.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("city").style.borderColor='#CCC';
	if(frm.zipcode.value=='')
			{
		document.getElementById("zipcode").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.zipcode.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("zipcode").style.borderColor='#CCC';
  	
	if(frm.address.value=='')
			{
			document.getElementById("address").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.address.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("address").style.borderColor='#CCC';	             		
			 
		if(checkFocus==1)
			return false;
		else
			return true;
 }
 
 
   function valid_cat_value(frm)
 {
  		 var checkFocus=0;
  	
		
		   
		if(frm.cat_name.value=='')
		{
			document.getElementById("cat_name").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.cat_name.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("cat_name").style.borderColor='#CCC';
			 
			
						 
			if(frm.keyword.value=='')
			{
			document.getElementById("keyword").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.keyword.focus();				checkFocus=1;			};
			}
			else
 				document.getElementById("keyword").style.borderColor='#CCC';
  	
	if(frm.description.value=='')
			{
		document.getElementById("description").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.description.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("description").style.borderColor='#CCC';
  	
	if(frm.logo.value=='')
			{
			document.getElementById("logo").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.logo.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("logo").style.borderColor='#CCC';
  	
			
			 
		if(checkFocus==1)
			return false;
		else
			return true;
  
 }



   function valid_sub_add_cat(frm)
 {
  		 var checkFocus=0;
  	
		
		   if(frm.category.value=='please Select')
			{
			document.getElementById("category").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.category.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("category").style.borderColor='#CCC';
  	
			
			 
		if(frm.cat_name.value=='')
		{
			document.getElementById("cat_name").style.borderColor='#F00';
			
			if(checkFocus!=1)
			{	frm.cat_name.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("cat_name").style.borderColor='#CCC';
			 
			
						 
			if(frm.keyword.value=='')
			{
			document.getElementById("keyword").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.keyword.focus();				checkFocus=1;			};
			}
			else
 				document.getElementById("keyword").style.borderColor='#CCC';
  	
	if(frm.description.value=='')
			{
		document.getElementById("description").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.description.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("description").style.borderColor='#CCC';
  	
	
		if(checkFocus==1)
			return false;
		else
			return true;
  
 }




  function valid_post(frm)
 {
  		 var checkFocus=0;
		 
		 	
		//var type_id=frm.type_id.value;user_type
		 if(frm.Title.value=='')
			{
			document.getElementById("Title").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.Title.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("Title").style.borderColor='';
			
		 if(frm.s_user_name.value=='')
			{
			document.getElementById("s_user_name").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.s_user_name.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("s_user_name").style.borderColor='';
			
		 if(frm.s_user_email.value=='')
			{
			document.getElementById("s_user_email").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.s_user_email.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("s_user_email").style.borderColor='';
		
		if(frm.mobile.value=='')
			{
			document.getElementById("mobile").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.mobile.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("mobile").style.borderColor='';
		
		
		
		if(frm.loc_sen.value=='')
			{
			document.getElementById("loc_sen").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.loc_sen.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("loc_sen").style.borderColor='';
		
		
		if(frm.r_user_name.value=='')
			{
			document.getElementById("r_user_name").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.r_user_name.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("r_user_name").style.borderColor='';
		
		
		if(frm.r_user_email.value=='')
			{
			document.getElementById("r_user_email").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.r_user_email.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("r_user_email").style.borderColor='';
		
		
		
		if(frm.r_mobile.value=='')
			{
			document.getElementById("r_mobile").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.r_mobile.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("r_mobile").style.borderColor='';
		
		
		if(frm.loc_rec.value=='')
			{
			document.getElementById("loc_rec").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.loc_rec.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("loc_rec").style.borderColor='';
		
		
		if(frm.Dimensions.value=='please Select')
			{
			document.getElementById("Dimensions").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.Dimensions.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("Dimensions").style.borderColor='';
		
		
		
		
		 if(frm.category.value=='please Select')
			{
			document.getElementById("category").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.category.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("category").style.borderColor='';
	             		
			  if(frm.sub_cat_id.value=='please Select')
			{
			document.getElementById("sub_cat_id").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.sub_cat_id.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("sub_cat_id").style.borderColor='';
	  
	  
	  	  if(frm.form_state.value=='please Select')
			{
			document.getElementById("form_state").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.form_state.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("form_state").style.borderColor='';     
			
			
			  if(frm.form_change.value=='please Select')
			{
			document.getElementById("form_change").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.form_change.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("form_change").style.borderColor='';     
			
			
			
			  if(frm.pickup_start_date.value=='')
			{
			document.getElementById("pickup_start_date").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.pickup_start_date.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("pickup_start_date").style.borderColor='';    
			
			  if(frm.pickup_end_date.value=='')
			{
			document.getElementById("pickup_end_date").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.pickup_end_date.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("pickup_end_date").style.borderColor='';    
			
			
			  if(frm.to_state.value=='please Select')
			{
			document.getElementById("to_state").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.to_state.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("to_state").style.borderColor='';     
			
			
			  if(frm.to_city.value=='please Select')
			{
			document.getElementById("to_city").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.to_city.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("to_city").style.borderColor='';     
			
			
			
			  if(frm.delv_start_date.value=='')
			{
			document.getElementById("delv_start_date").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.delv_start_date.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("delv_start_date").style.borderColor='';     
			
			
			  if(frm.delv_end_date.value=='')
			{
			document.getElementById("delv_end_date").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.delv_end_date.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("delv_end_date").style.borderColor='';   
			
			if(frm.Width.value=='')
			{
			document.getElementById("Width").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.Width.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("Width").style.borderColor='';  
			
			
			if(frm.Height.value=='')
			{
			document.getElementById("Height").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.Height.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("Height").style.borderColor='';  
			
			
				
			if(frm.Length.value=='')
			{
			document.getElementById("Length").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.Length.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("Length").style.borderColor='';  
			
			if(frm.Weight.value=='')
			{
			document.getElementById("Weight").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.Weight.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("Weight").style.borderColor='';  
			
			
			if(frm.Qty.value=='')
			{
			document.getElementById("Qty").style.borderColor='#F00';
			if(checkFocus!=1)
			{	frm.Qty.focus();				checkFocus=1;			};
			}
			else
 			document.getElementById("Qty").style.borderColor='';  
			       		
			       		
		if(checkFocus==1)
			return false;
		else
			return true;
 }
 
