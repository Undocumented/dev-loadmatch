{if $smarty.session.user_type=='C'}
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Complete Shipment </h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Complete Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
           My Complete Shipment
          </h4>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="{$site_url}/edit_post_shipping" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                     <th>
                     Transporter
                    </th>
                    <th>
                      Collection
                    </th>
                    <th>
                      Delivery
                    </th>
                    <th>
                     Category
                    </th>
                   <!-- <th>
                     Under Category
                    </th>-->
                     <th>
                     Status
                    </th>
                   <th>
                     Action
                    </th>
                    <th>
                     Feed Back
                    </th>
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                      {$search[data].order_id}
                    </td>
                     <td>
                       {$functions->get_agent_name($search[data].order_id)}
                    </td>
                    <td>
                     {$search[data].form_state}, {$search[data].form_city}
                    </td>
                    <td>
                     {$search[data].to_state}, {$search[data].to_city}
                    </td>
                    <td>
                      {$functions->get_category($search[data].category)}
                    </td>
                   
                     <td>
                        {if $search[data].transport_status!=''}<div class="btn btn-success btn-sm">{$search[data].transport_status} </div>{else} Not Awlable {/if}
                    </td>
                 <td>
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a style="cursor:pointer;" onclick="submitbuy_book_cancel_complet('{$search[data].id}','ct',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                    </td>
                    <td> {$functions->get_agent_id($search[data].order_id)}
                     {$functions->get_feedback($agent_info_id,$smarty.session.user_id)}
                  
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a style="cursor:pointer;" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"/>{if $customer_info11[0].user_id=='' && $customer_info11[0].transpoter_id==''}Add Feedback{else}update{/if}</a>
                    </td>
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="9" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    {/if}
    
    {if $smarty.session.user_type=='T'}
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Complete Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Complete Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          Complete Shipment
          </h4>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="{$site_url}/detail_shiping_booked" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                      Customer
                    </th>
                    <th>
                     Pick up Time
                    </th>
                    <th>
                     Delivery Time
                    </th>
                     <th>
                      Delivery Location
                    </th>
                     <th>
                     Status
                    </th>
                     <th>
                     Action
                    </th> 
                     <th>
                    Feedback
                    </th>
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                      {$search[data].order_id}
                    </td>
                     <td>
                       {$functions->get_name($search[data].user_id)}
                    </td>
                    <td>
                     {$search[data].collect_start_date|date_format:"%d- %m -%Y"}, {$search[data].collect_end_date|date_format:"%d- %m -%Y"}
                    </td>
                    <td>
                     {$search[data].delivery_start_date|date_format:"%d- %m -%Y"}, {$search[data].delivery_end_date|date_format:"%d- %m -%Y"}
                    </td>
                    <td>
                     {$functions->get_destnation_state_city($search[data].order_id)}
                    </td>
                    <td>
                       {if $search[data].transport_status!=''}<div class="btn btn-success btn-sm">{$search[data].transport_status}</div> {else} Pending {/if}
                    </td>
                     <td>
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a style="cursor:pointer;" onclick="submitbuy_agent_cencle_complet('{$search[data].id}','{$search[data].order_id}','ct',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                    </td>
                    <td>{if $smarty.session.user_type=='T'}
                   	
                 {$functions->get_agent_id($search[data].order_id)}
                 {$functions->get_feedbacktrans($agent_info_id,$search[data].user_id)}
                  
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a style="cursor:pointer;" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal1"/>{if $trans_info[0].user_id=='' && $trans_info[0].transpoter_id==''}Add Feedback{else}update{/if}</a>
                    {/if}
                    </td>
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    {/if}
    
  
    
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
  <form action="" method="post" name="AddReview">  
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Review</h4>
      </div>
      <div class="modal-body">
           <div class="container">
                <div class="row">
                   <!-- <h2>Working Star Ratings for Bootstrap 3 <small>Hover and click on a star</small></h2>-->
                </div>
                <div class="row lead">
                <span id="chkreview"></span>
                	<div class="col-md-8">
                    	<div class="row p_modul_set">
                    		<div class="col-md-6">
                         {$functions->get_update_Transpoter_detail($functions->get_Transporter_name($search[0].order_id),$smarty.session.user_id)}
                           <input type="hidden" name="transpoter_id" value="{$functions->get_Transporter_name($search[0].order_id)}" /> 
                            
                            
                           
                          
                                <p>Communication</p>
                             
                            </div>
                    
                            <div class="col-md-6">
                            <input type="hidden"   name="com_rating" id="count_value1" value="{$transuser[0].com_rating}" required />
                   
                             
                                <div id="stars" class="starrr"></div> <span id="count">0</span>/5
                               
                            </div>
                    	</div>
                        
                        <div class="row p_modul_set">
                    		<div class="col-md-6">
                                <p>Care of Goods</p>
                            </div>
                    
                            <div class="col-md-6">
          <input type="hidden"   name="care_rating"  id="count_value2" value="{$transuser[0].care_rating}" required />
                 
                                <div id="stars1" class="starrr"></div> <span id="count1">0</span>/5
                       
                            </div>
                    	</div>
                        
                        <div class="row p_modul_set">
                    		<div class="col-md-6">
                                <p>Punctuality</p>
                            </div>
                    
                            <div class="col-md-6">
                             <input type="hidden"   name="pun_rating" id="count_value3" value="{$transuser[0].pun_rating}" required />
                   
                                <div id="stars2" class="starrr"></div> <span id="count2">0</span> /5
                    
                            </div>
                    	</div>
                        
                        <div class="row p_modul_set">
                    		<div class="col-md-6">
                                <p>Services as Described</p>
                            </div>
                    
                            <div class="col-md-6">
                             <input type="hidden"   name="ser_rating" id="count_value4" value="{$transuser[0].ser_rating}" required />
                          
                                <div id="stars3" class="starrr"></div> <span id="count3">0</span> /5
                          
                            </div>
                    	</div>
                    </div>
                    
                    <div class="col-md-4">
                    	<select class="sr-bt" name="rating" id="optm">
                  <option value="">Over All Rating</option>
                  <option {if $transuser[0].rating==natural} selected="selected" {/if} value="natural">natural</option>
                  <option {if $transuser[0].rating==negative} selected="selected" {/if} value="negative">negative</option>
                  <option {if $transuser[0].rating==positive} selected="selected" {/if} value="positive">positive</option>
                
                </select>
                    </div>
                    <!--You gave a rating of <span id="count">0</span> star(s)-->
                </div>
                
                <div class="row">
                	<textarea name="review" class="form-control"  placeholder="Enter Your Feedback" required>{$transuser[0].review} </textarea>
             
                </div>
                
                
                <!--<div class="row lead">
                    <p>Also you can give a default rating by adding attribute data-rating</p>
                    <div id="stars-existing" class="starrr" data-rating='4'></div>
                    You gave a rating of <span id="count-existing">4</span> star(s)
                </div>-->
            </div>
      </div>
      <div class="modal-footer">
        <input type="submit" name="submit1" onclick="return validForm(document.AddReview);" class="btn btn-primary btn-sm"></input>
      </div>
    </div>
</form>
  </div>
</div>
{if $smarty.session.user_type=='T'}
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
  <form action="" method="post" name="AddReview_tran">  
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Review for Customer</h4>
      </div>
      <div class="modal-body">
           <div class="container">
                <div class="row">
                   <!-- <h2>Working Star Ratings for Bootstrap 3 <small>Hover and click on a star</small></h2>-->
                </div>
                <div class="row lead">
                <span id="chkreview1"></span>
                	<div class="col-md-8">
                    	<div class="row p_modul_set">
                    		<div class="col-md-6">
                             {$functions->get_update_customer_detail($functions->get_Transporter_name($search[0].order_id),$search[0].user_id)}
                            
                           <input type="hidden" name="transpoter_id" value="{$functions->get_Transporter_name($search[0].order_id)}" /> 
                            
                            
                     <input type="hidden" name="cust_id1" value="{$functions->get_id_user($search[0].user_id)}" /> 
                            
                                <p>Communication</p>
                            </div>
                    
                            <div class="col-md-6">
                            <input type="hidden"   name="com_rating1" value="{$custuser[0].com_rating}" id="count_valuet" />
                          
                                <div id="starst" class="starrr"></div> <span id="countt">0</span>/5
                            </div>
                    	</div>
                        
                      {*  <div class="row p_modul_set">
                    		<div class="col-md-6">
                                <p>Care of Goods</p>
                            </div>
                    
                            <div class="col-md-6">
                             <input type="hidden"   name="care_rating2" value="" id="count_value" />
                                <div id="starsc" class="starrr"></div> <span id="countc">0</span>/5 
                            </div>
                    	</div> *}
                        
                        <div class="row p_modul_set">
                    		<div class="col-md-6">
                                <p>Punctuality</p>
                            </div>
                    
                            <div class="col-md-6">
                             <input type="hidden"   name="pun_rating3" value="{$custuser[0].pun_rating}" id="count_valuee" />
                           
                                <div id="starsp" class="starrr"></div> <span id="countp">0</span> /5
                            </div>
                    	</div>
                        
                       {* <div class="row p_modul_set">
                    		<div class="col-md-6">
                                <p>Services as Described</p>
                            </div>
                    
                            <div class="col-md-6">
                             <input type="hidden"   name="ser_rating4" value="" id="count_value4" />
                                <div id="starss" class="starrr"></div> <span id="counts">0</span> /5
                            </div>
                    	</div>*}
                    </div>
                    
                    <div class="col-md-4">
                    	<select class="sr-bt" name="rating1" id="optm">
                  <option  value="">Over All Rating</option>
                  <option {if $custuser[0].rating==natural} selected="selected" {/if} value="natural">natural</option>
                  <option {if $custuser[0].rating==negative} selected="selected" {/if} value="negative">negative</option>
                  <option {if $custuser[0].rating==positive} selected="selected" {/if} value="positive">positive</option>
                 {* <option value="buy">Nepal</option>
                  <option value="trs">Canada</option>
                  <option value="ser">Japan</option>*}
                </select>
                    </div>
                    <!--You gave a rating of <span id="count">0</span> star(s)-->
                </div>
                
                <div class="row">
                	<textarea name="reviews" class="form-control" placeholder="Enter Your Feedback" required>{$custuser[0].review}</textarea>
                    
                </div>
                
                
                <!--<div class="row lead">
                    <p>Also you can give a default rating by adding attribute data-rating</p>
                    <div id="stars-existing" class="starrr" data-rating='4'></div>
                    You gave a rating of <span id="count-existing">4</span> star(s)
                </div>-->
            </div>
      </div>
      <div class="modal-footer">
        <input type="submit" name="submit2" onclick="return validForm_trans(document.AddReview_tran);" class="btn btn-primary btn-sm"></input>
      </div>
    </div>
</form>
  </div>
</div>
{/if}