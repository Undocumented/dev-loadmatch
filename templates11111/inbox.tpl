{if $smarty.session.user_type=='C'}
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Inbox</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My Inbox</a></li>
                       {if $edit_id!=''}
                       <li><a href="{$site_url}/inbox.html">Back</a></li>
                       {/if}
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          Inbox
          </h4>
          <div class="contact-form">
              <div class="table-responsive">
            
   {if $edit_id==''}
              <form name="view_detail" method="post" action="{$site_url}/detail_shiping_booked" id="view_detail">
              <table class="table table-bordered" >
              
                <thead>
                  <tr>
                    <th width="25px;">
                      Sr.no
                    </th>
                    <th style="text-align:center;" >
                      Regarding
                    </th>
                    <th style="text-align:center;" >
                      Title
                    </th>
                  <!--  <th>
                     Message
                    </th>-->
                    <th width="10%">
                    Date
                    </th>
                     <th width="10%">
                     Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                    
                    (Order Id: {$search[data].order_id})
                    </td>
                    <td>
                      {$search[data].title}
                    </td>
                   {* <td>
                     {$search[data].mes|truncate:15:'...'}
                    </td>*}
                    <td>
                     {$search[data].entery_date|date_format:"%d- %m -%Y"}
                    </td>
                    <td>
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a href="{$site_url}/eagleushp.php?page=myaccount&action=inbox&edit_id={$search[data].id}" title="View"><i class="fa fa-pencil-square-o"></i></a>
                    <a href="{$site_url}/eagleushp.php?page=myaccount&action=inbox&del_mail={$search[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
                {else}
                <table align="center" border="0" width="70%" style="border:1px solid #000; padding:15px" >
                <tr>
                <td align="center" >
                User:&nbsp;{$functions->get_name($search12[0].user_id)}
                </td>
                </tr>
                <tr><td align="center"><img src="{$site_url}/images/profile_pic/{$functions->get_image($search[0].user_id)}" width="100px" height="100px" /></td></tr>
                <tr>
                <td align="center" style=" background-color:#CCC; text-align:left;">
                Title :&nbsp; {$search12[0].title} (Order Id: {$search12[0].order_id})
                </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                 <tr>
                <td align="center">
               <label> Message</label> &nbsp; {$search12[0].mes}
                </td>
                </tr>
                 <!--<tr><td>Form:&nbsp;{$functions->get_name($search[0].user_id)}</br>
                 Email: {$functions->get_email($search[0].user_id)}</br>
                 Date:{$search[0].entery_date}
                 </td></tr>-->
                </table>
                
                {/if}
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    {/if}
    
    {if $smarty.session.user_type=='T'}
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Inbox</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My Inbox</a></li>
                       {if $edit_id!=''}
                       <li><a href="{$site_url}/inbox.html">Back</a></li>
                       {/if}
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          Inbox 
          </h4>
          <div class="contact-form">
              <div class="table-responsive">
             
              {if $edit_id==''}
              <form name="view_detail" method="post" action="{$site_url}/detail_shiping_booked" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th width="25px;">
                      Sr.no
                    </th>
                    <th style="text-align:center;" >
                      Title
                    </th>
                  <!--  <th>
                     Message
                    </th>-->
                    <th width="10%">
                    Date
                    </th>
                     <th width="10%">
                     Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                      {$search[data].title}
                    </td>
                   {* <td>
                     {$search[data].mes|truncate:13:'...'}
                    </td>*}
                    <td>
                     {$search[data].entery_date|date_format:"%d- %m -%Y"}
                    </td>
                    <td>
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a href="{$site_url}/eagleushp.php?page=myaccount&action=inbox&edit_id={$search[data].id}" title="View"><i class="fa fa-pencil-square-o"></i></a>
                    <a href="{$site_url}/eagleushp.php?page=myaccount&action=inbox&del_mail={$search[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
                {else}
                <table align="center" border="0" style="border:1px solid #000;" >
                <tr>
                <td>
                User:&nbsp;{$functions->get_name($search12[0].agent_id)}
                </td>
                </tr>
                <tr><td><img src="{$site_url}/images/profile_pic/{$functions->get_image($search[0].agent_id)}" width="100px" height="100px" /></td></tr>
                <tr>
                <td style=" background-color:#CCC; text-align:left;">
                Title :&nbsp; {$search12[0].title} (Order Id: {$search12[0].order_id})
                </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                 <tr>
                <td>
                Message :&nbsp; {$search12[0].mes}
                </td>
               <!-- </tr>
                 <tr><td>Form:&nbsp;{$functions->get_name($search[0].user_id)}</br>
                 Email: {$functions->get_email($search[0].user_id)}</br>
                 Date:{$search[0].entery_date}
                 </td></tr>-->
                </table>
                
                {/if}
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    {/if}