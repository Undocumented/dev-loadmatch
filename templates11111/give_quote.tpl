  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Give Quote</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Give Quote</a></li>
                        <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="component-bg">
        <div class="container">
            <!-- Forms
================================================== -->
<div class="bs-docs-section mar-b-30">
  <h1 id="forms" class="page-header">Give Quote</h1>
 <span style="color:red;">{$err}</span>
    <form class="form-horizontal" name="post_shipment"  method="post" action="#" role="form">
    <label for="exampleInputEmail1">Transporter Details</label>
     <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="{$smarty.session.name}" id="s_user_name" name="s_user_name" placeholder="User Name" readonly="readonly">
          <input type="hidden" name="user_id" value="{$smarty.session.user_id}" />
          
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" value="{$smarty.session.email}" id="s_user_email" name="s_user_email" placeholder="Email" readonly="readonly">
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="mobile" value="{$functions->get_mobile($smarty.session.user_id)}"  name="mobile" placeholder="Mobile no" readonly="readonly">
        </div>
      </div>
   <hr />
     <label for="exampleInputEmail1">Transport Timeframe</label>
     
     
     
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Pickup Timing</label>
        <div class="col-xs-12 text-left">
        &nbsp;&nbsp;&nbsp;&nbsp;<INPUT NAME="pickup_start_date" id="pickup_start_date" TYPE="date"   value="{$search1[0].collect_start_date}" />
        
        
       <label for="inputEmail3" class=" control-label">&nbsp;&nbsp;Between&nbsp;&nbsp;</label>
         
        <INPUT NAME="pickup_end_date" id="pickup_end_date" TYPE="date"   value="{$search1[0].collect_end_date}" />
        
       
        </div>
      
  </div>
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery Timing</label>
        <div class="col-xs-12 text-left">
         &nbsp;&nbsp;&nbsp;&nbsp;  <INPUT NAME="delv_start_date" id="delv_start_date" TYPE="date"   value="{$search1[0].delivery_start_date}" />
         <label for="inputEmail3" class=" control-label">&nbsp;&nbsp;Between&nbsp;&nbsp;</label>
            <INPUT NAME="delv_end_date" id="delv_end_date" TYPE="date"   value="{$search1[0].delivery_end_date}" />
        </div>
      
  </div>
   <hr />
        <label for="exampleInputEmail1">Quoteion Price</label>
      <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Price</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="{$search1[0].starting_price}" id="Price" name="Price" placeholder="Price">
         </div>
      </div>
     <hr />  
       <label for="exampleInputEmail1">Payment Terms</label>
      
        <div class="form-group">
         <label for="inputEmail3"  class="col-sm-2 control-label" >Payment Methods</label>
        <div class="col-sm-10">
         <table>
       <tr>
       <td>
        <label class="checkbox-inline">
        <input type="checkbox" id="pay" name="pay[]" value="Cash" {if $cash=="Y"} checked {/if}> Cash&nbsp;&nbsp;&nbsp;&nbsp;
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="pay" name="pay[]" value="Credit Card" {if $Credit_Card=="Y"} checked {/if}>Credit Card
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="pay" name="pay[]" value="Cheque" {if $Cheque=="Y"} checked {/if} > Cheque
      </label>
      </td>
      </tr>
      <tr>
      <td>
      <label class="checkbox-inline">
       <input type="checkbox" id="pay" name="pay[]" value="PayPal" {if $PayPal=="Y"} checked {/if}> PayPal
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="pay" name="pay[]" value="Money Order" {if $Money_Order=="Y"} checked {/if} >Money Order
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="pay" name="pay[]" value="Other" {if $Other=="Y"} checked {/if} >Other
      </label>
      </td>
      </tr>
      </table>
      </div>
       </div>
       <div class="form-group">
         <label for="inputEmail3"  class="col-sm-2 control-label" >Payment Accepted</label>
        <div class="col-sm-10">
         <table>
       <tr>
       <td>
        <label class="checkbox-inline">
        <input type="checkbox" id="inlineCheckbox1" name="accept[]" value="Before collection" {if $Beforecollection=="Y"} checked="checked" {/if}> Before collection
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="inlineCheckbox2" name="accept[]" value="At collection" {if $Atcollection=="Y"} checked="checked" {/if}>At collection
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="inlineCheckbox3" name="accept[]" value="At Delivery" {if $AtDelivery=="Y"} checked="checked" {/if}> At Delivery
      </label>
      </td>
      </tr>
      
      </table>
      </div>
       </div>
 
 <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Additional Payment Terms</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="payment_terms" rows="3">{$search1[0].payment_terms}</textarea>
     </div>
        
  </div>
     
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
        <input type="hidden" value="{$user_id}" name="user_id" />
        <input type="hidden" value="{$order_id}" name="order_id" />
          <button type="submit" name="submit1" class="btn btn-default" onclick="return Valid_add_quote(document.post_shipment);" >Submit Quote	</button>
        </div>
      </div>
    </form>
  </div><!-- /.bs-example -->

  
     
   

</div>
        </div>
    </div>
    <!--container end-->