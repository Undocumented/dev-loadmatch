<div class="main-2 clearfix">
    	<div class="container">
        	<div class="latest-titel">
      	<div class="container">
        	<h4> Customer Name :<span style="color:red;"> {$usercust[0].name}</span></h4>
        </div>
      </div>
        </div>
    </div>
    
    <div class="container">
    	<h4 class="widget-title   top-lab youck"><span class="star-img"><img src="images/feedback_16x16.png"></span><a class="uo" href="#/">Read Feedback</a><span><img src="images/flag_white_12x17.png"></span> <a class="uo" href="#/">Flag Violation</a> <span><img src="images/info-blue-12x12.png"></span></h4>
    </div>
    
    <div class="container">
    	<div class="container hello">
        <div class="col-md-3">
          <div class="row1">
            <div class="sidebar">
              <h4 class="widget-title  label label-primary top-lab">Member Information</h4>
                    <h6><span class="plase2">Member Since:</span>
                     <span class="pull-right">{$usercust[0].entry_date}</span></h6>
                    
                  </div>
                    <h6><span class="plase2">Total Matches: </span> 
                     <span class="pull-right"></span></h6>
                    
              
                    <h6><span class="plase2">Shipments Posted: </span> 
                     <span class="pull-right">{$count_record}</span></h6>
                    
              
                    <h6><span class="plase2">Delivered Shipment: </span>
                     <span class="pull-right">{$count_status}</span></h6>
                    
              
              
                    <h6><span class="plase2">Undelivered Shipment: </span> 
                     <span class="pull-right">{$Uncount_status}</span></h6>
                    
              
              
                    <h6><span class="plase2">Percent Positive:  </span>
                     <span class="pull-right">{$total_positive}</span></h6>
                    
              
              
                    <h6><span class="plase2">Total Natural: </span>
               			<span class="pull-right">{$totalcustreviewfornat_status}/{$totalcustreview_status}</span></h6>
                    
                    <h6>
                    <span class="plase2">Total Negative: </span> 
                   	<span class="pull-right">{$totalcustreviewneg_status}/{$totalcustreview_status}</span>
                   </h6>
                    
                   <h6>
                    <span class="plase2">Total Positive: </span> 
                   	<span class="pull-right">{$totalcustreviewforpos_status}/{$totalcustreview_status}</span>
                   </h6>
                  
                  
                  
        
            </div>
            <a href="#/">Reset All Filters</a>
            <div><input id="ContentBody_chkNoFollow" name="_ctl0:ContentBody:chkNoFollow" type="checkbox"><label>Filters, don't follow me</label></div>
            <span class="new_window"><input id="ContentBody_chkNewWindow" name="_ctl0:ContentBody:chkNewWindow" type="checkbox"><label for="chkNewWindow">Open Listings in New Window</label></span>
          </div>
        <div class="col-md-9">
        <div class="border-left clearfix">
        <h4 class="widget-title   top-lab">Customer History </h4>
        
          <div class="row1">
            <table class="table default footable-loaded footable">
              <thead>
                <tr>
                  <th data-toggle="true" class="footable-first-column"></th>
                  <th class="text-center">Total </th>
                  <th data-hide="phone,tablet" class="text-center">1 mo </th>
                  <th data-hide="phone,tablet" class="text-center">6 mo </th>
                  <th data-hide="phone,tablet" class="text-center">12 mo</th>
                </tr>
              </thead>
              <tbody>
                
                
                <tr>
                  <td class="footable-first-column">
                  <p class="quli">Positive: </p>
                  <p>Netural: </p>
                  <p> Negative: </p>
                  
                  </td>
                  
                  <td class="h-color text-center">
                  <p>{$totalcustreviewforpos_status}</p>
                  <p>{$totalcustreviewfornat_status}</p>
                  <p>{$totalcustreviewneg_status}</p>
                  </td>
                  
                  <td class="text-center">
				  <p>{$onemonth_data_status}</p>
                  <p>{$onemonthnatural_data_status}</p>
                  <p>{$onemonthnegative_data_status}</p>
                  </td>
                  
                  
                  <td class="text-center">
                  	  <p>{$sixmonth_data_status}</p>
                  <p>{$sixmonthnetural_data_status}</p>
                  <p>{$sixmonthnegative_data_status}</p>
                   </td>
                   
                  <td class="text-center">
                  	  <p>{$oneyear_data_status}</p>
                  <p>{$oneyearnatural_data_status}</p>
                  <p>{$oneyearnegative_data_status}</p>
                  </td>
                </tr>
                
                <tr>
                  <td class="footable-first-column">
                  <p>Total Count:  </p>
                  <p>Cancellations:  </p>
                  <p>Shipments Posted: </p>
                  
                  
                  </td>
                  
                  <td class="h-color text-center">
                  <p>{$all_total_count}</p>
                  <p>{$cancel_record}</p>
                  <p>{$total_shipment_status}</p>
              
                  </td>
                  
                  <td class="text-center">
				  <p>{$all_count_onemonth}</p>
                  <p>{$cancel_recordonemonth}</p>
                  <p>{$total_shipment_status}</p>
              
                  </td>
                  
                  
                  <td class="text-center">
                  	  <p>{$all_count_sixmonth}</p>
                  <p>{$sixmonthcancel_data_status}</p>
                  <p>{$totalsixmonth_shipment_status}</p>
                   </td>
                   
                  <td class="text-center">
                  	  <p>{$all_count_oneyear}</p>
                  <p>{$oneyearcancel_data_status}</p>
                  <p>{$totalonemonth_shipment_status}</p>
        
                  </td>
                </tr>

              </tbody>
            </table>
          </div>
          <div class="pluse"><a href="#/"><img src="images/add-white-icon.png" alt=""></a></div>
        </div>
      </div>
      </div>
    </div>
    </div>
    <div class="container">
    
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home"> Feedback Received (1) </a></li>
   
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
    {if $customerrating}
      {section name=data loop=$customerrating}
      <div class="border-2 clearfix">
                
                  <ul class="list-tab ">
                  <li>Rating: </li>
                  <li>Left by: </li>
                  <li> 	On:  </li>
                  <li>Review:</li>
                  
                  </ul>
                  
                  <ul class="list-tab-2">
                  <li>{$customerrating[data].rating}</li>
                  <li>{$username_data[data].name}</li>
                  <li>{$customerrating[data].date}</li>
                  <li>{$customerrating[data].review}</li>
                  </ul>
                  
                 <div>
                 
                  	<p class="pok">Communication <span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                   
                    <p class="pok">Punctuality<span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                   
                                      	
                     
                      
                              

                  </div>     
             
       </div> 
       
              
                
          {/section}      
{else}
  Record not found
  {/if}
    </div>
    
    
    
  </div>
  
</div>





        
                  
  <!--End Header-->

      <!-- END CLIENTS -->
    </div>