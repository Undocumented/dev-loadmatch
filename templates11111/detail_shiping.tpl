
<div id="wrapper">
  
    <div class="main-2 clearfix">
    <div class="container">
    
    	<div class="back-img">
        <h2>Using Loadmatch to Book Loads is Easy!</h2>
        
        <div class="row">
        
        	<div class="col-md-3">
            <div class="opo">
            	<h5>Step 1:</h5>
                <p>Join for FREE & find listings like the one below to bid on.</p>
                </div>
            </div>
<div class="col-md-3">
            <div class="opo">
            	<h5>Step 2:</h5>
                <p>Ask questions, work out details, and make bids on the listings.</p>
                </div>
            </div>
<div class="col-md-3">
            <div class="opo">
            	<h5>Step 3:</h5>
                <p>Pick up the shipment and make the delivery in order to complete the transaction and get paid.</p>
                </div>
            </div>
<div class="col-md-3">
            <div class="opo">
            	<h5>Step 4:</h5>
                <p>Shippers leave you feedback, which helps you book more loads.</p>
                </div>
            </div> 
            
            </div>
            
            
            
            <div class="col-md-3">{if $smarty.session.user_id==''} <a class="btn btn-warning top-y" href="{$site_url}/login.html">Members Sign In To Bid</a>{/if}</div>
            <div class="col-md-6"><p class="top-y test-center"> ---- Already reviewed this listing and ready to make a bid? ---- </p></div>
             <div class="col-md-3">{if $smarty.session.user_id==''}<a class="btn btn-warning top-y" href="{$site_url}/registration.html">Join Now To Place a Bid</a>{/if} </div> 
                 </div>
                 
    </div>
    
    <br />
    <div class="container">
		<div class="row">
			<div class="col-md-4">
				<h4>Category : {$functions->get_category($search[0].category)} > {$functions->get_category($search[0].sub_category)} > {$functions->get_sub_next_category($search[0].parent_sub_id)} >  {$functions->get_last_category($search[0].last_cat)}</h4>
				{if $smarty.session.user_type=='T'}
					<h6> {$search[0].title} </h6>
					<small><a href="{$site_url}/cust_fedback-{$search[0].user_id}.html">Customer Feed Back Profile</a></small>
				{/if}
				{if $smarty.session.user_id==''}                  
					<a href="{$site_url}/registration.html" >Not Have Account Yet?</a>
				{/if}
			</div>
			<div class="col-md-6">
				{if $alredy_quote=='Y'}       
				<button class="btn btn-info pull-right">You Have Already Given Quote on This Shipment </button>       
				{/if}
				
				{if $type=='su'}       
				<form method="post" action="" name="next">
					<input type="hidden" value="{$job_id}" name="job_id" />
					<input type="hidden" value="{$search[0].order_id}" name="order_id" />			
					<a href="{$site_url}/my_post_job.html" class="btn pull-right btn-info"> Go to My Account</a>		
				</form>        
				{/if}
			</div>
			{if $smarty.session.user_type=='T'}
			{if $smarty.session.user_id=='' || $alredy_quote!='Y' && $type!='su' }
			<div class="col-md-2">			
				<form method="post" action="{$site_url}/give_quote.html" name="give_quote">
					<input type="hidden" value="{$job_id}" name="job_id" />
					<input type="hidden" value="{$search[0].order_id}" name="order_id" />			
					<input type="submit" name="submit" value="Give Quote" class="btn given_quote" />			
				</form>
			</div>
			{/if}
            {/if}
			
		</div>
		<hr />
    	<!--<div class="col-md-3">
        
            {if $smarty.session.user_type=='T'}
        	<h6> {$search[0].title} </h6>
            <p><a href="{$site_url}/cust_fedback-{$search[0].user_id}.html">Customer Feed Back Profile</a></p>
            {/if}
        </div>-->
        
       <!-- <div class="col-md-2">
        </div>-->
		
        <!--{if $smarty.session.user_id==''}                  
        	<a href="{$site_url}/registration.html" > <button    class="btn btn-danger">Not Have Account Yet? </button></a>
      	{/if}-->
		
      <!--{if $smarty.session.user_id=='' || $alredy_quote!='Y' && $type!='su'}
        <div class="col-md-4">
        
        	<form method="post" action="{$site_url}/give_quote.html" name="give_quote">
                    <input type="hidden" value="{$job_id}" name="job_id" />
                    <input type="hidden" value="{$search[0].order_id}" name="order_id" />
                    
                    <input type="submit" name="submit" value="Give Quote" class="btn btn-info" />
                   
                    </form>
        </div>
        {/if}
		
        {if $type=='su'}       
		<form method="post" action="" name="next">
			<input type="hidden" value="{$job_id}" name="job_id" />
			<input type="hidden" value="{$search[0].order_id}" name="order_id" />			
			<a href="{$site_url}/my_post_job.html" class="btn btn-info"> Go to My Account</a>		
		</form>        
        {/if}
        {if $alredy_quote=='Y'}       
        	<button class="btn btn-info">You Have Already Given Quote on This Shipment </button>       
        {/if}-->
        
        <!--<div class="col-md-3">
        	<p>Category : {$functions->get_category($search[0].category)} </p>
            <p>Under Category:{$functions->get_category($search[0].sub_category)}</p>
        </div>-->
    </div>
    </div>
    
    
    <div class="container">
    	<div class="col-md-4">
        	<h4 class="widget-title   top-lab">Listing Information </h4>
            <div class="clearfix">
                <div class="col-md-6">
                    <ul class="list-k">
                        <li>Delivery Title:</li>
                        <li>Shipment ID:</li>
                        <li>Customer:</li>
                        <li>Pickup Between</li><br/>
                        <li>Quote :</li>
                      
                       {* <li>Date Listed:</li>
                        <li>Ends:</li>
                        <li>Budget:</li>
                        <li>Low Quote:</li>
                        <li># of Quotes:</li>
                        <li>Auction Goods:</li>*}
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>{$search[0].title}</li>
                        <li>{$search[0].order_id}</li>
                        <li>{$functions->get_name($search[0].user_id)}</li>
                    
                        <li>{$search[0].pickup_start_date|date_format:"%d- %m -%Y"}
                         To {$search[0].pickup_end_date|date_format:"%d- %m -%Y"}</li>
                         <li>({$functions->get_total_quote($search[0].order_id)})</li>
                    
                    </ul>
                </div>
                </div>
        	<h4 class="widget-title   top-lab">Shipment Detail</h4>
            <div class="col-md-6">
                    <ul class="list-k">
                        <li>Load Industry :</li>
                        <li>Load Type:</li>
                        <li>Transport Type:</li>
                        <li>Body &amp Bulk Type:</li>
                        <li>Size Classification:</li>
                        <li>Preferred Distance:</li>
                        <li>Other:</li>
                        <li>Dimensions:</li>
                        <li>Width:  </li>
                        <li>Height :</li>
                        <li>Length: </li>
                        <li>Weight :</li>
                      
                       <li>Quantity :</li>
                      
                       <li>Price :</li>
                         <li>Description :</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>{$functions->get_category($search[0].category)}</li>
                        <li>{$functions->get_category($search[0].sub_category)}</li>
                        <li>{$functions->get_category($search[0].sub_category)}</li>
                        <li>{$functions->get_category($search[0].sub_category)}</li>
                        <li>{$functions->get_category($search[0].sub_category)}</li>
                        <li>{$functions->get_category($search[0].sub_category)}</li>
                        <li>{$functions->get_category($search[0].sub_category)}</li>
                        <li>{$search[0].dimensions}</li>
                        <li>{$search[0].Width} </li>
                        <li>{$search[0].Height}</li>
                         <li>{$search[0].Length} </li>
                         <li>{$search[0].Weight}</li>
                         <li>{$search[0].Qty}</li>
                        
                          <li>{$search[0].value}</li>
                      
                         <li>{$search[0].description|truncate:100:'...'}</li>
                    </ul>
                </div>
                
                
                <div class="colmd-12">
                 {if $search[0].img}
            <img src="{$site_url}/images/shipment_image/{$search[0].img}" class="img-responsive" alt="">
            {else}
                          {$functions->get_cat_image($search[0].category)}
                          <img src="{$site_url}/images/{$currency_info[0].cat_image}" style="width:100px"; height="100px;" alt="">
                          {/if}
                          <div>&nbsp;</div>
                    </div>        
                          
        </div>
        <div class="col-md-8">
        	<h4 class="widget-title   top-lab">Origin, Destination, & Route Information </h4>
            
            <div class="col-md-8">
              {include_php file="gmap.php"}  
            	
            </div>
            <div class="col-md-4">
            	<ul class="tobot">
                	<li>
                    <div class="pull-left lochan">
                    	<img src="images/pin-pickup.png" >
                    </div>
                    
                    	<h5>Collection</h5>
                        
                        <p>Address :{$search[0].sender_loc}</p>
                        <p>{$search[0].form_city},{$search[0].form_state}</p>
                                             
                    </li>
                    
                    
                    <li>
                     <div class="pull-left lochan">
                    	<img src="images/pin-pickup.png" >
                    </div>
                    	<h5>Delivery</h5>
                        <p>Address :{$search[0].receiver_loc}</p>
                        <p>{$search[0].to_city},{$search[0].to_state}</p>
                        <p>Delivery Between:{$search[0].delv_start_date|date_format:"%d- %m -%Y"} to {$search[0].delv_end_date|date_format:"%d- %m -%Y"}</p>
                        
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="container">
    <div class="col-md-12">
    	<h4 class="widget-title top-lab">{$functions->get_category($search[0].category)} > {$functions->get_category($search[0].sub_category)} > {$functions->get_sub_next_category($search[0].parent_sub_id)} >  {$functions->get_last_category($search[0].last_cat)}  </h4>
    	</div>
        <div class="col-md-12">
        	<div class="jio-box">
            	<h5>{$search[0].title}</h5>
                <h6>Quantity :{$search[0].Qty}</h6>
            </div>
        </div>
        
        <!--<div class="col-md-12">
        <div class="toper clearfix">
        	<h5 class="pull-left">Motorcycle 1: <span>{$search[0].title}</span></h5>
            <ul class="pull-left ricko">
            
            
            	<li>Operational:</li>
                <li>Trike:</li>
                <li>Sidecar:</li>
                <li>Palletised:</li>
            </ul>
            </div>
                       <h6>Accepted Service Types:
Open Transport, Enclosed Transport, Part Load Freight - Transport Only</h6>
 
        </div> -->
    </div>
    
 {if $smarty.session.user_type!=''}
 <div class="container">
 	<div class="col-md-12">
        
    	
        
      <!--{if $data===''}  <p> - There are currently no questions for this shipment - </p> {/if}-->
        
        <h4 class="widget-title top-lab">Questions for this Item  </h4>
        
         {if $data===''}  <p> - There are currently no questions for this shipment - </p> {/if}
        
       <!-- <h4>Quotes on this Shipment</h4>-->
   <!-- </div> -->
   {if $smarty.session.user_type=='T' }
      <form action="" method="post">
    <div class="row">
    	<div class="col-md-12">
        	<div class="col-md-6">
                
                  <div class="form-group">
             
             
                
                    <label for="usr">Enter Your Question:</label>
                    <input type="text" class="form-control" name="qus" placeholder="Type Your Question Here...">
                    <input type="hidden" value="{$smarty.session.user_id}" name="tid" />
                    <input type="hidden" value="{$functions->get_cust_name($smarty.session.user_id)}" name="tran_name" />
                    
                     <input type="hidden" value="{$search[0].user_id}" name="cid" />
                    
                </div>
               
                
                <button type="submit" class="btn btn-primary" name="Submit">Submit</button>
                    
            </div>
           
            <div class="col-md-6"></div>
        </div>
    </div>
      </form>
      {else} 
      <form action="" method="post">
    <div class="row">
    	<div class="col-md-12">
        	<div class="row">
        	<div class="col-md-6">
                
                  <div class="form-group">
             
             
                   {section name=data loop=$data}
                    <label for="usr">Transpoter Name:{$data[data].trans_name}</label>
                    <input type="hidden" name="ques_id_{$data[data].ques_id}" value="{$data[data].ques_id}">
                    <input type="text" class="form-control" readonly="readonly" value="{$data[data].question}" name="question" placeholder="Type Your Question Here..."><br/>
                  <label>Ans{$smarty.section.data.index+$row_no+1}</label>
                 
                   <input type="text" class="form-control"  name="ans_{$data[data].ques_id}" value="{$data[data].answer}" placeholder="Type Your Answer Here...">
                
               {/section}
                </div>
                <button type="submit" class="btn btn-primary" name="Submit1">Submit</button>
                    
            </div>
           
            <div class="col-md-6"></div>
            </div>
        </div>
    </div>
      </form> 
      {/if}    
 </div>
 {/if}
 <br />
    <h4>Quotes on this Shipment</h4>
 
 <div class="container">
 	<div class="accordion-style1 panel-group" id="accordion">
                
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title ">
<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="accordion-toggle collapsed " aria-expanded="false">
<i data-icon-hide="icon-angle-down" data-icon-show="icon-angle-right" class="icon-angle-down bigger-110"></i>
<img style="float:left;" class="img-responsive" src="images/arrow.png">&ensp; Description  
</a>
</h4>
</div>
<div style="min-height: 100px;" class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
<div class="panel-body"> {$search[0].description} </div>
</div>
<div class="row">
    	<div class="col-md-12">
        	<div class="col-md-6">
                
                  <div class="form-group">
             <br />
         
                {section name=data loop=$usersdata1}
                    <label>Qus{$smarty.section.data.index+$row_no+1}</label>
                    <input type="text" class="form-control" readonly="readonly" value="{$usersdata1[data].question}" name="question" placeholder="Type Your Question Here..."><br/>
                  <label>Ans{$smarty.section.data.index+$row_no+1}</label>
                   <input type="text" class="form-control" readonly="readonly" value="{$usersdata1[data].answer}" name="ans" placeholder="Type Your Answer Here...">
                    
                    {/section}
                </div>
               
                    
            </div>
           
            <div class="col-md-6"></div>
        </div>
    </div>
</div>
<br />







</div>
 </div>
        
                  
  <!--End Header-->

      <!-- END CLIENTS -->
    </div>
  <!--End Main-->
  
  <!--End Footer--> 
</div>
</body>
</html>
