 <div class="main-2 clearfix">
    	<div class="container">
        	<div class="latest-titel">
      	<div class="container">
        	<h4> Transpoter Name : <span style="color:red">{$transcust[0].name}</span> </h4>
        </div>
      </div>
        </div>
    </div>
    
   <!-- <div class="container">
    	<div class="col-md-2"><a class="btn btn-warning top-y bb-btn" href="#/">Request Quote</a></div><div class="col-md-10"><div class="row"><h4 class="widget-title   top-lab youck"><span class="star-img"><img src="images/feedback_16x16.png"></span><a class="uo" href="#/">Read Feedback</a><span><img src="images/flag_white_12x17.png"></span> <a class="uo" href="#/">Flag Violation</a> <span><img src="images/info-blue-12x12.png"></span></h4></div></div>
    </div>-->
    
    <div class="container"><h4 class="widget-title   top-lab">Company Snapshot*  </h4></div>
    </div>
	
    
    
    <div class="container">
    	<div class="uko-con clearfix">
        	<div class="col-md-6 ">
            	<p> Company: {if $transcust[0].t_company_name!=''}{$transcust[0].t_company_name}{else} Company Not Available{/if}</p>
                <p>Number of empolyee:{if $transcust[0].no_employ!=''}{$transcust[0].no_employ} {else} not found{/if} </p>
                <p>Experience:{if $transcust[0].experiance!=''}{$transcust[0].experiance} {else} not found {/if}</p>
                <p><!--Image:-->{if $transcust[0].image!=''}<img src="{$site_url}/images/profile_pic/{$transcust[0].image}" style="max-height:100px;" {else}<img src="{$site_url}/images/profile_pic/notfound.png" style="max-height:100px;" {/if} </p>
               {* <p><a href="#/">View Terms & Conditions</a></p> *}
            </div>
            <div class="col-md-6 yo-border">
            {*	<p>Number of Trucks: 3 </p>
                <p>Number of Drivers: 3 </p> *}
                <p>Locations Served: India: Nationwide, International: Worldwide </p>
                <p>Categories Served: Boats, Business & Industrial Goods, Food & Agriculture, Full Load (FTL Freight), Plant & Heavy Equipment, Horses & Livestock, Household Goods, Household Moves, Part Load, Motorcycles, Other Goods, Pets & Livestock, Special Care, Vehicles </p>
                <p>Trailer Type: Dry Van, Flatbed, Low Loader, Step Deck, Power Only, Refrigerated vehicle, Air Ride Van, Auto Carrier, Dump, Flatbed Double, Removable Gooseneck, Stretch Flatbed, Tanker, Van Double, Other </p>
            </div>
        </div>
    </div>
 
 
<div class="container hello">
	<div class="col-md-4">
    <h4 class="widget-title   top-lab">Member Information</h4>
    	<table class="profile-table profile-table-memberinfo">
                                    
                <tbody><tr>
                    <td class="profile-table-label">Member Since:</td>
                    <td>08-07-15</td>
                </tr>
                <tr>
                    <td class="profile-table-label">Feedback Score:</td>
                    <td class="votes">{$total_fedback_store}</td>
                </tr>
                <tr>
                    <td class="profile-table-label">Positive Feedback:</td>
                    <td>{$positive_fedback}%</td>
                </tr>
                <tr>
                    <td class="profile-table-label">Star Rating:</td>
                    <td>
                        <div class="layout">
                        <div class="layout-unit layout3of4">
                            <span class="star-rating rating">
                                <span id="ContentBody_spanGoogleReviewFeedbackRating" class="value-title" style="width:91%;" title="4.6"></span>
                            </span>
                        </div>
                        <div class="layout-unit layout1of4">
                            <span style="margin-left:4px; font-weight:bold">{$star_rating}/5</span>
                        </div>
                        </div>
                        <a href="#Feedback"><span class="count">based on {$positive_row} reviews</span></a>
                   </td>
                </tr>
              
                <tr id="ContentBody_trExcessiveCancellations" style="display:none;">
	<td class="profile-table-label">Excessive Cancellations:</td>
	<td></td>
</tr>

            {*    
                <tr>
                    <td colspan="2"><ul id="ContentBody_ulRankings" class="rankings"><li><span style="font-weight:bold;">uShip Sales Rank:</span> #<span style="font-weight:bold;">702</span> of 851,726 overall</li><li>#<span style="font-weight:bold;">5</span> in Other Pets &amp; Livestock</li><li>#<span style="font-weight:bold;">50</span> in Pets</li><li>#<span style="font-weight:bold;">66</span> in Dogs</li><li><a id="lnk-all-rankings" href="#">See all rankings</a></li></ul></td>
                    <td></td>
                </tr> *}
            </tbody></table>
    </div>
    
    <div class="col-md-4">
    	<h4 class="widget-title   top-lab">Feedback History</h4>
        <table class="profile-table-feedback-history">
                    <tbody><tr class="profile-table-feedback-header">
                        <td style="width:10px;">&nbsp;</td>
                        <td class="layout-1of5"></td>
                        <td class="layout-1of5">Total</td>
                        <td class="layout-1of5">1 mo</td>
                        <td class="layout-1of5">6 mo</td>
                        <td class="layout-1of5">12 mo</td>
                    </tr>
                    <tr>
                        <td style="width:10px;">&nbsp;</td>
                        <td class="layout-1of5 profile-positive">Positive:</td>
                        <td class="layout-1of5 profile-center">
                            <div class="profile-table-feedback-greyCol">
                                
                                       {$trans_rating}
                                
                            </div>
                        </td>
                        <td class="layout-1of5 profile-center">{$total_ratingonemonth_status}</td>
                        <td class="layout-1of5 profile-center">{$total_ratingsixmonth_status}</td>
                        <td class="layout-1of5 profile-center">{$total_ratingoneyearpositive_status}</td>
                    </tr>
                    <tr>
                        <td style="width:10px;">&nbsp;</td>
                        <td class="layout-1of5 profile-neutral">Neutral:</td>
                        <td class="layout-1of5 profile-center">
                            <div class="profile-table-feedback-greyCol">
                                
                                   {$trans_ratingnatural}
                                
                            </div>
                        </td>
                        <td class="layout-1of5 profile-center">{$total_ratingnetural_status}</td>
                        <td class="layout-1of5 profile-center">{$total_ratingsixmonthnetural_status}</td>
                        <td class="layout-1of5 profile-center">{$total_ratingoneyearnatural_status}</td>
                    </tr>
                    <tr>
                        <td style="width:10px;">&nbsp;</td>
                        <td class="layout-1of5 profile-negative">Negative:</td>
                        <td class="layout-1of5 profile-center">
                            <div class="profile-table-feedback-greyCol">
                                
                                   {$trans_ratingngative}
                                
                            </div>
                        </td>
                        <td class="layout-1of5 profile-center">{$total_ratingnegative_status}</td>
                        <td class="layout-1of5 profile-center">{$total_ratingsixmonthnegative_status}</td>
                        <td class="layout-1of5 profile-center">{$total_ratingoneyearnegative_status}</td>
                    </tr>
                    <tr>
                        <td style="width:10px;">&nbsp;</td>
                        <td class="layout-1of5">Total:</td>
                        <td class="layout-1of5 profile-center">
                            <div class="profile-table-feedback-greyCol">{$total_rating}</div>
                        </td>
                        <td class="layout-1of5 profile-center">{$total_onemonthrating}</td>
                        <td class="layout-1of5 profile-center">{$total_sixmonthrating}</td>
                        <td class="layout-1of5 profile-center">{$total_oneyearrating}</td>
                    </tr>
                  {*  <tr>
                        <td style="width:10px;">&nbsp;</td>
                        <td class="layout-1of5">Cancellations:</td>
                        <td class="layout-1of5 profile-center">
                            <div class="profile-table-feedback-greyCol">10</div>
                        </td>
                        <td class="layout-1of5 profile-center">0</td>
                        <td class="layout-1of5 profile-center">4</td>
                        <td class="layout-1of5 profile-center">8</td>
                    </tr> *}
                </tbody></table>
    </div>
    
    <div class="col-md-4">
    	<h4 class="widget-title   top-lab">Feedback History</h4>
        <div id="detailed">
            <table class="profile-table-feedback">
                <tbody><tr class="profile-table-feedback-header">
                    <td class="profile-table-feedback-label"></td>
                    <td class="profile-table-feedback-content" style="text-align:left;">Average Rating (last yr)</td>
                </tr>
                <tr>
                    <td class="profile-table-feedback-label">Communication</td>
                    <td class="profile-table-feedback-content">
                        <div class="layout profile-table-feedback-StarContainer">
                            
                            
                             {if $comunication_per1==''}
                            
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                            {if $comunication_per1=='1'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             
                            {if $comunication_per1=='2'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                            {if $comunication_per1=='3'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             {if $comunication_per1=='4'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             {if $comunication_per1=='5'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i> 
                            {/if}
                            
                                
                                
                                
                            <span class="str_set">{$comunication_per1}/5</span>    
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="profile-table-feedback-label">Care of Goods</td>
                    <td class="profile-table-feedback-content">
                        <div class="layout profile-table-feedback-StarContainer">
                            
                            
                             {if $caregood_per1==''}
                            
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                            {if $caregood_per1=='1'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             
                            {if $caregood_per1=='2'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                            {if $caregood_per1=='3'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             {if $caregood_per1=='4'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             {if $caregood_per1=='5'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i> 
                            {/if}
                            
                                
                                
                                
                            <span class="str_set">{$caregood_per1}/5</span>    
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="profile-table-feedback-label">Punctuality</td>
                    <td class="profile-table-feedback-content">
                        <div class="layout profile-table-feedback-StarContainer">
                            
                            
                             {if $punctuality_per1==''}
                            
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                            {if $punctuality_per1=='1'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             
                            {if $punctuality_per1=='2'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                            {if $punctuality_per1=='3'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             {if $punctuality_per1=='4'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             {if $punctuality_per1=='5'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i> 
                            {/if}
                            
                                
                                
                                
                            <span class="str_set">{$punctuality_per1}/5</span>    
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="profile-table-feedback-label">Services as Described</td>
                    <td class="profile-table-feedback-content">
                        <div class="layout profile-table-feedback-StarContainer">
                            
                            
                             {if $service_per1==''}
                            
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                            {if $service_per1=='1'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             
                            {if $service_per1=='2'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                            {if $service_per1=='3'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             {if $service_per1=='4'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            {/if}
                             {if $service_per1=='5'}
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i> 
                            {/if}
                            
                                
                                
                                
                            <span class="str_set">{$service_per1}/5</span>    
                        </div>
                    </td>
                </tr>
            </tbody></table>
        </div>
    </div>
</div>

<div class="container">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Feedback Received ({$total_fedback_store}) </a></li>
   <!-- <li><a data-toggle="tab" href="#menu1"> Feedback Left (153)</a></li>
    <li><a data-toggle="tab" href="#kk"> Cancellations (10)</a></li>
    -->
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>Items: 1 - 1 of 1 </h3>
      <div class="border-2 clearfix">
<table class="table default footable-loaded footable">
              <thead>
                
              </thead>
              <tbody>
              {section name=data loop=$transpoterrating}
                <tr>
                  
                  <td>
                      <ul class="uya-list pull-left">
                      	<li>Rating:</li>
                       
                        <li>Left by:</li>
                        <li>On:</li>
                      
                      </ul>
                      
                      <ul class="uya-list pull-left">
                      	<li>{$transpoterrating[data].rating}</li>
                       
                        <li>{$username_data[data].name}</li>
                        <li>{$transpoterrating[data].date}</li>
                       
                       
                      </ul>
                  </td>
                  <td>
                  	<p class="pok">Communication <span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                    <p class="pok">Care of Goods<span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                    <p class="pok">Punctuality<span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                    <p class="pok">Services as Described<span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                                      	
                     
                      
                              

                  </td>
                  <td>
                  	<p>	{$transpoterrating[data].review} </p>
                  <td></td>
                  
                </tr>
                {/section}
               
               
               
                {*
                <tr class="color-t">
                  
                  <td>
                      <ul class="uya-list pull-left">
                      	<li>Rating:</li>
                        <li>Company:</li>
                        <li>Left by:</li>
                        <li>On:</li>
                        <li>For:</li>
                      </ul>
                      
                      <ul class="uya-list pull-left">
                      	<li>Positive</li>
                        <li>Sumant Bhattacharya</li>
                        <li>Ajay S.</li>
                        <li>22-09-2016</li>
                        <li>maruti esteem</li>
                      </ul>
                  </td>
                  <td>
                  	<p class="pok">Communication <span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                    <p class="pok">Care of Goods<span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                    <p class="pok">Punctuality<span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                    <p class="pok">Services as Described<span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                                      	
                     
                      
                              

                  </td>
                  
                  <td>Bangalore to Rewa via Nagpur. Nicely executed by Sumant Bhattacharya. Thank you.</td>
                  
                </tr>
                *}

              </tbody>
            </table>             
       </div>            
                
                

    </div>
    

    
                      
                      
    
  </div>
</div>


        