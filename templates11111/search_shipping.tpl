<!--End Header-->
<section id="main">
<div class="main-inner">
<div class="container">
	<div class="clearfix">
		<form name="search" id="search" method="post" action="#">
			<div class="col-md-3">
				<div class="row1">
					<div class="sidebar">
						<h4 class="widget-title  label label-primary top-lab" >Refine Search</h4>
						<div class="widget">
							<div class="items">
								<div class="item">
									<h6><span class="plase2">Deliver To</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-1"><img src="images/Add-icon.png"></a> </div>
										</span> <span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
								<div class="collapse" id="more-items-1">
									<div class="item">
										<div class="check-box-item">
											<select name="to"  id="to" class="form-control border-radius" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
      				{section name=data loop=$categories}
                        
                    
												<option value="{$categories[data].city}" {if $filter_to==$categories[data].city} selected="selected"{/if} >{$categories[data].city} </option>
												
                  {/section}
                   
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="widget">
							<div class="items">
								<div class="item">
									<h6><span class="plase2">Pickup From</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-2"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
								<div class="collapse" id="more-items-2">
									<div class="item">
										<div class="check-box-item">
											<select name="from"  id="form" class="form-control border-radius" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
      				{section name=data loop=$categories}
                        
                    
												<option value="{$categories[data].city}" {if $filter_from==$categories[data].city} selected="selected"{/if} >{$categories[data].city} </option>
												
                  {/section}
                   
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="widget">
							<div class="items">
								<div class="item">
									<h6><span class="plase2">Load Industry</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-3"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
								<div class="collapse" id="more-items-3">
									<div class="item">
										<div class="check-box-item">
											<select name="category"  id="category" class="form-control border-radius" onchange="document.search.submit()" >
												<option value="please Select" selected="selected" >please Select</option>
												
      				{section name=data loop=$useredit1}
                        
                    
												<option value="{$useredit1[data].id}" {if $filter_category==$useredit1[data].id} selected="selected"{/if} >{$useredit1[data].category} </option>
												
                  {/section}
                   
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="widget">
							<div class="items">
								<div class="item">
									<h6><span class="plase2">Load Type</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-4"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-4">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="sub_cat_id" class="form-control border-radius" id="sub_cat_id" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        {section name=data1 loop=$useredit11}
       
												<option value="{$useredit11[data1].id}" >{$useredit11[data1].category} </option>
												
   {/section}

<!--{if $filter_category_sub==$useredit11[data1].id} selected="selected"{/if}
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>
                        
                        
                        
                        							</div>
                                
  <!-------------------------------------------------------Filter Sub Next and last------------------------------------------><div class="items">
								<div class="item">
									<h6><span class="plase2">Transport Type</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-5"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-5">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="sub_next_cat_id" class="form-control border-radius" id="sub_next_cat_id" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        {section name=data15 loop=$useredit15}
       
												<option value="{$useredit15[data15].id}" >{$useredit15[data15].category} </option>
												
   {/section}

<!--{if $filter_sub_cat_next_id==$useredit15[data15].id} selected="selected" {/if}
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Sub Next and last------------------------------------------>                               
         
  <!-------------------------------------------------------Filter Last Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Body &amp; Bulk Type</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-6"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-6">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="last_cat" class="form-control border-radius" id="last_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        {section name=data21 loop=$last_cat}
       
												<option value="{$last_cat[data21].id}"  >{$last_cat[data21].category}  </option>
												
   {/section}

<!--{if $filter_last_cat==$last_cat[data21].id} selected="selected"{/if}
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Lact Cat------------------------------------------>                               
 
 
 
 
         
  <!-------------------------------------------------------Filter Fifth Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Size Classification</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-7"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-7">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="fifth_cat" class="form-control border-radius" id="fifth_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        {section name=data5 loop=$fifth_cat}
       
												<option value="{$fifth_cat[data5].id}"  >{$fifth_cat[data5].category}  </option>
												
   {/section}

<!--{if $filter_last_cat==$last_cat[data21].id} selected="selected"{/if}
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Fifth Cat------------------------------------------>                               
                                                               
                                



         
  <!-------------------------------------------------------Filter Sixth Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Payload KG</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-8"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-8">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="sixth_cat" class="form-control border-radius" id="sixth_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        {section name=data6 loop=$sixth_cat}
       
												<option value="{$sixth_cat[data6].id}"  >{$sixth_cat[data6].category}  </option>
												
   {/section}

<!--{if $filter_last_cat==$last_cat[data21].id} selected="selected"{/if}
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Sixth Cat------------------------------------------>



         
  <!-------------------------------------------------------Filter Seventh Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Length Meters</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-9"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-9">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="seventh_cat" class="form-control border-radius" id="seventh_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        {section name=data7 loop=$seventh_cat}
       
												<option value="{$seventh_cat[data7].id}"  >{$seventh_cat[data7].category}  </option>
												
   {/section}

<!--{if $filter_last_cat==$last_cat[data21].id} selected="selected"{/if}
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Seventh Cat------------------------------------------>

  
  <!-------------------------------------------------------Filter Eight Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Width Meters</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-10"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-10">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="eigth_cat" class="form-control border-radius" id="eight_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        {section name=data8 loop=$eight_cat}
       
												<option value="{$eight_cat[data8].id}"  >{$eight_cat[data8].category}  </option>
												
   {/section}

<!--{if $filter_last_cat==$last_cat[data21].id} selected="selected"{/if}
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Eight Cat------------------------------------------>


 
  <!-------------------------------------------------------Filter Ninth Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Preferred Distance</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-11"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-11">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="ninth_cat" class="form-control border-radius" id="ninth_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        {section name=data9 loop=$ninth_cat}
       
												<option value="{$ninth_cat[data9].id}"  >{$ninth_cat[data9].category}  </option>
												
   {/section}

<!--{if $filter_last_cat==$last_cat[data21].id} selected="selected"{/if}
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Ninth Cat------------------------------------------>

                                
   <!-------------------------------------------------------Filter Tenth Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Other</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-12"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-12">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="tenth_cat" class="form-control border-radius" id="tenth_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        {section name=data10 loop=$tenth_cat}
       
												<option value="{$tenth_cat[data10].id}"  >{$tenth_cat[data10].category}  </option>
												
   {/section}

<!--{if $filter_last_cat==$last_cat[data21].id} selected="selected"{/if}
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Tenth Cat------------------------------------------>

                                                               
                                
                                
                                   
                                
                                
     
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-12 text-left control-label">Pickup Date</label>
							<input type="date" name="pickup_date" id="pickup_date" class="form-control" value="{$filter_pickup_date}" onchange="document.search.submit()"/>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-12 text-left">Deliver Date</label>
							<input type="date"  name="deliver_date" id="deliver_date" class="form-control" value="{$filter_deliver_date}" onchange="document.search.submit()"/>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="col-md-9">
			<div class="border-left clearfix">
				<div class="widget-title top-lab"><!--<a class="a-new" href="#/">Map View  | </a> <span class="fs-search-maparea-enable">
					<input type="checkbox" style="position: relative; top: 1px;" id="chkUseMapAsFilter">
					Use map to filter results</span>-->
					Find All Deliveries
				</div>
				<!--<div class="widget-title top-lab clearfix">
					<div class="pull-left"><span data-selenium="commonResultsTableHeader" class="fs-pagination-total">Results: 1 - 50 of 810</span> <span><a class="a-new" href="#/">Next ></a></span></div>
					<select title="Select a category for search" class="pull-right cityes" >
						<option selected="selected" value="0">Time: Recently Updated</option>
						<option value="">Antiques</option>
						<option value="">Art</option>
						<option value="">Baby</option>
					</select>
				</div>-->
				<div class="row1">
					<form name="view_detail" method="post" action="{$site_url}/detail_shiping.html" id="view_detail">
						<table class="table">
							<thead>
								<tr>
									<th data-toggle="true">Shipment</th>
									<th >Quote</th>
									<th data-hide="phone,tablet">Collection</th>
									<th data-hide="phone,tablet">Delivery</th>
									<th data-hide="phone,tablet">Pickup Date</th>
									<th data-hide="phone,tablet">Delivery Date</th>
								</tr>
							</thead>
							<tbody>
							
							{if $search}
							{section name=data loop=$search}
							<tr {if $smarty.section.data.index is even}tr_even} style="background-color:#F1F1F2;" {/if}>
								<td><span class="more hidden-lg  hidden-md hidden-sm">more</span> <a style="cursor:pointer; color:#337ab7;"  onclick="submitbuy('{$search[data].id}',document.view_detail);">{$search[data].title}</a>
									<figure class="user-photo">
										<div class="pull-left"> <a style="cursor:pointer; color:#00F;" onclick="submitbuy('{$search[data].id}',document.view_detail);"/> {if $search[data].img} <img style="width:50px"; height="50px;" src="{$site_url}/images/shipment_image/{$search[data].img}" alt=""> {else}
											{$functions->get_cat_image($search[data].category)} <img style="width:50px"; height="50px;" src="{$site_url}/images/{$currency_info[0].cat_image}" alt=""> {/if} </a> </div>
										<div class="tab-box">
											<p>{$functions->get_category($search[data].category)} </p>
											<div class="shoshal"><div class="addthis_inline_share_toolbox"   ></div></div>
										</div>
									</figure></td>
								<td><span style=" color:#337ab7;"  class="quli">quote( {$functions->get_total_quote($search[data].order_id)} active)</span>
									<!--<p>Low: 4,001</p>-->
									<a style=" cursor:pointer;color:#5C9659;" onclick="submitbuy('{$search[data].id}',document.view_detail);"/> Quote</a></td>
								<td><p>{$search[data].form_city}</p>
									<p>{$search[data].form_state}</p></td>
								<td><p>{$search[data].to_city}</p>
									<p>{$search[data].to_state}</p></td>
								<td>{$search[data].pickup_start_date|date_format:"%d- %m -%Y"}</td>
								<td> {$search[data].delv_start_date|date_format:"%d- %m -%Y"} </td>
							</tr>
							{/section}
							{else}
							<tr>
								<td colspan="9" align="center">Record Not Found</td>
							</tr>
							{/if}
							</tbody>
							
						</table>
					</form>
					<form action="" method="post" >
						<input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />
						{$show_pagi}
						</td>
						</tr>
					</form>
				</div>
				<div class="pluse"><a href="#/"><img src="images/add-white-icon.png" alt=""></a></div>
			</div>
		</div>
	</div>
</div>
</section>
{literal} 
<script type="text/javascript">
        $(function () {
			$('.table').footable();
        });
    </script> 
<script type="text/javascript">
    $('#jq-dropdown-id-1').on('show', function(event, dropdownData) {
    console.log(dropdownData);
}).on('hide', function(event, dropdownData) {
    console.log(dropdownData);
});
</script> 
{/literal}