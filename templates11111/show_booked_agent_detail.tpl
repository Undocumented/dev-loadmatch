  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Job Detail </h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Job Detail </a></li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
 <div class="white-bg">

        <!-- career -->
    <div class="container career-inner">
        <div class="row">
            <div class="col-md-12 career-head">
               

            </div>
        </div>
        <hr>
       
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="candidate wow fadeInLeft">
                    <h1>Agent Detail</h1>
                    <p class="align-left"></p>
                    <h4></h4>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-angle-right pr-10"></i>Agent Name : {$functions->get_name($search[0].user_id)}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Agent State : {$search[0].form_state} </li>
                        <li><i class="fa fa-angle-right pr-10"></i>Agent City : {$search[0].form_city}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>location : {$search[0].sender_loc}</li>
                       
                    </ul>
                   
                </div>
                <hr>
                <div class="candidate wow fadeInLeft">
                    <h1>Status</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                         <li><i class="fa fa-angle-right pr-10"></i>Start Date : {$search[0].pickup_start_date|date_format:"%d- %m -%Y"}</li>
                       
                       
                    </ul>
                    
                </div>
                <hr>
            </div>
            <div class="col-md-6">
                <div class="candidate wow fadeInRight">
                    <h1>Route Detail</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                         <li><i class="fa fa-angle-right pr-10"></i>Form : {$search[0].reciver_name}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>To : {$search[0].to_state} </li>
                      
                    </ul>
                  
                </div>
                <hr>
              
        </div>
        
    <!-- career -->
       </div>
    </div>
  