<?php /* Smarty version 2.6.19, created on 2017-04-25 13:26:50
         compiled from admin/show_trans_review.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'admin/show_trans_review.tpl', 63, false),)), $this); ?>
<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Transpoter Review List</h2>
                     <?php if ($this->_tpl_vars['show_message'] != ''): ?>
                       <h5 class=" btn-success btn-lg" align="center"><?php echo $this->_tpl_vars['show_message']; ?>
</h5>  
                       <?php endif; ?> 
                      
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <b> Transporter Review List </b>
                          <div  style="text-align:right;">    
                 
                        </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="7%">Sr. No.</th>
                                            <th>Transpoter name</th>
                                            <th>Customer name</th>
                                            <th>Communication Rating</th>
                                             <th>service rating</th> 
                                            <th>punctuality Rating</th>
                                             <th>care of goods Rating</th>
                                            <th>Over All rating</th>
                                            <th>Date</th>
                                             <th>Comment</th>
                                            
                                             <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($this->_tpl_vars['userdata']): ?>
                                        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['userdata']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                                        <tr class="gradeU">
                                            <td align="center"><?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>
</td>
                                            <td><?php echo $this->_tpl_vars['functions']->get_Transpoter_detail($this->_tpl_vars['userdata'][$this->_sections['data']['index']]['transpoter_id']); ?>

                                             <?php echo $this->_tpl_vars['Transpoter_info'][0]['name']; ?>

                                             </td>
                                            <td><?php echo $this->_tpl_vars['functions']->get_customer_detail($this->_tpl_vars['userdata'][$this->_sections['data']['index']]['user_id']); ?>

                                             <?php echo $this->_tpl_vars['customer_info'][0]['name']; ?>

                                            </td>
                                             
                                             <td><?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['com_rating']; ?>
</td>
                                              <td><?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['ser_rating']; ?>
</td>
                                               <td><?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['pun_rating']; ?>
</td>
                                               <td><?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['care_rating']; ?>
</td>
                                               <td><?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['rating']; ?>
</td>
                                                                                              
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['userdata'][$this->_sections['data']['index']]['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                                <td><?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['review']; ?>
</td>

                                            
                                               <td>
                                               	<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=edit_review&edit_id=<?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['user_id']; ?>
&type=<?php echo $this->_tpl_vars['Transpoter_info'][0]['type']; ?>
" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                                           <?php if ($this->_tpl_vars['userdata'][$this->_sections['data']['index']]['status'] != 'Y'): ?>
                                               	<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=show_trans_review&active_id=<?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['id']; ?>
" onclick="return confirm('Do You Want to Change it to Inactive?');" title="active"><span class="btn btn-success btn-sm" style="background-color:#F90;">Active&nbsp;</span></a>
                                                <?php else: ?>
                                                	<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=show_trans_review&inactive_id=<?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['id']; ?>
" onclick="return confirm('Do You Want to Change it to Active?');" title="inactive"><span class="btn btn-success btn-sm" style="background-color:#069;">Inactive</span></a>
                                                    <?php endif; ?>
                                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=show_trans_review&del_id=<?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['id']; ?>
" onclick="return confirm('Do You  want to delete ?');" title="Delete"><span class="btn btn-success btn-sm" style="background-color:#F00;">Delete</span></a>
                                               </td>
                                        </tr>
                                        <?php endfor; endif; ?>
                                        
                                         <tr>
            <td colspan="8"><form name="frm_pagi" action="#" method="post">
                <input type="hidden"  name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" />
                <?php echo $this->_tpl_vars['show_pagi']; ?>

              </form></td>
          </tr>
          <?php else: ?>
          <tr>
            <td colspan="8"><span style="color:red;">Record Not Found</span></td>
          </tr>
          <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>
