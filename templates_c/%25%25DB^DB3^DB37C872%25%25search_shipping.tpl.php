<?php /* Smarty version 2.6.19, created on 2017-08-10 11:06:47
         compiled from search_shipping.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'search_shipping.tpl', 497, false),)), $this); ?>
<!--End Header-->
<section id="main">
<div class="main-inner">
<div class="container">
	<div class="clearfix">
		<form name="search" id="search" method="post" action="#">
			<div class="col-md-3">
				<div class="row1">
					<div class="sidebar">
						<h4 class="widget-title  label label-primary top-lab" >Refine Search</h4>
						<div class="widget">
							<div class="items">
								<div class="item">
									<h6><span class="plase2">Deliver To</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-1"><img src="images/Add-icon.png"></a> </div>
										</span> <span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
								<div class="collapse" id="more-items-1">
									<div class="item">
										<div class="check-box-item">
											<select name="to"  id="to" class="form-control border-radius" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
                    
												<option value="<?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['filter_to'] == $this->_tpl_vars['categories'][$this->_sections['data']['index']]['city']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['city']; ?>
 </option>
												
                  <?php endfor; endif; ?>
                   
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="widget">
							<div class="items">
								<div class="item">
									<h6><span class="plase2">Pickup From</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-2"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
								<div class="collapse" id="more-items-2">
									<div class="item">
										<div class="check-box-item">
											<select name="from"  id="form" class="form-control border-radius" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
                    
												<option value="<?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['filter_from'] == $this->_tpl_vars['categories'][$this->_sections['data']['index']]['city']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['city']; ?>
 </option>
												
                  <?php endfor; endif; ?>
                   
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="widget">
							<div class="items">
								<div class="item">
									<h6><span class="plase2">Load Industry</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-3"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
								<div class="collapse" id="more-items-3">
									<div class="item">
										<div class="check-box-item">
											<select name="category"  id="category" class="form-control border-radius" onchange="document.search.submit()" >
												<option value="please Select" selected="selected" >please Select</option>
												
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
                    
												<option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['filter_category'] == $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']): ?> selected="selected"<?php endif; ?> ><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
												
                  <?php endfor; endif; ?>
                   
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="widget">
							<div class="items">
								<div class="item">
									<h6><span class="plase2">Load Type</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-4"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-4">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="sub_cat_id" class="form-control border-radius" id="sub_cat_id" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        <?php unset($this->_sections['data1']);
$this->_sections['data1']['name'] = 'data1';
$this->_sections['data1']['loop'] = is_array($_loop=$this->_tpl_vars['useredit11']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data1']['show'] = true;
$this->_sections['data1']['max'] = $this->_sections['data1']['loop'];
$this->_sections['data1']['step'] = 1;
$this->_sections['data1']['start'] = $this->_sections['data1']['step'] > 0 ? 0 : $this->_sections['data1']['loop']-1;
if ($this->_sections['data1']['show']) {
    $this->_sections['data1']['total'] = $this->_sections['data1']['loop'];
    if ($this->_sections['data1']['total'] == 0)
        $this->_sections['data1']['show'] = false;
} else
    $this->_sections['data1']['total'] = 0;
if ($this->_sections['data1']['show']):

            for ($this->_sections['data1']['index'] = $this->_sections['data1']['start'], $this->_sections['data1']['iteration'] = 1;
                 $this->_sections['data1']['iteration'] <= $this->_sections['data1']['total'];
                 $this->_sections['data1']['index'] += $this->_sections['data1']['step'], $this->_sections['data1']['iteration']++):
$this->_sections['data1']['rownum'] = $this->_sections['data1']['iteration'];
$this->_sections['data1']['index_prev'] = $this->_sections['data1']['index'] - $this->_sections['data1']['step'];
$this->_sections['data1']['index_next'] = $this->_sections['data1']['index'] + $this->_sections['data1']['step'];
$this->_sections['data1']['first']      = ($this->_sections['data1']['iteration'] == 1);
$this->_sections['data1']['last']       = ($this->_sections['data1']['iteration'] == $this->_sections['data1']['total']);
?>
       
												<option value="<?php echo $this->_tpl_vars['useredit11'][$this->_sections['data1']['index']]['id']; ?>
" ><?php echo $this->_tpl_vars['useredit11'][$this->_sections['data1']['index']]['category']; ?>
 </option>
												
   <?php endfor; endif; ?>

<!--<?php if ($this->_tpl_vars['filter_category_sub'] == $this->_tpl_vars['useredit11'][$this->_sections['data1']['index']]['id']): ?> selected="selected"<?php endif; ?>
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>
                        
                        
                        
                        							</div>
                                
  <!-------------------------------------------------------Filter Sub Next and last------------------------------------------><div class="items">
								<div class="item">
									<h6><span class="plase2">Transport Type</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-5"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-5">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="sub_next_cat_id" class="form-control border-radius" id="sub_next_cat_id" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        <?php unset($this->_sections['data15']);
$this->_sections['data15']['name'] = 'data15';
$this->_sections['data15']['loop'] = is_array($_loop=$this->_tpl_vars['useredit15']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data15']['show'] = true;
$this->_sections['data15']['max'] = $this->_sections['data15']['loop'];
$this->_sections['data15']['step'] = 1;
$this->_sections['data15']['start'] = $this->_sections['data15']['step'] > 0 ? 0 : $this->_sections['data15']['loop']-1;
if ($this->_sections['data15']['show']) {
    $this->_sections['data15']['total'] = $this->_sections['data15']['loop'];
    if ($this->_sections['data15']['total'] == 0)
        $this->_sections['data15']['show'] = false;
} else
    $this->_sections['data15']['total'] = 0;
if ($this->_sections['data15']['show']):

            for ($this->_sections['data15']['index'] = $this->_sections['data15']['start'], $this->_sections['data15']['iteration'] = 1;
                 $this->_sections['data15']['iteration'] <= $this->_sections['data15']['total'];
                 $this->_sections['data15']['index'] += $this->_sections['data15']['step'], $this->_sections['data15']['iteration']++):
$this->_sections['data15']['rownum'] = $this->_sections['data15']['iteration'];
$this->_sections['data15']['index_prev'] = $this->_sections['data15']['index'] - $this->_sections['data15']['step'];
$this->_sections['data15']['index_next'] = $this->_sections['data15']['index'] + $this->_sections['data15']['step'];
$this->_sections['data15']['first']      = ($this->_sections['data15']['iteration'] == 1);
$this->_sections['data15']['last']       = ($this->_sections['data15']['iteration'] == $this->_sections['data15']['total']);
?>
       
												<option value="<?php echo $this->_tpl_vars['useredit15'][$this->_sections['data15']['index']]['id']; ?>
" ><?php echo $this->_tpl_vars['useredit15'][$this->_sections['data15']['index']]['category']; ?>
 </option>
												
   <?php endfor; endif; ?>

<!--<?php if ($this->_tpl_vars['filter_sub_cat_next_id'] == $this->_tpl_vars['useredit15'][$this->_sections['data15']['index']]['id']): ?> selected="selected" <?php endif; ?>
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Sub Next and last------------------------------------------>                               
         
  <!-------------------------------------------------------Filter Last Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Body &amp; Bulk Type</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-6"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-6">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="last_cat" class="form-control border-radius" id="last_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        <?php unset($this->_sections['data21']);
$this->_sections['data21']['name'] = 'data21';
$this->_sections['data21']['loop'] = is_array($_loop=$this->_tpl_vars['last_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data21']['show'] = true;
$this->_sections['data21']['max'] = $this->_sections['data21']['loop'];
$this->_sections['data21']['step'] = 1;
$this->_sections['data21']['start'] = $this->_sections['data21']['step'] > 0 ? 0 : $this->_sections['data21']['loop']-1;
if ($this->_sections['data21']['show']) {
    $this->_sections['data21']['total'] = $this->_sections['data21']['loop'];
    if ($this->_sections['data21']['total'] == 0)
        $this->_sections['data21']['show'] = false;
} else
    $this->_sections['data21']['total'] = 0;
if ($this->_sections['data21']['show']):

            for ($this->_sections['data21']['index'] = $this->_sections['data21']['start'], $this->_sections['data21']['iteration'] = 1;
                 $this->_sections['data21']['iteration'] <= $this->_sections['data21']['total'];
                 $this->_sections['data21']['index'] += $this->_sections['data21']['step'], $this->_sections['data21']['iteration']++):
$this->_sections['data21']['rownum'] = $this->_sections['data21']['iteration'];
$this->_sections['data21']['index_prev'] = $this->_sections['data21']['index'] - $this->_sections['data21']['step'];
$this->_sections['data21']['index_next'] = $this->_sections['data21']['index'] + $this->_sections['data21']['step'];
$this->_sections['data21']['first']      = ($this->_sections['data21']['iteration'] == 1);
$this->_sections['data21']['last']       = ($this->_sections['data21']['iteration'] == $this->_sections['data21']['total']);
?>
       
												<option value="<?php echo $this->_tpl_vars['last_cat'][$this->_sections['data21']['index']]['id']; ?>
"  ><?php echo $this->_tpl_vars['last_cat'][$this->_sections['data21']['index']]['category']; ?>
  </option>
												
   <?php endfor; endif; ?>

<!--<?php if ($this->_tpl_vars['filter_last_cat'] == $this->_tpl_vars['last_cat'][$this->_sections['data21']['index']]['id']): ?> selected="selected"<?php endif; ?>
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Lact Cat------------------------------------------>                               
 
 
 
 
         
  <!-------------------------------------------------------Filter Fifth Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Size Classification</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-7"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-7">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="fifth_cat" class="form-control border-radius" id="fifth_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        <?php unset($this->_sections['data5']);
$this->_sections['data5']['name'] = 'data5';
$this->_sections['data5']['loop'] = is_array($_loop=$this->_tpl_vars['fifth_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data5']['show'] = true;
$this->_sections['data5']['max'] = $this->_sections['data5']['loop'];
$this->_sections['data5']['step'] = 1;
$this->_sections['data5']['start'] = $this->_sections['data5']['step'] > 0 ? 0 : $this->_sections['data5']['loop']-1;
if ($this->_sections['data5']['show']) {
    $this->_sections['data5']['total'] = $this->_sections['data5']['loop'];
    if ($this->_sections['data5']['total'] == 0)
        $this->_sections['data5']['show'] = false;
} else
    $this->_sections['data5']['total'] = 0;
if ($this->_sections['data5']['show']):

            for ($this->_sections['data5']['index'] = $this->_sections['data5']['start'], $this->_sections['data5']['iteration'] = 1;
                 $this->_sections['data5']['iteration'] <= $this->_sections['data5']['total'];
                 $this->_sections['data5']['index'] += $this->_sections['data5']['step'], $this->_sections['data5']['iteration']++):
$this->_sections['data5']['rownum'] = $this->_sections['data5']['iteration'];
$this->_sections['data5']['index_prev'] = $this->_sections['data5']['index'] - $this->_sections['data5']['step'];
$this->_sections['data5']['index_next'] = $this->_sections['data5']['index'] + $this->_sections['data5']['step'];
$this->_sections['data5']['first']      = ($this->_sections['data5']['iteration'] == 1);
$this->_sections['data5']['last']       = ($this->_sections['data5']['iteration'] == $this->_sections['data5']['total']);
?>
       
												<option value="<?php echo $this->_tpl_vars['fifth_cat'][$this->_sections['data5']['index']]['id']; ?>
"  ><?php echo $this->_tpl_vars['fifth_cat'][$this->_sections['data5']['index']]['category']; ?>
  </option>
												
   <?php endfor; endif; ?>

<!--<?php if ($this->_tpl_vars['filter_last_cat'] == $this->_tpl_vars['last_cat'][$this->_sections['data21']['index']]['id']): ?> selected="selected"<?php endif; ?>
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Fifth Cat------------------------------------------>                               
                                                               
                                



         
  <!-------------------------------------------------------Filter Sixth Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Payload KG</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-8"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-8">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="sixth_cat" class="form-control border-radius" id="sixth_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        <?php unset($this->_sections['data6']);
$this->_sections['data6']['name'] = 'data6';
$this->_sections['data6']['loop'] = is_array($_loop=$this->_tpl_vars['sixth_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data6']['show'] = true;
$this->_sections['data6']['max'] = $this->_sections['data6']['loop'];
$this->_sections['data6']['step'] = 1;
$this->_sections['data6']['start'] = $this->_sections['data6']['step'] > 0 ? 0 : $this->_sections['data6']['loop']-1;
if ($this->_sections['data6']['show']) {
    $this->_sections['data6']['total'] = $this->_sections['data6']['loop'];
    if ($this->_sections['data6']['total'] == 0)
        $this->_sections['data6']['show'] = false;
} else
    $this->_sections['data6']['total'] = 0;
if ($this->_sections['data6']['show']):

            for ($this->_sections['data6']['index'] = $this->_sections['data6']['start'], $this->_sections['data6']['iteration'] = 1;
                 $this->_sections['data6']['iteration'] <= $this->_sections['data6']['total'];
                 $this->_sections['data6']['index'] += $this->_sections['data6']['step'], $this->_sections['data6']['iteration']++):
$this->_sections['data6']['rownum'] = $this->_sections['data6']['iteration'];
$this->_sections['data6']['index_prev'] = $this->_sections['data6']['index'] - $this->_sections['data6']['step'];
$this->_sections['data6']['index_next'] = $this->_sections['data6']['index'] + $this->_sections['data6']['step'];
$this->_sections['data6']['first']      = ($this->_sections['data6']['iteration'] == 1);
$this->_sections['data6']['last']       = ($this->_sections['data6']['iteration'] == $this->_sections['data6']['total']);
?>
       
												<option value="<?php echo $this->_tpl_vars['sixth_cat'][$this->_sections['data6']['index']]['id']; ?>
"  ><?php echo $this->_tpl_vars['sixth_cat'][$this->_sections['data6']['index']]['category']; ?>
  </option>
												
   <?php endfor; endif; ?>

<!--<?php if ($this->_tpl_vars['filter_last_cat'] == $this->_tpl_vars['last_cat'][$this->_sections['data21']['index']]['id']): ?> selected="selected"<?php endif; ?>
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Sixth Cat------------------------------------------>



         
  <!-------------------------------------------------------Filter Seventh Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Length Meters</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-9"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-9">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="seventh_cat" class="form-control border-radius" id="seventh_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        <?php unset($this->_sections['data7']);
$this->_sections['data7']['name'] = 'data7';
$this->_sections['data7']['loop'] = is_array($_loop=$this->_tpl_vars['seventh_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data7']['show'] = true;
$this->_sections['data7']['max'] = $this->_sections['data7']['loop'];
$this->_sections['data7']['step'] = 1;
$this->_sections['data7']['start'] = $this->_sections['data7']['step'] > 0 ? 0 : $this->_sections['data7']['loop']-1;
if ($this->_sections['data7']['show']) {
    $this->_sections['data7']['total'] = $this->_sections['data7']['loop'];
    if ($this->_sections['data7']['total'] == 0)
        $this->_sections['data7']['show'] = false;
} else
    $this->_sections['data7']['total'] = 0;
if ($this->_sections['data7']['show']):

            for ($this->_sections['data7']['index'] = $this->_sections['data7']['start'], $this->_sections['data7']['iteration'] = 1;
                 $this->_sections['data7']['iteration'] <= $this->_sections['data7']['total'];
                 $this->_sections['data7']['index'] += $this->_sections['data7']['step'], $this->_sections['data7']['iteration']++):
$this->_sections['data7']['rownum'] = $this->_sections['data7']['iteration'];
$this->_sections['data7']['index_prev'] = $this->_sections['data7']['index'] - $this->_sections['data7']['step'];
$this->_sections['data7']['index_next'] = $this->_sections['data7']['index'] + $this->_sections['data7']['step'];
$this->_sections['data7']['first']      = ($this->_sections['data7']['iteration'] == 1);
$this->_sections['data7']['last']       = ($this->_sections['data7']['iteration'] == $this->_sections['data7']['total']);
?>
       
												<option value="<?php echo $this->_tpl_vars['seventh_cat'][$this->_sections['data7']['index']]['id']; ?>
"  ><?php echo $this->_tpl_vars['seventh_cat'][$this->_sections['data7']['index']]['category']; ?>
  </option>
												
   <?php endfor; endif; ?>

<!--<?php if ($this->_tpl_vars['filter_last_cat'] == $this->_tpl_vars['last_cat'][$this->_sections['data21']['index']]['id']): ?> selected="selected"<?php endif; ?>
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Seventh Cat------------------------------------------>

  
  <!-------------------------------------------------------Filter Eight Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Width Meters</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-10"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-10">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="eigth_cat" class="form-control border-radius" id="eight_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        <?php unset($this->_sections['data8']);
$this->_sections['data8']['name'] = 'data8';
$this->_sections['data8']['loop'] = is_array($_loop=$this->_tpl_vars['eight_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data8']['show'] = true;
$this->_sections['data8']['max'] = $this->_sections['data8']['loop'];
$this->_sections['data8']['step'] = 1;
$this->_sections['data8']['start'] = $this->_sections['data8']['step'] > 0 ? 0 : $this->_sections['data8']['loop']-1;
if ($this->_sections['data8']['show']) {
    $this->_sections['data8']['total'] = $this->_sections['data8']['loop'];
    if ($this->_sections['data8']['total'] == 0)
        $this->_sections['data8']['show'] = false;
} else
    $this->_sections['data8']['total'] = 0;
if ($this->_sections['data8']['show']):

            for ($this->_sections['data8']['index'] = $this->_sections['data8']['start'], $this->_sections['data8']['iteration'] = 1;
                 $this->_sections['data8']['iteration'] <= $this->_sections['data8']['total'];
                 $this->_sections['data8']['index'] += $this->_sections['data8']['step'], $this->_sections['data8']['iteration']++):
$this->_sections['data8']['rownum'] = $this->_sections['data8']['iteration'];
$this->_sections['data8']['index_prev'] = $this->_sections['data8']['index'] - $this->_sections['data8']['step'];
$this->_sections['data8']['index_next'] = $this->_sections['data8']['index'] + $this->_sections['data8']['step'];
$this->_sections['data8']['first']      = ($this->_sections['data8']['iteration'] == 1);
$this->_sections['data8']['last']       = ($this->_sections['data8']['iteration'] == $this->_sections['data8']['total']);
?>
       
												<option value="<?php echo $this->_tpl_vars['eight_cat'][$this->_sections['data8']['index']]['id']; ?>
"  ><?php echo $this->_tpl_vars['eight_cat'][$this->_sections['data8']['index']]['category']; ?>
  </option>
												
   <?php endfor; endif; ?>

<!--<?php if ($this->_tpl_vars['filter_last_cat'] == $this->_tpl_vars['last_cat'][$this->_sections['data21']['index']]['id']): ?> selected="selected"<?php endif; ?>
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Eight Cat------------------------------------------>


 
  <!-------------------------------------------------------Filter Ninth Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Preferred Distance</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-11"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-11">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="ninth_cat" class="form-control border-radius" id="ninth_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        <?php unset($this->_sections['data9']);
$this->_sections['data9']['name'] = 'data9';
$this->_sections['data9']['loop'] = is_array($_loop=$this->_tpl_vars['ninth_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data9']['show'] = true;
$this->_sections['data9']['max'] = $this->_sections['data9']['loop'];
$this->_sections['data9']['step'] = 1;
$this->_sections['data9']['start'] = $this->_sections['data9']['step'] > 0 ? 0 : $this->_sections['data9']['loop']-1;
if ($this->_sections['data9']['show']) {
    $this->_sections['data9']['total'] = $this->_sections['data9']['loop'];
    if ($this->_sections['data9']['total'] == 0)
        $this->_sections['data9']['show'] = false;
} else
    $this->_sections['data9']['total'] = 0;
if ($this->_sections['data9']['show']):

            for ($this->_sections['data9']['index'] = $this->_sections['data9']['start'], $this->_sections['data9']['iteration'] = 1;
                 $this->_sections['data9']['iteration'] <= $this->_sections['data9']['total'];
                 $this->_sections['data9']['index'] += $this->_sections['data9']['step'], $this->_sections['data9']['iteration']++):
$this->_sections['data9']['rownum'] = $this->_sections['data9']['iteration'];
$this->_sections['data9']['index_prev'] = $this->_sections['data9']['index'] - $this->_sections['data9']['step'];
$this->_sections['data9']['index_next'] = $this->_sections['data9']['index'] + $this->_sections['data9']['step'];
$this->_sections['data9']['first']      = ($this->_sections['data9']['iteration'] == 1);
$this->_sections['data9']['last']       = ($this->_sections['data9']['iteration'] == $this->_sections['data9']['total']);
?>
       
												<option value="<?php echo $this->_tpl_vars['ninth_cat'][$this->_sections['data9']['index']]['id']; ?>
"  ><?php echo $this->_tpl_vars['ninth_cat'][$this->_sections['data9']['index']]['category']; ?>
  </option>
												
   <?php endfor; endif; ?>

<!--<?php if ($this->_tpl_vars['filter_last_cat'] == $this->_tpl_vars['last_cat'][$this->_sections['data21']['index']]['id']): ?> selected="selected"<?php endif; ?>
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Ninth Cat------------------------------------------>

                                
   <!-------------------------------------------------------Filter Tenth Cat------------------------------------------>
  <div class="items">
								<div class="item">
									<h6><span class="plase2">Other</span> <span class="plase">
										<div class="more dropdown-toggle pull-left"> <a data-toggle="collapse"  href="#more-items-12"><img src="images/Add-icon.png"></a> </div>
										</span><span class="pull-right font_sm"><a href="#/">Clear</a></span></h6>
								</div>
                                
                                
                                
								<div class="collapse" id="more-items-12">
									<div class="item">
										<div class="check-box-item"> <span id="sub_cat">
											<select name="tenth_cat" class="form-control border-radius" id="tenth_cat" onchange="document.search.submit()">
												<option value="please Select" selected="selected" >please Select</option>
												
        <?php unset($this->_sections['data10']);
$this->_sections['data10']['name'] = 'data10';
$this->_sections['data10']['loop'] = is_array($_loop=$this->_tpl_vars['tenth_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data10']['show'] = true;
$this->_sections['data10']['max'] = $this->_sections['data10']['loop'];
$this->_sections['data10']['step'] = 1;
$this->_sections['data10']['start'] = $this->_sections['data10']['step'] > 0 ? 0 : $this->_sections['data10']['loop']-1;
if ($this->_sections['data10']['show']) {
    $this->_sections['data10']['total'] = $this->_sections['data10']['loop'];
    if ($this->_sections['data10']['total'] == 0)
        $this->_sections['data10']['show'] = false;
} else
    $this->_sections['data10']['total'] = 0;
if ($this->_sections['data10']['show']):

            for ($this->_sections['data10']['index'] = $this->_sections['data10']['start'], $this->_sections['data10']['iteration'] = 1;
                 $this->_sections['data10']['iteration'] <= $this->_sections['data10']['total'];
                 $this->_sections['data10']['index'] += $this->_sections['data10']['step'], $this->_sections['data10']['iteration']++):
$this->_sections['data10']['rownum'] = $this->_sections['data10']['iteration'];
$this->_sections['data10']['index_prev'] = $this->_sections['data10']['index'] - $this->_sections['data10']['step'];
$this->_sections['data10']['index_next'] = $this->_sections['data10']['index'] + $this->_sections['data10']['step'];
$this->_sections['data10']['first']      = ($this->_sections['data10']['iteration'] == 1);
$this->_sections['data10']['last']       = ($this->_sections['data10']['iteration'] == $this->_sections['data10']['total']);
?>
       
												<option value="<?php echo $this->_tpl_vars['tenth_cat'][$this->_sections['data10']['index']]['id']; ?>
"  ><?php echo $this->_tpl_vars['tenth_cat'][$this->_sections['data10']['index']]['category']; ?>
  </option>
												
   <?php endfor; endif; ?>

<!--<?php if ($this->_tpl_vars['filter_last_cat'] == $this->_tpl_vars['last_cat'][$this->_sections['data21']['index']]['id']): ?> selected="selected"<?php endif; ?>
-->											</select>
											</span> </div>
									</div>
	                           
                                
                                
							</div>
						</div>                  
                                
                                
                                                      
 <!----------------------------------------------------End Filter Tenth Cat------------------------------------------>

                                                               
                                
                                
                                   
                                
                                
     
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-12 text-left control-label">Pickup Date</label>
							<input type="date" name="pickup_date" id="pickup_date" class="form-control" value="<?php echo $this->_tpl_vars['filter_pickup_date']; ?>
" onchange="document.search.submit()"/>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-12 text-left">Deliver Date</label>
							<input type="date"  name="deliver_date" id="deliver_date" class="form-control" value="<?php echo $this->_tpl_vars['filter_deliver_date']; ?>
" onchange="document.search.submit()"/>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="col-md-9">
			<div class="border-left clearfix">
				<div class="widget-title top-lab"><!--<a class="a-new" href="#/">Map View  | </a> <span class="fs-search-maparea-enable">
					<input type="checkbox" style="position: relative; top: 1px;" id="chkUseMapAsFilter">
					Use map to filter results</span>-->
					Find All Deliveries
				</div>
				<!--<div class="widget-title top-lab clearfix">
					<div class="pull-left"><span data-selenium="commonResultsTableHeader" class="fs-pagination-total">Results: 1 - 50 of 810</span> <span><a class="a-new" href="#/">Next ></a></span></div>
					<select title="Select a category for search" class="pull-right cityes" >
						<option selected="selected" value="0">Time: Recently Updated</option>
						<option value="">Antiques</option>
						<option value="">Art</option>
						<option value="">Baby</option>
					</select>
				</div>-->
				<div class="row1">
					<form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/detail_shiping.html" id="view_detail">
						<table class="table">
							<thead>
								<tr>
									<th data-toggle="true">Shipment</th>
									<th >Quote</th>
									<th data-hide="phone,tablet">Collection</th>
									<th data-hide="phone,tablet">Delivery</th>
									<th data-hide="phone,tablet">Pickup Date</th>
									<th data-hide="phone,tablet">Delivery Date</th>
								</tr>
							</thead>
							<tbody>
							
							<?php if ($this->_tpl_vars['search']): ?>
							<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
							<tr <?php if (!(1 & $this->_sections['data']['index'])): ?>tr_even} style="background-color:#F1F1F2;" <?php endif; ?>>
								<td><span class="more hidden-lg  hidden-md hidden-sm">more</span> <a style="cursor:pointer; color:#337ab7;"  onclick="submitbuy('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
',document.view_detail);"><?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['title']; ?>
</a>
									<figure class="user-photo">
										<div class="pull-left"> <a style="cursor:pointer; color:#00F;" onclick="submitbuy('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
',document.view_detail);"/> <?php if ($this->_tpl_vars['search'][$this->_sections['data']['index']]['img']): ?> <img style="width:50px"; height="50px;" src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/shipment_image/<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['img']; ?>
" alt=""> <?php else: ?>
											<?php echo $this->_tpl_vars['functions']->get_cat_image($this->_tpl_vars['search'][$this->_sections['data']['index']]['category']); ?>
 <img style="width:50px"; height="50px;" src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/<?php echo $this->_tpl_vars['currency_info'][0]['cat_image']; ?>
" alt=""> <?php endif; ?> </a> </div>
										<div class="tab-box">
											<p><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][$this->_sections['data']['index']]['category']); ?>
 </p>
											<div class="shoshal"><div class="addthis_inline_share_toolbox"   ></div></div>
										</div>
									</figure></td>
								<td><span style=" color:#337ab7;"  class="quli">quote( <?php echo $this->_tpl_vars['functions']->get_total_quote($this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']); ?>
 active)</span>
									<!--<p>Low: 4,001</p>-->
									<a style=" cursor:pointer;color:#5C9659;" onclick="submitbuy('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
',document.view_detail);"/> Quote</a></td>
								<td><p><?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['form_city']; ?>
</p>
									<p><?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['form_state']; ?>
</p></td>
								<td><p><?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['to_city']; ?>
</p>
									<p><?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['to_state']; ?>
</p></td>
								<td><?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['pickup_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
								<td> <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['delv_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
 </td>
							</tr>
							<?php endfor; endif; ?>
							<?php else: ?>
							<tr>
								<td colspan="9" align="center">Record Not Found</td>
							</tr>
							<?php endif; ?>
							</tbody>
							
						</table>
					</form>
					<form action="" method="post" >
						<input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" />
						<?php echo $this->_tpl_vars['show_pagi']; ?>

						</td>
						</tr>
					</form>
				</div>
				<div class="pluse"><a href="#/"><img src="images/add-white-icon.png" alt=""></a></div>
			</div>
		</div>
	</div>
</div>
</section>
<?php echo ' 
<script type="text/javascript">
        $(function () {
			$(\'.table\').footable();
        });
    </script> 
<script type="text/javascript">
    $(\'#jq-dropdown-id-1\').on(\'show\', function(event, dropdownData) {
    console.log(dropdownData);
}).on(\'hide\', function(event, dropdownData) {
    console.log(dropdownData);
});
</script> 
'; ?>