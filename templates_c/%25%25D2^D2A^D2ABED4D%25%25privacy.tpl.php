<?php /* Smarty version 2.6.19, created on 2017-08-21 23:06:13
         compiled from privacy.tpl */ ?>
 <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-sm-4" style="margin-top:60px;">
            <h1>
               Privacy Policy 
            </h1>
          </div>
          <div class="col-lg-8 col-sm-8">
            <ol class="breadcrumb pull-right">
              <li>
                <a href="index.html">
                  Home
                </a>
              </li>
             <li class="active">
                 Privacy Policy
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="container" >
      <div class="row">
</div>


          <p>
           Thank you for visiting our web site. This privacy policy tells you how we use personal information collected at this site. Please read this privacy policy before using the site or submitting any personal information. By using the site, you are accepting the practices described in this privacy policy. These practices may be changed, but any changes will be posted and changes will only apply to activities and information on a going forward, not retroactive basis. You are encouraged to review the privacy policy whenever you visit the site to make sure that you understand how any personal information you provide will be used. 
           The privacy practices of this statement apply to our services available under the domain and subdomains of www.loadmatch.co.za (the "Site") owned and operated by LoadMatch (Pty) Ltd and viplogiX (Pty) Ltd ("LOADMATCH"). By visiting this website, you agree to be bound by the terms and conditions of this Privacy Policy. If you do not agree, please do not use or access our site.
</p>
<p>
By accepting the Privacy Policy and the User Agreement in registration, you expressly consent to our use and disclosure of your personal information in accordance with this Privacy Policy. This Privacy Policy is incorporated into and subject to the terms of the LoadMatch User Agreement.
</p>
<p>
Your privacy is very important to all of us at LoadMatch. We have established this privacy policy to explain to you how your personal information is protected, collected and used. Personal information is information about you that is personally identifiable such as your name, address, phone number, and email address that is not otherwise publicly available.
</p>
<p>
Note, the privacy practices set forth in this privacy policy are for this web site only. If you link to other web sites, please review the privacy policies posted at those sites.
</p>
<p>
<H4><B>Collection of Information</H4></B>
</p>
<p>
We collect personally identifiable information, like names, postal addresses, email addresses, etc., when voluntarily submitted by our visitors. The information you provide is used to fulfil your specific request. This information is only used to fulfil your specific request, unless you give us permission to use it in another manner, for example to add you to one of our mailing lists.
</p>
<p>
<H4><B>Cookie/Tracking Technology</H4></B>
<p>
    The Site may use cookie and tracking technology depending on the features offered. Cookie and tracking technology are useful for gathering information such as browser type and operating system, tracking the number of visitors to the Site, and understanding how visitors use the Site. Cookies can also help customize the Site for visitors. Personal information cannot be collected via cookies and other tracking technology; however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.
</p>
<p>
<H4><B>Distribution of Information</H4></B>
<p>
We may share information with governmental agencies or other companies assisting us in fraud prevention or investigation. We may do so when: (1) permitted or required by law; or, (2) trying to protect against or prevent actual or potential fraud or unauthorized transactions; or, (3) investigating fraud which has already taken place. The information is not provided to these companies for marketing purposes.
</p>
<p>
<H4><B>Commitment to Data Security</H4></B>
<p>
Your personally identifiable information is kept secure. Only authorised employees, agents and contractors (who have agreed to keep information secure and confidential) have access to this information. All emails and newsletters from this site allow you to opt out of further mailings.
</p>
<p>
<H4><B>Advertising</H4></B>
<p>
We may, from time to time, display banner ads or other forms of adverts on our site from third parties. The cookies used by these companies are used to track their origins, namely, our site, and may contain other information that is outside our control. If you believe that these adverts are in conflict with our privacy policy, please email us immediately at info@viplogix.com
Public Forums
</p>
<p>
This site may make chat rooms, forums, message boards, and/or news groups available to its users. Personal details provided on these are outside our control, and you may request that certain entries be edited or removed by emailing info@viplogix.com if you are unable to do so yourself. Ultimately, the responsibility on disclosure of personal details on any public forum is that of the user.
          </p>
         

        <!-- End row -->

      </div>
      <!-- End container -->
    </div>


    <!--container end-->