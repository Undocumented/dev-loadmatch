<?php /* Smarty version 2.6.19, created on 2017-08-10 11:06:53
         compiled from detail_shiping.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'detail_shiping.tpl', 161, false),array('modifier', 'truncate', 'detail_shiping.tpl', 208, false),)), $this); ?>

<div id="wrapper">
  
    <div class="main-2 clearfix">
    <div class="container">
    
    	<div class="back-img">
        <h2>Using uShip to Book Loads is Easy!</h2>
        
        <div class="row">
        
        	<div class="col-md-3">
            <div class="opo">
            	<h5>Step 1:</h5>
                <p>Join for FREE & find listings like the one below to bid on.</p>
                </div>
            </div>
<div class="col-md-3">
            <div class="opo">
            	<h5>Step 2:</h5>
                <p>Ask questions, work out details, and make bids on the listings.</p>
                </div>
            </div>
<div class="col-md-3">
            <div class="opo">
            	<h5>Step 3:</h5>
                <p>Pick up the shipment and make the delivery in order to complete the transaction and get paid.</p>
                </div>
            </div>
<div class="col-md-3">
            <div class="opo">
            	<h5>Step 4:</h5>
                <p>Shippers leave you feedback, which helps you book more loads.</p>
                </div>
            </div> 
            
            </div>
            
            
            
            <div class="col-md-3"><?php if ($_SESSION['user_id'] == ''): ?> <a class="btn btn-warning top-y" href="<?php echo $this->_tpl_vars['site_url']; ?>
/login.html">Members Sign In To Bid</a><?php endif; ?></div>
            <div class="col-md-6"><p class="top-y test-center"> ---- Already reviewed this listing and ready to make a bid? ---- </p></div>
             <div class="col-md-3"><?php if ($_SESSION['user_id'] == ''): ?><a class="btn btn-warning top-y" href="<?php echo $this->_tpl_vars['site_url']; ?>
/registration.html">Join Now To Place a Bid</a><?php endif; ?> </div> 
                 </div>
                 
    </div>
    
    <br />
    <div class="container">
		<div class="row">
			<div class="col-md-4">
				<h4>Category : <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['category']); ?>
 > <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['sub_category']); ?>
 > <?php echo $this->_tpl_vars['functions']->get_sub_next_category($this->_tpl_vars['search'][0]['parent_sub_id']); ?>
 >  <?php echo $this->_tpl_vars['functions']->get_last_category($this->_tpl_vars['search'][0]['last_cat']); ?>
</h4>
				<?php if ($_SESSION['user_type'] == 'T'): ?>
					<h6> <?php echo $this->_tpl_vars['search'][0]['title']; ?>
 </h6>
					<small><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/cust_fedback-<?php echo $this->_tpl_vars['search'][0]['user_id']; ?>
.html">Customer Feed Back Profile</a></small>
				<?php endif; ?>
				<?php if ($_SESSION['user_id'] == ''): ?>                  
					<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/registration.html" >Not Have Account Yet?</a>
				<?php endif; ?>
			</div>
			<div class="col-md-6">
				<?php if ($this->_tpl_vars['alredy_quote'] == 'Y'): ?>       
				<button class="btn btn-info pull-right">You Have Already Given Quote on This Shipment </button>       
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['type'] == 'su'): ?>       
				<form method="post" action="" name="next">
					<input type="hidden" value="<?php echo $this->_tpl_vars['job_id']; ?>
" name="job_id" />
					<input type="hidden" value="<?php echo $this->_tpl_vars['search'][0]['order_id']; ?>
" name="order_id" />			
					<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/my_post_job.html" class="btn pull-right btn-info"> Go to My Account</a>		
				</form>        
				<?php endif; ?>
			</div>
			<?php if ($_SESSION['user_type'] == 'T'): ?>
			<?php if ($_SESSION['user_id'] == '' || $this->_tpl_vars['alredy_quote'] != 'Y' && $this->_tpl_vars['type'] != 'su'): ?>
			<div class="col-md-2">			
				<form method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/give_quote.html" name="give_quote">
					<input type="hidden" value="<?php echo $this->_tpl_vars['job_id']; ?>
" name="job_id" />
					<input type="hidden" value="<?php echo $this->_tpl_vars['search'][0]['order_id']; ?>
" name="order_id" />			
					<input type="submit" name="submit" value="Give Quote" class="btn given_quote" />			
				</form>
			</div>
			<?php endif; ?>
            <?php endif; ?>
			
		</div>
		<hr />
    	<!--<div class="col-md-3">
        
            <?php if ($_SESSION['user_type'] == 'T'): ?>
        	<h6> <?php echo $this->_tpl_vars['search'][0]['title']; ?>
 </h6>
            <p><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/cust_fedback-<?php echo $this->_tpl_vars['search'][0]['user_id']; ?>
.html">Customer Feed Back Profile</a></p>
            <?php endif; ?>
        </div>-->
        
       <!-- <div class="col-md-2">
        </div>-->
		
        <!--<?php if ($_SESSION['user_id'] == ''): ?>                  
        	<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/registration.html" > <button    class="btn btn-danger">Not Have Account Yet? </button></a>
      	<?php endif; ?>-->
		
      <!--<?php if ($_SESSION['user_id'] == '' || $this->_tpl_vars['alredy_quote'] != 'Y' && $this->_tpl_vars['type'] != 'su'): ?>
        <div class="col-md-4">
        
        	<form method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/give_quote.html" name="give_quote">
                    <input type="hidden" value="<?php echo $this->_tpl_vars['job_id']; ?>
" name="job_id" />
                    <input type="hidden" value="<?php echo $this->_tpl_vars['search'][0]['order_id']; ?>
" name="order_id" />
                    
                    <input type="submit" name="submit" value="Give Quote" class="btn btn-info" />
                   
                    </form>
        </div>
        <?php endif; ?>
		
        <?php if ($this->_tpl_vars['type'] == 'su'): ?>       
		<form method="post" action="" name="next">
			<input type="hidden" value="<?php echo $this->_tpl_vars['job_id']; ?>
" name="job_id" />
			<input type="hidden" value="<?php echo $this->_tpl_vars['search'][0]['order_id']; ?>
" name="order_id" />			
			<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/my_post_job.html" class="btn btn-info"> Go to My Account</a>		
		</form>        
        <?php endif; ?>
        <?php if ($this->_tpl_vars['alredy_quote'] == 'Y'): ?>       
        	<button class="btn btn-info">You Have Already Given Quote on This Shipment </button>       
        <?php endif; ?>-->
        
        <!--<div class="col-md-3">
        	<p>Category : <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['category']); ?>
 </p>
            <p>Under Category:<?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['sub_category']); ?>
</p>
        </div>-->
    </div>
    </div>
    
    
    <div class="container">
    	<div class="col-md-4">
        	<h4 class="widget-title   top-lab">Listing Information </h4>
            <div class="clearfix">
                <div class="col-md-6">
                    <ul class="list-k">
                        <li>Delivery Title:</li>
                        <li>Shipment ID:</li>
                        <li>Customer:</li>
                        <li>Pickup Between</li><br/>
                        <li>Quote :</li>
                      
                                           </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li><?php echo $this->_tpl_vars['search'][0]['title']; ?>
</li>
                        <li><?php echo $this->_tpl_vars['search'][0]['order_id']; ?>
</li>
                        <li><?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][0]['user_id']); ?>
</li>
                    
                        <li><?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['pickup_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                         To <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['pickup_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</li>
                         <li>(<?php echo $this->_tpl_vars['functions']->get_total_quote($this->_tpl_vars['search'][0]['order_id']); ?>
)</li>
                    
                    </ul>
                </div>
                </div>
        	<h4 class="widget-title   top-lab">Shipment Detail</h4>
            <div class="col-md-6">
                    <ul class="list-k">
                        <li>Load Industry :</li>
                        <li>Load Type:</li>
                        <li>Transport Type:</li>
                        <li>Body &amp Bulk Type:</li>
                        <li>Size Classification:</li>
                        <li>Preferred Distance:</li>
                        <li>Other:</li>
                        <li>Dimensions:</li>
                        <li>Width:  </li>
                        <li>Height :</li>
                        <li>Length: </li>
                        <li>Weight :</li>
                      
                       <li>Quantity :</li>
                      
                       <li>Price :</li>
                         <li>Description :</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['category']); ?>
</li>
                        <li><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['sub_category']); ?>
</li>
                        <li><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['sub_category']); ?>
</li>
                        <li><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['sub_category']); ?>
</li>
                        <li><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['sub_category']); ?>
</li>
                        <li><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['sub_category']); ?>
</li>
                        <li><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['sub_category']); ?>
</li>
                        <li><?php echo $this->_tpl_vars['search'][0]['dimensions']; ?>
</li>
                        <li><?php echo $this->_tpl_vars['search'][0]['Width']; ?>
 </li>
                        <li><?php echo $this->_tpl_vars['search'][0]['Height']; ?>
</li>
                         <li><?php echo $this->_tpl_vars['search'][0]['Length']; ?>
 </li>
                         <li><?php echo $this->_tpl_vars['search'][0]['Weight']; ?>
</li>
                         <li><?php echo $this->_tpl_vars['search'][0]['Qty']; ?>
</li>
                        
                          <li><?php echo $this->_tpl_vars['search'][0]['value']; ?>
</li>
                      
                         <li><?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['description'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 100, '...') : smarty_modifier_truncate($_tmp, 100, '...')); ?>
</li>
                    </ul>
                </div>
                
                
                <div class="colmd-12">
                 <?php if ($this->_tpl_vars['search'][0]['img']): ?>
            <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/shipment_image/<?php echo $this->_tpl_vars['search'][0]['img']; ?>
" class="img-responsive" alt="">
            <?php else: ?>
                          <?php echo $this->_tpl_vars['functions']->get_cat_image($this->_tpl_vars['search'][0]['category']); ?>

                          <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/<?php echo $this->_tpl_vars['currency_info'][0]['cat_image']; ?>
" style="width:100px"; height="100px;" alt="">
                          <?php endif; ?>
                          <div>&nbsp;</div>
                    </div>        
                          
        </div>
        <div class="col-md-8">
        	<h4 class="widget-title   top-lab">Origin, Destination, & Route Information </h4>
            
            <div class="col-md-8">
              <?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "gmap.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>
  
            	
            </div>
            <div class="col-md-4">
            	<ul class="tobot">
                	<li>
                    <div class="pull-left lochan">
                    	<img src="images/pin-pickup.png" >
                    </div>
                    
                    	<h5>Collection</h5>
                        
                        <p>Address :<?php echo $this->_tpl_vars['search'][0]['sender_loc']; ?>
</p>
                        <p><?php echo $this->_tpl_vars['search'][0]['form_city']; ?>
,<?php echo $this->_tpl_vars['search'][0]['form_state']; ?>
</p>
                                             
                    </li>
                    
                    
                    <li>
                     <div class="pull-left lochan">
                    	<img src="images/pin-pickup.png" >
                    </div>
                    	<h5>Delivery</h5>
                        <p>Address :<?php echo $this->_tpl_vars['search'][0]['receiver_loc']; ?>
</p>
                        <p><?php echo $this->_tpl_vars['search'][0]['to_city']; ?>
,<?php echo $this->_tpl_vars['search'][0]['to_state']; ?>
</p>
                        <p>Delivery Between:<?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['delv_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
 to <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['delv_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</p>
                        
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="container">
    <div class="col-md-12">
    	<h4 class="widget-title top-lab"><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['category']); ?>
 > <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['sub_category']); ?>
 > <?php echo $this->_tpl_vars['functions']->get_sub_next_category($this->_tpl_vars['search'][0]['parent_sub_id']); ?>
 >  <?php echo $this->_tpl_vars['functions']->get_last_category($this->_tpl_vars['search'][0]['last_cat']); ?>
  </h4>
    	</div>
        <div class="col-md-12">
        	<div class="jio-box">
            	<h5><?php echo $this->_tpl_vars['search'][0]['title']; ?>
</h5>
                <h6>Quantity :<?php echo $this->_tpl_vars['search'][0]['Qty']; ?>
</h6>
            </div>
        </div>
        
        <!--<div class="col-md-12">
        <div class="toper clearfix">
        	<h5 class="pull-left">Motorcycle 1: <span><?php echo $this->_tpl_vars['search'][0]['title']; ?>
</span></h5>
            <ul class="pull-left ricko">
            
            
            	<li>Operational:</li>
                <li>Trike:</li>
                <li>Sidecar:</li>
                <li>Palletised:</li>
            </ul>
            </div>
                       <h6>Accepted Service Types:
Open Transport, Enclosed Transport, Part Load Freight - Transport Only</h6>
 
        </div> -->
    </div>
    
 <?php if ($_SESSION['user_type'] != ''): ?>
 <div class="container">
 	<div class="col-md-12">
        
    	
        
      <!--<?php if ($this->_tpl_vars['data'] === ''): ?>  <p> - There are currently no questions for this shipment - </p> <?php endif; ?>-->
        
        <h4 class="widget-title top-lab">Questions for this Item  </h4>
        
         <?php if ($this->_tpl_vars['data'] === ''): ?>  <p> - There are currently no questions for this shipment - </p> <?php endif; ?>
        
       <!-- <h4>Quotes on this Shipment</h4>-->
   <!-- </div> -->
   <?php if ($_SESSION['user_type'] == 'T'): ?>
      <form action="" method="post">
    <div class="row">
    	<div class="col-md-12">
        	<div class="col-md-6">
                
                  <div class="form-group">
             
             
                
                    <label for="usr">Enter Your Question:</label>
                    <input type="text" class="form-control" name="qus" placeholder="Type Your Question Here...">
                    <input type="hidden" value="<?php echo $_SESSION['user_id']; ?>
" name="tid" />
                    <input type="hidden" value="<?php echo $this->_tpl_vars['functions']->get_cust_name($_SESSION['user_id']); ?>
" name="tran_name" />
                    
                     <input type="hidden" value="<?php echo $this->_tpl_vars['search'][0]['user_id']; ?>
" name="cid" />
                    
                </div>
               
                
                <button type="submit" class="btn btn-primary" name="Submit">Submit</button>
                    
            </div>
           
            <div class="col-md-6"></div>
        </div>
    </div>
      </form>
      <?php else: ?> 
      <form action="" method="post">
    <div class="row">
    	<div class="col-md-12">
        	<div class="row">
        	<div class="col-md-6">
                
                  <div class="form-group">
             
             
                   <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['data']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                    <label for="usr">Transpoter Name:<?php echo $this->_tpl_vars['data'][$this->_sections['data']['index']]['trans_name']; ?>
</label>
                    <input type="hidden" name="ques_id_<?php echo $this->_tpl_vars['data'][$this->_sections['data']['index']]['ques_id']; ?>
" value="<?php echo $this->_tpl_vars['data'][$this->_sections['data']['index']]['ques_id']; ?>
">
                    <input type="text" class="form-control" readonly="readonly" value="<?php echo $this->_tpl_vars['data'][$this->_sections['data']['index']]['question']; ?>
" name="question" placeholder="Type Your Question Here..."><br/>
                  <label>Ans<?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>
</label>
                 
                   <input type="text" class="form-control"  name="ans_<?php echo $this->_tpl_vars['data'][$this->_sections['data']['index']]['ques_id']; ?>
" value="<?php echo $this->_tpl_vars['data'][$this->_sections['data']['index']]['answer']; ?>
" placeholder="Type Your Answer Here...">
                
               <?php endfor; endif; ?>
                </div>
                <button type="submit" class="btn btn-primary" name="Submit1">Submit</button>
                    
            </div>
           
            <div class="col-md-6"></div>
            </div>
        </div>
    </div>
      </form> 
      <?php endif; ?>    
 </div>
 <?php endif; ?>
 <br />
    <h4>Quotes on this Shipment</h4>
 
 <div class="container">
 	<div class="accordion-style1 panel-group" id="accordion">
                
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title ">
<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="accordion-toggle collapsed " aria-expanded="false">
<i data-icon-hide="icon-angle-down" data-icon-show="icon-angle-right" class="icon-angle-down bigger-110"></i>
<img style="float:left;" class="img-responsive" src="images/arrow.png">&ensp; Description  
</a>
</h4>
</div>
<div style="min-height: 100px;" class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
<div class="panel-body"> <?php echo $this->_tpl_vars['search'][0]['description']; ?>
 </div>
</div>
<div class="row">
    	<div class="col-md-12">
        	<div class="col-md-6">
                
                  <div class="form-group">
             <br />
         
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['usersdata1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                    <label>Qus<?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>
</label>
                    <input type="text" class="form-control" readonly="readonly" value="<?php echo $this->_tpl_vars['usersdata1'][$this->_sections['data']['index']]['question']; ?>
" name="question" placeholder="Type Your Question Here..."><br/>
                  <label>Ans<?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>
</label>
                   <input type="text" class="form-control" readonly="readonly" value="<?php echo $this->_tpl_vars['usersdata1'][$this->_sections['data']['index']]['answer']; ?>
" name="ans" placeholder="Type Your Answer Here...">
                    
                    <?php endfor; endif; ?>
                </div>
               
                    
            </div>
           
            <div class="col-md-6"></div>
        </div>
    </div>
</div>
<br />







</div>
 </div>
        
                  
  <!--End Header-->

      <!-- END CLIENTS -->
    </div>
  <!--End Main-->
  
  <!--End Footer--> 
</div>
</body>
</html>