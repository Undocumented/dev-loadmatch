<?php /* Smarty version 2.6.19, created on 2017-08-18 14:21:13
         compiled from booked.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'booked.tpl', 278, false),)), $this); ?>
<?php if ($_SESSION['user_type'] == 'C'): ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Booked Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My Booked Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          My Booked Shipment
          </h4>
           <?php if ($this->_tpl_vars['err']): ?>
          <?php echo $this->_tpl_vars['err']; ?>

          <?php endif; ?>
          <table align="right">
          <tr>
          <td align="left">
           <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
                        <p><b style="color:#F00;">Pending&nbsp;:</b>&nbsp;&nbsp;Still no Action Tacken by Transporter</p>
         				<p><b style="color:#F00;">Picked up&nbsp;:</b>&nbsp;&nbsp;Shipment Picked up by Transporter</p>
          				<p><b style="color:#F00;">In Transit&nbsp;:</b>&nbsp;&nbsp;Shipment is In Transit</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
            <br/>
          
         
             </td>
             </tr>
             </table>
             <br/><br/><br/>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/edit_post_shipping" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    
                    <th>
                      Order ID
                    </th>
                    <th>
                     Transporter
                    </th>
                    <th>
                      Collection
                    </th>
                    <th>
                     Delivery
                    </th>
                    <th>
                     Category
                    </th>
                  <!--  <th>
                     Under Category
                    </th>-->
                     <th>
                     Status
                    </th>
                     <th>
                     Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['search']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>

                    </td>
                    <td>
                       <?php echo $this->_tpl_vars['functions']->get_agent_name($this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']); ?>

                    </td>
                    <td>
                     <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['form_state']; ?>
, <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['form_city']; ?>

                    </td>
                    <td>
                     <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['to_state']; ?>
, <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['to_city']; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][$this->_sections['data']['index']]['category']); ?>

                    </td>
                    
                     <td>
                        <?php if ($this->_tpl_vars['search'][$this->_sections['data']['index']]['transport_status'] == 'Picked up'): ?><div class="btn btn-success btn-sm">Picked up</div>	
                        
                        
                         <?php elseif ($this->_tpl_vars['search'][$this->_sections['data']['index']]['transport_status'] == 'In transit'): ?> <div class="btn btn-info btn-sm">In transit </div><?php else: ?> <div class="btn btn-danger btn-sm">Pending</div> <?php endif; ?>
                    </td>
                   <td>
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                    <a style="cursor:pointer;" onclick="submitbuy_book_cancel_complet('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
','b',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                    </td>
                  </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>
</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    <?php endif; ?>
    
    <?php if ($_SESSION['user_type'] == 'T'): ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My booked Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My booked Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
           My Booked Shipment
          </h4>
           <?php if ($this->_tpl_vars['suss']): ?>
          <?php echo $this->_tpl_vars['suss']; ?>

          <?php endif; ?>
            <table align="right">
          <tr>
          <td align="left">
          
           <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
                         <p><b style="color:#F00;">Pending&nbsp;:</b>&nbsp;&nbsp;Still no Action Tacken by Transporter</p>
         				 <p><b style="color:#F00;">Picked up&nbsp;:</b>&nbsp;&nbsp;Shipment Picked up by Transporter</p>
       					 <p><b style="color:#F00;">In Transit&nbsp;:</b>&nbsp;&nbsp;Shipment is In Transit</p>
        				 <p><b style="color:#F00;">Update&nbsp;:</b>&nbsp;&nbsp;Change Shipment Status</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
            <br/>
             </td>
             </tr>
             
             </table>
             <br/><br/><br/>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/detail_shiping_booked" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                   <th>
                      Customer
                    </th>
                    <th>
                     Pickup Date
                    </th>
                    <th>
                     Delivery Date
                    </th>
                    <th>
                      Delivery Location
                    </th>
                     <th>
                     Status
                    </th>
                     <th>
                     Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['search']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>

                    </td>
                    <td>
                       <?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][$this->_sections['data']['index']]['user_id']); ?>

                    </td>
                    <td>
                     <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['collect_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
, <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['collect_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                    </td>
                    <td>
                     <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['delivery_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
, <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['delivery_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                    </td>
                     <td>
                     <?php echo $this->_tpl_vars['functions']->get_destnation_state_city($this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']); ?>

                    </td>
                    <td id="status_change_<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
">
                      
                       <?php if ($this->_tpl_vars['search'][$this->_sections['data']['index']]['transport_status'] == 'Picked up'): ?><div class="btn btn-success btn-sm">Picked up</div>	
                        
                        
                         <?php elseif ($this->_tpl_vars['search'][$this->_sections['data']['index']]['transport_status'] == 'In transit'): ?> <div class="btn btn-info btn-sm">In transit</div><?php elseif ($this->_tpl_vars['search'][$this->_sections['data']['index']]['transport_status'] == 'Cancel'): ?> <div class="btn btn-danger btn-sm">Canceled</div> <?php else: ?><div class="btn btn-danger btn-sm">Pending</div> <?php endif; ?>
                    </td>
                    <td>
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                    <a style="cursor:pointer;" onclick="submitbuy_agent('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
','<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>
',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                  
                    <a onclick="reaison_update(<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
)" class="btn btn-primary btn-sm"/>Update</a>
                    </td>
                  </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>
</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->
  <div id="cancel_value" style="display:none; position:fixed;  top:37%; left:50%; margin-top:-220px; margin-left:-200px; z-index:99999" >

           <div  style="width:350px; background-color: #fff;border:2px solid #888;text-align:center;padding:0px;margin:0px" >
<div style="margin:0px; padding: 3px 0px 2px 0px;text-align:center;background: #485b79 url(<?php echo $this->_tpl_vars['site_url']; ?>
/images/admin/default/headerbg.png); min-height: 10px; overflow: hidden; border-bottom: 2px solid #fb9337; border:1px solid #888;width:102;">
<a title="Upgrade Package" class="VSlabel" style="text-weight:bold;text-decoration:none;color:#fff;"><b>Update Shipment</b></a>
<img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/converter_close.png" class="hand" onclick="reaison_close(0);" align="right" style="cursor:pointer"  />


 </div>
 
            <input type="hidden" id="cencel_id" />    
      
                    
             <table cellpadding="10" cellspacing="5"  class="stdtable1"  >
                  
                    <tr><td>&nbsp;</td></tr>
                      <tr>
                      <td>
                      <span id="update_status"></span>
                    Change Shipment Status <select name="transport_status"  id="transport_status"   >
          <option value="" selected="selected" >Please Select</option>
                    <option value="Picked up" <?php if ($this->_tpl_vars['search'][0]['transport_status'] == 'Picked up'): ?> selected="selected" <?php endif; ?> >Picked Up</option>
      				 <option value="In transit" <?php if ($this->_tpl_vars['search'][0]['transport_status'] == 'In transit'): ?> selected="selected" <?php endif; ?> >In transit</option>
                      <option value="Delivered" <?php if ($this->_tpl_vars['search'][0]['transport_status'] == 'Delivered'): ?> selected="selected" <?php endif; ?> >Delivered</option>
                      <option value="Cancel" <?php if ($this->_tpl_vars['search'][0]['transport_status'] == 'Cancel'): ?> selected="selected" <?php endif; ?> >Cancel</option>   
                     
                 
                   </select>
      
     
                    </ul>
                  
                   
              
                   </td>
                       </tr>
                       <tr>
                     <td align="center">
                     <a onClick="update_status_booking();" class="btn btn-primary btn-sm" >Submit
                     </a>
                      <!-- onClick="return add_city_name_pincode(document.frm_addcity);"-->
                      <!-- <a class="btn btn-primary btn-sm"  onclick="this.submit();" >Submit</a>-->
                     
                           
                        <td></tr> 
                    
                        
                        
                        
                        </table>
                  


          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    <?php endif; ?>