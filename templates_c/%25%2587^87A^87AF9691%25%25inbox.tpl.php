<?php /* Smarty version 2.6.19, created on 2017-08-15 18:21:31
         compiled from inbox.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'inbox.tpl', 79, false),)), $this); ?>
<?php if ($_SESSION['user_type'] == 'C'): ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Inbox</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My Inbox</a></li>
                       <?php if ($this->_tpl_vars['edit_id'] != ''): ?>
                       <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/inbox.html">Back</a></li>
                       <?php endif; ?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          Inbox
          </h4>
          <div class="contact-form">
              <div class="table-responsive">
            
   <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
              <form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/detail_shiping_booked" id="view_detail">
              <table class="table table-bordered" >
              
                <thead>
                  <tr>
                    <th width="25px;">
                      Sr.no
                    </th>
                    <th style="text-align:center;" >
                      Regarding
                    </th>
                    <th style="text-align:center;" >
                      Title
                    </th>
                  <!--  <th>
                     Message
                    </th>-->
                    <th width="10%">
                    Date
                    </th>
                     <th width="10%">
                     Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['search']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    <td>
                    
                    (Order Id: <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>
)
                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['title']; ?>

                    </td>
                                       <td>
                     <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['entery_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                    </td>
                    <td>
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/eagleushp.php?page=myaccount&action=inbox&edit_id=<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" title="View"><i class="fa fa-pencil-square-o"></i></a>
                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/eagleushp.php?page=myaccount&action=inbox&del_mail=<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" onclick="return confirm('Do You  want to delete ?');" title="Delete"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>
</td></tr>
                </form>
                <?php else: ?>
                <table align="center" border="0" width="70%" style="border:1px solid #000; padding:15px" >
                <tr>
                <td align="center" >
                User:&nbsp;<?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search12'][0]['user_id']); ?>

                </td>
                </tr>
                <tr><td align="center"><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/profile_pic/<?php echo $this->_tpl_vars['functions']->get_image($this->_tpl_vars['search'][0]['user_id']); ?>
" width="100px" height="100px" /></td></tr>
                <tr>
                <td align="center" style=" background-color:#CCC; text-align:left;">
                Title :&nbsp; <?php echo $this->_tpl_vars['search12'][0]['title']; ?>
 (Order Id: <?php echo $this->_tpl_vars['search12'][0]['order_id']; ?>
)
                </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                 <tr>
                <td align="center">
               <label> Message</label> &nbsp; <?php echo $this->_tpl_vars['search12'][0]['mes']; ?>

                </td>
                </tr>
                 <!--<tr><td>Form:&nbsp;<?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][0]['user_id']); ?>
</br>
                 Email: <?php echo $this->_tpl_vars['functions']->get_email($this->_tpl_vars['search'][0]['user_id']); ?>
</br>
                 Date:<?php echo $this->_tpl_vars['search'][0]['entery_date']; ?>

                 </td></tr>-->
                </table>
                
                <?php endif; ?>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    <?php endif; ?>
    
    <?php if ($_SESSION['user_type'] == 'T'): ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Inbox</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My Inbox</a></li>
                       <?php if ($this->_tpl_vars['edit_id'] != ''): ?>
                       <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/inbox.html">Back</a></li>
                       <?php endif; ?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          Inbox 
          </h4>
          <div class="contact-form">
              <div class="table-responsive">
             
              <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
              <form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/detail_shiping_booked" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th width="25px;">
                      Sr.no
                    </th>
                    <th style="text-align:center;" >
                      Title
                    </th>
                  <!--  <th>
                     Message
                    </th>-->
                    <th width="10%">
                    Date
                    </th>
                     <th width="10%">
                     Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['search']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['title']; ?>

                    </td>
                                       <td>
                     <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['entery_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                    </td>
                    <td>
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/eagleushp.php?page=myaccount&action=inbox&edit_id=<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" title="View"><i class="fa fa-pencil-square-o"></i></a>
                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/eagleushp.php?page=myaccount&action=inbox&del_mail=<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" onclick="return confirm('Do You  want to delete ?');" title="Delete"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>
</td></tr>
                </form>
                <?php else: ?>
                <table align="center" border="0" style="border:1px solid #000;" >
                <tr>
                <td>
                User:&nbsp;<?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search12'][0]['agent_id']); ?>

                </td>
                </tr>
                <tr><td><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/profile_pic/<?php echo $this->_tpl_vars['functions']->get_image($this->_tpl_vars['search'][0]['agent_id']); ?>
" width="100px" height="100px" /></td></tr>
                <tr>
                <td style=" background-color:#CCC; text-align:left;">
                Title :&nbsp; <?php echo $this->_tpl_vars['search12'][0]['title']; ?>
 (Order Id: <?php echo $this->_tpl_vars['search12'][0]['order_id']; ?>
)
                </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                 <tr>
                <td>
                Message :&nbsp; <?php echo $this->_tpl_vars['search12'][0]['mes']; ?>

                </td>
               <!-- </tr>
                 <tr><td>Form:&nbsp;<?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][0]['user_id']); ?>
</br>
                 Email: <?php echo $this->_tpl_vars['functions']->get_email($this->_tpl_vars['search'][0]['user_id']); ?>
</br>
                 Date:<?php echo $this->_tpl_vars['search'][0]['entery_date']; ?>

                 </td></tr>-->
                </table>
                
                <?php endif; ?>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    <?php endif; ?>