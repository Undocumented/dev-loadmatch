<?php /* Smarty version 2.6.19, created on 2017-04-25 13:44:07
         compiled from admin/show_message.tpl */ ?>
<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Message List</h2>
                    
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <h3>Message List</h3>
                           
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
              
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					 <thead align="left" >
					<tr>
						<th width="50">Sr.No.</th>
                        <th  width="600">Name</th>
						<th  width="600">Subject</th>
                        <th  width="600">Email</th>
						<th width="80">Action</th>
					</tr>
				 </thead>
				  <tbody>
					<?php if ($this->_tpl_vars['userdata']): ?>
						<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['userdata']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
							<tr>
								<td align="center"><?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>
</td>
                                <td><?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['subject']; ?>
</td>
								<td><?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['subject']; ?>
</td>
                                <td><?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['email']; ?>
</td>
								<td >
								
								  <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=detail_message&view_id=<?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['id']; ?>
" title="view"><i class="fa fa-pencil-square-o"></i></a>
							      <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=show_message&del_id=<?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['id']; ?>
" onclick="return confirm('Do You  want to delete ?');" title="Delete"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						<?php endfor; endif; ?>
                        <tr>
				 <td colspan="8"><form name="frm_pagi" action="#" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" />
                <?php echo $this->_tpl_vars['show_pagi']; ?>

              </form></td>
          </tr>
				
					<?php else: ?>
						<tr><td colspan="7" align="center">Record Not Found</td></tr>
					<?php endif; ?>
					 <tbody>			
				</table>
					
					
 </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>