<?php /* Smarty version 2.6.19, created on 2017-08-18 03:01:57
         compiled from admin/add_slider.tpl */ ?>
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2> Edit Slider</h2>
      </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading"> Edit Slider
         
          
          
          </div>
          <div class="panel-body">
           <label  style="color:red; text-align:center;"><?php echo $this->_tpl_vars['massage']; ?>
</label>
            <div class="row">
           
              <div class="col-md-8"> 
                
                <!--===============customer form======================-->
                <form role="form" name="company" id="company" action="" method="post"  enctype="multipart/form-data">
                <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
               <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['usersdata']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                    <div class="form-group">
                    <label> Slider <?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['id']; ?>
 &nbsp;&nbsp;&nbsp;&nbsp;</label>
                   <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/slider/<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['image_path']; ?>
" width="100px" height="100px" style="border:2px solid #999;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=add_banner&action=add_slider&edit_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['id']; ?>
">Edit <i class="fa fa-pencil-square-o"></i></a>
                   </div>
                    <?php endfor; endif; ?>
                   <?php endif; ?>
                   <?php if ($this->_tpl_vars['edit_id'] != ''): ?>
                  <div class="form-group">
                    <label>Edit Slider: <?php echo $this->_tpl_vars['edit_id']; ?>
  </label>
                   <div style="text-align:right;"> <?php if ($this->_tpl_vars['usersdata1'][0]['image_path'] != ''): ?>
               <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/slider/<?php echo $this->_tpl_vars['usersdata1'][0]['image_path']; ?>
" width="150px" height="150px" style="border:2px solid #999;" />
               <?php endif; ?>
               </div> 
                   </div>
                  <div class="form-group" style="margin-top:-120px;">
                    <label>Select Slider Image</label>
                   <input type="file" name="logo"  id="logo"/>
                   <?php if ($this->_tpl_vars['usersdata1'][0]['id'] == '1'): ?>  <label>size must be(300*477)</label></br><?php endif; ?>
                   <?php if ($this->_tpl_vars['usersdata1'][0]['id'] == '2'): ?>  <label>size must be(430*334)</label></br><?php endif; ?>
                   <?php if ($this->_tpl_vars['usersdata1'][0]['id'] == '3'): ?>  <label>size must be(412*453)</label></br><?php endif; ?>
                   <?php if ($this->_tpl_vars['usersdata1'][0]['id'] == '4'): ?>  <label>size must be(1800*550)</label></br><?php endif; ?>
                   <input type="hidden" name="old_main_img" value="<?php echo $this->_tpl_vars['usersdata1'][0]['image_path']; ?>
" />
                   
              
                    </div>
                      <div class="form-group">
                    <label>Title </label>
                   <input type="text" name="title" id="" value="<?php echo $this->_tpl_vars['usersdata1'][0]['title']; ?>
" class="form-control"/>
                   </div>
                    
                       <div class="form-group">
                    <label>Keyword </label>
                   <input type="text" name="keyword" id="" value="<?php echo $this->_tpl_vars['usersdata1'][0]['keyword']; ?>
" class="form-control"/>
                   </div>
                  <button type="submit" class="btn btn-primary" name="company_btn" value="add_new_customer" onclick="return add_valid_comapny(document.company);">  Update Slider</button>
                  <?php endif; ?>
                </form>
                <!--==================================================--> 
                
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>