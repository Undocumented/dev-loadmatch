<?php /* Smarty version 2.6.19, created on 2017-08-18 04:48:48
         compiled from my_post_job.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'my_post_job.tpl', 321, false),)), $this); ?>
  <!--breadcrumbs start-->
   <?php if ($_SESSION['user_type'] == 'C'): ?>
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Active Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
           My Shipment
          </h4>
          <?php if ($this->_tpl_vars['err']): ?>
          <?php echo $this->_tpl_vars['err']; ?>

          <?php endif; ?>
          <table align="right" >
          <tr>
          <td align="left">
           <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
                       <p><b style="color:#F00;">Active&nbsp;:</b>&nbsp;&nbsp;Shipment is Approved By Admin</p>
                      <p><b style="color:#F00;">Pending&nbsp;:</b>&nbsp;&nbsp;Shipment is Still in Admin Rewiew</p>
                      <p><b style="color:#F00;">View Quote&nbsp;:</b>&nbsp;&nbsp;View Quote Given By Transporter</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
            <br/>
             </td>
             </tr>
             </table>
           
          <div class="contact-form">
            
              <div class="table-responsive">
              <form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/edit_post_shipping" id="view_detail">
             
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                    Collection
                    </th>
                    <th>
                      Delivery
                    </th>
                    <th>
                     Category
                    </th>
                    <th>
                     Under Category
                    </th>
                     <th>
                     Status
                    </th>
                     <th width="12%">
                     Action
                    </th>
                    
                    <th>
                     Quote
                    </th>
                  </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['search']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>

                    </td>
                    <td>
                     <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['form_state']; ?>
, <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['form_city']; ?>

                    </td>
                    <td>
                     <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['to_state']; ?>
, <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['to_city']; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][$this->_sections['data']['index']]['category']); ?>

                    </td>
                    <td>
                       <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][$this->_sections['data']['index']]['sub_category']); ?>

                    </td>
                     <td>
                       <?php if ($this->_tpl_vars['search'][$this->_sections['data']['index']]['status'] == 'Y'): ?><div class="btn btn-success btn-sm" style=" cursor:move;"> Active </div><?php else: ?><div class="btn btn-danger btn-sm"> Pending </div><?php endif; ?>
                    </td>
                    <td>
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                    <a style="cursor:pointer;" onclick="submitbuy1('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
',document.view_detail);" class="btn btn-primary btn-sm"/>Edit</a>
                   <!--href="<?php echo $this->_tpl_vars['site_url']; ?>
/eagleushp.php?page=myaccount&action=booked_cancel&cancel_user=<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
"-->
                     <a  class="btn btn-primary btn-sm" onclick="reaison(<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
)">Cancel </a>
                    </td>
                    <td>
                    <?php if ($this->_tpl_vars['search'][$this->_sections['data']['index']]['status'] == 'Y'): ?>
                    <a style="cursor:pointer;" onclick="submitbuy12('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
',document.view_detail);" class="btn btn-primary btn-sm"/>View Quote(<?php echo $this->_tpl_vars['functions']->get_total_quote($this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']); ?>
) </a>
                    <?php else: ?>
                    <a style="cursor:pointer;" class="btn btn-primary btn-sm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Null&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a>
                    <?php endif; ?>
                    </td>
                  </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="10" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>
</td></tr>
                </form>
                <div id="cancel_value" style="display:none; position:fixed;  top:37%; left:50%; margin-top:-220px; margin-left:-200px; z-index:99999" >

           <div  style="width:500px; background-color: #fff;border:2px solid #888;text-align:center;padding:0px;margin:0px" >
<div style="margin:0px; padding: 3px 0px 2px 0px;text-align:center;background: #485b79 url(<?php echo $this->_tpl_vars['site_url']; ?>
/images/admin/default/headerbg.png); min-height: 10px; overflow: hidden; border-bottom: 2px solid #fb9337; border:1px solid #888;width:102;">
<a title="Upgrade Package" class="VSlabel" style="text-weight:bold;text-decoration:none;color:#fff;"><b>Cancel Shipment</b></a>
<img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/converter_close.png" class="hand" onclick="reaison_close(0);" align="right" style="cursor:pointer"  />


 </div>
 
                 
        <form class="stdform " name="frm_addcity" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/eagleushp.php?page=myaccount&action=booked_cancel" enctype="multipart/form-data">
        <input type="hidden" id="cencel_id" name="cancel_user" value="" />
                    
             <table cellpadding="10" cellspacing="5"  class="stdtable1"  >
                  
                    <tr><td>&nbsp;</td></tr>
                      <tr>
                      <td>
                     Reason For Cencle:<textarea name="resone" rows="5"  cols="30"  ></textarea>
                   </td>
                       </tr>
                       <tr>
                     <td>
                      <!-- onClick="return add_city_name_pincode(document.frm_addcity);"-->
                      <!-- <a class="btn btn-primary btn-sm"  onclick="this.submit();" >Submit</a>-->
                      <input type="submit" name="submit" class="btn btn-primary btn-sm" />
                           
                        <td></tr> 
                    
                        
                        
                        
                        </table>
                    </form>
 			
 

 
 
	
   
</div>
			  
</div>
          </div>
        </div>
     

    </div>
    
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <?php endif; ?>
     <!--breadcrumbs start-->
   <?php if ($_SESSION['user_type'] == 'T'): ?>
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Active Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                         <li><a href="#">My Account</a></li>
                        <li>Active Shipment</li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          Active Shipment
          </h4>
          <?php if ($this->_tpl_vars['err']): ?>
          <?php echo $this->_tpl_vars['err']; ?>

          <?php endif; ?>
            <table align="right">
          <tr>
          <td>
          
          <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
                     <p><b style="color:#F00;">Pending&nbsp;:</b>&nbsp;&nbsp;Shipment Quote is Under Review of Admin</p>
                      <p><b style="color:#F00;">Active&nbsp;:</b>&nbsp;&nbsp;Shipment Quote is Active</p>
                      <p><b style="color:#F00;">Edit&nbsp;:</b>&nbsp;&nbsp;Edit Shipment Quote </p>
                      <p><b style="color:#F00;">Cancel&nbsp;:</b>&nbsp;&nbsp;Cancel Shipment Quote</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
                <br/><br/>
             </td>
             </tr>
             </table>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/give_quote.html" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                      Pickup Date
                    </th>
                    <th>
                      Delivery Date
                    </th>
                     <th>
                      Delivery Location
                    </th>
                    <th>
                     Status
                    </th>
                     <th>
                     Action
                    </th>
                   
                  </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['search']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>

                    </td>
                    <td>
                     <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['collect_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
 to <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['collect_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                    </td>
                    <td>
                     <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['delivery_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
 to <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['delivery_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                    </td>
                    <td>
                     <?php echo $this->_tpl_vars['functions']->get_destnation_state_city($this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']); ?>

                    </td>
                   
                     <td>
                       <?php if ($this->_tpl_vars['search'][$this->_sections['data']['index']]['status'] == 'Y'): ?> <div class="btn btn-success btn-sm">Active</div> <?php else: ?> <div class="btn btn-danger btn-sm">Pending</div> <?php endif; ?>
                    </td>
                    <td>
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                    <a style="cursor:pointer;" onclick="submitbuy1234('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
',document.view_detail);" class="btn btn-primary btn-sm"/>Edit</a>
                                    
  
  <a  class="btn btn-primary btn-sm" onclick="reaison(<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
)">Cancel </a>
                   
                    </td>
                  </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
              
          
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>
</td></tr>
                </form>
                
                
                <div id="cancel_value" style="display:none; position:fixed;  top:37%; left:50%; margin-top:-220px; margin-left:-200px; z-index:99999" >

           <div  style="width:500px; background-color: #fff;border:2px solid #888;text-align:center;padding:0px;margin:0px" >
<div style="margin:0px; padding: 3px 0px 2px 0px;text-align:center;background: #485b79 url(<?php echo $this->_tpl_vars['site_url']; ?>
/images/admin/default/headerbg.png); min-height: 10px; overflow: hidden; border-bottom: 2px solid #fb9337; border:1px solid #888;width:102;">
<a title="Upgrade Package" class="VSlabel" style="text-weight:bold;text-decoration:none;color:#fff;"><b>Cancel Shipment</b></a>
<img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/converter_close.png" class="hand" onclick="reaison_close(0);" align="right" style="cursor:pointer"  />


 </div>
 
                 
        <form class="stdform " name="frm_addcity" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/eagleushp.php?page=myaccount&action=booked_cancel" enctype="multipart/form-data">
        <input type="hidden" id="cencel_id" name="cancel_user" value="" />
                    
             <table cellpadding="10" cellspacing="5"  class="stdtable1"  >
                  
                    <tr><td>&nbsp;</td></tr>
                      <tr>
                      <td>
                     Reason For Cencle:<textarea name="resone" rows="5"  cols="30"  ></textarea>
                   </td>
                       </tr>
                       <tr>
                     <td>
                      <!-- onClick="return add_city_name_pincode(document.frm_addcity);"-->
                      <!-- <a class="btn btn-primary btn-sm"  onclick="this.submit();" >Submit</a>-->
                      <input type="submit" name="submit" class="btn btn-primary btn-sm" />
                           
                        <td></tr> 
                    
                        
                        
                        
                        </table>
                    </form>
 			
          </div>
          
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <?php endif; ?>
    <!--container end-->
    
    