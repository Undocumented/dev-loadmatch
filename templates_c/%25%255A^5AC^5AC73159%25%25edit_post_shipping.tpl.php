<?php /* Smarty version 2.6.19, created on 2017-08-18 14:19:43
         compiled from edit_post_shipping.tpl */ ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Edit Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Edit Shipment</a></li>
                        <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="component-bg">
        <div class="container">
            <!-- Forms
================================================== -->
<div class="bs-docs-section mar-b-30">
  <h1 id="forms" class="page-header">Edit Shipment</h1>
 
    <form class="form-horizontal" name="post_shipment"  method="post" action="#" role="form" enctype="multipart/form-data">
      <h3 align="left">Sender Details</h3>
     <hr>
     <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Order Id</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" readonly="readonly" value="<?php echo $this->_tpl_vars['search'][0]['order_id']; ?>
" id="Order" name="Order" >
         
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Title</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="<?php echo $this->_tpl_vars['search'][0]['title']; ?>
" id="Title" name="Title" placeholder="Enter Title Here">
          
        </div>
        </div>
     <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Full Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" readonly="readonly" value="<?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][0]['user_id']); ?>
" id="s_user_name" name="s_user_name" placeholder="User Name">
          <input type="hidden"  name="user_id" value="<?php echo $_SESSION['user_id']; ?>
" />
          
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" readonly="readonly" value="<?php echo $this->_tpl_vars['functions']->get_email($this->_tpl_vars['search'][0]['user_id']); ?>
" id="s_user_email" name="s_user_email" placeholder="Email">
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
        <div class="col-sm-10">
          <input type="text" readonly="readonly" class="form-control" id="mobile"  name="mobile" value="<?php echo $this->_tpl_vars['functions']->get_mobile($this->_tpl_vars['search'][0]['user_id']); ?>
" placeholder="Mobile no">
        </div>
      </div>
   
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Collection Province</label>
        <div class="col-sm-10">
         
         <select name="form_state" id="form_state" class="form-control border-radius"  onchange="get_state_city(this.value);">
        
         <option value="please Select" selected="selected" >Please Select Collection Province</option>
      
						<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
  <option value="<?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
" <?php if ($this->_tpl_vars['search'][0]['form_state'] == $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
 </option>
   <?php endfor; endif; ?>
               
                 
</select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Collection City</label>
        <div class="col-sm-10">
         
          <span id="city_change">
                     <select class="form-control"  name="form_change" id="form_change" >
                     <option value="please Select" selected="selected" >Please Select  City</option>
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['search'][0]['form_city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
   
</select>
     
      </div>
  </div>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Location/Address</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="loc_sen" id="loc_sen" rows="3"><?php echo $this->_tpl_vars['search'][0]['sender_loc']; ?>
</textarea>
     </div>
        
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Pickup Dates</label>
        <div class="col-xs-2">
        <INPUT NAME="pickup_start_date" id="pickup_start_date" TYPE="date"   value="<?php echo $this->_tpl_vars['search'][0]['pickup_start_date']; ?>
" />
       <label for="inputEmail3" class="col-sm-2 control-label">Between</label>
         
        <INPUT NAME="pickup_end_date" id="pickup_end_date" TYPE="date"   value="<?php echo $this->_tpl_vars['search'][0]['pickup_end_date']; ?>
" />
        
       
        </div>
      
  </div>
  <hr />
   <h3 align="left">Receiver Details</h3>
    <hr />
   <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Receiver Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="<?php echo $this->_tpl_vars['search'][0]['receiver_name']; ?>
" id="r_user_name" name="r_user_name" placeholder="User Name">
          
          
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label"> Receiver Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" value="<?php echo $this->_tpl_vars['search'][0]['receiver_email']; ?>
" id="r_user_email" name="r_user_email" placeholder="Email">
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Receiver Mobile</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="r_mobile" value="<?php echo $this->_tpl_vars['search'][0]['receiver_mobile']; ?>
"  name="r_mobile" placeholder="Mobile no">
        </div>
      </div>
   
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery Province</label>
        <div class="col-sm-10">
         
           <select name="to_state" id="to_state" class="form-control border-radius"  onchange="get_state_city_to(this.value);">
        
         <option value="please Select" selected="selected" >please Select</option>
      
						<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
  <option value="<?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
" <?php if ($this->_tpl_vars['search'][0]['to_state'] == $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
 </option>
   <?php endfor; endif; ?>
               
                 
</select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery City</label>
        <div class="col-sm-10">
         
          <span id="city_change1">
                     <select class="form-control"  name="to_city" id="to_city" >
                     
        <option value="please Select" selected="selected" >please Select</option>
       
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat1'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['search'][0]['to_city'] == $this->_tpl_vars['city_cat1'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat1'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     
</select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Location/Address</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="loc_rec" id="loc_rec" rows="3"><?php echo $this->_tpl_vars['search'][0]['receiver_loc']; ?>
</textarea>
     </div>
     </div>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery Dates</label>
        <div class="col-xs-2">
           <INPUT NAME="delv_start_date" id="delv_start_date" TYPE="date"   value="<?php echo $this->_tpl_vars['search'][0]['delv_start_date']; ?>
" />
         <label for="inputEmail3" class="col-sm-2 control-label">Between</label>
            <INPUT NAME="delv_end_date" id="delv_end_date" TYPE="date"   value="<?php echo $this->_tpl_vars['search'][0]['delv_end_date']; ?>
" />
        </div>
      
  </div>
  
       <hr />
        <h3 align="left">Shipment Details</h3>
        <hr />
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Load Industry</label>
        <div class="col-sm-10">
         <select name="category"  id="category" class="form-control border-radius" onchange="get_sub_cat(this.value);" >
                    <option value="please Select" selected="selected" >Please Select Load Industry</option>
      				<?php echo $this->_tpl_vars['search'][0]['category']; ?>

                    <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                       
                    <option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['search'][0]['category'] == $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']): ?> selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
                  <?php endfor; endif; ?>
                   </select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Load Type</label>
        <div class="col-sm-10">
         
         <span id="sub_cat">
                     <select class="form-control"  name="sub_cat_id" id="sub_cat_id"  >
                    
        <option value="please Select" selected="selected" >Please Select Load Type</option>
           
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit11']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['useredit11'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['search'][0]['sub_category'] == $this->_tpl_vars['useredit11'][$this->_sections['data']['index']]['id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['useredit11'][$this->_sections['data']['index']]['category']; ?>
 </option>
        <?php endfor; endif; ?>
     
</select>
      
      </div>
  </div>
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Transport Type</label>
        <div class="col-sm-10">
         
         <span id="sub_next_cat">
                     <select class="form-control"  name="sub_cat_id" id="sub_next_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Transport Type</option>
           
        <?php unset($this->_sections['data1']);
$this->_sections['data1']['name'] = 'data1';
$this->_sections['data1']['loop'] = is_array($_loop=$this->_tpl_vars['sub_cat_edit']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data1']['show'] = true;
$this->_sections['data1']['max'] = $this->_sections['data1']['loop'];
$this->_sections['data1']['step'] = 1;
$this->_sections['data1']['start'] = $this->_sections['data1']['step'] > 0 ? 0 : $this->_sections['data1']['loop']-1;
if ($this->_sections['data1']['show']) {
    $this->_sections['data1']['total'] = $this->_sections['data1']['loop'];
    if ($this->_sections['data1']['total'] == 0)
        $this->_sections['data1']['show'] = false;
} else
    $this->_sections['data1']['total'] = 0;
if ($this->_sections['data1']['show']):

            for ($this->_sections['data1']['index'] = $this->_sections['data1']['start'], $this->_sections['data1']['iteration'] = 1;
                 $this->_sections['data1']['iteration'] <= $this->_sections['data1']['total'];
                 $this->_sections['data1']['index'] += $this->_sections['data1']['step'], $this->_sections['data1']['iteration']++):
$this->_sections['data1']['rownum'] = $this->_sections['data1']['iteration'];
$this->_sections['data1']['index_prev'] = $this->_sections['data1']['index'] - $this->_sections['data1']['step'];
$this->_sections['data1']['index_next'] = $this->_sections['data1']['index'] + $this->_sections['data1']['step'];
$this->_sections['data1']['first']      = ($this->_sections['data1']['iteration'] == 1);
$this->_sections['data1']['last']       = ($this->_sections['data1']['iteration'] == $this->_sections['data1']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['sub_cat_edit'][$this->_sections['data1']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['search'][0]['parent_sub_id'] == $this->_tpl_vars['sub_cat_edit'][$this->_sections['data1']['index']]['id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['sub_cat_edit'][$this->_sections['data1']['index']]['category']; ?>
 </option>
        <?php endfor; endif; ?>
     
</select>
      
      </div>
  </div>
 
 
 
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Body & Bulk Type</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Body & Bulk Type</option>
           
        <?php unset($this->_sections['last_data']);
$this->_sections['last_data']['name'] = 'last_data';
$this->_sections['last_data']['loop'] = is_array($_loop=$this->_tpl_vars['last_cat_edit']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['last_data']['show'] = true;
$this->_sections['last_data']['max'] = $this->_sections['last_data']['loop'];
$this->_sections['last_data']['step'] = 1;
$this->_sections['last_data']['start'] = $this->_sections['last_data']['step'] > 0 ? 0 : $this->_sections['last_data']['loop']-1;
if ($this->_sections['last_data']['show']) {
    $this->_sections['last_data']['total'] = $this->_sections['last_data']['loop'];
    if ($this->_sections['last_data']['total'] == 0)
        $this->_sections['last_data']['show'] = false;
} else
    $this->_sections['last_data']['total'] = 0;
if ($this->_sections['last_data']['show']):

            for ($this->_sections['last_data']['index'] = $this->_sections['last_data']['start'], $this->_sections['last_data']['iteration'] = 1;
                 $this->_sections['last_data']['iteration'] <= $this->_sections['last_data']['total'];
                 $this->_sections['last_data']['index'] += $this->_sections['last_data']['step'], $this->_sections['last_data']['iteration']++):
$this->_sections['last_data']['rownum'] = $this->_sections['last_data']['iteration'];
$this->_sections['last_data']['index_prev'] = $this->_sections['last_data']['index'] - $this->_sections['last_data']['step'];
$this->_sections['last_data']['index_next'] = $this->_sections['last_data']['index'] + $this->_sections['last_data']['step'];
$this->_sections['last_data']['first']      = ($this->_sections['last_data']['iteration'] == 1);
$this->_sections['last_data']['last']       = ($this->_sections['last_data']['iteration'] == $this->_sections['last_data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['last_cat_edit'][$this->_sections['last_data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['search'][0]['last_cat'] == $this->_tpl_vars['last_cat_edit'][$this->_sections['last_data']['index']]['id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['last_cat_edit'][$this->_sections['last_data']['index']]['category']; ?>
 </option>
        <?php endfor; endif; ?>
     
</select>
      
      </div>
  </div>
  
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Size Classification</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Size Classification</option>
           
        <?php unset($this->_sections['fifth_data']);
$this->_sections['fifth_data']['name'] = 'fifth_data';
$this->_sections['fifth_data']['loop'] = is_array($_loop=$this->_tpl_vars['fifth_cat_edit']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['fifth_data']['show'] = true;
$this->_sections['fifth_data']['max'] = $this->_sections['fifth_data']['loop'];
$this->_sections['fifth_data']['step'] = 1;
$this->_sections['fifth_data']['start'] = $this->_sections['fifth_data']['step'] > 0 ? 0 : $this->_sections['fifth_data']['loop']-1;
if ($this->_sections['fifth_data']['show']) {
    $this->_sections['fifth_data']['total'] = $this->_sections['fifth_data']['loop'];
    if ($this->_sections['fifth_data']['total'] == 0)
        $this->_sections['fifth_data']['show'] = false;
} else
    $this->_sections['fifth_data']['total'] = 0;
if ($this->_sections['fifth_data']['show']):

            for ($this->_sections['fifth_data']['index'] = $this->_sections['fifth_data']['start'], $this->_sections['fifth_data']['iteration'] = 1;
                 $this->_sections['fifth_data']['iteration'] <= $this->_sections['fifth_data']['total'];
                 $this->_sections['fifth_data']['index'] += $this->_sections['fifth_data']['step'], $this->_sections['fifth_data']['iteration']++):
$this->_sections['fifth_data']['rownum'] = $this->_sections['fifth_data']['iteration'];
$this->_sections['fifth_data']['index_prev'] = $this->_sections['fifth_data']['index'] - $this->_sections['fifth_data']['step'];
$this->_sections['fifth_data']['index_next'] = $this->_sections['fifth_data']['index'] + $this->_sections['fifth_data']['step'];
$this->_sections['fifth_data']['first']      = ($this->_sections['fifth_data']['iteration'] == 1);
$this->_sections['fifth_data']['last']       = ($this->_sections['fifth_data']['iteration'] == $this->_sections['fifth_data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['fifth_cat_edit'][$this->_sections['fifth_data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['search'][0]['fifth_cat'] == $this->_tpl_vars['fifth_cat_edit'][$this->_sections['fifth_data']['index']]['id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['fifth_cat_edit'][$this->_sections['fifth_data']['index']]['category']; ?>
 </option>
        <?php endfor; endif; ?>
     
</select>
      
      </div>
  </div>
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Payload KG</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Payload KG</option>
           
        <?php unset($this->_sections['sixth_data']);
$this->_sections['sixth_data']['name'] = 'sixth_data';
$this->_sections['sixth_data']['loop'] = is_array($_loop=$this->_tpl_vars['sixth_cat_edit']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sixth_data']['show'] = true;
$this->_sections['sixth_data']['max'] = $this->_sections['sixth_data']['loop'];
$this->_sections['sixth_data']['step'] = 1;
$this->_sections['sixth_data']['start'] = $this->_sections['sixth_data']['step'] > 0 ? 0 : $this->_sections['sixth_data']['loop']-1;
if ($this->_sections['sixth_data']['show']) {
    $this->_sections['sixth_data']['total'] = $this->_sections['sixth_data']['loop'];
    if ($this->_sections['sixth_data']['total'] == 0)
        $this->_sections['sixth_data']['show'] = false;
} else
    $this->_sections['sixth_data']['total'] = 0;
if ($this->_sections['sixth_data']['show']):

            for ($this->_sections['sixth_data']['index'] = $this->_sections['sixth_data']['start'], $this->_sections['sixth_data']['iteration'] = 1;
                 $this->_sections['sixth_data']['iteration'] <= $this->_sections['sixth_data']['total'];
                 $this->_sections['sixth_data']['index'] += $this->_sections['sixth_data']['step'], $this->_sections['sixth_data']['iteration']++):
$this->_sections['sixth_data']['rownum'] = $this->_sections['sixth_data']['iteration'];
$this->_sections['sixth_data']['index_prev'] = $this->_sections['sixth_data']['index'] - $this->_sections['sixth_data']['step'];
$this->_sections['sixth_data']['index_next'] = $this->_sections['sixth_data']['index'] + $this->_sections['sixth_data']['step'];
$this->_sections['sixth_data']['first']      = ($this->_sections['sixth_data']['iteration'] == 1);
$this->_sections['sixth_data']['last']       = ($this->_sections['sixth_data']['iteration'] == $this->_sections['sixth_data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['sixth_cat_edit'][$this->_sections['sixth_data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['search'][0]['sixth_cat'] == $this->_tpl_vars['sixth_cat_edit'][$this->_sections['sixth_data']['index']]['id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['sixth_cat_edit'][$this->_sections['sixth_data']['index']]['category']; ?>
 </option>
        <?php endfor; endif; ?>
     
</select>
      
      </div>
  </div>
  
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Length Meters</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Length Meters</option>
           
        <?php unset($this->_sections['seventh_data']);
$this->_sections['seventh_data']['name'] = 'seventh_data';
$this->_sections['seventh_data']['loop'] = is_array($_loop=$this->_tpl_vars['seventh_cat_edit']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['seventh_data']['show'] = true;
$this->_sections['seventh_data']['max'] = $this->_sections['seventh_data']['loop'];
$this->_sections['seventh_data']['step'] = 1;
$this->_sections['seventh_data']['start'] = $this->_sections['seventh_data']['step'] > 0 ? 0 : $this->_sections['seventh_data']['loop']-1;
if ($this->_sections['seventh_data']['show']) {
    $this->_sections['seventh_data']['total'] = $this->_sections['seventh_data']['loop'];
    if ($this->_sections['seventh_data']['total'] == 0)
        $this->_sections['seventh_data']['show'] = false;
} else
    $this->_sections['seventh_data']['total'] = 0;
if ($this->_sections['seventh_data']['show']):

            for ($this->_sections['seventh_data']['index'] = $this->_sections['seventh_data']['start'], $this->_sections['seventh_data']['iteration'] = 1;
                 $this->_sections['seventh_data']['iteration'] <= $this->_sections['seventh_data']['total'];
                 $this->_sections['seventh_data']['index'] += $this->_sections['seventh_data']['step'], $this->_sections['seventh_data']['iteration']++):
$this->_sections['seventh_data']['rownum'] = $this->_sections['seventh_data']['iteration'];
$this->_sections['seventh_data']['index_prev'] = $this->_sections['seventh_data']['index'] - $this->_sections['seventh_data']['step'];
$this->_sections['seventh_data']['index_next'] = $this->_sections['seventh_data']['index'] + $this->_sections['seventh_data']['step'];
$this->_sections['seventh_data']['first']      = ($this->_sections['seventh_data']['iteration'] == 1);
$this->_sections['seventh_data']['last']       = ($this->_sections['seventh_data']['iteration'] == $this->_sections['seventh_data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['seventh_cat_edit'][$this->_sections['seventh_data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['search'][0]['seventh_cat'] == $this->_tpl_vars['seventh_cat_edit'][$this->_sections['seventh_data']['index']]['id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['seventh_cat_edit'][$this->_sections['seventh_data']['index']]['category']; ?>
 </option>
        <?php endfor; endif; ?>
     
</select>
      
      </div>
  </div>
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Width Meters</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Width Meters</option>
           
        <?php unset($this->_sections['eigth_data']);
$this->_sections['eigth_data']['name'] = 'eigth_data';
$this->_sections['eigth_data']['loop'] = is_array($_loop=$this->_tpl_vars['eight_cat_edit']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['eigth_data']['show'] = true;
$this->_sections['eigth_data']['max'] = $this->_sections['eigth_data']['loop'];
$this->_sections['eigth_data']['step'] = 1;
$this->_sections['eigth_data']['start'] = $this->_sections['eigth_data']['step'] > 0 ? 0 : $this->_sections['eigth_data']['loop']-1;
if ($this->_sections['eigth_data']['show']) {
    $this->_sections['eigth_data']['total'] = $this->_sections['eigth_data']['loop'];
    if ($this->_sections['eigth_data']['total'] == 0)
        $this->_sections['eigth_data']['show'] = false;
} else
    $this->_sections['eigth_data']['total'] = 0;
if ($this->_sections['eigth_data']['show']):

            for ($this->_sections['eigth_data']['index'] = $this->_sections['eigth_data']['start'], $this->_sections['eigth_data']['iteration'] = 1;
                 $this->_sections['eigth_data']['iteration'] <= $this->_sections['eigth_data']['total'];
                 $this->_sections['eigth_data']['index'] += $this->_sections['eigth_data']['step'], $this->_sections['eigth_data']['iteration']++):
$this->_sections['eigth_data']['rownum'] = $this->_sections['eigth_data']['iteration'];
$this->_sections['eigth_data']['index_prev'] = $this->_sections['eigth_data']['index'] - $this->_sections['eigth_data']['step'];
$this->_sections['eigth_data']['index_next'] = $this->_sections['eigth_data']['index'] + $this->_sections['eigth_data']['step'];
$this->_sections['eigth_data']['first']      = ($this->_sections['eigth_data']['iteration'] == 1);
$this->_sections['eigth_data']['last']       = ($this->_sections['eigth_data']['iteration'] == $this->_sections['eigth_data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['eight_cat_edit'][$this->_sections['eigth_data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['search'][0]['eight_cat'] == $this->_tpl_vars['eight_cat_edit'][$this->_sections['eigth_data']['index']]['id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['eight_cat_edit'][$this->_sections['eigth_data']['index']]['category']; ?>
 </option>
        <?php endfor; endif; ?>
     
</select>
      
      </div>
  </div>
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Preferred Distance</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Preferred Distance</option>
           
        <?php unset($this->_sections['ninth_data']);
$this->_sections['ninth_data']['name'] = 'ninth_data';
$this->_sections['ninth_data']['loop'] = is_array($_loop=$this->_tpl_vars['ninth_cat_edit']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['ninth_data']['show'] = true;
$this->_sections['ninth_data']['max'] = $this->_sections['ninth_data']['loop'];
$this->_sections['ninth_data']['step'] = 1;
$this->_sections['ninth_data']['start'] = $this->_sections['ninth_data']['step'] > 0 ? 0 : $this->_sections['ninth_data']['loop']-1;
if ($this->_sections['ninth_data']['show']) {
    $this->_sections['ninth_data']['total'] = $this->_sections['ninth_data']['loop'];
    if ($this->_sections['ninth_data']['total'] == 0)
        $this->_sections['ninth_data']['show'] = false;
} else
    $this->_sections['ninth_data']['total'] = 0;
if ($this->_sections['ninth_data']['show']):

            for ($this->_sections['ninth_data']['index'] = $this->_sections['ninth_data']['start'], $this->_sections['ninth_data']['iteration'] = 1;
                 $this->_sections['ninth_data']['iteration'] <= $this->_sections['ninth_data']['total'];
                 $this->_sections['ninth_data']['index'] += $this->_sections['ninth_data']['step'], $this->_sections['ninth_data']['iteration']++):
$this->_sections['ninth_data']['rownum'] = $this->_sections['ninth_data']['iteration'];
$this->_sections['ninth_data']['index_prev'] = $this->_sections['ninth_data']['index'] - $this->_sections['ninth_data']['step'];
$this->_sections['ninth_data']['index_next'] = $this->_sections['ninth_data']['index'] + $this->_sections['ninth_data']['step'];
$this->_sections['ninth_data']['first']      = ($this->_sections['ninth_data']['iteration'] == 1);
$this->_sections['ninth_data']['last']       = ($this->_sections['ninth_data']['iteration'] == $this->_sections['ninth_data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['ninth_cat_edit'][$this->_sections['ninth_data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['search'][0]['ninth_cat'] == $this->_tpl_vars['ninth_cat_edit'][$this->_sections['ninth_data']['index']]['id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['ninth_cat_edit'][$this->_sections['ninth_data']['index']]['category']; ?>
 </option>
        <?php endfor; endif; ?>
     
</select>
      
      </div>
  </div>
  
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Other</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select</option>
           
        <?php unset($this->_sections['tenth_data']);
$this->_sections['tenth_data']['name'] = 'tenth_data';
$this->_sections['tenth_data']['loop'] = is_array($_loop=$this->_tpl_vars['tenth_cat_edit']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['tenth_data']['show'] = true;
$this->_sections['tenth_data']['max'] = $this->_sections['tenth_data']['loop'];
$this->_sections['tenth_data']['step'] = 1;
$this->_sections['tenth_data']['start'] = $this->_sections['tenth_data']['step'] > 0 ? 0 : $this->_sections['tenth_data']['loop']-1;
if ($this->_sections['tenth_data']['show']) {
    $this->_sections['tenth_data']['total'] = $this->_sections['tenth_data']['loop'];
    if ($this->_sections['tenth_data']['total'] == 0)
        $this->_sections['tenth_data']['show'] = false;
} else
    $this->_sections['tenth_data']['total'] = 0;
if ($this->_sections['tenth_data']['show']):

            for ($this->_sections['tenth_data']['index'] = $this->_sections['tenth_data']['start'], $this->_sections['tenth_data']['iteration'] = 1;
                 $this->_sections['tenth_data']['iteration'] <= $this->_sections['tenth_data']['total'];
                 $this->_sections['tenth_data']['index'] += $this->_sections['tenth_data']['step'], $this->_sections['tenth_data']['iteration']++):
$this->_sections['tenth_data']['rownum'] = $this->_sections['tenth_data']['iteration'];
$this->_sections['tenth_data']['index_prev'] = $this->_sections['tenth_data']['index'] - $this->_sections['tenth_data']['step'];
$this->_sections['tenth_data']['index_next'] = $this->_sections['tenth_data']['index'] + $this->_sections['tenth_data']['step'];
$this->_sections['tenth_data']['first']      = ($this->_sections['tenth_data']['iteration'] == 1);
$this->_sections['tenth_data']['last']       = ($this->_sections['tenth_data']['iteration'] == $this->_sections['tenth_data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['tenth_cat_edit'][$this->_sections['tenth_data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['search'][0]['tenth_cat'] == $this->_tpl_vars['tenth_cat_edit'][$this->_sections['tenth_data']['index']]['id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['tenth_cat_edit'][$this->_sections['tenth_data']['index']]['category']; ?>
 </option>
        <?php endfor; endif; ?>
     
</select>
      
      </div>
  </div>
 
  
 <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Dimensions</label>
        <div class="col-sm-10">
         <select name="Dimensions"  id="Dimensions" class="form-control border-radius" >
          <option value="please Select" selected="selected" >please Select</option>
                    <option value="Feet" <?php if ($this->_tpl_vars['search'][0]['dimensions'] == 'Feet'): ?> selected="selected" <?php endif; ?> >Feet</option>
      				 <option value="Inches" <?php if ($this->_tpl_vars['search'][0]['dimensions'] == 'Inches'): ?> selected="selected" <?php endif; ?> >Inches</option>
                      <option value="Centimeters" <?php if ($this->_tpl_vars['search'][0]['dimensions'] == 'Centimeters'): ?> selected="selected" <?php endif; ?> >Centimeters</option>   
                     <option value="Meters" <?php if ($this->_tpl_vars['search'][0]['dimensions'] == 'Meters'): ?> selected="selected" <?php endif; ?> >Meters</option>   
                 
                   </select>
      
      </div>
  </div>
   <table >
  <tr><td width="20%;"></td><td>
  <table>
  <tr><td>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Width</label>
       <div class="col-xs-2">
         <input type="text" name="Width" id="Width" value="<?php echo $this->_tpl_vars['search'][0]['Width']; ?>
" class="form-control" style="min-width:70px;" placeholder="Width">
        </div>
              
  </div>
  </td>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Height</label>
       <div class="col-xs-2">
          <input type="text" name="Height" id="Height" value="<?php echo $this->_tpl_vars['search'][0]['Height']; ?>
" class="form-control" style="min-width:70px;" placeholder="Height">
        </div>
        
  </div>
  </td>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Length</label>
       <div class="col-xs-2">
         <input type="text" name="Length" id="Length" value="<?php echo $this->_tpl_vars['search'][0]['Length']; ?>
" class="form-control" style="min-width:70px;" placeholder="Length">
        </div>
        
  </div>
  </td>
  </tr>
  <tr>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Weight</label>
       <div class="col-xs-2">
       <input type="text" name="Weight" id="Weight" value="<?php echo $this->_tpl_vars['search'][0]['Weight']; ?>
" class="form-control" style="min-width:70px;" placeholder="Weight">
        </div>
        
  </div>
  </td>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Quantity</label>
       <div class="col-xs-2">
       <input type="text" name="Qty" id="Qty" value="<?php echo $this->_tpl_vars['search'][0]['Qty']; ?>
" class="form-control" style="min-width:70px;" placeholder="Quantity">
        </div>
        
  </div>
 </td>
 <td>
 <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Value</label>
       <div class="col-xs-2">
       <input type="text" name="value" id="value" value="<?php echo $this->_tpl_vars['search'][0]['value']; ?>
" class="form-control" style="min-width:70px;" placeholder="">
        </div>
        
  </div>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Shipment Image</label>
      <div class="col-sm-10">
      <input type="file" name="logo" class="btn btn-default" style="max-width:240px;"/>
       <input type="hidden" name="old_main_img" value="<?php echo $this->_tpl_vars['search'][0]['img']; ?>
" />
         </div>
 
  </div>
   <?php if ($this->_tpl_vars['search'][0]['img'] != ''): ?>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label"></label>
      <div class="col-sm-10" style="text-align:left;" >
     <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/shipment_image/<?php echo $this->_tpl_vars['search'][0]['img']; ?>
" width="100" />
     </div>
    </div>
    <?php endif; ?>
 <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Description</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="description" rows="3"><?php echo $this->_tpl_vars['search'][0]['description']; ?>
</textarea>
     </div>
   
        
  </div>
     
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="submit" class="btn btn-default" onclick="return valid_post(document.post_shipment);">Submit</button>
        </div>
      </div>
    </form>
  </div><!-- /.bs-example -->

  
     
   

</div>
        </div>
    </div>
    <!--container end-->