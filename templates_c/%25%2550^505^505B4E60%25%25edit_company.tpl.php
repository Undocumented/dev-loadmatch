<?php /* Smarty version 2.6.19, created on 2017-08-18 03:00:04
         compiled from admin/edit_company.tpl */ ?>
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2> Edit Company</h2>
      </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading"> Edit Company
         
          
          
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
                
                <!--===============customer form======================-->
                <form role="form" name="company" id="company" action="" method="post"  enctype="multipart/form-data">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id']; ?>
" />
                  <input type="hidden" name="edit_mobile" id="edit_mobile" value="<?php echo $this->_tpl_vars['edit_query_data'][0]['customer_mobile']; ?>
" />
                  <div class="form-group">
                    <label>Company Name</label>
                    <input class="form-control" type="text" name="company_name" id="alert_company_name" value="<?php echo $this->_tpl_vars['edit_data'][0]['company_name']; ?>
" placeholder="Enter Company Name " />
                   </div>
                  <div class="form-group">
                    <label>Company Email</label>
                    <input class="form-control" type="email" name="email" id="alert_email" value="<?php echo $this->_tpl_vars['edit_data'][0]['email']; ?>
" placeholder="Enter Company Email " />
                    </div>
                 
                      <div class="form-group">
                    <label > Support Email</label>
                    <input class="form-control" type="email" name="support_email" id="alert_support_email" value="<?php echo $this->_tpl_vars['edit_data'][0]['support_email']; ?>
" placeholder="Enter Company Support Email " />
                   </div>
                      <div class="form-group">
                    <label > Webmaster Email</label>
                    <input class="form-control" type="email" name="webmaster_email" id="alert_webmaster_email" value="<?php echo $this->_tpl_vars['edit_data'][0]['webmaster_email']; ?>
" placeholder="Enter Company  Webmaster Email " />
                   </div>
                   
                    <div class="form-group">
                    <label > Mobile Number</label>
                    <input class="form-control" type="text" name="mobile_no" id="alert_mobile" value="<?php echo $this->_tpl_vars['edit_data'][0]['mobile_no']; ?>
" placeholder="Enter Company Mobile Number " />
                   </div>
                   
                     <div class="form-group">
                    <label > Phone Number</label>
                    <input class="form-control" type="text" name="phone" id="alert_phone" value="<?php echo $this->_tpl_vars['edit_data'][0]['phone']; ?>
" placeholder="Enter Company Mobile Number " />
                   </div>
                   
                    <div class="form-group">
                    <label >Company  Fax</label>
                    <input class="form-control" type="text" name="fax" id="alert_mobile" value="<?php echo $this->_tpl_vars['edit_data'][0]['fax']; ?>
" placeholder="Enter Company  Webmaster Email " />
                   </div>
                  
                 
                
                  <div class="form-group">
                    <label>Company Address</label>
                    <input class="form-control" type="text" name="company_address" id="alert_company_address" value="<?php echo $this->_tpl_vars['edit_data'][0]['company_address']; ?>
" placeholder="Enter Address Number "  />
                 
                  </div>
                  
                  <div class="form-group">
                    <label>Page Row</label>
                    <input class="form-control" type="text" name="page_row" id="alert_page_row" value="<?php echo $this->_tpl_vars['edit_data'][0]['page_row']; ?>
" placeholder="Enter Site Page Row"  />
                 
                  </div>
                  
                     <div class="form-group">
                    <label>About Us Content</label>
                    <textarea class="form-control" name="about_us" id="alert_about_us" rows="3" placeholder="Enter About Us Content"><?php echo $this->_tpl_vars['edit_data'][0]['about_us']; ?>
</textarea>
                   
                 
                  </div>
                  
                    <div class="form-group">
                    <label>Logo</label>
                   <input type="file" name="logo" />
                   <input type="hidden" name="old_main_img" value="<?php echo $this->_tpl_vars['edit_data'][0]['logo']; ?>
" />
                   <?php if ($this->_tpl_vars['edit_data'][0]['logo'] != ''): ?>
                   <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/<?php echo $this->_tpl_vars['edit_data'][0]['logo']; ?>
" width="100px" />
                   <?php endif; ?>
                   <span class="alert" id="alert_logo"></span>
                   
                 
                  </div>
                  <button type="submit" class="btn btn-primary" name="company_btn" value="add_new_customer" onclick="return add_valid_comapny(document.company);">  Update Company</button>
                </form>
                <!--==================================================--> 
                
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>