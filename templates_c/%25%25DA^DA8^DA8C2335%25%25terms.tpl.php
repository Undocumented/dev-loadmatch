<?php /* Smarty version 2.6.19, created on 2017-08-21 23:05:50
         compiled from terms.tpl */ ?>
 <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-sm-4" style="margin-top:60px;">
            <h1>
             Term & condition
            </h1>
          </div>
          <div class="col-lg-8 col-sm-8">
            <ol class="breadcrumb pull-right">
              <li>
                <a href="index.html">
                  Home
                </a>
              </li>
             
              <li class="active">
              Term & condition
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="container" >
      <div class="row">
</div>


    <b>Introduction</b> 
<p>
This document describes the terms with which LoadMatch offers you access to our services. This agreement describes the terms and conditions applicable to your use of our available services. The User Agreement constitutes a legally binding agreement between you and LoadMatch. Please ensure that you read and understand these terms since you will be bound by it once a contract comes into effect by means of subscription. These terms and conditions govern your use of this website and by using this website, you therefore accept these terms and conditions in full. If you disagree with these terms and conditions or any part of these terms and conditions, please do not use this website.
<p>
<br>
This website uses cookies.  By using this website and agreeing to these terms and conditions, you consent to LOADMATCH’s use of cookies in accordance with the terms of LoadMatch's privacy and cookies policies.
LoadMatch provides prequalification and validation upon both Customer and TSP registration but cannot guarantee validity of information and proof of documentation supplied by Customers and TSPs with respect to their licensure, insurance and registration. We do have an external vetting process linked, so we attempt to make every attempt to ensure that subscribers are certified service providers. As a result, LoadMatch is a Neutral Party and not a Transportation Service Provider (TSP), transport company, freight forwarder or broker. Our site acts as a portal where Customers and TSPs can meet and enter into agreements. We are not involved in the actual transaction between Customers and TSPs. We and our website function solely as a neutral venue and digital clearinghouse where two parties may agree on a price for a particular type of service. We are the neutral venue for this connection between you and other members. We do not provide any endorsement for you or your services, you acknowledge and agree that we do not provide the services and we are not in any way responsible for assisting you in any manner with your provision of the services. As a result, we have no control over the quality, safety, or legal aspects of the transactions that take place on our website. We cannot and will not guarantee the ability of members to complete payment for any of the provided services. Furthermore, due to the difficulty of individual authentication, especially on the Internet, we cannot and will not in any manner verify or confirm the identity or ability of members to pay for the provided services. You acknowledge and agree that any and all communications, correspondence, verbal or written, or any warranties or representations, made with regard to the services are not provided by us and are specifically and solely between you and the other member. Because we are not involved in the actual transaction between delivery customers and TSPs, we have no control over the accuracy of listings, the ability of TSPs to transport items, or the ability of delivery customers to send items. We cannot ensure that a delivery customer or TSP will actually complete a delivery.
<p>
<br>
<b>License to use website</b>
<p>
    Unless otherwise stated, LoadMatch and/or its licensors own the intellectual property rights in the website and material on the website.  Subject to the license below, all these intellectual property rights are reserved.
<p>
<br>
You may view, download for caching purposes only, and print pages or other content from the website for your own personal use, subject to the restrictions set out below and elsewhere in these terms and conditions. You must not:
<p>
•	republish material from this website (including republication on another website);
<p>
•	sell, rent or sub-license material from the website;
<p>
•	show any material from the website in public;
<p>
•	reproduce, duplicate, copy or otherwise exploit material on this website for a commercial purpose;
<p>
•	edit or otherwise modify any material on the website; or
<p>
•	redistribute material from this website [except for content specifically and expressly made available for redistribution.
<p>
<br>
Where content is specifically made available for redistribution, it may only be redistributed within your organisation.
<p>
<br>
<b>Acceptable use</b>
<p>
You must not
<p>
•	use this website in any way that causes, or may cause, damage to the website or impairment of the availability or accessibility of the website; or in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity.
<p>
•	use this website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, trojan horse, worm, keystroke logger, rootkit or other malicious computer software.
<p>
•	conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to this website without LoadMatch’s express written consent.
<p>
•	use this website to transmit or send unsolicited commercial communications.
<p>
•	use this website for any purposes related to marketing without LoadMatch’s express written consent.
<p>
<br>
Making use of any tools on this website does not make LoadMatch liable in any way or manner should the tools not produce desired results. Any transactions that take place between users of this system is entirely between these parties and LoadMatch cannot be held liable in any way or manner for these transactions.
<p>
<br>
<b>Restricted access</b>
<p>
Access to certain areas of this website is restricted. LoadMatch reserves the right to restrict access to other areas of this website, or indeed this entire website, at LoadMatch’s discretion.
<p>
<br>
If LoadMatch provides you with a user ID and password to enable you to access restricted areas of this website or other content or services, you must ensure that the user ID and password are kept confidential.
<p>
<br>
LoadMatch may disable your user ID and password at LoadMatch’s sole discretion without notice or explanation.
<p>
<br>
<b>User content</b>
<p>
In these terms and conditions, “your user content” means material (including without limitation text, images, audio material, video material and audio-visual material) that you submit to this website, for whatever purpose.
 <p>
<br>
You grant to LoadMatch a worldwide, irrevocable, non-exclusive, royalty-free license to use, reproduce, adapt, publish and translate your user content in any existing or future media.  You also grant to LoadMatch the right to sub-license these rights, and the right to bring an action for infringement of these rights. Distribution of you user content will only be done with prior consent.
 <p>
<br>
Your user content must not be illegal or unlawful, must not infringe any third party's legal rights, and must not be capable of giving rise to legal action whether against you or LoadMatch or a third party (in each case under any applicable law).
<p>
<br>
You must not submit any user content to the website that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint.
<p>
<br>
LoadMatch reserves the right to edit or remove any material submitted to this website, or stored on LoadMatch servers, or hosted or published on this website.
<p>
<br>
Notwithstanding LoadMatch’s rights under these terms and conditions in relation to user content, LoadMatch does not undertake to monitor the submission of such content to, or the publication of such content on, this website.
<p>
<br>
<b>No warranties</b>
<p>
This website is provided “as is” without any representations or warranties, express or implied.  LoadMatch makes no representations or warranties in relation to this website or the information and materials provided on this website.
<p>
<br>
•	this website will be constantly available, or available at all; or
•	the information on this website is complete, true, accurate or non-misleading.
<p>
<br>
Nothing on this website constitutes, or is meant to constitute, advice of any kind.  If you require advice in relation to any legal, financial or medical matter you should consult an appropriate professional.
<p>
<br>
<b>Limitations of liability</b>
<p>
LoadMatch will not be liable to you (whether under the law of contact, the law of torts or otherwise) in relation to the contents of, or use of, or otherwise in connection with, this website:
 <p>
<br>
•	to the extent that the website is provided free-of-charge, for any direct loss;
<p>
<br>
•	for any indirect, special or consequential loss; or
<p>
<br>
•	for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships; or
<p>
<br>
•	loss of reputation or goodwill, or loss or corruption of information or data.
<p>
<br>
These limitations of liability apply even if LoadMatch has been expressly advised of the potential loss.
<p>
<br>
<b>Exceptions</b>
<p>
Nothing in this website disclaimer will exclude or limit any warranty implied by law that it would be unlawful to exclude or limit; and nothing in this website disclaimer will exclude or limit LoadMatch’s liability in respect of any:
These limitations of liability apply even if LoadMatch has been expressly advised of the potential loss.
<p>
•	death or personal injury caused by LoadMatch negligence;
<p>
•	fraud or fraudulent misrepresentation on the part of LoadMatch; or
<p>
•	matter which it would be illegal or unlawful for LoadMatch to exclude or limit, or to attempt or purport to exclude or limit its liability.
<p>
<br>
<b>Reasonableness</b>
<p>
By using this website, you agree that the exclusions and limitations of liability set out in this website disclaimer are reasonable.  If you do not think they are reasonable, you must not use this website.
<p>
<br>
<b>Other parties</b>
<p>
You accept that, as a limited liability entity, LoadMatch has an interest in limiting the personal liability of its officers and employees.  You agree that you will not bring any claim personally against LoadMatch’s officers or employees in respect of any losses you suffer in connection with the website.
<p>
<br>
Without prejudice to the foregoing paragraph, you agree that the limitations of warranties and liability set out in this website disclaimer will protect LoadMatch’s officers, employees, agents, subsidiaries, successors, assigns and sub-contractors as well as LoadMatch.
<p>
<br>
<b>Unenforceable provisions</b>
<p>
If any provision of this website disclaimer is, or is found to be, unenforceable under applicable law, that will not affect the enforceability of the other provisions of this website disclaimer.
<p>
<br>
<b>Indemnity</b>
<p>
You hereby indemnify LoadMatch and undertake to keep LoadMatch indemnified against any losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by LoadMatch to a third party in settlement of a claim or dispute on the advice of LoadMatch’s legal advisers) incurred or suffered by LoadMatch arising out of any breach by you of any provision of these terms and conditions, or arising out of any claim that you have breached any provision of these terms and conditions.
<p>
<br>
<b>Breach of these terms and conditions</b>
<p>
Without prejudice to LoadMatch’s other rights under these terms and conditions, if you breach these terms and conditions in any way, LoadMatch may take such action as LoadMatch deems appropriate to deal with the breach, including suspending your access to the website, prohibiting you from accessing the website, blocking computers using your IP address from accessing the website, contacting your internet service provider to request that they block your access to the website and/or bringing court proceedings against you.
<p>
<br>
<b>Variation</b>
<p>
LoadMatch may revise these terms and conditions from time-to-time.  Revised terms and conditions will apply to the use of this website from the date of the publication of the revised terms and conditions on this website.  Please check this page regularly to ensure you are familiar with the current version.
<p>
<br>
<b>Assignment</b>
<p>
LoadMatch may transfer, sub-contract or otherwise deal with LoadMatch’s rights and/or obligations under these terms and conditions without notifying you or obtaining your consent.
<p>
<br>
You may not transfer, sub-contract or otherwise deal with your rights and/or obligations under these terms and conditions.  
<p>
<br>
<b>Severability</b>
<p>
If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.  If any unlawful and/or unenforceable provision would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect. 
<p>
<br>
<b>Entire agreement</b>
<p>
These terms and conditions, together with relevant documents, constitute the entire agreement between you and LoadMatch in relation to your use of this website, and supersede all previous agreements in respect of your use of this website.
<p>
<br>
<b>Law and jurisdiction</b>
<p>
These terms and conditions will be governed by and construed in accordance with South African law, and any disputes relating to these terms and conditions will be subject to the [non-]exclusive jurisdiction of the courts of South Africa.
<p>
<br>
<b>Registrations and authorisations</b>
<p>
The full name of the organisation is LoadMatch (Pty) Ltd. LoadMatch is registered with the Companies and Intellectual Property Commission (CIPC) of South Africa. You can find the online version of the registration at www.cipc.co.za. Loadmatch’s registered address is 490 Tedstone Road, Wadeville, Germiston, Gauteng, South Africa, 1423.  
<p>
<br>
You can contact LoadMatch via email at info@loadmatch.co.za if you are unclear about any of our terms or conditions.<p>
<p>
<br>
 <!-- End row -->

      </div>
      <!-- End container -->
    </div>


    <!--container end-->