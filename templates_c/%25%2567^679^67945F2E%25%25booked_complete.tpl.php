<?php /* Smarty version 2.6.19, created on 2017-08-15 18:30:37
         compiled from booked_complete.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'booked_complete.tpl', 213, false),)), $this); ?>
<?php if ($_SESSION['user_type'] == 'C'): ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Complete Shipment </h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Complete Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
           My Complete Shipment
          </h4>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/edit_post_shipping" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                     <th>
                     Transporter
                    </th>
                    <th>
                      Collection
                    </th>
                    <th>
                      Delivery
                    </th>
                    <th>
                     Category
                    </th>
                   <!-- <th>
                     Under Category
                    </th>-->
                     <th>
                     Status
                    </th>
                   <th>
                     Action
                    </th>
                    <th>
                     Feed Back
                    </th>
                  </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['search']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>

                    </td>
                     <td>
                       <?php echo $this->_tpl_vars['functions']->get_agent_name($this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']); ?>

                    </td>
                    <td>
                     <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['form_state']; ?>
, <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['form_city']; ?>

                    </td>
                    <td>
                     <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['to_state']; ?>
, <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['to_city']; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][$this->_sections['data']['index']]['category']); ?>

                    </td>
                   
                     <td>
                        <?php if ($this->_tpl_vars['search'][$this->_sections['data']['index']]['transport_status'] != ''): ?><div class="btn btn-success btn-sm"><?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['transport_status']; ?>
 </div><?php else: ?> Not Awlable <?php endif; ?>
                    </td>
                 <td>
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                    <a style="cursor:pointer;" onclick="submitbuy_book_cancel_complet('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
','ct',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                    </td>
                    <td> <?php echo $this->_tpl_vars['functions']->get_agent_id($this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']); ?>

                     <?php echo $this->_tpl_vars['functions']->get_feedback($this->_tpl_vars['agent_info_id'],$_SESSION['user_id']); ?>

                  
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                    <a style="cursor:pointer;" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"/><?php if ($this->_tpl_vars['customer_info11'][0]['user_id'] == '' && $this->_tpl_vars['customer_info11'][0]['transpoter_id'] == ''): ?>Add Feedback<?php else: ?>update<?php endif; ?></a>
                    </td>
                  </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="9" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>
</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    <?php endif; ?>
    
    <?php if ($_SESSION['user_type'] == 'T'): ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Complete Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Complete Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          Complete Shipment
          </h4>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/detail_shiping_booked" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                      Customer
                    </th>
                    <th>
                     Pick up Time
                    </th>
                    <th>
                     Delivery Time
                    </th>
                     <th>
                      Delivery Location
                    </th>
                     <th>
                     Status
                    </th>
                     <th>
                     Action
                    </th> 
                     <th>
                    Feedback
                    </th>
                  </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['search']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>

                    </td>
                     <td>
                       <?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][$this->_sections['data']['index']]['user_id']); ?>

                    </td>
                    <td>
                     <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['collect_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
, <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['collect_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                    </td>
                    <td>
                     <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['delivery_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
, <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['delivery_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                    </td>
                    <td>
                     <?php echo $this->_tpl_vars['functions']->get_destnation_state_city($this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']); ?>

                    </td>
                    <td>
                       <?php if ($this->_tpl_vars['search'][$this->_sections['data']['index']]['transport_status'] != ''): ?><div class="btn btn-success btn-sm"><?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['transport_status']; ?>
</div> <?php else: ?> Pending <?php endif; ?>
                    </td>
                     <td>
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                    <a style="cursor:pointer;" onclick="submitbuy_agent_cencle_complet('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
','<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>
','ct',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                    </td>
                    <td><?php if ($_SESSION['user_type'] == 'T'): ?>
                   	
                 <?php echo $this->_tpl_vars['functions']->get_agent_id($this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']); ?>

                 <?php echo $this->_tpl_vars['functions']->get_feedbacktrans($this->_tpl_vars['agent_info_id'],$this->_tpl_vars['search'][$this->_sections['data']['index']]['user_id']); ?>

                  
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                    <a style="cursor:pointer;" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal1"/><?php if ($this->_tpl_vars['trans_info'][0]['user_id'] == '' && $this->_tpl_vars['trans_info'][0]['transpoter_id'] == ''): ?>Add Feedback<?php else: ?>update<?php endif; ?></a>
                    <?php endif; ?>
                    </td>
                  </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>
</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    <?php endif; ?>
    
  
    
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
  <form action="" method="post" name="AddReview">  
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Review</h4>
      </div>
      <div class="modal-body">
           <div class="container">
                <div class="row">
                   <!-- <h2>Working Star Ratings for Bootstrap 3 <small>Hover and click on a star</small></h2>-->
                </div>
                <div class="row lead">
                <span id="chkreview"></span>
                	<div class="col-md-8">
                    	<div class="row p_modul_set">
                    		<div class="col-md-6">
                         <?php echo $this->_tpl_vars['functions']->get_update_Transpoter_detail($this->_tpl_vars['functions']->get_Transporter_name($this->_tpl_vars['search'][0]['order_id']),$_SESSION['user_id']); ?>

                           <input type="hidden" name="transpoter_id" value="<?php echo $this->_tpl_vars['functions']->get_Transporter_name($this->_tpl_vars['search'][0]['order_id']); ?>
" /> 
                            
                            
                           
                          
                                <p>Communication</p>
                             
                            </div>
                    
                            <div class="col-md-6">
                            <input type="hidden"   name="com_rating" id="count_value1" value="<?php echo $this->_tpl_vars['transuser'][0]['com_rating']; ?>
" required />
                   
                             
                                <div id="stars" class="starrr"></div> <span id="count">0</span>/5
                               
                            </div>
                    	</div>
                        
                        <div class="row p_modul_set">
                    		<div class="col-md-6">
                                <p>Care of Goods</p>
                            </div>
                    
                            <div class="col-md-6">
          <input type="hidden"   name="care_rating"  id="count_value2" value="<?php echo $this->_tpl_vars['transuser'][0]['care_rating']; ?>
" required />
                 
                                <div id="stars1" class="starrr"></div> <span id="count1">0</span>/5
                       
                            </div>
                    	</div>
                        
                        <div class="row p_modul_set">
                    		<div class="col-md-6">
                                <p>Punctuality</p>
                            </div>
                    
                            <div class="col-md-6">
                             <input type="hidden"   name="pun_rating" id="count_value3" value="<?php echo $this->_tpl_vars['transuser'][0]['pun_rating']; ?>
" required />
                   
                                <div id="stars2" class="starrr"></div> <span id="count2">0</span> /5
                    
                            </div>
                    	</div>
                        
                        <div class="row p_modul_set">
                    		<div class="col-md-6">
                                <p>Services as Described</p>
                            </div>
                    
                            <div class="col-md-6">
                             <input type="hidden"   name="ser_rating" id="count_value4" value="<?php echo $this->_tpl_vars['transuser'][0]['ser_rating']; ?>
" required />
                          
                                <div id="stars3" class="starrr"></div> <span id="count3">0</span> /5
                          
                            </div>
                    	</div>
                    </div>
                    
                    <div class="col-md-4">
                    	<select class="sr-bt" name="rating" id="optm">
                  <option value="">Over All Rating</option>
                  <option <?php if ($this->_tpl_vars['transuser'][0]['rating'] == natural): ?> selected="selected" <?php endif; ?> value="natural">natural</option>
                  <option <?php if ($this->_tpl_vars['transuser'][0]['rating'] == negative): ?> selected="selected" <?php endif; ?> value="negative">negative</option>
                  <option <?php if ($this->_tpl_vars['transuser'][0]['rating'] == positive): ?> selected="selected" <?php endif; ?> value="positive">positive</option>
                
                </select>
                    </div>
                    <!--You gave a rating of <span id="count">0</span> star(s)-->
                </div>
                
                <div class="row">
                	<textarea name="review" class="form-control"  placeholder="Enter Your Feedback" required><?php echo $this->_tpl_vars['transuser'][0]['review']; ?>
 </textarea>
             
                </div>
                
                
                <!--<div class="row lead">
                    <p>Also you can give a default rating by adding attribute data-rating</p>
                    <div id="stars-existing" class="starrr" data-rating='4'></div>
                    You gave a rating of <span id="count-existing">4</span> star(s)
                </div>-->
            </div>
      </div>
      <div class="modal-footer">
        <input type="submit" name="submit1" onclick="return validForm(document.AddReview);" class="btn btn-primary btn-sm"></input>
      </div>
    </div>
</form>
  </div>
</div>
<?php if ($_SESSION['user_type'] == 'T'): ?>
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
  <form action="" method="post" name="AddReview_tran">  
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Review for Customer</h4>
      </div>
      <div class="modal-body">
           <div class="container">
                <div class="row">
                   <!-- <h2>Working Star Ratings for Bootstrap 3 <small>Hover and click on a star</small></h2>-->
                </div>
                <div class="row lead">
                <span id="chkreview1"></span>
                	<div class="col-md-8">
                    	<div class="row p_modul_set">
                    		<div class="col-md-6">
                             <?php echo $this->_tpl_vars['functions']->get_update_customer_detail($this->_tpl_vars['functions']->get_Transporter_name($this->_tpl_vars['search'][0]['order_id']),$this->_tpl_vars['search'][0]['user_id']); ?>

                            
                           <input type="hidden" name="transpoter_id" value="<?php echo $this->_tpl_vars['functions']->get_Transporter_name($this->_tpl_vars['search'][0]['order_id']); ?>
" /> 
                            
                            
                     <input type="hidden" name="cust_id1" value="<?php echo $this->_tpl_vars['functions']->get_id_user($this->_tpl_vars['search'][0]['user_id']); ?>
" /> 
                            
                                <p>Communication</p>
                            </div>
                    
                            <div class="col-md-6">
                            <input type="hidden"   name="com_rating1" value="<?php echo $this->_tpl_vars['custuser'][0]['com_rating']; ?>
" id="count_valuet" />
                          
                                <div id="starst" class="starrr"></div> <span id="countt">0</span>/5
                            </div>
                    	</div>
                        
                                              
                        <div class="row p_modul_set">
                    		<div class="col-md-6">
                                <p>Punctuality</p>
                            </div>
                    
                            <div class="col-md-6">
                             <input type="hidden"   name="pun_rating3" value="<?php echo $this->_tpl_vars['custuser'][0]['pun_rating']; ?>
" id="count_valuee" />
                           
                                <div id="starsp" class="starrr"></div> <span id="countp">0</span> /5
                            </div>
                    	</div>
                        
                                           </div>
                    
                    <div class="col-md-4">
                    	<select class="sr-bt" name="rating1" id="optm">
                  <option  value="">Over All Rating</option>
                  <option <?php if ($this->_tpl_vars['custuser'][0]['rating'] == natural): ?> selected="selected" <?php endif; ?> value="natural">natural</option>
                  <option <?php if ($this->_tpl_vars['custuser'][0]['rating'] == negative): ?> selected="selected" <?php endif; ?> value="negative">negative</option>
                  <option <?php if ($this->_tpl_vars['custuser'][0]['rating'] == positive): ?> selected="selected" <?php endif; ?> value="positive">positive</option>
                                 </select>
                    </div>
                    <!--You gave a rating of <span id="count">0</span> star(s)-->
                </div>
                
                <div class="row">
                	<textarea name="reviews" class="form-control" placeholder="Enter Your Feedback" required><?php echo $this->_tpl_vars['custuser'][0]['review']; ?>
</textarea>
                    
                </div>
                
                
                <!--<div class="row lead">
                    <p>Also you can give a default rating by adding attribute data-rating</p>
                    <div id="stars-existing" class="starrr" data-rating='4'></div>
                    You gave a rating of <span id="count-existing">4</span> star(s)
                </div>-->
            </div>
      </div>
      <div class="modal-footer">
        <input type="submit" name="submit2" onclick="return validForm_trans(document.AddReview_tran);" class="btn btn-primary btn-sm"></input>
      </div>
    </div>
</form>
  </div>
</div>
<?php endif; ?>