<?php /* Smarty version 2.6.19, created on 2017-04-26 17:40:43
         compiled from admin/add_users.tpl */ ?>
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2><?php if ($this->_tpl_vars['edit_id'] == ''): ?>Add <?php else: ?>Edit<?php endif; ?>Customer/Transpoter</h2>
      </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading"><b> <?php if ($this->_tpl_vars['edit_id'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?> Customer/Transpoter</b>
       
         </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
                
                <!--===============customer form======================-->
                <form role="form" name="add_new_customer" id="add_new_customer" action="" method="post">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id']; ?>
" />
                  <input type="hidden" name="edit_mobile" id="edit_mobile" value="<?php echo $this->_tpl_vars['edit_query_data'][0]['customer_mobile']; ?>
" />
                     <h4><?php echo $this->_tpl_vars['show_message']; ?>
</h4> 
               <div class="form-group">
                    <label>User Type</label>
                     <select class="form-control"  name="user_type" id="user_type" >
                     <option value="please Select" selected="selected" >please Select</option>
                      <option value="C" <?php if ($this->_tpl_vars['useredit'][0]['type'] == 'C'): ?> selected="selected" <?php endif; ?>>Customer</option>
                      <option value="T"  <?php if ($this->_tpl_vars['useredit'][0]['type'] == 'T'): ?> selected="selected" <?php endif; ?>>Transporter</option>
                     </select>
                   </div>
                  <div class="form-group">
                    <label>Customer/Transporter Name</label>
                    <input class="form-control" type="text" name="user_name" id="user_name" value="<?php echo $this->_tpl_vars['useredit'][0]['name']; ?>
" placeholder="Enter  Name "  required/>
                   </div>
                  <div class="form-group">
                    <label>Email Address</label>
                    <input class="form-control" type="email" name="email" id="email" value="<?php echo $this->_tpl_vars['useredit'][0]['email']; ?>
" placeholder="Enter  Email " required />
                    </div>
                  <div class="form-group">
                    <label>Mobile Number</label>
                    <input class="form-control" type="text" name="mobile_no" id="mobile_no" value="<?php echo $this->_tpl_vars['useredit'][0]['mobile']; ?>
" placeholder="Enter  Mobile Number " required />
                   </div>
                  
                  <!--==========password=========-->
                  
                  <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" type="password" name="password" id="password" value="" placeholder="Enter Password "  />
                    </div>
                 <!-- <div class="form-group">
                    <label>Confirm Password</label>
                    <input class="form-control" type="password" name="confirm_password" id="alert_confirm_password" value="" placeholder="Enter Confirm Password " />
                    </div>-->
                  
                  <!--==========password finish====-->
                  
                  <div class="form-group">
                    <label>State</label>
                     <select name="state" id="state"  class="form-control" onchange="get_state_city(this.value);">
        
        <option value="please Select" selected="selected" >please Select</option>
      
						<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
  <option value="<?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['state'] == $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
 </option>
   <?php endfor; endif; ?>
               
                 
</select>
                  </div>
                  <div class="form-group">
                    <label>City</label>
                    
                    <span id="city_change">
                     <select class="form-control"  name="city" id="city" >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >please Select</option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select></span>
                  </div>
                  <div class="form-group">
                    <label>Zip Code</label>
                    <input class="form-control" type="text" name="zipcode" id="zipcode" value="<?php echo $this->_tpl_vars['useredit'][0]['zip_code']; ?>
" placeholder="Enter Your  Zipcode " required />
                
                
                  </div>
                  <div class="form-group">
                    <label>Address</label>
                    <input class="form-control" type="text" name="address" id="address" value="<?php echo $this->_tpl_vars['useredit'][0]['address']; ?>
" placeholder="Enter Address " required  />
                 
                  </div>
                  <button type="submit" class="btn btn-primary" name="user_btn" value="add_new_customer" > <?php if ($this->_tpl_vars['edit_id'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?> User</button>
                </form>
                <!--==================================================--> 
                
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>