<?php 

class payment_getway
{
    public $dbh;

    public function __construct()
    {
        $this->dbh = new db_MySQL();
        $this->dbh->Connect();
    }

    public function process()
    {
        global $objSmarty;
        global $Paypal_Email;
        $objSmarty->assign("paidtype", $_REQUEST["paidtype"]);
        if( $_REQUEST["oid"] != "" ) 
        {
            $sqlResult = "select package,amount,amount_inr from account where deleted='N' and status='Y' and order_id='" . $_REQUEST["oid"] . "'";
            $sqlResult = $this->dbh->FetchAllResults($sqlResult);
            $amount = $sqlResult[0]["amount"];
            $amount_inr = $sqlResult[0]["amount_inr"];
            $package = $sqlResult[0]["package"];
            $objSmarty->assign("oid", $_REQUEST["oid"]);
            $objSmarty->assign("Paypal_Email", $Paypal_Email);
            $objSmarty->assign("amount", $amount);
            $objSmarty->assign("amount_inr", $amount_inr);
            $objSmarty->assign("package", $package);
        }

    }

    public function updgrade_package()
    {
        global $objSmarty;
        global $SITE_EMAIL;
        global $Paypal_Email;
        global $company_name;
        if( $_REQUEST["msg"] == 2 ) 
        {
            $objSmarty->assign("succ1", "<strong>Upgrade Package Successfully</strong>.<br> <div style='font-size:13px'> your updated package first you logout and login agian</div> ");
        }

        if( isset($_REQUEST["package_id"]) ) 
        {
            $sqlResult = "select * from member_package where deleted='N' and status='Y' and id='" . $_REQUEST["package_id"] . "'";
            $sqlResult = $this->dbh->FetchAllResults($sqlResult);
            $amount = $sqlResult[0]["price"];
            $pack_name = $sqlResult[0]["package_name"];
            $pack_type = $sqlResult[0]["pack_type"];
            $sqlResult = "select max(order_id) as order_id from account where deleted='N'";
            $sqlResult = $this->dbh->FetchAllResults($sqlResult);
            $ord_id = $sqlResult[0]["order_id"];
            if( $ord_id < 1 ) 
            {
                $ord_id = 1000;
            }
            else
            {
                $ord_id = $ord_id + 1;
            }

            if( $_SESSION["user_id"] == "" && $_SESSION["fre_exp_id"] != "" ) 
            {
                $usr_ses_id = $_SESSION["fre_exp_id"];
            }
            else
            {
                $usr_ses_id = $_SESSION["user_id"];
            }

            $date = date("Y-m-d");
            $qrySavePage = "insert into account(user_id,package,amount,payment_opt,entrydate,order_id,pack_type)values('" . $usr_ses_id . "','" . $pack_name . "','" . $amount . "','P','" . $date . "','" . $ord_id . "','" . $pack_type . "')";
            $this->dbh->Query($qrySavePage);
            if( $amount != 0 ) 
            {
                redirect("process-" . $ord_id . ".html");
            }
            else
            {
                $qry = "select * from account where order_id='" . $ord_id . "'";
                $Data = $this->dbh->FetchAllResults($qry);
                $package_upgrade = "select * from account where pack_status='Y' and user_id='" . $Data[0]["user_id"] . "' and paystatus='TRUE' and approved='Y' and status='Y' and deleted='N'";
                $this->dbh->Query($package_upgrade);
                if( $this->dbh->num_rows ) 
                {
                    $data_package_upgrade = $this->dbh->FetchAllResults($package_upgrade);
                    $sqlquery_upgrade = "update account set pack_status='N' where user_id='" . $data_package_upgrade[0]["user_id"] . "' and id='" . $data_package_upgrade[0]["id"] . "'";
                    $this->dbh->Query($sqlquery_upgrade);
                }

                $mem_pack_query = "select valid_day from member_package where package_name='" . $Data[0]["package"] . "'";
                $Data_mem_pack_query = $this->dbh->FetchAllResults($mem_pack_query);
                $valid_days = $Data_mem_pack_query[0]["valid_day"];
                $sqlquery = "update account set paystatus='TRUE',pack_validat_day='" . $valid_days . "',pack_status='Y', approved='Y' where order_id='" . $ord_id . "'";
                $this->dbh->Query($sqlquery);
                $sql_user_query = "update users set package_type='" . $Data[0]["pack_type"] . "' where id='" . $Data[0]["user_id"] . "'";
                $this->dbh->Query($sql_user_query);
                $sql_custmer_query = "update customer set paid='Y',package_type='" . $Data[0]["pack_type"] . "' where id='" . $Data1[0]["cust_id"] . "'";
                $this->dbh->Query($sql_custmer_query);
                $message2 = "<p>Your Package Upgrade Successfully.</p>\r\n\t\t\t<br />\r\n\t\t\tYour Amount Transfer  : " . $Data[0]["amount"];
                $Subject = "Upgrade Package";
                $msgBody1 = $message2;
                Send_mail($SITE_EMAIL, $company_name, $Data1[0]["email"], $Subject, $msgBody1);
                redirect("package_success-2.html");
            }

        }

    }

    public function pay_success()
    {
        global $objSmarty;
        global $SITE_EMAIL;
        global $Paypal_Email;
        global $company_name;
        $payment_status = $HTTP_POST_VARS["payment_status"];
        $option = $HTTP_POST_VARS["option"];
        $order_id = $HTTP_POST_VARS["id"];
        $oid = $order_id;
        if( $option == "A" ) 
        {
            $pay_title = "";
            if( $payment_status == "TRUE" ) 
            {
                $sqlquery = "update account set paystatus='TRUE', approved='Y' where order_id='" . $oid . "'";
                $this->dbh->Query($sqlquery);
                $qry = "select * from account where order_id='" . $oid . "'";
                $Data = $this->dbh->FetchAllResults($qry);
                $qry1 = "select * from users where  id='" . $Data[0]["user_id"] . "'";
                $Data1 = $this->dbh->FetchAllResults($qry1);
                $message2 = "<p>Your Payment Tranjection successfully completed.</p>\r\n\t\t\t<br />\r\n\t\t\tYour Amount Transfer  : " . $Data[0]["amount"];
                $Subject = $subject2;
                $msgBody1 = $message2;
                Send_mail($SITE_EMAIL, $company_name, $Data1[0]["email"], $Subject, $msgBody1);
                $heading = "Payment Transaction Success Completed";
                $objSmarty->assign("heading", $heading);
            }
            else
            {
                $heading = "Payment Transaction Not Success Completed";
                $objSmarty->assign("heading", $heading);
                $retry = "<td align=\"center\"><input type=\"submit\" class=\"sub_button\" value=\"Retry\" name=\"retry\"></td>";
                $objSmarty->assign("retry", $retry);
            }

        }
        else
        {
            if( $option == "P" ) 
            {
                $pay_title = "";
                if( $payment_status == "Completed" ) 
                {
                    $qry = "select * from account where order_id='" . $oid . "'";
                    $Data = $this->dbh->FetchAllResults($qry);
                    $package_upgrade = "select * from account where pack_status='Y' and user_id='" . $Data[0]["user_id"] . "' and paystatus='TRUE' and approved='Y' and status='Y' and deleted='N'";
                    $this->dbh->Query($package_upgrade);
                    if( $this->dbh->num_rows ) 
                    {
                        $data_package_upgrade = $this->dbh->FetchAllResults($package_upgrade);
                        $sqlquery_upgrade = "update account set pack_status='N' where user_id='" . $data_package_upgrade[0]["user_id"] . "' and id='" . $data_package_upgrade[0]["id"] . "'";
                        $this->dbh->Query($sqlquery_upgrade);
                    }

                    $mem_pack_query = "select valid_day from member_package where package_name='" . $Data[0]["package"] . "'";
                    $Data_mem_pack_query = $this->dbh->FetchAllResults($mem_pack_query);
                    $valid_days = $Data_mem_pack_query[0]["valid_day"];
                    $sqlquery = "update account set paystatus='TRUE',pack_validat_day='" . $valid_days . "',pack_status='Y', approved='Y' where order_id='" . $oid . "'";
                    $this->dbh->Query($sqlquery);
                    $sql_user_query = "update users set package_type='" . $Data[0]["pack_type"] . "' where id='" . $Data[0]["user_id"] . "'";
                    $this->dbh->Query($sql_user_query);
                    $qry1 = "select * from users where id='" . $Data[0]["user_id"] . "'";
                    $Data1 = $this->dbh->FetchAllResults($qry1);
                    if( $Data[0]["pack_type"] == "A_G" || $Data[0]["pack_type"] == "A_P" || $Data[0]["pack_type"] == "A_S" ) 
                    {
                        $sql_custmer_query = "update customer set paid='Y',package_type='" . $Data[0]["pack_type"] . "' where id='" . $Data1[0]["cust_id"] . "'";
                        $this->dbh->Query($sql_custmer_query);
                    }

                    $message2 = "<p>Your Payment Tranjection successfully completed.</p>\r\n\t\t\t<br />\r\n\t\t\tYour Amount Transfer  : " . $Data[0]["amount"];
                    $Subject = $subject2;
                    $msgBody1 = $message2;
                    Send_mail($SITE_EMAIL, $company_name, $Data1[0]["email"], $Subject, $msgBody1);
                    $heading = "Payment Transaction Success Completed";
                    $objSmarty->assign("heading", $heading);
                }
                else
                {
                    $heading = "Payment Transaction Not Success Completed";
                    $objSmarty->assign("heading", $heading);
                    $retry = "<td align=\"center\"><input type=\"submit\" class=\"sub_button\" value=\"Retry\" name=\"retry\"></td>";
                    $objSmarty->assign("retry", $retry);
                }

                $objSmarty->assign("heading", $heading);
            }
            else
            {
                if( $option == "" ) 
                {
                    $qry = "select * from account where order_id='" . $oid . "'";
                    $Data3 = $this->dbh->FetchAllResults($qry);
                    $qry1 = "select * from users where id='" . $Data3[0]["user_id"] . "'";
                    $Data2 = $this->dbh->FetchAllResults($qry1);
                    $message2 = "<p>Your Payment Transaction Not completed.</p>\r\n\t\t\t<br />\r\n\t\t\tYour Amount Transfer  : " . $Data3[0]["amount"];
                    $Subject = $subject2;
                    $msgBody1 = $message2;
                    Send_mail($SITE_EMAIL, $company_name, $Data1[0]["email"], $Subject, $msgBody1);
                    $heading = "Payment Transaction Not Completed";
                    $objSmarty->assign("heading", $heading);
                    $message3 = "<p>" . $Data2[0]["user_name"] . "was transaction successfully completed  </p>\r\n\t\t\t<br />\r\n\t\t\t email id " . $Data2[0]["email"] . "";
                    if( $option == "A" ) 
                    {
                        $subject3 = "<p>AUTHORISED.NET PAYMENT CONFIGRATION</p>";
                    }

                    if( $option == "P" ) 
                    {
                        $subject3 = "<p>PAYPAL PAYMENT CONFIGRATION</p>";
                    }

                    Send_mail($Data1[0]["email"], $company_name, $SITE_EMAIL, $Subject, $msgBody1);
                }

            }

        }

    }

    public function pay_option()
    {
        global $objSmarty;
        global $SITE_EMAIL;
        global $Paypal_Email;
        global $paid_amt;
        $payment_opt = $_REQUEST["payment_opt"];
        $oid = $_REQUEST["ord_id"];
        $sqlResult = "select package,amount,amount_inr from account where deleted='N' and status='Y' and order_id='" . $oid . "'";
        $sqlResult = $this->dbh->FetchAllResults($sqlResult);
        $amount = $sqlResult[0]["amount"];
        $amount_inr = $sqlResult[0]["amount_inr"];
        $package = $sqlResult[0]["package"];
        if( $payment_opt == "A" ) 
        {
            $sqlquery = "update account set payment_opt='A' where order_id='" . $oid . "'";
            $this->dbh->Query($sqlquery);
            echo "<script>document.location=\"./authorise.net/sim.php?oid=" . $oid . "&amount=" . $amount . "\";</script>";
        }
        else
        {
            if( $payment_opt == "P" ) 
            {
                echo "<script>document.location=\"./paypal/process.php?oid=" . $oid . "&email=" . $Paypal_Email . "&amount=" . $amount . "&package=" . $package . "\";</script>";
            }
            else
            {
                if( $payment_opt == "C" ) 
                {
                    echo "<script>document.location=\"./ccavenue/checkout.php3?oid=" . $oid . "&email=" . $Paypal_Email . "&amount=" . $amount_inr . "&package=" . $package . "\";</script>";
                }

            }

        }

    }

    public function pay_option_ad()
    {
        global $objSmarty;
        global $SITE_EMAIL;
        global $Paypal_Email;
        global $paid_amt;
        $payment_opt = $_REQUEST["payment_opt"];
        $oid = $_REQUEST["ord_id"];
        $sqlResult = "select package,amount,amount_inr from account where deleted='N' and status='Y' and order_id='" . $oid . "'";
        $sqlResult = $this->dbh->FetchAllResults($sqlResult);
        $amount = $sqlResult[0]["amount"];
        $amount_inr = $sqlResult[0]["amount_inr"];
        $package = $sqlResult[0]["package"];
        if( $payment_opt == "A" ) 
        {
            $sqlquery = "update account set payment_opt='A' where order_id='" . $oid . "'";
            $this->dbh->Query($sqlquery);
            echo "<script>document.location=\"./authorise.net/sim.php?oid=" . $oid . "&amount=" . $amount . "\";</script>";
        }
        else
        {
            if( $payment_opt == "P" ) 
            {
                echo "<script>document.location=\"./paypal/process.php?oid=" . $oid . "&email=" . $Paypal_Email . "&amount=" . $amount . "&package=" . $package . "\";</script>";
            }
            else
            {
                if( $payment_opt == "C" ) 
                {
                    echo "<script>document.location=\"./ccavenue/checkout.php3?oid=" . $oid . "&email=" . $Paypal_Email . "&amount=" . $amount_inr . "&package=" . $package . "\";</script>";
                }

            }

        }

    }

    public function cc_redirecturl()
    {
        global $objSmarty;
        global $site_url;
        global $cc_working_key;
        global $SITE_EMAIL;
        $WorkingKey = $cc_working_key;
        $Merchant_Id = $_REQUEST["Merchant_Id"];
        $Amount = $_REQUEST["Amount"];
        $Order_Id = $_REQUEST["Order_Id"];
        $Merchant_Param = $_REQUEST["Merchant_Param"];
        $Checksum = $_REQUEST["Checksum"];
        $AuthDesc = $_REQUEST["AuthDesc"];
        $user_email = $_REQUEST["billing_cust_email"];
        $Checksum = verifyChecksum($Merchant_Id, $Order_Id, $Amount, $AuthDesc, $Checksum, $WorkingKey);
        $Checksum == "true";
        $AuthDesc == "Y";
        if( $Checksum == "true" && $AuthDesc == "Y" ) 
        {
            $mess = "Thank you for Join with us. Your credit card has been charged and your transaction is successful.";
            $admin_mess = "You got payment Rs.: " . $Amount . " . form the email " . $user_email . " form order id is " . $Order_Id . " ";
            $active_user = "update account set status='Y' and payment_opt='C' where order_id='" . $Order_Id . "' ";
            $this->dbh->Query($active_user);
        }
        else
        {
            if( $Checksum == "true" && $AuthDesc == "B" ) 
            {
                $mess = "Thank you for Join with us.We will keep you posted regarding the status of your order through e-mail";
                $admin_mess = "The Payment of the Email: " . $user_email . " transaction id: " . $Order_Id . " is wating due to some technical reason. ";
                $active_user = "update account set status='Y' and  payment_opt='C' where order_id='" . $Order_Id . "' ";
                $this->dbh->Query($active_user);
            }
            else
            {
                if( $Checksum == "true" && $AuthDesc == "N" ) 
                {
                    $mess = "Thank you for Join with us.However,the transaction has been declined.";
                    $admin_mess = "The transaction from order id " . $Order_Id . " has been delined. Email Id of user is " . $user_email;
                    $active_user = "update account set  payment_opt='C' where order_id='" . $Order_Id . "' ";
                    $this->dbh->Query($active_user);
                }
                else
                {
                    $mess = "Security Error. Illegal access detected from order id " . $Order_Id . " on your request regrading  " . $site_url . " membership.";
                    $admin_mess = "A illegal access detected from email " . $user_email . " .The Order is id user this transaction is " . $Order_Id . " ";
                    $active_user = "update account set  payment_opt='C' where order_id='" . $Order_Id . "' ";
                    $this->dbh->Query($active_user);
                }

            }

        }

        $objSmarty->assign("mess", $mess);
        $Title = "information reagrding Order Number " . $Order_Id . " from " . $site_url;
        $Subject = "information reagrding Order Number  " . $Order_Id . " from " . $site_url;
        $mess = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n\t\t\t\t<tr><td>Dear</td></tr>\r\n\t\t\t\t<tr><td>" . $mess . "</td></tr>\r\n\t\t\t\t<tr><td>Thanks</td></tr>\r\n\t\t\t\t<tr><td>" . $site_url . "</td></tr>\r\n\t\t\t\t</table>\r\n\t\t";
        $admin_mess = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n\t\t\t\t<tr><td>Dear Admin</td></tr>\r\n\t\t\t\t<tr><td>" . $admin_mess . "</td></tr>\r\n\t\t\t\t<tr><td>Thanks</td></tr>\r\n\t\t\t\t<tr><td>" . $site_url . "</td></tr>\r\n\t\t\t\t</table>\r\n\t\t";
        Send_mail($SITE_EMAIL, $Title, $user_email, $Subject, $mess);
        Send_mail($SITE_EMAIL, $Title, $SITE_EMAIL, $Subject, $admin_mess);
    }

    public function __destruct()
    {
        $this->dbh->Disconnect();
    }

}


