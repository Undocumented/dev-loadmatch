<?php 

class users
{
    public $dbh;

    public function __construct()
    {
        $this->dbh = new db_MySQL();
        $this->dbh->Connect();
    }

    public function login()
    {
        global $objSmarty;
        global $functions;
        if( isset($_REQUEST["err"]) ) 
        {
            if( $_REQUEST["err"] == 1 ) 
            {
                $objSmarty->assign("err", "<div class='alert alert-danger' id='success-alert'>\r\n    <button type='button' class='close' data-dismiss='alert'></button>\r\n\tIt seems the Username or Password you entered is wrong </div>");
            }

            if( $_REQUEST["err"] == 2 ) 
            {
                $objSmarty->assign("err", "<div class='alert alert-danger' id='success-alert'>\r\n    <button type='button' class='close' data-dismiss='alert'></button>Your Account has not been activated yet</div>.");
            }

            if( $_REQUEST["err"] == 3 ) 
            {
                $objSmarty->assign("err", "<div class='alert alert-success' id='success-alert'>\r\n    <button type='button' class='close' data-dismiss='alert'></button>Congratulations. Your Account was created successfully. <br/>You can login now!</div>");
            }

        }

        if( isset($_REQUEST["submit"]) ) 
        {
            $pass = md5($_REQUEST["password"]);
            $uname = $functions->spacial_chr_blank($_REQUEST["user_name"]);
            $loginQry = "select * from user where email='" . $uname . "' and password='" . $pass . "' and deleted='N'";
            if( $functions->mysqlErrorHandlingFunctionForHacking($loginQry) == false ) 
            {
                redirect("login-1.html");
            }
            else
            {
                $userdata = $this->dbh->Query($loginQry);
                if( $this->dbh->num_rows ) 
                {
                    if( $userdata["status"] == "N" ) 
                    {
                        redirect("login-2.html");
                    }
                    else
                    {
                        $_SESSION["user_id"] = $userdata["id"];
                        $_SESSION["name"] = $userdata["name"];
                        $_SESSION["email"] = $userdata["email"];
                        $_SESSION["password"] = md5($_REQUEST["password"]);
                        $_SESSION["user_type"] = $userdata["type"];
                        if( isset($_POST["remember"]) ) 
                        {
                            setcookie("user_name", $_POST["user_name"], time() + 60 * 60 * 24 * 365, "/account", "{\$site_url}");
                            setcookie("password", md5($_POST["password"]), time() + 60 * 60 * 24 * 365, "/account", "{\$site_url}");
                        }

                        redirect("/");
                    }

                }
                else
                {
                    redirect("login-1.html");
                }

            }

        }

    }

    public function logout()
    {
        session_unset();
        session_destroy();
        redirect("eagleushp.php");
    }

    public function __destruct()
    {
        $this->dbh->Disconnect();
    }

}


