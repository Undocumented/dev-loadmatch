<?php 

class pagination
{
    public $dbh;
    public $pages;
    public $counts;
    public $show;

    public function __construct()
    {
        global $page_row;
        $this->dbh = new db_MySQL();
        $this->dbh->Connect();
        $this->show = $page_row;
    }

    public function pageCount($Qry, $link)
    {
        global $objSmarty;
        global $site_url;
        $this->dbh->Query($Qry);
        $this->counts = $this->dbh->num_rows;
        $this->pages = ceil($this->counts / $this->show);
        if( isset($_REQUEST["page_num"]) ) 
        {
            $page_num = $_REQUEST["page_num"];
        }
        else
        {
            $page_num = 1;
        }

        $show_link = 0;
        $links = "<div class=\"oferta_pagination\">";
        if( 1 < $this->pages ) 
        {
            if( 5 < $page_num ) 
            {
                $i = $page_num - 5;
            }
            else
            {
                $i = 1;
            }

            for( $li = 1; $i < $page_num; $i++ ) 
            {
                $li = $i;
                $show_link = 1;
            }
            if( $page_num != 1 ) 
            {
                $links .= "<span class=\"pagearro\"><a href=\"" . $link . "/p-" . $li . "\"><img src=\"" . $site_url . "/templates/images/arrow_left.gif\" width=\"22\" height=\"22\" /></a></span>";
            }

            $links .= "<span class=\"current\">" . $page_num . "</span>";
            $i++;
            for( $j = 1; $j <= 1 && $i <= $this->pages; $j++ ) 
            {
                $show_link = 1;
                $i++;
            }
            if( $this->pages == $page_num ) 
            {
                $ri = $page_num;
            }
            else
            {
                $ri = $page_num + 1;
            }

            $links .= "<span class=\"pagearro\"><a href=\"" . $link . "/p-" . $ri . "\"><img src=\"" . $site_url . "/templates/images/arrow_right.gif\" width=\"22\" height=\"22\" /></a></span>";
        }

        $links .= "</div>";
        $objSmarty->assign("page_links", $links);
        $objSmarty->assign("show_link", $show_link);
        $objSmarty->assign("total_record", $this->pages);
    }

    public function __destruct()
    {
        $this->dbh->Disconnect();
    }

}


