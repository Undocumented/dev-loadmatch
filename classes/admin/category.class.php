<?php
class category {
    var $dbh;
    var $parent_types = [
        'industry_types' => ['0','','',''],
        'load_types' => ['industry_types','industry_to_load','load_type_id','industry_type_id'],
        'transport_types' => ['load_types','load_to_transport','transport_type_id','load_type_id'],
        'body_bulk_types' => ['transport_types','transport_to_body_bulk','body_bulk_type_id','transport_type_id'],
        'size_types' => ['body_bulk_types','body_bulk_to_size','size_type_id','body_bulk_type_id']
    ];

    function __construct()
    {
        $this->dbh=new db_MySQL();
        $this->dbh->Connect();
    }

    function add_cat() {
        global $objSmarty, $site_url,$functions;
//        print_r($_REQUEST);
//        echo "okkk";exit;
        if(isset($_REQUEST['type']))
            $type=$_REQUEST['type'];
        $objSmarty->assign("type", $type);



        if(isset($_REQUEST['add_sucess'])) {

            $objSmarty->assign("add_sucess",'Y');
        }
        if(isset($_REQUEST['add_suce'])) {

            $objSmarty->assign("add_suce",'Y');
        }


        if(isset($_REQUEST['edit_id'])) {
            $edit_id=$_REQUEST['edit_id'];
            $objSmarty->assign("edit_id", $edit_id);
        }


        $listingQry12="select * from category where parent_id='0'  and deleted='N'";
        $useredit12=$this->dbh->FetchAllResults($listingQry12);
        $objSmarty->assign("useredit12", $useredit12);


        if($edit_id!='') {

            $useredit = $this->dbh->FetchAllResults("SELECT * FROM ".$type." WHERE id = '".$edit_id."'");
            $objSmarty->assign("useredit", $useredit);

            $linkedQry = "SELECT main.id, pivot.name, pivot.keyword, pivot.description";
            if($this->parent_types[$_REQUEST['type']][1] != '') {
                $linkedQry .= ", main.".$this->parent_types[$_REQUEST['type']][2].", main.".$this->parent_types[$_REQUEST['type']][3];
            }


            if($this->parent_types[$_REQUEST['type']][0] != '0') {

                $linkedQry .= " FROM ".$this->parent_types[$_REQUEST['type']][1]." as main INNER JOIN ".$this->parent_types[$_REQUEST['type']][0]." as pivot ON (main.".$this->parent_types[$_REQUEST['type']][3]." = pivot.id) WHERE main.".$this->parent_types[$_REQUEST['type']][2]." = '".$edit_id."'";
            }

            $linked_type = $this->dbh->FetchAllResults($linkedQry);
            foreach ($linked_type as $key => $item) {
                $linked_type[$key]['key'] = $key;
            }
            $objSmarty->assign("linked_type", $linked_type);
            /*echo "<pre>";
            var_dump($linked_type);
            echo "</pre>";*/





//            $listingQry="select * from category where id='".$edit_id."'";
//            $useredit=$this->dbh->FetchAllResults($listingQry);
//            $objSmarty->assign("useredit", $useredit);
        }

        if(isset($_REQUEST['edit_id_sub'])) {
            $edit_id_sub=$_REQUEST['edit_id_sub'];
            $objSmarty->assign("edit_id_sub", $edit_id_sub);

            $listingQry="select * from category where id='".$edit_id_sub."'";
            $useredit=$this->dbh->FetchAllResults($listingQry);
            $objSmarty->assign("useredit", $useredit);

            $listing_sub_Qry="select * from category where id='".$useredit[0]['parent_sub_id']."'";
            $user_cat_edit=$this->dbh->FetchAllResults($listing_sub_Qry);
            $objSmarty->assign("user_cat_edit", $user_cat_edit);

            $listing_last_Qry="select * from category where id='".$user_cat_edit[0]['parent_id']."'";
            $user_last_edit=$this->dbh->FetchAllResults($listing_last_Qry);
            $objSmarty->assign("user_last_edit", $user_last_edit);

        }



        if($_REQUEST['type'] == 'industry_types' || $_REQUEST['type'] == 'load_types' || $_REQUEST['type'] == 'transport_types' || $_REQUEST['type'] == 'body_bulk_types' || $_REQUEST['type'] == 'size_types') {
            if($this->parent_types[$_REQUEST['type']][0] != '0') {
                $listingQry = "select * from " . $this->parent_types[$_REQUEST['type']][0] . " ";

                $type_data = $this->dbh->FetchAllResults($listingQry);
                $objSmarty->assign("type_data", $type_data);

/*                highlight_string("<?php\n\$data =\n" . var_export($type_data, true) . ";\n?>");*/
            }



//            $listingQry = "SELECT main.id, main.name, main.keyword, main.description";
//            if($this->parent_types[$_REQUEST['type']][1] != '') {
//                $listingQry .= ", pivot.".$this->parent_types[$_REQUEST['type']][2].", pivot.".$this->parent_types[$_REQUEST['type']][3];
//            }
//            $listingQry .= " FROM ".$type." as main";
//
//            if($this->parent_types[$_REQUEST['type']][1] != '') {
//                $listingQry .= " INNER JOIN " . $this->parent_types[$_REQUEST['type']][1] . " as pivot ON (pivot.".$this->parent_types[$_REQUEST['type']][2]." = main.id)";
//            }
//
//            $useredit1 = $this->dbh->FetchAllResults($listingQry);
//
//            $objSmarty->assign("useredit1", $useredit1);
//
        }


        if($_REQUEST['type']=='S'  || $_REQUEST['type']=='SN' || $_REQUEST['type']=='FC' || $_REQUEST['type']=='FFC' || $_REQUEST['type']=='SC' || $_REQUEST['type']=='SVC' || $_REQUEST['type']=='EC' || $_REQUEST['type']=='NC'|| $_REQUEST['type']=='TC') {
            $listingQry="select * from category where parent_id='0' and parent_sub_id='0' and last_cat='0' and 	fifth_cat='0' and sixth_cat='0' and seventh_cat='0' and eigth_cat='0' and ninth_cat='0' and tenth_cat='0' and deleted='N'";
            $useredit1=$this->dbh->FetchAllResults($listingQry);
            $objSmarty->assign("useredit1", $useredit1);

        }

        /* Added New Industry */
        if(isset($_REQUEST['add_industry_type_btn'])) {
            if($edit_id == "") {

                $add_industry = "INSERT INTO industry_types (name,keyword,description) VALUES('".$_REQUEST['cat_name']."', '".$_REQUEST['keyword']."', '".$_REQUEST['description']."')";
                $this->dbh->Query($add_industry);

                redirect($site_url."/admin.php?page=category&action=add_cat&type=industry_types&add_suce=Y");
            } else {
                $updateQry = "UPDATE industry_types SET name = '".$_REQUEST['cat_name']."', keyword = '".$_REQUEST['keyword']."', description = '".$_REQUEST['description']."' WHERE id = '".$edit_id."'";
                $this->dbh->Query($updateQry);

                redirect($site_url."/admin.php?page=category&action=show_cat_list&type=industry_types");
            }
        }

        /* Added New Load */
        if(isset($_REQUEST['add_load_type_btn'])) {
            if($edit_id == "") {

                $add_load = "INSERT INTO load_types (name,keyword,description) VALUES('".$_REQUEST['cat_name']."', '".$_REQUEST['keyword']."', '".$_REQUEST['description']."')";
                $this->dbh->Query($add_load);

                $new_load = $this->dbh->FetchAllResults("SELECT * FROM load_types WHERE name = '".$_REQUEST['cat_name']."' AND keyword = '".$_REQUEST['keyword']."' AND description = '".$_REQUEST['description']."'");

                $add_industry_to_load = "INSERT INTO industry_to_load (load_type_id,industry_type_id) VALUES('".$new_load[0]['id']."', '".$_REQUEST['parent_id']."')";
                $this->dbh->Query($add_industry_to_load);

                redirect($site_url."/admin.php?page=category&action=add_cat&type=load_types&add_suce=Y");

            } else {


                $updateQry = "UPDATE load_types SET name = '".$_REQUEST['cat_name']."', keyword = '".$_REQUEST['keyword']."', description = '".$_REQUEST['description']."' WHERE id = '".$edit_id."'";
                $this->dbh->Query($updateQry);

                $this->dbh->Query("DELETE FROM industry_to_load WHERE load_type_id = '".$edit_id."'");

                $values = array();

                foreach ($_REQUEST['linked_type'] as $link) {
                    $values[] = "('".$link['edit_id']."', '".$link['parent_id']."')";
                }

                $add_industry_to_load = "INSERT INTO industry_to_load (load_type_id,industry_type_id) VALUES".implode(',',$values);

                $this->dbh->Query($add_industry_to_load);

                redirect($site_url."/admin.php?page=category&action=show_cat_list&id={$_REQUEST['parent_id']}&type=load_types");

            }
        }

        /* Added New Transport */
        if(isset($_REQUEST['add_transport_type_btn'])) {
            if($edit_id == "") {

                $add_transport = "INSERT INTO transport_types (name,keyword,description) VALUES('".$_REQUEST['cat_name']."', '".$_REQUEST['keyword']."', '".$_REQUEST['description']."')";
                $this->dbh->Query($add_transport);

                $new_transport = $this->dbh->FetchAllResults("SELECT * FROM transport_types WHERE name = '".$_REQUEST['cat_name']."' AND keyword = '".$_REQUEST['keyword']."' AND description = '".$_REQUEST['description']."'");

                $add_load_to_transport = "INSERT INTO load_to_transport (transport_type_id,load_type_id) VALUES('".$new_transport[0]['id']."', '".$_REQUEST['parent_id']."')";
                $this->dbh->Query($add_load_to_transport);

                redirect($site_url."/admin.php?page=category&action=add_cat&type=transport_types&add_suce=Y");

            } else {

                $updateQry = "UPDATE transport_types SET name = '".$_REQUEST['cat_name']."', keyword = '".$_REQUEST['keyword']."', description = '".$_REQUEST['description']."' WHERE id = '".$edit_id."'";

                $this->dbh->Query($updateQry);
                $this->dbh->Query("DELETE FROM load_to_transport WHERE transport_type_id = '".$edit_id."'");

                $add_load_to_transport = "INSERT INTO load_to_transport (transport_type_id,load_type_id) VALUES('".$edit_id."', '".$_REQUEST['parent_id']."')";
                $this->dbh->Query($add_load_to_transport);

                redirect($site_url."/admin.php?page=category&action=show_cat_list&id={$_REQUEST['parent_id']}&type=transport_types");

            }
        }

        /* Added New Body Bulk */
        if(isset($_REQUEST['add_body_bulk_type_btn'])) {
            if($edit_id == "") {

                $add_body_bulk = "INSERT INTO body_bulk_types (name,keyword,description) VALUES('".$_REQUEST['cat_name']."', '".$_REQUEST['keyword']."', '".$_REQUEST['description']."')";
                $this->dbh->Query($add_body_bulk);

                $new_body_bulk = $this->dbh->FetchAllResults("SELECT * FROM body_bulk_types WHERE name = '".$_REQUEST['cat_name']."' AND keyword = '".$_REQUEST['keyword']."' AND description = '".$_REQUEST['description']."'");

                $add_transport_to_body_bulk = "INSERT INTO transport_to_body_bulk (body_bulk_type_id,transport_type_id) VALUES('".$new_body_bulk[0]['id']."', '".$_REQUEST['parent_id']."')";
                $this->dbh->Query($add_transport_to_body_bulk);

                redirect($site_url."/admin.php?page=category&action=add_cat&type=body_bulk_types&add_suce=Y");

            } else {

                $updateQry = "UPDATE body_bulk_types SET name = '".$_REQUEST['cat_name']."', keyword = '".$_REQUEST['keyword']."', description = '".$_REQUEST['description']."' WHERE id = '".$edit_id."'";

                $this->dbh->Query($updateQry);
                $this->dbh->Query("DELETE FROM transport_to_body_bulk WHERE body_bulk_type_id = '".$edit_id."'");

                $add_transport_to_body_bulk = "INSERT INTO transport_to_body_bulk (body_bulk_type_id,transport_type_id) VALUES('".$edit_id."', '".$_REQUEST['parent_id']."')";
                $this->dbh->Query($add_transport_to_body_bulk);

                redirect($site_url."/admin.php?page=category&action=show_cat_list&id={$_REQUEST['parent_id']}&type=body_bulk_types");

            }
        }

        /* Added New Size */
        if(isset($_REQUEST['add_size_type_btn'])) {
            if($edit_id == "") {

                $add_size= "INSERT INTO size_types (name,keyword,description) VALUES('".$_REQUEST['cat_name']."', '".$_REQUEST['keyword']."', '".$_REQUEST['description']."')";
                $this->dbh->Query($add_size);

                $new_size = $this->dbh->FetchAllResults("SELECT * FROM body_bulk_types WHERE name = '".$_REQUEST['cat_name']."' AND keyword = '".$_REQUEST['keyword']."' AND description = '".$_REQUEST['description']."'");

                $add_body_bulk_to_size = "INSERT INTO body_bulk_to_size (size_type_id,body_bulk_type_id) VALUES('".$new_size[0]['id']."', '".$_REQUEST['parent_id']."')";
                $this->dbh->Query($add_body_bulk_to_size);

                redirect($site_url."/admin.php?page=category&action=add_cat&type=size_types&add_suce=Y");

            } else {

                $updateQry = "UPDATE size_types SET name = '".$_REQUEST['cat_name']."', keyword = '".$_REQUEST['keyword']."', description = '".$_REQUEST['description']."' WHERE id = '".$edit_id."'";

                $this->dbh->Query($updateQry);
                $this->dbh->Query("DELETE FROM body_bulk_to_size WHERE body_bulk_type_id = '".$edit_id."'");

                $add_body_bulk_to_size = "INSERT INTO body_bulk_to_size (size_type_id,body_bulk_type_id) VALUES('".$edit_id."', '".$_REQUEST['parent_id']."')";
                $this->dbh->Query($add_body_bulk_to_size);

                redirect($site_url."/admin.php?page=category&action=show_cat_list&id={$_REQUEST['parent_id']}&type=size_types");

            }
        }






        if(isset($_REQUEST['cat_btn'])) {

            if($functions->checkImageUploadYesOrNotForHacking($_FILES['logo']['name'])==true) {
                $d1=mktime(date(h),date(i), date(s),date(m),date(d),date(y));

                if($_FILES['logo']['name']!='') {

                    $filename = stripslashes($_FILES['logo']['name']);

                    $extension = $functions->getExtension($filename);
                    $extension = strtolower($extension);

                    //echo $_FILES["add_image"]["type"]; exit;

                    $path="images/".$d1.'.'.$extension;

                    if (( ($_FILES["logo"]["type"] == "image/png") || ($_FILES["logo"]["type"] == "image/jpeg")
                            || ($_FILES["logo"]["type"] == "image/pjpeg")) ||($_FILES["logo"]["type"] == "application/x-shockwave-flash"))
                    {

                        move_uploaded_file($_FILES['logo']['tmp_name'], "$path");
                        $uploaded_file=$d1.'.'.$extension;

                        unlink('images/'.$_REQUEST['old_main_img']);
                    }
                } else {
                    $uploaded_file=$_REQUEST['old_main_img'];
                }

            } else {
                $uploaded_file=$_REQUEST['old_main_img'];
            }
            if($edit_id=="") {

                $add_customer="insert into category (category,cat_image,cat_keyword,cat_description) values (
				 '".$_REQUEST['cat_name']."', '".$uploaded_file."','".$_REQUEST['keyword']."','".$_REQUEST['description']."')";

                $this->dbh->Query($add_customer);

                redirect($site_url."/admin.php?page=category&action=add_cat&type=M&add_sucess=Y");
            } else {
                $updateQry="update  category set category='".$_REQUEST['cat_name']."', cat_keyword='".$_REQUEST['keyword']."', cat_description='".$_REQUEST['description']."', cat_image='".$uploaded_file."' where id='".$edit_id."'";
                $this->dbh->Query($updateQry);
                redirect($site_url."/admin.php?page=category&action=show_cat_list&type=M&message=upd_sucess");
            }


            if(isset($_REQUEST['del_cat'])) {
                $updateQry="update  category set deleted='Y' where id='".$_REQUEST['del_cat']."'";
                $this->dbh->Query($updateQry);
                redirect($site_url."/admin.php?page=category&action=show_cat_list&type=M");

            }
        }


        if($_REQUEST['sub_cat_btn']) {
            if($edit_id_sub=="") {

                $add_customer="insert into category (category,parent_id,cat_keyword,cat_description) values('".$_REQUEST['cat_name']."','".$_REQUEST['category']."', '".$_REQUEST['keyword']."','".$_REQUEST['description']."')";
                $this->dbh->Query($add_customer);

                redirect($site_url."/admin.php?page=category&action=add_cat&type=S&add_suce=Y");
            } else {
                $updateQry="update  category set parent_id='".$_REQUEST['category']."', category='".$_REQUEST['cat_name']."', cat_keyword='".$_REQUEST['keyword']."', cat_description='".$_REQUEST['description']."' where id='".$edit_id_sub."'";

                $this->dbh->Query($updateQry);
                redirect($site_url."/admin.php?page=category&action=show_cat_list");

            }

        }

        if($_REQUEST['sub_nxt_cat_btn']) {

            if($edit_id_sub=="") {

                $add_customer="insert into category (category,parent_sub_id,cat_keyword,cat_description) values('".$_REQUEST['cat_name']."','".$_REQUEST['sub_next_cat']."', '".$_REQUEST['keyword']."','".$_REQUEST['description']."')";
                ; $this->dbh->Query($add_customer);

                redirect($site_url."/admin.php?page=category&action=add_cat&type=SN&add_suce=Y");
            } else {
                $updateQry="update  category set parent_sub_id='".$_REQUEST['sub_next_cat']."', category='".$_REQUEST['cat_name']."', cat_keyword='".$_REQUEST['keyword']."', cat_description='".$_REQUEST['description']."' where id='".$edit_id_sub."'";

                $this->dbh->Query($updateQry);
                redirect($site_url."/admin.php?page=category&action=show_cat_list");

            }

        }

        if($_REQUEST['last_cat_btn']) {

            if($edit_id_sub=="") {

                $add_customer="insert into category (category,last_cat,cat_keyword,cat_description) values('".$_REQUEST['cat_name']."','".$_REQUEST['last_sub_cat']."', '".$_REQUEST['keyword']."','".$_REQUEST['description']."')";
                ; $this->dbh->Query($add_customer);

                redirect($site_url."/admin.php?page=category&action=add_cat&type=FC&add_suce=Y");
            } else {
                $updateQry="update  category set last_cat='".$_REQUEST['last_sub_cat']."', category='".$_REQUEST['cat_name']."', cat_keyword='".$_REQUEST['keyword']."', cat_description='".$_REQUEST['description']."' where id='".$edit_id_sub."'";

                $this->dbh->Query($updateQry);
                redirect($site_url."/admin.php?page=category&action=show_cat_list");

            }

        }


        if($_REQUEST['fifth_cat_btn']) {

            if($edit_id_sub=="") {

                $add_customer="insert into category (category,fifth_cat,cat_keyword,cat_description) values('".$_REQUEST['cat_name']."','".$_REQUEST['fourth_cat']."', '".$_REQUEST['keyword']."','".$_REQUEST['description']."')";
                ; $this->dbh->Query($add_customer);

                redirect($site_url."/admin.php?page=category&action=add_cat&type=FFC&add_suce=Y");
            } else {
                $updateQry="update  category set fifth_cat='".$_REQUEST['fourth_cat']."', category='".$_REQUEST['cat_name']."', cat_keyword='".$_REQUEST['keyword']."', cat_description='".$_REQUEST['description']."' where id='".$edit_id_sub."'";

                $this->dbh->Query($updateQry);
                redirect($site_url."/admin.php?page=category&action=show_cat_list");

            }

        }

        //print_r($_REQUEST); exit;
        if($_REQUEST['sixth_cat_btn']) {
            if($edit_id_sub=="") {

                $add_customer="insert into category (category,sixth_cat,cat_keyword,cat_description) values('".$_REQUEST['cat_name']."','".$_REQUEST['fifth_cat']."', '".$_REQUEST['keyword']."','".$_REQUEST['description']."')";
                $this->dbh->Query($add_customer);

                redirect($site_url."/admin.php?page=category&action=add_cat&type=SC&add_suce=Y");
            } else {
                $updateQry="update  category set sixth_cat='".$_REQUEST['fifth_cat']."', category='".$_REQUEST['cat_name']."', cat_keyword='".$_REQUEST['keyword']."', cat_description='".$_REQUEST['description']."' where id='".$edit_id_sub."'";

                $this->dbh->Query($updateQry);
                redirect($site_url."/admin.php?page=category&action=show_cat_list");

            }

        }

        if($_REQUEST['seveth_cat_btn']) {
            if($edit_id_sub=="") {
                $add_customer="insert into category (category,seventh_cat,cat_keyword,cat_description) values('".$_REQUEST['cat_name']."','".$_REQUEST['sixth_cat1']."', '".$_REQUEST['keyword']."','".$_REQUEST['description']."')";
                $this->dbh->Query($add_customer);
                redirect($site_url."/admin.php?page=category&action=add_cat&type=SVC&add_suce=Y");
            } else {
                $updateQry="update  category set seventh_cat='".$_REQUEST['sixth_cat1']."', category='".$_REQUEST['cat_name']."', cat_keyword='".$_REQUEST['keyword']."', cat_description='".$_REQUEST['description']."' where id='".$edit_id_sub."'";

                $this->dbh->Query($updateQry);
                redirect($site_url."/admin.php?page=category&action=show_cat_list");

            }

        }

        if($_REQUEST['eigth_cat_btn']) {
            if($edit_id_sub=="") {
                $add_customer="insert into category (category,eigth_cat,cat_keyword,cat_description) values('".$_REQUEST['cat_name']."','".$_REQUEST['seventh_cat']."', '".$_REQUEST['keyword']."','".$_REQUEST['description']."')";
                $this->dbh->Query($add_customer);
                redirect($site_url."/admin.php?page=category&action=add_cat&type=EC&add_suce=Y");
            } else {
                $updateQry="update  category set eigth_cat='".$_REQUEST['seventh_cat']."', category='".$_REQUEST['cat_name']."', cat_keyword='".$_REQUEST['keyword']."', cat_description='".$_REQUEST['description']."' where id='".$edit_id_sub."'";

                $this->dbh->Query($updateQry);
                redirect($site_url."/admin.php?page=category&action=show_cat_list");

            }




        }

        if($_REQUEST['ninth_cat_btn']) {
            if($edit_id_sub=="") {
                $add_customer="insert into category (category,ninth_cat,cat_keyword,cat_description) values('".$_REQUEST['cat_name']."','".$_REQUEST['eigth_cat']."', '".$_REQUEST['keyword']."','".$_REQUEST['description']."')";
                $this->dbh->Query($add_customer);
                redirect($site_url."/admin.php?page=category&action=add_cat&type=NC&add_suce=Y");
            } else {
                $updateQry="update  category set ninth_cat='".$_REQUEST['eigth_cat']."', category='".$_REQUEST['cat_name']."', cat_keyword='".$_REQUEST['keyword']."', cat_description='".$_REQUEST['description']."' where id='".$edit_id_sub."'";

                $this->dbh->Query($updateQry);
                redirect($site_url."/admin.php?page=category&action=show_cat_list");

            }

        }

        if($_REQUEST['tenth_cat_btn']) {
            if($edit_id_sub=="") {
                $add_customer="insert into category (category,tenth_cat,cat_keyword,cat_description) values('".$_REQUEST['cat_name']."','".$_REQUEST['ninth_cat']."', '".$_REQUEST['keyword']."','".$_REQUEST['description']."')";
                $this->dbh->Query($add_customer);
                redirect($site_url."/admin.php?page=category&action=add_cat&type=TC&add_suce=Y");
            } else {
                $updateQry="update  category set tenth_cat='".$_REQUEST['ninth_cat']."', category='".$_REQUEST['cat_name']."', cat_keyword='".$_REQUEST['keyword']."', cat_description='".$_REQUEST['description']."' where id='".$edit_id_sub."'";

                $this->dbh->Query($updateQry);
                redirect($site_url."/admin.php?page=category&action=show_cat_list");

            }

        }

    }

    public function show_cat_list() {
        global $objSmarty, $site_url,$page_row ,$eagle_pagination;
        if(isset($_REQUEST['type'])) {
            $type = $_REQUEST['type'];
            $objSmarty->assign("type", $type);
        }
        if(isset($_REQUEST['id'])) {
            $type_id = $_REQUEST['id'];
            $type_filter_name = $this->dbh->FetchAllResults("SELECT name FROM ".$this->parent_types[$type][0]." WHERE id = '".(int)$type_id."'");
            $objSmarty->assign("type_filter_name", $type_filter_name[0]['name']);
        }
        if($_REQUEST['masage']=='add_sucess') {
            $show_message = "Sucefully Added Your Data.";
        } else if($_REQUEST['masage']=='upd_sucess') {
            $show_message = "Sucefully Update Your Data.";
        } else if($_REQUEST['message']=='sucess') {
            $show_message = "Sucefully Deleted  Data.";
        }
        $objSmarty->assign("show_message",$show_message);

        //pagination start
        $page = intval($_REQUEST["pageval"]);
        $objSmarty->assign("page_val", $page);
        if($page<=0) $page = 1;
        $start_r=($page-1)* $page_row;
        $objSmarty->assign("row_no", abs($start_r));

        //pagination stop

        if(isset($type)) {


            $listingQry = "SELECT main.id, main.name, main.keyword, main.description";
//            if($this->parent_types[$_REQUEST['type']][1] != '') {
//                $listingQry .= ", pivot.".$this->parent_types[$_REQUEST['type']][2].", pivot.".$this->parent_types[$_REQUEST['type']][3];
//            }
            $listingQry .= " FROM ".$type." as main";
//
//            if($this->parent_types[$_REQUEST['type']][0] != '0') {
//                $listingQry .= " INNER JOIN " . $this->parent_types[$_REQUEST['type']][1] . " as pivot ON (pivot.".$this->parent_types[$_REQUEST['type']][2]." = main.id)";
//            }
//            if(isset($type_id)) {
//                $listingQry .= " WHERE pivot." . $this->parent_types[$_REQUEST['type']][3] . " = " . (int)$type_id;
//            }
            $listingQry .= " ORDER BY main.id ASC LIMIT ".$start_r.",".$page_row;

            $type_data = $this->dbh->FetchAllResults($listingQry);
            $objSmarty->assign("cat_data", $type_data);

            foreach ($this->parent_types as $key => $type) {
                if($type[0] == $_REQUEST['type']) {
                    $objSmarty->assign("child_type", $key);
                }
            }

        }

        ///////////////////////////////////////////// Pagination start//////////////////////////////////////////////////////////////

        // count total number of appropriate listings:
        $countrylist=mysql_query($listingQry);
        $tcount = mysql_num_rows($countrylist);


        //echo $page."//////".$tcount."//////".$page_row;
        // call pagination function:
        $show_pagi=$eagle_pagination->paginate_three($page, $tcount, $page_row);
        $objSmarty->assign("show_pagi",$show_pagi);

        ///////////////////////////////////////////// Pagination stop//////////////////////////////////////////////////////////////

        if(isset($_REQUEST['del_type'])) {

            $deleteQry = "DELETE FROM ".$type." WHERE id = '".$_REQUEST['del_type']."'";
            $this->dbh->Query($deleteQry);
            $deleteConnectedQry = "DELETE FROM ".$this->parent_types[$_REQUEST['type']][1]." WHERE ".$this->parent_types[$_REQUEST['type']][2]." = '".$_REQUEST['del_type']."'";
            $this->dbh->Query($deleteConnectedQry);
            redirect($site_url."/admin.php?page=category&action=show_cat_list&type=M");

        }

    }

    function show_cat_list222(){
        global $objSmarty, $site_url,$page_row ,$eagle_pagination;

        if($_REQUEST['masage']=='add_sucess')
            $show_message="Sucefully Added Your Data.";
        else if($_REQUEST['masage']=='upd_sucess')
            $show_message="Sucefully Update Your Data.";
        else if($_REQUEST['message']=='sucess')
            $show_message="Sucefully Deleted  Data.";
        $objSmarty->assign("show_message",$show_message);


        $_REQUEST["pageval"];

        //pagination start
        $page = intval($_REQUEST["pageval"]);
        $objSmarty->assign("page_val", $page);
        if($page<=0) $page = 1;
        $start_r=($page-1)* $page_row;
        $objSmarty->assign("row_no", abs($start_r));

        //pagination stop


        $listingQry="SELECT category. * FROM category WHERE parent_id='0' and parent_sub_id='0' and last_cat='0' and 	fifth_cat='0' and sixth_cat='0' and seventh_cat='0' and eigth_cat='0' and ninth_cat='0' and tenth_cat='0' and deleted='N' order by id desc ";
        $view_cou_list_limit= $listingQry." limit ".$start_r.",".$page_row;
        $cat_data=$this->dbh->FetchAllResults($view_cou_list_limit);
        $objSmarty->assign("cat_data", $cat_data);



        ///////////////////////////////////////////// Pagination start//////////////////////////////////////////////////////////////

        // count total number of appropriate listings:
        $countrylist=mysql_query($listingQry);
        $tcount = mysql_num_rows($countrylist);


        //echo $page."//////".$tcount."//////".$page_row;
        // call pagination function:
        $show_pagi=$eagle_pagination->paginate_three($page, $tcount, $page_row);
        $objSmarty->assign("show_pagi",$show_pagi);

        ///////////////////////////////////////////// Pagination stop//////////////////////////////////////////////////////////////

        if(isset($_REQUEST['del_id'])) {
            $delete_query="update category set deleted='Y' where id='".$_REQUEST['del_id']."'";
            $this->dbh->Query($delete_query);
            redirect($site_url."/admin.php?page=category&action=show_cat_list");
        }
        if(isset($_REQUEST['del_cat'])) {

            $updateQry="update  category set deleted='Y' where id='".$_REQUEST['del_cat']."'";
            $this->dbh->Query($updateQry);
            redirect($site_url."/admin.php?page=category&action=show_cat_list&type=M");

        }


    }



    function show_sub_cat_list() {
        global $objSmarty, $site_url,$page_row, $eagle_pagination;


        $id=$_REQUEST['id'];

        /*	$page = intval($_REQUEST["pageval"]);
            $objSmarty->assign("page_val", $page);
            if($page<=0) $page = 1;
            $start_r=($page-1)* $page_row;
            $objSmarty->assign("row_no", abs($start_r));*/




        $listingQry="SELECT category. * FROM category WHERE parent_id='".$id."' and deleted='N' order by id desc ";
        $view_cou_list_limit= $listingQry." limit ".$start_r.",".$page_row;
        $cat_data=$this->dbh->FetchAllResults($listingQry);
        $objSmarty->assign("cat_data", $cat_data);

        if(isset($_REQUEST['del_id'])) {
            $delete_query="update category set deleted='Y' where id='".$_REQUEST['del_id']."'";
            $this->dbh->Query($delete_query);
            redirect($site_url."/admin.php?page=category&action=show_cat_list");
        }

    }



    function show_sub_next_cat_list() {
        global $objSmarty, $site_url,$page_row, $eagle_pagination;


        $id=$_REQUEST['id'];

        /*	$page = intval($_REQUEST["pageval"]);
            $objSmarty->assign("page_val", $page);
            if($page<=0) $page = 1;
            $start_r=($page-1)* $page_row;
            $objSmarty->assign("row_no", abs($start_r));*/

        $listingQry="SELECT category. * FROM category WHERE parent_sub_id='".$id."' and parent_id='0'  and deleted='N' order by id desc ";
        $view_cou_list_limit= $listingQry." limit ".$start_r.",".$page_row;
        $cat_data=$this->dbh->FetchAllResults($listingQry);
        $objSmarty->assign("cat_data", $cat_data);

        if(isset($_REQUEST['del_id'])) {
            $delete_query="update category set deleted='Y' where id='".$_REQUEST['del_id']."'";
            $this->dbh->Query($delete_query);
            redirect($site_url."/admin.php?page=category&action=show_cat_list");
        }

    }


    function show_last_cat() {
        global $objSmarty, $site_url,$page_row, $eagle_pagination;
        $id=$_REQUEST['id'];
        $type=$_REQUEST['type'];
        $objSmarty->assign("type", $type);


        if($_REQUEST['type']=='FC') {

            $condition=" last_cat='".$id."' and ";

        }
        if($_REQUEST['type']=='FFC') {

            $condition=" fifth_cat='".$id."' and ";

        }

        if($_REQUEST['type']=='SC') {

            $condition=" sixth_cat='".$id."' and ";
        }

        if($_REQUEST['type']=='SVC') {

            $condition=" seventh_cat='".$id."' and ";
        }
        if($_REQUEST['type']=='EC') {

            $condition=" eigth_cat='".$id."' and ";
        }
        if($_REQUEST['type']=='NC') {

            $condition=" ninth_cat='".$id."' and ";
        }
        if($_REQUEST['type']=='TC') {

            $condition=" tenth_cat='".$id."' and ";
        }

        $listingQry="SELECT  * FROM category WHERE".$condition."  deleted='N' order by id desc ";
        $view_cou_list_limit= $listingQry." limit ".$start_r.",".$page_row;
        $cat_data=$this->dbh->FetchAllResults($listingQry);
        $objSmarty->assign("cat_data", $cat_data);
        if(isset($_REQUEST['del_id'])) {
            $delete_query="update category set deleted='Y' where id='".$_REQUEST['del_id']."'";
            $this->dbh->Query($delete_query);
            redirect($site_url."/admin.php?page=category&action=show_cat_list");
        }
    }


    function show_vehicle() {
        global $objSmarty, $site_url;

        if(isset($_REQUEST['user_id']))
            $user_id=$_REQUEST['user_id'];
        else
            $user_id="";


        $objSmarty->assign("user_id", $user_id);

        $listQry="select * from vehicle_types where  deleted='N'" ;

        $this->dbh->Query($listQry);
        if($this->dbh->num_rows) {
            $userdetail=$this->dbh->FetchAllResults($listQry);
            $objSmarty->assign("userdetail", $userdetail);
        }

        if(isset($_REQUEST['del_id'])) {
            $delete_query="update vehicle_types set deleted='Y' where id='".$_REQUEST['del_id']."'";
            $this->dbh->Query($delete_query);
            redirect($site_url."/admin.php?page=category&action=show_vehicle");
        }
    }


    function add_vehicle() {
        global $objSmarty;
        if(isset($_REQUEST['edit_id']))
            $id=$_REQUEST['edit_id'];
        $objSmarty->assign("edit_id", $_REQUEST['edit_id']);


        if($id!='')
        {
            $listingQry="select * from vehicle_types where id=$id ";
            $useredit=$this->dbh->FetchAllResults($listingQry);
            $objSmarty->assign("useredit", $useredit);
        }
        if(isset($_REQUEST['add_new_vehicle']))
        {

            if($_REQUEST['edit_id']=="")
            {
                $add_customer="insert into vehicle_types (vehicle_type) values ('".($_REQUEST['vehicle_type_name'])."')";

                $this->dbh->Query($add_customer);
            }
            else
            {
                $updateQry="update vehicle_types set vehicle_type='".$_REQUEST['vehicle_type_name']."' where id='".$_REQUEST['edit_id']."'";
                $this->dbh->Query($updateQry);

            }


            redirect("admin.php?page=category&action=show_vehicle");
        }
    }


    function __destruct()
    {
        $this->dbh->Disconnect();
    }

}
?>