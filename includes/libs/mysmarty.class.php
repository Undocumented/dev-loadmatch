<?php

# include manadatory files
require_once( DOC .'includes/libs/Smarty.class.php' );
/**
 * This class returns a object or instance of smarty
 * This class is compatible for PHP 5.x
 * 
 * Version 1 enables you to create an object for smarty
 *
 *
 * @version 1
 * @author Abhay Kumar Singh
 * @uses smarty class for create an object
 * 
 */

class MySmarty extends Smarty
{
	/**
	 * constructor for smarty class
	 * 
	 * @param	string	$path	
	 *
	 */

	public function __construct( $path = "" )
	{
		$this->Smarty();
		$this->template_dir		=		DOC . '/templates/';
		$this->compile_dir 		= 		DOC . '/templates_c/';
		$this->config_dir 		= 		DOC . '/configs/';
		$this->cache_dir 		= 		DOC . '/cache/';
		$this->caching 			= 		true;
		$this->assign('app_name', 'Initial Project Structure');
	}
}

?>