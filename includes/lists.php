<?php

	$states=array(
					"Andhra Pradesh" => array("Adilabad", "Nizamabad", "Karimnagar", "Medak", "Warangal", "Rangareddy", "Nalgonda", "Mahbubnagar", "Khammam", "Hyderabad", "Anantapur", "Chittoor", "Cuddapah", "Kurnool", "East Godavari", "Guntur", "West Godavari", "Krishna", "Nellore", "Prakasam", "Srikakulam", "Visakhapatnam (Vizag)", "Vizianagaram"),
					
					"Arunachal Pradesh" => array("Anjaw district", "Changlang district", "East Kameng district", "East Siang district", "Kurung Kumey district", "Lohit district", "Lower Dibang Valley district", "Lower Subansiri district", "Papum Pare district", "Tawang district", "Tirap district", "Upper Dibang Valley district", "Upper Subansiri district", "Upper Siang district", "West Kameng district", "West Siang district"),
					
					"Assam" => array("Tinsukia", "Dibrugarh", "Sibsagar", "Dhemaji", "Jorhat", "Lakhimpur", "Golaghat", "Sonitpur", "Karbi Anglong", "Nagaon", "Marigaon", "Darrang", "Kamrup","Nalbari", "Barpeta", "Bongaigaon", "Goalpara", "Kokrajhar", "Dhubri", "North Cachar Hills", "Cachar", "Hailakandi", "Karimganj", "Kamrup Metropolitan", "Baksa", "Udalguri", "Chirang"),
					
					"Bihar" => array("Araria", "Aurangabad", "Banka", "Begusarai", "Bhagalpur", "Bhojpur", "Buxar", "Darbhanga", "Purba Champaran", "Gaya", "Gopalganj", "Jamui", "Jehanabad", "Khagaria", "Kishanganj", "Kaimur", "Katihar", "Lakhisarai", "Madhubani", "Munger", "Madhepura", "Muzaffarpur", "Nalanda", "Nawada", "Patna", "Purnia", "Rohtas", "Saharsa", "Samastipur", "Sheohar", "Sheikhpura", "Saran", "Sitamarhi", "Supaul", "Siwan", "Vaishali", "Pashchim Champaran"),
	
					"Chhattisgarh" => array("Bastar","Bijapur","Bilaspur","Dantewada (South Bastar)","Dhamtari","Durg","Janjgir-Champa","Jashpur","Kanker (North Bastar)","Kabirdham (Kawardha)","Korba","Koriya (Korea)","Mahasamund","Narayanpur","Raigarh","Raipur","Rajnandgaon","Surguja"),
	
					"Delhi" => array("Delhi"),
	
					"Goa" => array("Goa"),	
	
					"Gujarat" => array("Ahmedabad","Amreli District","Anand","Banaskantha","Bharuch","Bhavnagar","Dahod","The Dangs","Gandhinagar","Jamnagar","Junagadh","Kutch","Kheda","Mehsana","Narmada","Navsari","Patan","Panchmahal","Porbandar","Rajkot","Sabarkantha","Surendranagar","Surat","Tapi","Vadodara","Valsad","Halol"),

					"Haryana" => array("Ambala","Bhiwani","Chandigarh","Faridabad","Fatehabad","Gurgaon","Hisar","Jhajjar","Jind","Karnal","Kaithal","Karnal","Kurukshetra","Mahendragarh","Mewat","Panchkula","Panipat","Rewari","Rohtak","Sirsa","Sonipat","Yamuna Nagar","Palwal"),

					"Himachal Pradesh" => array("Bilaspur","Chamba","Hamirpur","Kangra","Kinnaur","Kulu","Lahaul and Spiti","Mandi","Shimla","Sirmaur","Solan","Una"),
					
					"Jammu and Kashmir" => array("Anantnag","Badgam","Bandipore","Baramula","Doda","Jammu","Kargil","Kathua","Kupwara","Leh","Poonch","Pulwama","Rajauri","Srinagar","Samba","Udhampur"),
					
					"Jharkhand" => array("Bokaro","Chatra","Deoghar","Dhanbad","Dumka","Purba Singhbhum","Garhwa","Giridih","Godda","Gumla","Hazaribagh","Koderma","Lohardaga","Pakur","Palamu","Ranchi","Sahibganj","Serai kela & Kharsawan","Pashchim Singhbhum","Ramgarh"),
					
					"Karnataka" => array("Bidar", "Belgaum", "Bijapur", "Bagalkot", "Bellary", "Bangalore Rural District", "Bangalore Urban district", "Chamarajnagar", "Chikmagalur", "Chitradurga", "Davanagere", "Dharwad", "Dakshina Kannada", "Gadag", "Gulbarga", "Hassan", "Haveri District", "Kodagu", "Kolar", "Koppal", "Mandya", "Mysore", "Raichur", "Shimoga", "Tumkur", "Udupi", "Uttara Kannada", "Ramanagara", "Chikballapur", "Yadagiri"),
					
					"Kerala" => array("Alappuzha","Cochin", "Ernakulam", "Idukki", "Kollam", "Kannur", "Kasaragod", "Kottayam ", "Kozhikode", "Malappuram", "Palakkad", "Pathanamthitta", "Thrissur", "Thiruvananthapuram ", "Wayanad"),

					"Madhya Pradesh" => array("Alirajpur", "Anuppur", "Ashok Nagar", "Balaghat", "Barwani", "Betul", "Bhind", "Bhopal", "Burhanpur", "Chhatarpur", "Chhindwara", "Damoh", "Datia", "Dewas", "Dhar", "Dindori", "Guna ", "Gwalior", "Harda", "Hoshangabad", "Indore", "Jabalpur", "Jhabua", "Katni", "Khandwa (East Nimar) ", "Khargone (West Nimar)", "Mandla", "Mandsaur", "Morena", "Narsinghpur", "Neemuch ", "Panna", "Rajgarh", "Ratlam", "Raisen", "Sagar", "Sehore", "Shahdol ", "Shajapur", "Sheopur", "Shivpuri", "Sidhi", "Singrauli", "Tikamgarh", "Ujjain", "Umaria", "Vidisha"),

					"Maharashtra" => array("Ahmednagar", "Akola", "Amrawati", "Aurangabad", "Bhandara", "Beed", "Buldhana", "Chandrapur", "Dhule", "Gadchiroli", "Gondiya", "Hingoli", "Jalgaon", "Jalna", "Kolhapur", "Latur", "City suburban", "Mumbai", "Nanded", "Nagpur", "Nashik", "Osmanabad", "Parbhani", "Pune", "Raigad", "Ratnagiri", "Sindhudurg", "Solapur", "Thane", "Wardha", "Washim", "Yavatmal"),
					
					"Manipur" => array("Bishnupur", "Churachandpur", "Chandel", "Imphal East", "Senapati", "Tamenglong", "Thoubal", "Ukhrul", "Imphal West"),
					
					"Meghalaya" => array("East Garo Hills", "East Khasi Hills", "Jaintia Hills", "Ri-Bhoi", "South Garo Hills", "West Garo Hills", "West Khasi Hills"),
					
					"Mizoram" => array("Aizawl", "Champhai", "Kolasib", "Lawngtlai", "Lunglei", "Saiha", "Serchhip"),
					
					"Nagaland" => array("Dimapur", "Kohima", "Mon", "Phek", "Tuensang", "Wokha", "Zunheboto"),
					
					"Orissa" => array("Angul", "Boudh", "Bhadrak ", "Bargarh", "Cuttack", "Debagarh", "Dhenkanal", "Ganjam", "Gajapati", "Jharsuguda", "Jajapur", "Jagatsinghpur", "Khordha", "Kendujhar", "Kalahandi", "Kandhamal", "Koraput", "Kendrapara", "Malkangiri", "Mayurbhanj", "Nabarangpur", "Nuapada", "Nayagarh", "Puri", "Rayagada", "Sambalpur", "Subarnapur", "Sundargarh"),
					
					"Puducherry" => array("Karaikal", "Mahe", "Puducherry", "Yanam"),
					
					"Punjab" => array("Amritsar", "Bathinda ", "Chandigarh", "Firozpur", "Fatehgarh Sahib", "Gurdaspur", "Jalandhar", "Kapurthala", "Ludhiana", "Mansa ", "Moga", "Mukatsar", "Nawan Shehar", "Patiala", "Rupnagar", 
					"Sangrur","Phagwara","Hoshiarpur"),
					
					"Rajasthan" => array("Ajmer", "Alwar", "Barmer", "Bharatpur", "Baran", "Bundi", "Bhilwara", "Churu", "Chittorgarh", "Dausa", "Dholpur", "Dungapur ", "Ganganagar ", "Hanumangarh", "Juhnjhunun", "Jalore", "Jodhpur", "Jaipur", "Jaisalmer", "Jhalawar", "Karauli", "Kota", "Nagaur", "Pali", "Pratapgarh", "Rajsamand ", "Sikar", "Sawai Madhopur", "Sirohi", "Tonk", "Udaipur","Bikaner","Banswara"),
					
					"Sikkim" => array("East Sikkim", "North Sikkim", "South Sikkim", "West Sikkim"),
					
					"Tamil Nadu" => array("Ariyalur", "Chennai", "Coimbatore", "Cuddalore", "Dharmapuri", "Dindigul", "Erode", "Kanchipuram", "Kanyakumari", "Karur", "Madurai", "Nagapattinam", "The Nilgiris", "Namakkal", "Perambalur", "Pudukkottai", "Ramanathapuram", "Salem", "Sivagangai", "Tiruppur", "Tiruchirappalli", "Theni", "Tirunelveli", "Thanjavur", "Thoothukudi", "Thiruvallur ", "Thiruvarur", "Tiruvannamalai", "Vellore", "Villupuram" ,"Tuticorin"),
					
					"Tripura" => array("Dhalai", "North Tripura", "South Tripura", "West Tripura"),
					
					"Uttarakhand" => array("Almora", "Bageshwar", "Chamoli", "Dehradun ", "Haridwar", "Nainital", "Pauri Garhwal", "Pithoragharh", "Rudraprayag", "Rishikesh" , "Tehri Garhwal", "Udham Singh Nagar", "Uttarkashi"),
					
					"Uttar Pradesh" => array("Agra ", "Allahabad", "Aligarh ", "Ambedkar Nagar", "Auraiya", "Azamgarh", "Barabanki", "Badaun ", "Bagpat", "Bahraich", "Bijnor", "Ballia", "Banda", "Balrampur", "Bareilly", "Basti", "Bulandshahr", "Chandauli", "Chitrakoot", "Deoria", "Etah", "Kanshiram Nagar", "Etawah", "Firozabad", "Farrukhabad", "Fatehpur", "Faizabad", "Gautam Buddha Nagar", "Gonda", "Ghazipur", "Gorkakhpur", "Ghaziabad", "Hamirpur", "Hardoi", "Mahamaya Nagar", "Jhansi", "Jalaun", "Jyotiba Phule Nagar", "District", "Kanpur Dehat", "Kannauj", "Kanpur Nagar", "Kaushambi", "Kushinagar", "Lalitpur", "Lakhimpur", "Lucknow", "Mau", "Meerut", "Maharajganj", "Mahoba", "Mirzapur ", "Moradabad", "Mainpuri", "Mathura", "Muzaffarnagar", "Noida", "Pilibhit", "Pratapgarh", "Rampur", "Rae Bareli ", "Saharanpur", "Sitapur ", "Shahjahanpur", "Sant Kabir Nagar", "Siddharthnagar", "Sonbhadra", "Sant Ravidas Nagar", "Sultanpur", "Shravasti", "Unnao", "Varanasi"),
					
					"West Bengal" => array("Birbhum", "Bankura", "Bardhaman", "Darjeeling", "Dakshin Dinajpur", "Hooghly", "Howrah", "Jalpaiguri", "Cooch Behar", "Kolkata", "Malda", "Murshidabad", "Nadia", "North 24 Parganas", "South 24 Parganas", "Purulia", "Uttar Dinajpur"),	
	);
	
	ksort($states);					
	
		foreach($states as $key=>$val)
		{
			if(is_array($states[$key]))
			{	asort($states[$key]);			
			}
		}	
		
	$reg_yrs=array("2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931", "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915", "1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907", "1906", "1905", "1904", "1903", "1902", "1901", "1900");
	
	$emp_det=array("Less than 5 People", "5-10 People", "11-50 People", "51-100 People", "101-500 People", "501-1000 People", "Above 1000 People");
	
	$own_type=array("Corporation/Limited Liability Company", "Partnership", "LLC (Ltd Liability Corp)", "Individual (Sole proprietorship)", "Professional Association", "Others");
	
	$certification=array("HACCP", "ISO 9001:2000", "ISO 9001:2008", "QS-9000", "ISO 14001:2004", "ISO/TS 16949", "SA8000", "ISO 17799", "OHSAS 18001", "TL 9000", "Others");
	

?>