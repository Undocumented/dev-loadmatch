{literal}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
{/literal}
<div class="container-fluid container-dark">
        <div class="row">
            <div class="col col-lg-10 offset-md-2">
                <h1 class="mt-2 mb-2">
                    Available Loads
                </h1>  
            </div>
        </div>
    </div>
</div>
<div class="container container-light">
    <div class="row">
        <div class="col col-lg-4">
            <form name="search" id="search" method="post" action="#">
            <h4>Refine Search</h4>
            <div class="form-group">
                <label for="sel1">Deliver To:</label>
                <select class="form-group" id="sel1">
                    <option value="please Select" selected="selected" >Please Select</option>
                    {section name=data loop=$categories}
                    <option value="{$categories[data].city}" {if $filter_to==$categories[data].city} selected="selected"{/if} >{$categories[data].city} </option>
                    {/section}
                </select>
            </div>
            <p>Pickup From</p>
            <p>Load Industry</p>
            <p>Load Type</p>
            <p>Transport Type</p>
            <p>Body &amp; Bulk Type</p>
            <p>Size Classification</p>
            <p>Payload KG</p>
            <p>Length Meters</p>
            <p>Width Meters</p>
            <p>Preferred Distance</p>
            <p>Other</p>
            </form>
        </div>
        <div class="col col-lg-8">
                {*$categories|@print_r*}
                    
        </div>
    </div>
</div>

{literal}
    <script type="text/javascript">
    $('.selectpicker').selectpicker({
  style: 'btn-info',
  size: 4
});
    </script> 
{/literal}
