<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="{$site_url}/">
            <img class="img-fluid" src="{$site_url}/images/{$logo}" alt="{$company_name}" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{$site_url}/about.html">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{$site_url}/subscriptions.html">Subscriptions</a>
                </li>
                {if $smarty.session.user_id==''}
                <!--{$page}//{$action}//-->                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLogin" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Login
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownLogin">
                        <a class="dropdown-item" href="{$site_url}/login.html">Login</a>
                        <a class="dropdown-item" href="{$site_url}/registration.html">Registration</a>
                    </div>
                </li>
                {else}
                <!--{$page}//{$action}-->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownAccount" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        My Account
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownAccount">
                        <a class="dropdown-item" href="profile.html">Profile</a>
                        <a class="dropdown-item" href="inbox.html">Inbox</a>
                        {if $smarty.session.user_type=='C'}
                        <a class="dropdown-item" href="my_post_job.html">My Shipments</a>
                        {/if}
                        {if $smarty.session.user_type=='T'}                        
                        <a class="dropdown-item" href="my_post_job.html">Active Shipment</a>
                        {/if}                        
                        <a class="dropdown-item" href="booked.html">Booked Shipment</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="booked_complete.html">Completed Shipment</a>
                        <a class="dropdown-item" href="canceled.html">Canceled Shipment</a>
                        <a class="dropdown-item" href="logout.html">Logout</a>
                    </div>
                </li>                
                {/if}
                <li class="nav-item">
                    <a class="nav-link" href="{$site_url}/post_shipping.html">New Shipment</a>                    
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{$site_url}/search_shipping.html">Available Loads</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://loadmatch.co.za/classifieds/">Classifieds</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Services
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="partners.html">Loadmatch Partners</a>
                        <a class="dropdown-item" href="directory.html">Directory Services</a>
                        <a class="dropdown-item" href="insurance.html">Insurance</a> 
                        <a class="dropdown-item" href="benefits.html">Group Benefits</a> 
                        <a class="dropdown-item" href="http://loadmatch.co.za/jobs/"> Jobs</a>
                        <a class="dropdown-item" href="training.html">Training</a>
                        <a class="dropdown-item" href="tools.html">Tools</a>                  
                    </div>
                </li>                
                <li class="nav-item">
                    <a class="nav-link" href="contact.html">Contact </a>
                </li> 
                {if $smarty.session.user_type=='C'}
                <li class="nav-item">
                    <a class="nav-link" href="#"><img src="{$site_url}/images/user.png" width="20" height="20" /> Customer </a>
                </li>
                {/if}
                {if $smarty.session.user_type=='T'}
                <li class="nav-item">
                    <a class="nav-link" href="#"><img src="{$site_url}/images/agent.png" width="20" height="20" /> Transporter </a>
                </li>
                {/if}
                {if $smarty.session.user_type=='T'}
                <li class="nav-item">
                    <a class="nav-link" href="#"><img src="{$site_url}/images/agent.png" width="20" height="20" /> Broker </a>
                </li>
                {/if}                
            </ul>
        </div>
    </div>
</nav>