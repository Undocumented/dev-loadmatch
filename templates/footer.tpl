<footer class="footer">
    <div class="container">
        <div class="row">
            <div data-wow-delay=".1s" data-wow-duration="2s" class="col-lg-4 col-sm-4 address wow fadeInUp animated" style="visibility: visible; animation-duration: 2s; animation-delay: 0.1s; animation-name: fadeInUp;" data-wow-animation-name="fadeInUp">
                <h4>Contact Info </h4>
                <address>
                    <h5>{$company_name}</h5>
                    <p><i class="fa fa-home pr-10"> </i>Address: {$company_address}<br>
                    <i class="fa fa-mobile pr-10"></i> Mobile: {$mobile_no} <br>
                    <i class="fa fa-phone pr-10"></i> Phone: {$phone} <br>
                    <i class="fa fa-envelope pr-10"></i> Email: <a href="javascript:;">{$SITE_EMAIL}</a></p>
                </address>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div data-wow-delay=".5s" data-wow-duration="2s" class="page-footer wow fadeInUp animated" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeInUp;" data-wow-animation-name="fadeInUp">
                    <h4>Our Company</h4>
                    <ul class="page-footer-list">
                        <li> <i class="fa fa-angle-right"></i> <a href="about.html"> About Us</a></li>
                        <li> <i class="fa fa-angle-right"></i> <a href="privacy.html"> Privacy Policy</a></li>
                        <li> <i class="fa fa-angle-right"></i> <a href="terms.html"> Terms &amp; Conditions</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div data-wow-delay=".7s" data-wow-duration="2s" class="text-footer wow fadeInUp animated" style="visibility: visible; animation-duration: 2s; animation-delay: 0.7s; animation-name: fadeInUp;" data-wow-animation-name="fadeInUp">
                    <h4>Why Choose Us</h4>
                    <p> Load, freight and transport exchange service with a robust, instinctive load-matching and freight exchange calculator. Powerful real-time load tracking from departure to destination. Optimise your fleet’s utilisation quickly and efficiently. </p>
                </div>
            </div>
        </div>
    </div>
</footer>
                
<footer class="footer-small">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="copyright">
                    <p>{$company_name} &nbsp;&copy; 2015</p>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 pull-right">
                <ul class="social-link-footer list-unstyled">
                    <li data-wow-delay=".1s" data-wow-duration="2s" class="wow flipInX animated" style="visibility: visible; animation-duration: 2s; animation-delay: 0.1s; animation-name: flipInX;" data-wow-animation-name="flipInX"><a target="_new" href="https://www.facebook.com/loadmatchafrica"><i class="fa fa-facebook"></i></a></li>

		</ul>
            </div>

        </div>
    </div>
</footer>
