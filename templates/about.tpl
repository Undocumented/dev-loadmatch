{*Page Veriables*}
{* Add on Page *}
{assign var="reviveid" value="43422af75a9f9066297995789acc7f48"}
{assign var="zone_tmain" value="83"}
{assign var="mzone_tmain" value="83"}
{assign var="zone_tleft" value="91"}
{assign var="mzone_tleft" value="91"}
{assign var="zone_tright" value="92"}
{assign var="mzone_tright" value="92"}
{assign var="zone_mleft" value="93"}
{assign var="mzone_mleft" value="93"}
{assign var="zone_mright" value="96"}
{assign var="mzone_mright" value="96"}
{assign var="zone_bleft" value="94"}
{assign var="mzone_bleft" value="94"}
{assign var="zone_bright" value="97"}
{assign var="mzone_bright" value="97"}

{* MDR: About Header *}
<div class="container-fluid container-dark">
        <div class="row">
            <div class="col col-lg-10 offset-md-2">
                <h1 class="mt-2 mb-2">
                    About Us
                </h1>  
            </div>
        </div>
    </div>

{* MDR: Slider *}
{* MDR: Banner Adds landscape *}
<div class="container-fluid d-none d-md-block text-center ">
    <div class="row">
        <div class="col col-md-12 no-padding d-none d-md-block" >
             {include file="zone_tmain.tpl"}
           
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col col-lg-12 linespace ">
        </div>
    </div>
    <div class="row">
        <div class="col col-md-2 d-none d-md-block">
            <div class="container ">
                <div class="row">
                    <div class="col col-md-12">
                        {include file="zone_tleft.tpl"}
                    </div>
                </div>
                <div class="row">                
                    <div class="col col-md-12">
                        {include file="zone_mleft.tpl"}
                     </div>
                </div>
                <div class="row">                
                    <div class="col col-md-12">
                        {include file="zone_bleft.tpl"}
                    </div>
                </div>
            </div>
        </div>

        <div class="col col-md-8">
            <div class="container">
                <div class="row">
                    <div class="col col-md-12 linespace"></div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col col-md-12">
                        <div id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="card mb-2">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h5 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            History of LoadMatch
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <p>
                                                        LoadMatch was created due to the ever-increasing need for business efficiency. Another driving force for the system was to make modern logistics operations and applications available to the small to medium transport enterprise as well, right down to owner driver level.
                                                    </p>
                                                    <p>
                                                        LoadMatch was designed by transport and supply chain experts and modelers, with the first planning for an all-inclusive service portal kicking off in January 2015. The base of the system is built around the ever-increasing challenges in the industry such as driver shortage, increasing compliance complexities, volatile costs and constantly evolving consumer demand and the tension between cost-cutting and maximising profit.
                                                    </p>
                                                    <p>
                                                        In a sector as multifaceted as transportation, LoadMatch was therefore built around improvement and efficiency, incorporating existing complicated approaches and models with best known practices and innovations on the horizon by addressing the constraints -
                                                    </p>
                                                    <div>
                                                        <ul class="list-style">
                                                            <li>how transportation and logistics headlines affect the industry;</li>
                                                            <li >how valuable knowledge and service models available in the smaller niche markets can be incorporated into the chain; and</li>
                                                            <li >how carriers and shippers of all sizes can join forces to work toward mutual goals.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-2">
                                <div class="card-header" role="tab" id="headingTwo">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Connecting People
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="card-block">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <p>LoadMatch is an online transport marketplace that connects customers with transport and delivery companies for all shipment collections and deliveries by means of loading shipments. Transporters place competing quotes to win a customer's business, which brings down delivery costs and makes transport more affordable and efficient.</p>
                                                    <p>After reviewing quoted prices, using the LoadMatch freight calculator™ (based on proprietary freight data built into the system) to compare shipping rates, Load Owners can enjoy significant savings when accepting quotes for their shipments. When Carriers have empty space in their trucks, they can use LoadMatch to fill it at discounted rates. Our advanced search tools and apps allow Truckers to find shipments nearby as well as along their routes.</p>
                                                    <p>LoadMatch is a privately owned company of transport, supply chain and distribution centre experts based in Johannesburg, Gauteng, South Africa, operating in South Africa and Africa.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-2">
                                <div class="card-header" role="tab" id="headingThree">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            LoadMatch for Carriers
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="card-block">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <p>LoadMatch provides subscribed transport, distribution and delivery companies immediate access to transport jobs. LoadMatch charges a small admin fee on completed jobs, unlike similar companies who inflate prices substantially. The admin fee charged by LoadMatch is to cover resources, administrative, ongoing development, app updates and system management costs.  With thousands of deliveries listed and available on any given day, owner-operators and large freight companies alike have an equal opportunity to bid on jobs that suit their needs and routes. Through LoadMatch, service providers can fill excess capacity or find profitable backloads that help maximise their business and time on the road. LoadMatch also gives service providers free tools to help them establish and build their online business reputation through its feedback system and customer reviews.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-2">
                                <div class="card-header" role="tab" id="headingFour">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            LoadMatch for Delivery Customers
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="card-block">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <p>LoadMatch makes it easy to get in touch with thousands of transport, distribution and delivery companies, making it simple and affordable to deliver or transport anything. Customers decide on transporters based on feedback and unbiased customer reviews through a vetting and rating system.</p>
                                                    <p>Our rating and review system allows our Load Owners to make informed decisions when selecting a transporter. High customer satisfaction rates can be achieved as well as vast savings on rates through the LoadMatch service portal.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingFive">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            LoadMatch Service Partners
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingFive">
                                    <div class="card-block">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <p>For more information on becoming a LoadMatch Service Partner for specialist services and products to the Transport Industry through our portal, please contact martin@loadmatch.co.za.</p>
                                                    <p>The admin fee charged by LoadMatch is to cover resources, administrative, ongoing development, app updates and system management costs.  With thousands of deliveries listed and available on any given day, owner-operators and large freight companies alike have an equal opportunity to bid on jobs that suit their needs and routes. Through LoadMatch, service providers can fill excess capacity or find profitable backloads that help maximise their business and time on the road. LoadMatch also gives service providers free tools to help them establish and build their online business reputation through its feedback system and customer reviews.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col col-md-2 d-none d-md-block">
            <div class="container">
                <div class="row">
                    <div class="col col-md-12">
                        {include file="zone_tright.tpl"}
                    </div>
                </div>
                <div class="row">                
                    <div class="col col-md-12">
                        {include file="zone_mright.tpl"}
                    </div>
                </div>
                <div class="row">                
                    <div class="col col-md-12">
                        {include file="zone_bright.tpl"}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12 linespace ">
        </div>
    </div>    
</div>
  
