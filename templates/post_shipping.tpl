<!--breadcrumbs start-->
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-4">
                <h1>New Shipment</h1>
            </div>
            <div class="col-lg-8 col-sm-8">
                <ol class="breadcrumb pull-right">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="#">New Shipment</a></li>
                    <li class="active">Add</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--breadcrumbs end-->

<!--container start-->
<div class="component-bg">
    <div class="container">
        <!-- Forms
================================================== -->
        <div class="bs-docs-section mar-b-5">
            <h2 id="forms" class="page-header">New Shipment</h2>

            <form class="form-horizontal" name="post_shipment"  method="post" action="#" role="form" enctype="multipart/form-data">
                <h3 align="left">Sender Details</h3>
                <hr>
                <div class="form-group">
                    <label for="inputEmail3"  class="col-sm-2 control-label" >Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="" id="Title" name="Title" placeholder="Enter Title Here">

                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3"  class="col-sm-2 control-label" >Full Name</label>
                    <div class="col-sm-10">
                        <input type="text" readonly="readonly" class="form-control" value="{$smarty.session.name}" id="s_user_name" name="s_user_name" placeholder="User Name">
                        <input type="hidden" name="user_id" value="{$smarty.session.user_id}" />

                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3"  class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" readonly="readonly" class="form-control" value="{$smarty.session.email}" id="s_user_email" name="s_user_email" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
                    <div class="col-sm-10">
                        <input type="text" readonly="readonly" class="form-control" id="mobile"  name="mobile" placeholder="Mobile no" value="{$functions->get_mobile($smarty.session.user_id)}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Collection Province</label>
                    <div class="col-sm-10">

                        <select name="form_state" id="form_state" class="form-control border-radius"  onchange="get_state_city(this.value);">

                            <option value="please Select" selected="selected" >Please Select Collection Province</option>

                            {section name=data loop=$categories}

                            <option value="{$categories[data].state}" {if $useredit[0].state==$categories[data].state} selected="selected" {/if} >{$categories[data].state} </option>
                            {/section}


                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Collection City</label>
                    <div class="col-sm-10">
         
          <span id="city_change">
                     <select class="form-control"  name="form_change" id="form_change" >
                     {if $edit_id==''}
        <option value="please Select" selected="selected" >Please Select Collection City</option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
                         {/section}
     {/if}
</select>

                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Collection Location/Address</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="loc_sen" id="loc_sen" rows="3"></textarea>
                    </div>

                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pickup Dates</label>
                    <div class="col-xs-5" align="left">

                        <INPUT NAME="pickup_start_date" id="pickup_start_date" TYPE="date" value="{$functions->get_current_date()}" />--Between--

                        <INPUT NAME="pickup_end_date" id="pickup_end_date" TYPE="date"   value="{$functions->get_current_date('1')}" />
                    </div>
                </div>
                <hr />
                <h3 align="left">Receiver Details</h3>
                <hr />
                <div class="form-group">
                    <label for="inputEmail3"  class="col-sm-2 control-label" >Receiver Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="" id="r_user_name" name="r_user_name" placeholder="Full Name">


                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Receiver Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" value="" id="r_user_email" name="r_user_email" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Receiver Mobile</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="r_mobile"  name="r_mobile" placeholder="Mobile no">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Delivery Province</label>
                    <div class="col-sm-10">

                        <select name="to_state" id="to_state" class="form-control border-radius"  onchange="get_state_city_to(this.value);">

                            <option value="please Select" selected="selected" >Please Select Delivery Province</option>

                            {section name=data loop=$categories}

                            <option value="{$categories[data].state}" {if $useredit[0].state==$categories[data].state} selected="selected" {/if} >{$categories[data].state} </option>
                            {/section}


                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Delivery City</label>
                    <div class="col-sm-10">
         
          <span id="city_change1">
                     <select class="form-control"  name="to_city" id="to_city" >
                     {if $edit_id==''}
        <option value="please Select" selected="selected" >Please Select Delivery City</option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
                         {/section}
     {/if}
</select>

                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Delivery Location/Address</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="loc_rec" id="loc_rec" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Delivery Dates</label>
                    <div class="col-xs-6" align="left">
                        <INPUT NAME="delv_start_date" id="delv_start_date" TYPE="date"   value="{$functions->get_current_date('3')}" />--Between--

                        <INPUT NAME="delv_end_date" id="delv_end_date" TYPE="date"   value="{$functions->get_current_date('5')}" />
                    </div>

                </div>

                <hr />
                <h3 align="left">Shipment Details</h3>
                <hr />


                <div class="form-group">
                    <label for="industry_type" class="col-sm-2 control-label">Industry Type</label>
                    <div class="col-sm-10">
                        <select name="industry_type"  id="industry_type" class="form-control border-radius" onchange="get_load(this.value);" >
                            <option value="Please Select" selected="selected" >Please Select Load Industry</option>
                            {section name=data loop=$industry}

                            <option value="{$industry[data].id}" >{$industry[data].name} </option>
                            {/section}
                        </select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="load_type" class="col-sm-2 control-label">Load Type</label>
                    <div class="col-sm-10">
                        <span id="load">
                            <select name="load_type"  id="load_type" class="form-control border-radius" onchange="get_transport(this.value);" >
                                <option value="Please Select" selected="selected" >Please Select Load Type</option>
                                {section name=data loop=$load}

                                <option value="{$load[data].id}" >{$load[data].name} </option>
                                {/section}
                            </select>
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="transport_type" class="col-sm-2 control-label">Transport Type</label>
                    <div class="col-sm-10">
                        <span id="transport">
                            <select name="transport_type"  id="transport_type" class="form-control border-radius" onchange="get_bulk(this.value);" >
                                <option value="Please Select" selected="selected" >Please Select Transport Type</option>
                                {section name=data loop=$transport}

                                <option value="{$transport[data].id}" >{$transport[data].name} </option>
                                {/section}
                            </select>
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="body_bulk_type" class="col-sm-2 control-label">Body Bulk Type</label>
                    <div class="col-sm-10">
                        <span id="body_bulk">
                            <select name="body_bulk_type"  id="body_bulk_type" class="form-control border-radius" onchange="get_size(this.value);" >
                                <option value="Please Select" selected="selected" >Please Select Body Bulk Type</option>
                                {section name=data loop=$body_bulk}

                                <option value="{$body_bulk[data].id}" >{$body_bulk[data].name} </option>
                                {/section}
                            </select>
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="size_type" class="col-sm-2 control-label">Size Classification</label>
                    <div class="col-sm-10">
                        <span id="size">
                            <select name="size_type"  id="size_type" class="form-control border-radius" >
                                <option value="Please Select" selected="selected" >Please Select Size Classification</option>
                                {section name=data loop=$size}

                                <option value="{$size[data].id}" >{$size[data].name} </option>
                                {/section}
                            </select>
                        </span>
                    </div>
                </div>










                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Transport Type</label>
                    <div class="col-sm-10">
                        <span id="category">
                            <select name="category_id"  id="category_id" class="form-control border-radius" onchange="get_sub_cat(this.value);" >
                                <option value="please Select" selected="selected" >Please Select Load Industry</option>
                                {section name=data loop=$useredit1}

                                <option value="{$useredit1[data].id}" >{$useredit1[data].category} </option>
                                {/section}
                            </select>
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Load Type</label>
                    <div class="col-sm-10">
         
                     <span id="sub_cat">
                         <select class="form-control"  name="sub_cat_id" id="sub_cat_id" onchange="get_sub_next_cat(this.value);" >
                                             {if $edit_id==''}
                            <option value="please Select" selected="selected" >Please Select Load Type</option>
                            {else}

                            {section name=data loop=$city_cat}
                            <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
                                             {/section}
                             {/if}
                        </select>
                     </span>

                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Transport Type</label>
                    <div class="col-sm-10">
         
         <span id="sub_next_cat">
                     <select class="form-control"  name="sub_cat_id" id="sub_cat_id" onchange="get_last_cat(this.value);" >
                     {if $edit_id==''}
        <option value="please Select" selected="selected" >Please Select Transport Type</option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
                         {/section}
     {/if}
</select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Body &amp Bulk Type</label>
                    <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="last_cat_id" onchange="get_fifth_cat(this.value);" id="sublast_cat_id_cat_id" >
                     {if $edit_id==''}
        <option value="please Select" selected="selected" >Please Select Body &amp Bulk Type</option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
                         {/section}
     {/if}
</select>

                    </div>
                </div>


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label" >Size Classification</label>
                    <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="fifth_cat_id" id="fifth_cat" onchange="get_sixth_cat(this.value);" >
                     {if $edit_id==''}
        <option value="please Select" selected="selected" >Please Select Size Classification </option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
                         {/section}
     {/if}
</select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Payload KG</label>
                    <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sixth_cat_id" id="sixth_cat" onchange="get_seventh_cat(this.value);" >
                     {if $edit_id==''}
        <option value="please Select" selected="selected" >Please Select Payload KG</option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
                         {/section}
     {/if}
</select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Length Meters</label>
                    <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="seventh_cat_id" id="seventh_cat" onchange="get_eigth_cat(this.value);" >
                     {if $edit_id==''}
        <option value="please Select" selected="selected" >Please Select Length Meters </option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
                         {/section}
     {/if}
</select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Width Meters </label>
                    <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="eight_cat_id" id="eigth_cat" onchange="get_ninth_cat(this.value);" >
                     {if $edit_id==''}
        <option value="please Select" selected="selected" >Please Select Width Meters </option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
                         {/section}
     {/if}
</select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Preferred Distance </label>
                    <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="ninth_cat_id" id="ninth_cat"  onchange="get_tenth_cat_id(this.value);" >
                     {if $edit_id==''}
        <option value="please Select" selected="selected" >Please Select Preferred Distance </option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
                         {/section}
     {/if}
</select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Other</label>
                    <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="tenth_cat_id" id="tenth_cat"   >
                     {if $edit_id==''}
        <option value="please Select" selected="selected" >Please Select </option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
                         {/section}
     {/if}
</select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="Dimensions" class="col-sm-2 control-label">Dimensions</label>
                    <div class="col-sm-10">
                        <select name="Dimensions"  id="Dimensions" class="form-control border-radius" >
                            <option value="please Select" selected="selected" >Please Select Dimensions</option>
                            <!--   <option value="Feet"  >Feet</option>-->
                            <option value="Liters" >Liters</option>
                            <option value="Centimeters"  >Centimeters</option>
                            <option value="Meters"  >Meters</option>

                        </select>

                    </div>
                </div>
                <table>
                    <tr><td width="25%"></td><td><table>
                                <tr><td>
                                        <div class="form-group">
                                            <label for="Width" class="col-sm-3 control-label">Width  </label>
                                            <div class="col-xs-2">
                                                <input type="text" name="Width" id="Width" class="form-control" style="min-width:70px;" placeholder="Width">
                                            </div>

                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="Height" class="col-sm-3 control-label">Height  </label>
                                            <div class="col-xs-2">
                                                <input type="text" name="Height" id="Height" class="form-control" style="min-width:70px;" placeholder="Height">
                                            </div>

                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="Length" class="col-sm-3 control-label">Length  </label>
                                            <div class="col-xs-2">
                                                <input type="text" name="Length" id="Length" class="form-control" style="min-width:70px;" placeholder="Length">
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <label for="Weight" class="col-sm-3 control-label">Weight  </label>
                                            <div class="col-xs-2">
                                                <input type="text" name="Weight" id="Weight" class="form-control" style="min-width:70px;" placeholder="Weight">
                                            </div>

                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="Qty" class="col-sm-3 control-label">Quantity  </label>
                                            <div class="col-xs-2">
                                                <input type="text" name="Qty" id="Qty" class="form-control" style="min-width:75px;" placeholder="Quantity">
                                            </div>

                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label for="value" class="col-sm-3 control-label">Value ({$functions->get_currency()})</label>
                                            <div class="col-xs-2">
                                                <input type="text" name="value" id="value" class="form-control" style="min-width:70px;" placeholder="Value">
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <!-- renamed from logo to shipmaent image-->
                <div class="form-group">
                    <label for="Shipment_image" class="col-sm-2 control-label">Shipment Image </label>
                    <div class="col-sm-10">
                        <input type="file" name="Shipment_image" class="btn btn-default" style="max-width:240px;"/>
                    </div>

                </div>
                <div class="form-group" >
                    <label for="description" class="col-sm-2 control-label">Description </label>
                    <div class="col-sm-10">

                        <textarea class="ckeditor" style="width:100%; height:200px;" name="description" > </textarea>

                    </div>

                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" name="submit" class="btn btn-default" onclick="return valid_post(document.post_shipment);">Submit</button>
                    </div>
                </div>
            </form>
        </div><!-- /.bs-example -->





    </div>
</div>
</div>
<!--container end-->