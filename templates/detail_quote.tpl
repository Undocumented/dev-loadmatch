  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1> Transporter Detail </h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#"> Transporter Detail</a></li>
                          <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
 <div class="white-bg">

        <!-- career -->
    <div class="container career-inner">
        <div class="row">
            <div class="col-md-12 career-head">
                <h1>Order id :{$search[0].order_id}
</h1> 

            </div>
            <form method="post" action="#" name="book" style="text-align:right;" > 
                    <input type="hidden" value="{$search[0].order_id}" name="order_id" />
                    <input type="hidden" value="{$search[0].id}" name="id" />
                    <input type="submit" name="submit" value="Book Now" class="btn btn-info" onclick="return confirm('Do You  want to book it ?');"/>
                    
                    </form>		
        </div>
        <hr>
       
         <table align="center" width="100%" border="0">
     <tr>
     <td>
        <div class="row" style="background-color:#CCC;">
            <div class="col-md-6">
                <div class="candidate wow fadeInLeft"  style="text-align:left;">
                    <h1> Transporter Detail</h1>
                    <p class="align-left"></p>
                    <h4></h4>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-angle-right pr-10"></i> Transporter Name :{$functions->get_name($search[0].agent_id)}</li>
                        <li><i class="fa fa-angle-right pr-10"></i> Transporter Province :{$functions->get_State($search[0].agent_id)} </li>
                        <li><i class="fa fa-angle-right pr-10"></i> Transporter City :{$functions->get_city($search[0].agent_id)}</li>
                        <li><i class="fa fa-angle-right pr-10"></i> location :{$functions->get_location($search[0].agent_id)}</li>
                                            
                    </ul>
                 {*  <a onclick="view_agent_review('{$search[0].agent_id}',document.view_detail);" class="btn btn-info" >View Transporter Review</a> *}
                </div>
                
                <div class="candidate wow fadeInLeft"  style="text-align:left;">
                    <h1>Pickup Dates</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                         <li><i class="fa fa-angle-right pr-10"></i>Start Date :{$search[0].collect_start_date|date_format:"%d- %m -%Y"}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>End Date :{$search[0].collect_end_date|date_format:"%d- %m -%Y"} </li>
                       
                    </ul>
                    
                </div>
               
            </div>
            <div class="col-md-6">
                <div class="candidate wow fadeInRight"  style="text-align:left;">
                    <h1>Payment Detail</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                     <li><i class="fa fa-angle-right pr-10"></i>Quote Price :({$functions->get_currency()}){$search[0].starting_price}</li>
                         <li><i class="fa fa-angle-right pr-10"></i>Payment Methods :{$search[0].payment_methods}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Payment Accepted :{$search[0].payment_accepted} </li>
                        <li><i class="fa fa-angle-right pr-10"></i>Payment Terms :{$search[0].payment_terms}</li>
                        
                    </ul>
                  
                </div>
                
                <div class="candidate wow fadeInRight" style="text-align:left;">
                    <h1>Delivery Dates</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                       <li><i class="fa fa-angle-right pr-10"></i>Start Date :{$search[0].delivery_start_date|date_format:"%d- %m -%Y"}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>End Date :{$search[0].delivery_end_date|date_format:"%d- %m -%Y"} </li>
                    </ul>
                   
                </div>
               
                
            </div>
             
        </div>
        <div class="row" style="background-color:#9FC">
         <div class="col-md-6">
                <div class="candidate wow fadeInRight"  style="text-align:left;">
                    <h1>Available Vehicle</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                     {$functions->get_agent_vehicle_front($search[0].agent_id)}
                        
                    </ul>
                  
                </div>
               
                
        </div>
         <div class="col-md-6">
                <div class="candidate wow fadeInRight"  style="text-align:left;">
                    <h1>Other Detail</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                      <li><i class="fa fa-angle-right pr-10"></i>Experience :{$functions->get_agent_detail_experiance($search[0].agent_id)} Years</li>
                    {*  <li><i class="fa fa-angle-right pr-10"></i>Number of Drivers  :{$functions->get_agent_detail_no_employ($search[0].agent_id)} </li> *}
                      <li><i class="fa fa-angle-right pr-10"></i><a onclick="view_agent_review('{$search[0].agent_id}',document.view_detail);" style="font-style:bold; color:#F00;cursor: pointer;">TRANSPORTER REVIEW</a></li>
                    </ul>
                  
                </div>
              
                
        </div>
        </div>
         </td>
        </tr>   
        </table>   
    <!-- career -->
       </div>
    </div>
  