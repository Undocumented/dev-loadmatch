{* MDR: Login Header *}
<div class="container container-dark">
        <div class="row">
            <div class="col col-lg-12">
                <h1  class="mt-2 mb-2">
                    Sign In
                </h1>            
            </div>
        </div>
    </div>
<div class="container container-light">
    <div class="row">
        <div class="col col-md-12  linespace">
            {if $err!=''} 
            <div class="notibar msgsuccess">
                {$err}
            </div>
            {/if}
            {if $msg=='Y'} 
                <div class="notibar msgsuccess">
                    <div class="alert alert-success" >Please Enter Correct Login Detail!</div>
                </div>
            {/if}   
        </div>
    </div>
    <div class="row">
        <div class="col col-md-12">
            <form id="loginForm" method="POST" action="" novalidate="novalidate" name="login">
                <div class="form-group">
                    <label for="username" class="control-label">eMail address or LoadMatch username:</label>
                    <input type="text" class="form-control" id="username" name="user_name" value="" required="" title="Please enter you username" placeholder="example@gmail.com">
                    <span class="form-text"></span>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label">LoadMatch password:</label>
                    <input type="password" class="form-control" id="password" name="password" value="" required="" title="Please enter your password" placeholder="Enter password">
                    <span class="form-text"></span>
                </div>
                {* MDR: Not sure what this is for *}
                <div id="loginErrorMsg" class="alert alert-error hide alert-dismissible">
                    Wrong username or password?
                    <input type="checkbox" aria-label="Checkbox for following text input">
                </div>
                <div class="checkbox">
                </div>
                <button class="btn btn-lg btn-login btn-block" name="submit" type="submit" onclick="return Valid_add_login(document.login);">Sign in</button>
                <div class="clearfix deatel">
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-12 linespace">
            <p class="lead">Register now for Free, <span class="text-success">or SUBSCRIBE to:</span></p>
            <ul class="list-unstyled">
                <li><span class="fa fa-check text-success"></span> See all your orders</li>
                <li><span class="fa fa-check text-success"></span> See all available loads</li>
                <li><span class="fa fa-check text-success"></span> Enjoy benefits and special offers</li>
                <li><span class="fa fa-check text-success"></span> Be part of a virtual transport network</li>
            </ul>
            <p><a href="registration.html" class="btn btn-info btn-block">Yes please, register me now!</a></p>
            </div>
        </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col col-lg-12 linespace"> 
        </div>
    </div>
</div>