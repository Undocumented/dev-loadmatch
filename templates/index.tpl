<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    {literal}
    <link rel="dns-prefetch" href="//maxcdn.bootstrapcdn.com">
    <link rel="dns-prefetch" href="//code.jquery.com">
    <link rel="dns-prefetch" href="https://cdnjs.cloudflare.com">
    <link rel="dns-prefetch" href="https://fonts.googleapis.com">
    {/literal}
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Developed By M Abdur Rokib Promy">
    <meta name="author" content="cosmic">
    <meta name="keywords" content="Bootstrap 3, Template, Theme, Responsive, Corporate, Business">
    {*<link rel="shortcut icon" href="img/favicon.png">*}
    <link rel="apple-touch-icon" sizes="57x57" href="img/ico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/ico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/ico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/ico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/ico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/ico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/ico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/ico/favicon-16x16.png">
<link rel="manifest" href="img/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

    <title>
      {$company_name}
    </title>
    <script type="text/javascript">var site_url="{$site_url}";</script>
    <script type="text/javascript" src="{$site_url}/templates/js/ajax.js"></script>
    <script type="text/javascript" src="{$site_url}/templates/js/validation.js"></script>
    <!-- Bootstrap core CSS -->
    {literal}
    <!-- MDR : Update Bootstrap Core to V4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <!---->
    <!-- MDR : Remote Files -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Anton|Barlow+Condensed|Pragati+Narrow" rel="stylesheet">

    <!---->
    {/literal}
    <link href="{$site_url}/templates/css/bootstrap-reset.css" rel="stylesheet">
    <link href="{$site_url}/templates/css/pagination.css" rel="stylesheet" />
    <link rel="stylesheet" href="{$site_url}/templates/css/flexslider.css"/>
    <link href="{$site_url}/templates/assets/bxslider/jquery.bxslider.css" rel="stylesheet" />
    {*<link rel="stylesheet" href="{$site_url}/templates/css/tyrone.css">*}
    {*<link rel="stylesheet" href="{$site_url}/templates/css/animate.css">*}
    
    {if $action!='post_shipping'}
    {*<link rel="stylesheet" href="{$site_url}/templates/assets/owlcarousel/owl.carousel.css">
    <link rel="stylesheet" href="{$site_url}/templates/assets/owlcarousel/owl.theme.css">*}
    <link href="{$site_url}/templates/css/superfish.css" rel="stylesheet" media="screen">
    {/if}
    <!-- Custom styles for this template -->
    {*<link rel="stylesheet" type="text/css" href="{$site_url}/templates/css/parallax-slider/parallax-slider.css" />*}
    <script type="text/javascript" src="{$site_url}/templates/js/parallax-slider/modernizr.custom.28468.js">
    </script>
	
    {*<script src="{$site_url}/templates/js/owl.carousel.min.js"></script>*}
    <link href="{$site_url}/templates/css/style.css" rel="stylesheet">    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js">
    </script>
    <script src="js/respond.min.js">
    </script>
    <![endif]-->
    {literal}
        <!--
   <script>
 
      window.setTimeout(function() {
    $("#success-alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
},4000);
   </script>
   -->
    {/literal}
    {php}
        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
    {/php}
  </head>

<body>
    {$action|@print_r}

 {include file="header.tpl"}
 {if $action=='show_home'}
 {include file="slider.tpl"}
 {/if}
 {include file=$contentBody}
 {include file="footer.tpl"}

 {literal}
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-517771ed60ae75a6"></script> 
{/literal}
</body>
{* MDR : Not used *}
{literal}
<script>
//$(document).ready(function() {
 
//  $("#owl-demo").owlCarousel({
//    autoPlay: 3000, //Set AutoPlay to 3 seconds
//    navigation : true, // Show next and prev buttons
 //   slideSpeed : 300,
//    paginationSpeed : 400,
//    singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
//  });
 
//});
</script>
 {/literal}
 {literal}
 <script>
// Starrr plugin (https://github.com/dobtco/starrr)
var __slice = [].slice;

(function($, window) {
  var Starrr;

  Starrr = (function() {
    Starrr.prototype.defaults = {
      rating: void 0,
      numStars: 5,
      change: function(e, value) {}
    };

    function Starrr($el, options) {
      var i, _, _ref,
        _this = this;

      this.options = $.extend({}, this.defaults, options);
      this.$el = $el;
      _ref = this.defaults;
      for (i in _ref) {
        _ = _ref[i];
        if (this.$el.data(i) != null) {
          this.options[i] = this.$el.data(i);
        }
      }
      this.createStars();
      this.syncRating();
      this.$el.on('mouseover.starrr', 'span', function(e) {
        return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('mouseout.starrr', function() {
        return _this.syncRating();
      });
      this.$el.on('click.starrr', 'span', function(e) {
        return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('starrr:change', this.options.change);
    }

    Starrr.prototype.createStars = function() {
      var _i, _ref, _results;

      _results = [];
      for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
        _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
      }
      return _results;
    };

    Starrr.prototype.setRating = function(rating) {
      if (this.options.rating === rating) {
        rating = void 0;
      }
      this.options.rating = rating;
      this.syncRating();
      return this.$el.trigger('starrr:change', rating);
    };

    Starrr.prototype.syncRating = function(rating) {
      var i, _i, _j, _ref;

      rating || (rating = this.options.rating);
      if (rating) {
        for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
        }
      }
      if (rating && rating < 5) {
        for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        }
      }
      if (!rating) {
        return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
      }
    };

    return Starrr;

  })();
  return $.fn.extend({
    starrr: function() {
      var args, option;

      option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      return this.each(function() {
        var data;

        data = $(this).data('star-rating');
        if (!data) {
          $(this).data('star-rating', (data = new Starrr($(this), option)));
        }
        if (typeof option === 'string') {
          return data[option].apply(data, args);
        }
      });
    }
  });
})(window.jQuery, window);

$(function() {
  return $(".starrr").starrr();
});

$( document ).ready(function() {
	
      
  $('#stars').on('starrr:change', function(e, value){
   $('#count').html(value);
   $('#count_value1').val(value);
  
	
	
  });
  
  $('#stars1').on('starrr:change', function(e, value){
    $('#count1').html(value);
	$('#count_value2').val(value);
	
  });
   $('#stars2').on('starrr:change', function(e, value){
    $('#count2').html(value);
	$('#count_value3').val(value);
	
  });
   $('#stars3').on('starrr:change', function(e, value){
    $('#count3').html(value);
	$('#count_value4').val(value);
	
  });
  
  
    $('#starst').on('starrr:change', function(e, value){
    $('#countt').html(value);
	$('#count_valuet').val(value);
	
  });
   $('#starsc').on('starrr:change', function(e, value){
    $('#countc').html(value);
	$('#count_value2').val(value);
	
  });
  
  $('#starsp').on('starrr:change', function(e, value){
    $('#countp').html(value);
	$('#count_valuee').val(value);
	
  });
  
 $('#starss').on('starrr:change', function(e, value){
    $('#counts').html(value);
	$('#count_value2').val(value);
	
  });
    
  
  
  
  $('#stars-existing').on('starrr:change', function(e, value){
    $('#count-existing').html(value);
  });
  
  
  
  
  
  
});
</script>
 {/literal}
{literal}
    <script>
        javascript:(function(){var s=document.createElement("script");s.onload=function(){bootlint.showLintReportForCurrentDocument([]);};s.src="https://maxcdn.bootstrapcdn.com/bootlint/latest/bootlint.min.js";document.body.appendChild(s)})();
        </script>
        {/literal}
</html>