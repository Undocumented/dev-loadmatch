{*Page Veriables*}
{*Add on Page*}
{assign var="reviveid" value="43422af75a9f9066297995789acc7f48"}
{assign var="zone_tmain" value="86"}
{assign var="zone_tlmain" value="99"}
{assign var="zone_tcmain" value="100"}
{assign var="zone_trmain" value="101"}
{assign var="zone_tleft" value="28"}
{assign var="zone_tright" value="29"}
{assign var="zone_mleft" value="30"}
{assign var="zone_mright" value="31"}
{assign var="zone_bleft" value="32"}
{assign var="zone_bright" value="33"}

{*MDR: Banner Adds landscape*}
<div class="container-fluid d-none d-md-block text-center ">
    <div class="row">
        <div class="col col-md-12 no-padding d-none d-md-block" >
            <ins data-revive-zoneid="{ $zone_tmain }" data-revive-id="{ $reviveid }"></ins>
        {literal}<script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>{/literal}
        </div>
    </div>
</div>
{*MDR: 3 Banner Adds*}
<div class="container-fluid linespace text-center">
    <div class="row">
        <div class="col col-md-4">
            <ins data-revive-zoneid="{ $zone_tlmain }" data-revive-id="{ $reviveid }"></ins>
            {literal}<script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>{/literal}
        </div>
        <div class="col col-md-4">
            <ins data-revive-zoneid="{ $zone_tcmain }" data-revive-id="{ $reviveid }"></ins>
            {literal}<script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>{/literal}
        </div>
        <div class="col col-md-4">
            <ins data-revive-zoneid="{ $zone_trmain }" data-revive-id="{ $reviveid }"></ins>
            {literal}<script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>{/literal}
        </div>     
    </div>
</div>


{*MDR: Top 6 Shipments*}
{*
<div class="container">
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
      {$functions->get_home_shipment()}
        {if $shipment_info}
        {section name=data loop=$shipment_info}
    <div class="carousel-item{if $smarty.section.data.first} active{/if}">
                  <div class="card w-25">
                {if $shipment_info[data].img!=''}
                <img class="card-img-top imageopacity" src="{$site_url}/images/shipment_image/{$shipment_info[data].img}"/>
                {else}
                    <img class="card-img-top imageopacity" src="{$site_url}/images/shipment_image/default.jpg">
                {/if}
                <div class="card-body card-img-overlay ">
                    <h3 class="card-title">{$shipment_info[data].title|truncate:18:'...'}</h3>
                    <p class="card-text">{$shipment_info[data].description|truncate:15:'...'}</p>                    
                </div>
                <div class="card-footer">
                <a class="card-link" onclick="submitbuy('{$shipment_info[data].id}',document.view_detail);" href="#" > Read More</a>
                </div>
            </div> 
    </div>

            {/section}
        {/if}
  </div>
</div>
  {$shipment_info|@print_r}
</div>

*}

<div id="shipment" class="container linespace">
    <div class="row">
        {$functions->get_home_shipment()}
        {if $shipment_info}
        {section name=data loop=$shipment_info}
        <div class="col col-md-2 d-flex align-items-stretch text-center">
            
            <div class="card w-100">
                {if $shipment_info[data].img!=''}
                <img class="card-img-top imageopacity" src="{$site_url}/images/shipment_image/{$shipment_info[data].img}"/>
                {else}
                    <img class="card-img-top imageopacity" src="{$site_url}/images/shipment_image/default.jpg">
                {/if}
                <div class="card-body card-img-overlay ">
                    <h3 class="card-title"><h4>{$shipment_info[data].title|truncate:18:'...'}</h4></h3>
                    <p class="card-text">{$shipment_info[data].description|truncate:15:'...'}</p>                    
                </div>
                <div class="card-footer container-blue">
                <a class="card-link" onclick="submitbuy('{$shipment_info[data].id}',document.view_detail);" href="#" > Read More</a>
                </div>
            </div> 
        </div>
        {/section}
        {/if}
    </div>
</div>  


{*MDR: Page Content and Banners*}
<div class="container-fluid linespace container-dark">
    <div class="row">
        <div class="col col-lg-12 linespace"></div>
    </div>
    <div class="row">
        <div class="col col-lg-2 d-none d-md-block text-center">
            <span class="align-items-center"><ins data-revive-zoneid="{ $zone_tleft *}" data-revive-id="{ $reviveid *}"></ins></span>
            <script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
        </div>
        <div class="col col-lg-8 fill">
            <div class="centered">
                <h5 class="mt-0">{$sarvis_info[0].name}</h5>
                <p>{$sarvis_info[0].description}</p>
            </div>
                <img class="img-fluid imageopacity" src="{$site_url}/location_flage/{$sarvis_info[0].img}" alt="{$sarvis_info[0].name}"/>
        </div>
        <div class="col col-lg-2 d-none d-md-block text-center">
            <ins data-revive-zoneid="{ $zone_tright}" data-revive-id="{ $reviveid }"></ins>
            <script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
        </div>        
    </div>
          
</div>
<div class="container-fluid container-dark">
    <div class="row">
        <div class="col col-lg-2 d-none d-md-block text-center">
             {include file="zone_mleft.tpl"}
        </div>
        <div class="col col-lg-8 fill">
            <div class="centered">
                <h5 class="mt-0">{$sarvis_info[1].name}</h5>
                <p>{$sarvis_info[1].description}</p>
            </div>
                <img  class="img-fluid imageopacity" src="{$site_url}/location_flage/{$sarvis_info[1].img}" alt="{$sarvis_info[1].name}"/>
        </div>            
        <div class="col col-lg-2 d-none d-md-block text-center">
            <ins data-revive-zoneid="{ $zone_mright}" data-revive-id="{ $reviveid }"></ins>
            <script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
        </div>        
    </div>          
</div>
<div class="container-fluid container-dark">
    <div class="row">
        <div class="col col-lg-2 d-none d-md-block text-center">
            <span class="align-items-center"><ins data-revive-zoneid="{ $zone_bleft *}" data-revive-id="{ $reviveid *}"></ins></span>
            <script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
        </div>
        <div class="col col-lg-8 fill">
            <div class="centered">
                <h5 class="mt-0">{$sarvis_info[2].name}</h5>
                <p>{$sarvis_info[2].description}</p>
            </div>
                <img  class="img-fluid imageopacity" src="{$site_url}/location_flage/{$sarvis_info[2].img}" alt="{$sarvis_info[1].name}"/>
        </div>            
        <div class="col col-lg-2 d-none d-md-block text-center">
            <ins data-revive-zoneid="{ $zone_bright}" data-revive-id="{ $reviveid }"></ins>
            <script async src="//loadmatch.co.za/adserver/www/delivery/asyncjs.php"></script>
        </div>        
    </div>
    <div class="row">
        <div class="col col-lg-12 linespace">           
        </div>
    </div>            
</div> 
{* MDR: Counters on main page *}       
<div class="site-counter">
    <div class="container">
        <div class="row">
            <div class="col col-md-4">
                <div class="customers-count">
                    <span class="counter-count">3.5M</span>
                    <h6 class="customers-p">Customers</h6>
                </div>
            </div>
            <div class="col col-md-4">
                <div class="customers-count">
                    <span class="counter-count">788000</span>
                    <h6 class="transporters-p">Transporters</h6>
                </div>
            </div>
            <div class="col col-md-4">
                <div class="listings-count">                
                <span class="counter-count">5.7M</span>
                <h6 class="listings-p">Listings</h6>
                </div>
            </div>
        </div>
    </div>            
</div>
{* MDR:  *}
<div class="container-fluid container-blue50">
    <div class="row linespace20">
        <div class="col-md-12 text-center">
            <h2>
                Africa's only and largest all-inclusive logistics services platform with global reach.
            </h2>
            <h2>
                Leading the virtual market with vehicle and freight exchange services.
            </h2>
        </div>
    </div>
</div> 


{* MDR:  *}
{* MDR: Please provide a field for fontawesome *}
<div class="container">
    <div class="row">
        {section name=data loop=$testimonial_info}
        <div class="col-md-3  d-flex align-items-stretch text-center">
            <div class="card">
                <div class="icon-wrap ico-bg round-fifty fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;" data-wow-animation-name="fadeInDown">
                    {* MDR: Please provide a field for fontawesome Tempory Placement of this if*}
                    {if strstr($testimonial_info[data].name,'Business Directory')}
                    <i class="fa fa-address-card" aria-hidden="true"></i>
                    {elseif strstr($testimonial_info[data].name,'Load Visibility')}
                    <i class="fa fa-truck" aria-hidden="true"></i>
                    {elseif strstr($testimonial_info[data].name,'Business Efficiency')}
                    <i class="fa fa-tachometer" aria-hidden="true"></i> 
                    {else}
                    <i class="fa fa-users" aria-hidden="true"></i>                             
                    {/if} 
                </div>
                <div class="card-body">
                    <h4 class="card-title  text-center">{$testimonial_info[data].name}</h4>
                    <p class="card-text  text-center">{$testimonial_info[data].description}<br></p>
                </div>

            </div>
        </div>
        {/section}
    </div>
    <div class="row">
        <div class="col col-lg-12 linespace"></div>
    </div>
</div>
