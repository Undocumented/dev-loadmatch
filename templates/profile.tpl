
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Profile</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Profile</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="component-bg">
        <div class="container">
            <!-- Forms
================================================== -->
<div class="bs-docs-section mar-b-30">
  <h1 id="forms" class="page-header">Profile Detail</h1>
 <table align="center" width="80%" class="table-striped table-bordered table-hover" style="padding:10px" >

 <tr> <td style="text-align:left">
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-30 control-label" >User Type :</label></td><td style="text-align:left" valign="top"> <label for="inputEmail3" class="col-sm-30 control-label">{if $search[0].type=='C'} Customer {else} Transporter{/if}</label>
       
       </div>
      </td>
      </tr>
      <tr>
      <td style="text-align:left">
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-30 control-label" >Name : </label> </td><td style="text-align:left" valign="top"><label for="inputEmail3" class="col-sm-30 control-label">{$search[0].name}</label>
      </div>
       </td>
      </tr>
      <tr> <td style="text-align:left">
     
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-30 control-label">Email : </label></td><td style="text-align:left" valign="top"><label for="inputEmail3" class="col-sm-30 control-label">{$search[0].email}</label>
       </div>
      </td>
      </tr>
      <tr> <td style="text-align:left">
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-30 control-label">Mobile : </label></td><td style="text-align:left" valign="top"><label for="inputEmail3" class="col-sm-30 control-label">{$search[0].mobile}</label>
       </div>
       </td>
      </tr>
      <tr> <td style="text-align:left">
   
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-30 control-label">Province : </label></td><td style="text-align:left" valign="top"> <label for="inputEmail3" class="col-sm-30 control-label">{$search[0].state}</label>
      
  </div>
   </td>
      </tr>
      <tr> <td style="text-align:left">
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-30 control-label">City : </label> </td><td style="text-align:left" valign="top"><label for="inputEmail3" class="col-sm-30 control-label">{$search[0].city}</label>
       
  </div>
   </td>
      </tr>
      <tr> <td style="text-align:left">
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-30 control-label">Address : </label></td><td style="text-align:left" valign="top"> <label for="inputEmail3" class="col-sm-30 control-label">{$search[0].address}</label>
            
  </div>
   </td>
      </tr>
      {if $search[0].image}
      <tr> <td style="text-align:left">
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-30 control-label">Image : </label></td><td style="text-align:left" valign="top"><img src="{$site_url}/images/profile_pic/{$search[0].image}" width="80px" height="80px" />
            
  </div>
   </td>
      </tr>
      {/if}
      <tr> <td colspan="2" style="text-align:left">
  <form method="post" action="edit_profile.html" name="edit">
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
        <input type="hidden" name="user_id" value="{$smarty.session.user_id}" />
          <button type="submit" name="submit1" class="btn btn-default">Edit</button>
        </div>
      </div>
    </form>
     </td>
      </tr>
     </table>
    
    
   
  </div><!-- /.bs-example -->

  
     
   

</div>
        </div>
    </div>
    <!--container end-->