<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
                <h1 class="page-header">{if $edit_id!=''}Edit{else}Add{/if} State
                     </h1>        
           
        </div>
    </div>
  
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading">
          <b> {if $edit_id!=''}Edit{else}Add{/if} State</b>
                <!--========================================working Area=============================================-->
        <!--========================================site details=============================================-->
       </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
              {if $sucess!=''}
               <br />
    				 <div class="notibar msgsuccess">
                        <a class="close"></a>
                        <p>state added sucessfully</p>
                    </div><!-- notification msgsuccess -->
                  
                {/if} 

        <form class="stdform stdform2" name="frm_addstate" id="frm_addstate" method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="edit_id" value="{$edit_id}" />  
        <input type="hidden" name="Country_name" value="{$Country_name}" />  
        <input type="hidden" name="old_state_name" value="{$edit_data[0].state}" />
       {* <input type="hidden" name="sel_country" value="India" />*}
                    
                    	 <div class="form-group">
                        	<label>country</label> <span id="alert_country" class="alert"></span>
                           
                             {if $Country_name!=''}
                            {$functions->getCountryDropdownForAdmin($Country_name)}
                            {else}
							{$functions->getCountryDropdownForAdmin($edit_data[0].country)}
                            {/if}
                           
                    </div>
                      <div class="form-group">
                        	<label>state name</label> <span id="alert_state_name" class="alert"></span>
                           
                            <input type="text" name="state_name" id="state_name"  class="form-control" value="{$edit_data[0].state}" />
                          </div>
                     <div class="form-group">
                        	<label>title</label><span id="alert_title" class="alert"></span>
                          
                            <input type="text" name="title" id="title" class="form-control" value="{$edit_data[0].title}" />
                     </div>
                     <div class="form-group">
                        	<label>description</label><span  class="alert"></span>
                            
                            <textarea name="description" id="alert_description" class="form-control"  >{$edit_data[0].descen}</textarea>
                            
                        </div>
                        <div class="form-group">
                        	<label>keyword</label><span  class="alert"></span>
                            
                            <textarea name="keyword" id="alert_keyword" class="form-control" >{$edit_data[0].keyword}</textarea>
                            
                      </div>
                        
                      
                          <button type="submit" class="btn btn-default" name="submit" value="submit" onClick="return frm_add_state(document.frm_addstate);">Submit</button>
                                                  
                       
                    </form>
 			    
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>