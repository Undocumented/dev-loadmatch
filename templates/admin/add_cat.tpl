<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>{if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if} {if $type=='M'}Category {else} Category{/if}</h2>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading"><b>{if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if} {if $type=='M'}Category {else} Category{/if}</b>
                        {if $add_sucess=='Y' || $add_suce=='Y'}
                        <div class="notibar msgsuccess">
                            <div class="alert alert-success" >Category Added Successfully !</div>
                        </div>
                        {/if} </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-8">






                                {if $type=='industry_types'}
                                <!--===============Industry Type======================-->
                                <!--===============customer form======================-->
                                <form action="" method="post" name="add_industry_type"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id}" />
                                    <div class="form-group">
                                        <label>Load Industry</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].name}" placeholder="Enter Industry Name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].description}" placeholder="Enter Description " />
                                    </div>
                                    <button type="submit" class="btn btn-primary" name="add_industry_type_btn" id="add_industry_type_btn" value="Add Industry" onclick="return valid_cat_value(document.add_industry_type);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if} Industry Type</button>
                                </form>
                                <!--==================================================-->
                                {/if}

                                <!--===============Load Type======================-->
                                {if $type=='load_types'}
                                <form action="" method="post" name="add_load_type"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Load Type Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Add Load Name</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].name}" placeholder="Enter Load Name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].description}" placeholder="Enter Description " />
                                    </div>
                                    <div class="form-group">
                                        <label>Industry Links</label>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <td>ID</td>
                                                <td>Industry</td>
                                                <td>Action</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {section name=key loop=$linked_type}
                                            <tr id="link-{$linked_type[key].key}">
                                                <td>
                                                    {$linked_type[key].id}
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][id]" value="{$linked_type[key].id}" />
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][edit_id]" value="{$linked_type[key].load_type_id}" />
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][parent_id]" value="{$linked_type[key].industry_type_id}" />
                                                </td>
                                                <td>{$linked_type[key].name}</td>
                                                <td>
                                                    <button class="btn btn-primary delete_link" name="delete_link" value="DeleteLoadLink" onclick="$('#link-{$linked_type[key].key}').remove()" >Delete</button>
                                                </td>
                                            </tr>
                                            {/section}
                                            <tr id="new-link">
                                                <td>Add New</td>
                                                <td>
                                                    <select name="parent_id"  id="parent_id" class="form-control" >
                                                        <option value="Please Select" selected="selected" >Please Select</option>

                                                        {section name=data loop=$type_data}
                                                        <option value="{$type_data[data].id}" >{$type_data[data].name} </option>
                                                        {/section}
                                                    </select>
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary" id="add-link" >Add Load Link</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                    <button type="submit" class="btn btn-primary" name="add_load_type_btn" id="add_load_type_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_load_type);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Load Type</button>
                                </form>
                                <!--==================================================-->
                                {/if}

                                <!--===============Transport Type======================-->
                                {if $type=='transport_types'}
                                <form action="" method="post" name="add_transport_type"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Transport Type Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Add Transport Name</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].name}" placeholder="Enter Transport Name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].description}" placeholder="Enter Description " />
                                    </div>
                                    <div class="form-group">
                                        <label>Load Links</label>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <td>ID</td>
                                                <td>Load</td>
                                                <td>Action</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {section name=key loop=$linked_type}
                                            <tr id="link-{$linked_type[key].key}">
                                                <td>
                                                    {$linked_type[key].id}
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][id]" value="{$linked_type[key].id}" />
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][edit_id]" value="{$linked_type[key].load_type_id}" />
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][parent_id]" value="{$linked_type[key].industry_type_id}" />
                                                </td>
                                                <td>{$linked_type[key].name}</td>
                                                <td>
                                                    <button class="btn btn-primary delete_link" name="delete_link" value="DeleteLoadLink" onclick="$('#link-{$linked_type[key].key}').remove()" >Delete</button>
                                                </td>
                                            </tr>
                                            {/section}
                                            <tr id="new-link">
                                                <td>Add New</td>
                                                <td>
                                                    <select name="parent_id"  id="parent_id" class="form-control" >
                                                        <option value="Please Select" selected="selected" >Please Select</option>

                                                        {section name=data loop=$type_data}
                                                        <option value="{$type_data[data].id}" >{$type_data[data].name} </option>
                                                        {/section}
                                                    </select>
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary" id="add-link" >Add Transport Link</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                    <button type="submit" class="btn btn-primary" name="add_transport_type_btn" id="add_transport_type_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_transport_type);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Transport Type</button>
                                </form>
                                <!--==================================================-->
                                {/if}

                                <!--===============Bulk Body Type======================-->
                                {if $type=='body_bulk_types'}
                                <form action="" method="post" name="add_body_bulk_type"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Body Bulk Type Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Add Body Bulk Name</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].name}" placeholder="Enter Body Bulk Name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].description}" placeholder="Enter Description " />
                                    </div>
                                    <div class="form-group">
                                        <label>Transport Links</label>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <td>ID</td>
                                                <td>Transport</td>
                                                <td>Action</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {section name=key loop=$linked_type}
                                            <tr id="link-{$linked_type[key].key}">
                                                <td>
                                                    {$linked_type[key].id}
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][id]" value="{$linked_type[key].id}" />
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][edit_id]" value="{$linked_type[key].load_type_id}" />
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][parent_id]" value="{$linked_type[key].industry_type_id}" />
                                                </td>
                                                <td>{$linked_type[key].name}</td>
                                                <td>
                                                    <button class="btn btn-primary delete_link" name="delete_link" value="DeleteLoadLink" onclick="$('#link-{$linked_type[key].key}').remove()" >Delete</button>
                                                </td>
                                            </tr>
                                            {/section}
                                            <tr id="new-link">
                                                <td>Add New</td>
                                                <td>
                                                    <select name="parent_id"  id="parent_id" class="form-control" >
                                                        <option value="Please Select" selected="selected" >Please Select</option>

                                                        {section name=data loop=$type_data}
                                                        <option value="{$type_data[data].id}" >{$type_data[data].name} </option>
                                                        {/section}
                                                    </select>
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary" id="add-link" >Add Transport Link</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                    <button type="submit" class="btn btn-primary" name="add_body_bulk_type_btn" id="add_body_bulk_type_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_body_bulk_type);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Body Bulk Type</button>
                                </form>
                                <!--==================================================-->
                                {/if}

                                <!--===============Size Type======================-->
                                {if $type=='size_types'}
                                <form action="" method="post" name="add_size_type"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Size Type Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Add Size Name</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].name}" placeholder="Enter Size Name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].description}" placeholder="Enter Description " />
                                    </div>
                                    <div class="form-group">
                                        <label>Bulk Body Links</label>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <td>ID</td>
                                                <td>Body Bulk</td>
                                                <td>Action</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {section name=key loop=$linked_type}
                                            <tr id="link-{$linked_type[key].key}">
                                                <td>
                                                    {$linked_type[key].id}
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][id]" value="{$linked_type[key].id}" />
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][edit_id]" value="{$linked_type[key].load_type_id}" />
                                                    <input type="hidden" name="linked_type[{$linked_type[key].key}][parent_id]" value="{$linked_type[key].industry_type_id}" />
                                                </td>
                                                <td>{$linked_type[key].name}</td>
                                                <td>
                                                    <button class="btn btn-primary delete_link" name="delete_link" value="DeleteLoadLink" onclick="$('#link-{$linked_type[key].key}').remove()" >Delete</button>
                                                </td>
                                            </tr>
                                            {/section}
                                            <tr id="new-link">
                                                <td>Add New</td>
                                                <td>
                                                    <select name="parent_id"  id="parent_id" class="form-control" >
                                                        <option value="Please Select" selected="selected" >Please Select</option>

                                                        {section name=data loop=$type_data}
                                                        <option value="{$type_data[data].id}" >{$type_data[data].name} </option>
                                                        {/section}
                                                    </select>
                                                </td>
                                                <td>
                                                    <a class="btn btn-primary" id="add-link" >Add Bulk Body Link</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                    <button type="submit" class="btn btn-primary" name="add_size_type_btn" id="add_size_type_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_size_type);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Size Type</button>
                                </form>
                                <!--==================================================-->
                                {/if}


















                                {if $type=='M'}
                                <!--===============Industry Type======================-->
                                <!--===============customer form======================-->
                                <form action="" method="post" name="add_new_cat"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id}" />
                                    <div class="form-group">
                                        <label>Load Industry</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].category}" placeholder="Enter Industry Name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].cat_keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].cat_description}" placeholder="Enter Description " />
                                    </div>
                                    <div class="form-group">
                                        <label>Logo</label>
                                        <input type="file" name="logo"  id="logo"/>
                                        <input type="hidden" name="old_main_img" value="{$useredit[0].cat_image}" />
                                    </div>
                                    <button type="submit" class="btn btn-primary" name="cat_btn" value="Add Category" onclick="return valid_cat_value(document.add_new_cat);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if} Category</button>
                                </form>
                                <!--==================================================-->
                                {/if}

                                <!--===============Load Type======================-->
                                {if $type=='S'}
                                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id_sub}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Sub Category Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Industry Name</label>
                                        <select name="category"  id="category" class="form-control" >
                                            <option value="please Select" selected="selected" >please Select</option>

                                            {section name=data loop=$useredit1}


                                            <option value="{$useredit1[data].id}" {if $useredit1[data].id==$useredit[0].parent_id } selected="selected" {/if} >{$useredit1[data].category} </option>

                                            {/section}

                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Add Load Name</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].category}" placeholder="Enter Load Name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].cat_keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].cat_description}" placeholder="Enter Description " />
                                    </div>
                                    <button type="submit" class="btn btn-primary" name="sub_cat_btn" id="sub_cat_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Sub Category</button>
                                </form>
                                {/if}

                                <!--===============Transport Type======================-->
                                {if $type=='SN'}
                                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id_sub}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Sub Next Category Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Industry Name</label>
                                        <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                                            <option value="please Select" selected="selected" >please Select</option>

                                            {section name=data loop=$useredit1}


                                            <option value="{$useredit1[data].id}" {if $useredit1[data].id==$useredit[0].parent_id } selected="selected" {/if} >{$useredit1[data].category} </option>

                                            {/section}

                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Load Name</label>
                                        <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Add Transport Type Name</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].category}" placeholder="Enter Transport Type Name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].cat_keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].cat_description}" placeholder="Enter Description " />
                                    </div>
                                    <button type="submit" class="btn btn-primary" name="sub_nxt_cat_btn" id="sub_nxt_cat_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Sub Category</button>
                                </form>
                                {/if}

                                <!--===============Body & Bulk Type======================-->
                                {if $type=='FC'}
                                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id_sub}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Sub Next Category Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Industry Name</label>
                                        <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                                            <option value="please Select" selected="selected" >please Select</option>

                                            {section name=data loop=$useredit1}


                                            <option value="{$useredit1[data].id}" {if $useredit1[data].id==$user_cat_edit[0].parent_id } selected="selected" {/if} >{$useredit1[data].category} </option>

                                            {/section}

                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Load Type Name</label>
                                        <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Transport Type Name</label>
                                        <select name="last_sub_cat" id="sub_next_cat_dropdown" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Add Body & Bulk Type Name</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].category}" placeholder="Enter Body & Bulk Type Name" />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].cat_keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].cat_description}" placeholder="Enter Description " />
                                    </div>
                                    <button type="submit" class="btn btn-primary" name="last_cat_btn" id="last_cat" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Last Category</button>
                                </form>
                                {/if}

                                {if $type=='FFC'}
                                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id_sub}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Fifth Category Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Industry Name</label>
                                        <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                                            <option value="please Select" selected="selected" >please Select</option>

                                            {section name=data loop=$useredit1}


                                            <option value="{$useredit1[data].id}" {if $useredit1[data].id==$user_cat_edit[0].parent_id } selected="selected" {/if} >{$useredit1[data].category} </option>

                                            {/section}

                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Load Type Name</label>
                                        <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Transport Type Name</label>
                                        <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Body & Bulk Type</label>
                                        <select name="fourth_cat" id="fourth_cat" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Add Size Classification</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].category}" placeholder="Enter Size Classification" />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].cat_keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].cat_description}" placeholder="Enter Description " />
                                    </div>
                                    <button type="submit" class="btn btn-primary" name="fifth_cat_btn" id="fifth_cat" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Fifth Category</button>
                                </form>
                                {/if}

                                {if $type=='SC'}
                                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id_sub}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Fifth Category Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Industry Name</label>
                                        <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                                            <option value="please Select" selected="selected" >please Select</option>

                                            {section name=data loop=$useredit1}

                                            <option value="{$useredit1[data].id}" {if $useredit1[data].id==$user_cat_edit[0].parent_id } selected="selected" {/if} >{$useredit1[data].category} </option>

                                            {/section}

                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Load Type Name</label>
                                        <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Transport Type Name</label>
                                        <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Body & Bulk Type</label>
                                        <select name="fourth_cat" id="fourth_cat" onchange="Get_fifth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Size Classification</label>
                                        <select name="fifth_cat" id="fifth_id" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Add Payload KG</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].category}" placeholder="Enter Payload KG" />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].cat_keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].cat_description}" placeholder="Enter Description " />
                                    </div>
                                    <button type="submit" class="btn btn-primary" name="sixth_cat_btn" id="sixth_cat" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Sixth Category</button>
                                </form>
                                {/if}

                                {if $type=='SVC'}
                                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id_sub}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Seventh Category Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Industry Name</label>
                                        <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                                            <option value="please Select" selected="selected" >please Select</option>

                                            {section name=data loop=$useredit1}

                                            <option value="{$useredit1[data].id}" {if $useredit1[data].id==$user_cat_edit[0].parent_id } selected="selected" {/if} >{$useredit1[data].category} </option>

                                            {/section}

                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Load Type Name</label>
                                        <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Transport Type Name</label>
                                        <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Body & Bulk Type Name</label>
                                        <select name="fourth_cat" id="fourth_cat" onchange="Get_fifth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Size Classification</label>
                                        <select name="fifth_cat" id="fifth_id" onchange="Get_sixth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Payload KG</label>
                                        <select name="sixth_cat1" id="sixth_cat" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>

                                    <div class="form-group">
                                        <label>Add Seventh Category Name</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].category}" placeholder="Enter Sub Category name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].cat_keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].cat_description}" placeholder="Enter Description " />
                                    </div>
                                    <button type="submit" class="btn btn-primary" name="seveth_cat_btn" id="seveth_cat" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Seventh Category</button>
                                </form>
                                {/if}

                                {if $type=='EC'}
                                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id_sub}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Eigth Category Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Industry Name</label>
                                        <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                                            <option value="please Select" selected="selected" >please Select</option>

                                            {section name=data loop=$useredit1}

                                            <option value="{$useredit1[data].id}" {if $useredit1[data].id==$user_cat_edit[0].parent_id } selected="selected" {/if} >{$useredit1[data].category} </option>

                                            {/section}

                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Sub Category Name</label>
                                        <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Sub Next Category Name</label>
                                        <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Fourth Category</label>
                                        <select name="fourth_cat" id="fourth_cat" onchange="Get_fifth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Fifth  Category</label>
                                        <select name="fifth_cat" id="fifth_id" onchange="Get_sixth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Sixth  Category</label>
                                        <select name="sixth_cat1" id="sixth_cat" onchange="Get_seventh_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>

                                    <div class="form-group">
                                        <label>Sevnth  Category</label>
                                        <select name="seventh_cat" id="seventh_cat" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>

                                    <div class="form-group">
                                        <label>Add Eight Category Name</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].category}" placeholder="Enter Sub Category name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].cat_keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].cat_description}" placeholder="Enter Description " />
                                    </div>
                                    <button type="submit" class="btn btn-primary" name="eigth_cat_btn" id="eigth_cat_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Seventh Category</button>
                                </form>
                                {/if}

                                {if $type=='NC'}
                                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id_sub}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Ninth Category Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Industry Name</label>
                                        <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                                            <option value="please Select" selected="selected" >please Select</option>

                                            {section name=data loop=$useredit1}

                                            <option value="{$useredit1[data].id}" {if $useredit1[data].id==$user_cat_edit[0].parent_id } selected="selected" {/if} >{$useredit1[data].category} </option>

                                            {/section}

                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Sub Category Name</label>
                                        <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Sub Next Category Name</label>
                                        <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Fourth Category</label>
                                        <select name="fourth_cat" id="fourth_cat" onchange="Get_fifth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Fifth  Category</label>
                                        <select name="fifth_cat" id="fifth_id" onchange="Get_sixth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Sixth  Category</label>
                                        <select name="sixth_cat1" id="sixth_cat" onchange="Get_seventh_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>

                                    <div class="form-group">
                                        <label>Sevnth  Category</label>
                                        <select name="seventh_cat" id="seventh_cat"  onchange="Get_eigth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label>Eigth  Category</label>
                                        <select name="eigth_cat" id="eigth_cat" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>

                                    <div class="form-group">
                                        <label>Add Ninth Category Name</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].category}" placeholder="Enter Sub Category name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].cat_keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].cat_description}" placeholder="Enter Description " />
                                    </div>
                                    <button type="submit" class="btn btn-primary" name="ninth_cat_btn" id="ninth_cat_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Ninth Category</button>
                                </form>
                                {/if}

                                {if $type=='TC'}
                                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                                    <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id_sub}" />
                                    {if $add_suce=='Y'}
                                    <div class="notibar msgsuccess">
                                        <div class="alert alert-success" >Tenth Category Added Successfully !</div>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <label>Industry Name</label>
                                        <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                                            <option value="please Select" selected="selected" >please Select</option>

                                            {section name=data loop=$useredit1}

                                            <option value="{$useredit1[data].id}" {if $useredit1[data].id==$user_cat_edit[0].parent_id } selected="selected" {/if} >{$useredit1[data].category} </option>

                                            {/section}

                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Sub Category Name</label>
                                        <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Sub Next Category Name</label>
                                        <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Fourth Category</label>
                                        <select name="fourth_cat" id="fourth_cat" onchange="Get_fifth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Fifth  Category</label>
                                        <select name="fifth_cat" id="fifth_id" onchange="Get_sixth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>
                                    <div class="form-group">
                                        <label>Sixth  Category</label>
                                        <select name="sixth_cat1" id="sixth_cat" onchange="Get_seventh_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>

                                    <div class="form-group">
                                        <label>Sevnth  Category</label>
                                        <select name="seventh_cat" id="seventh_cat"  onchange="Get_eigth_cat(this.value);" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label>Eigth  Category</label>
                                        <select name="eigth_cat" id="eigth_cat" onchange="Get_ninth_cat(this.value);"  class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>

                                    <div class="form-group">
                                        <label>Ninth  Category</label>
                                        <select name="ninth_cat" id="ninth_cat" class="form-control"   >
                                            <option value="please Select" selected="selected" >please Select</option>
                                        </select>
                                        <span class="alert" id="alert_cat_id"></span> </div>

                                    <div class="form-group">
                                        <label>Add Tenth Category Name</label>
                                        <input class="form-control" type="text" name="cat_name" id="cat_name" value="{$useredit[0].category}" placeholder="Enter Sub Category name " />
                                    </div>
                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <input class="form-control" type="text" name="keyword" id="keyword" value="{$useredit[0].cat_keyword}" placeholder="Enter Keyword " />
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input class="form-control" type="text" name="description" id="description" value="{$useredit[0].cat_description}" placeholder="Enter Description " />
                                    </div>
                                    <button type="submit" class="btn btn-primary" name="tenth_cat_btn" id="tenth_cat_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> {if $edit_id=='' && $edit_id_sub==''}Add{else}Edit{/if}  Tenth Category</button>
                                </form>
                                {/if}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
        <!-- /. ROW  -->

        <!-- /. ROW  -->
    </div>
    <!-- /. PAGE INNER  -->
</div>