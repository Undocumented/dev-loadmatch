<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>{if $type_filter_name}{$type_filter_name} {/if}Category List</h2>
                     {if $show_message!=''}
                       <h5 class=" btn-success btn-lg" align="center">{$show_message}</h5>  
                       {/if} 
                      
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Category List
                           
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="8%">Sr. No.</th>
                                            <th>Category Name</th>
                                          <!--  <th>Sub Category name</th>-->
                                         <!--   <th>Under Category</th>-->

                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if $cat_data}
                                        {section name=data loop=$cat_data}
                                        <tr class="gradeU">
                                            <td align="center">{$smarty.section.data.index+$row_no+1}</td>
                                            <td><a href="{$site_url}/admin.php?page=category&action=show_cat_list&id={$cat_data[data].id}&type={$child_type}">{$cat_data[data].name}</a></td>
                                              {*<td><a href="{$site_url}/admin.php?page=category&action=show_cat_list&id={$cat_data[data].id}&type={$child_type}">Show All Sub Category</a></td>*}
                                            
                                               <td>
                                               	<a href="{$site_url}/admin.php?page=category&action=add_cat&edit_id={$cat_data[data].id}&type={$type}" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                                                
                                              <a href="{$site_url}/admin.php?page=category&action=show_cat_list&type={$type}&del_cat={$cat_data[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><span class="btn btn-success btn-sm" style="background-color:#F00;">Delete</span></a>
                                               </td>
                                        </tr>
                                        {/section}
                                        
                                         <tr>
            <td colspan="6"><form name="frm_pagi" action="" method="post">
                                        <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />
                                        {$show_pagi}
                       				 </form></td>
          </tr>
          {else}
          <tr>
            <td colspan="8"><span style="color:red;">Record Not Found</span></td>
          </tr>
          {/if}
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>

