<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Users List</h2>
                     {if $show_message!=''}
                       <h5 class=" btn-success btn-lg" align="center">{$show_message}</h5>  
                       {/if} 
                      
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <b>  Users List </b>
                      <div  style="text-align:right;">    
                   <form id="myform" name="myform" action="" method="post"  >
          <input type="hidden" name="sort_selection" value="{$sort_selection}" />
         Filter by  <select name="sort_selection" style="width:70px;" onchange="onchane_submit();">
            <option value="">All</option>
            <option value="C" {if $sort_selection=='C'} selected="selected" {/if}>Customer</option>
            <option value="T" {if $sort_selection=='T'} selected="selected" {/if}>Transporter</option>
          </select>
        </form>
                        </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="7%">Sr. No.</th>
                                            <th>Name</th>
                                            <th>User Type</th>
                                            <th>Mobile</th>
                                            <th>Email Address</th>
                                          
                                            <th>Entry Date</th>
                                             <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if $usersdata}
                                        {section name=data loop=$usersdata}
                                        <tr class="gradeU">
                                            <td align="center">{$smarty.section.data.index+$row_no+1}</td>
                                            <td>{$usersdata[data].name}</td>
                                             <td>{if $usersdata[data].type=='C'} Customer {else} Transporter {/if}</td>
                                             <td>{$usersdata[data].mobile}</td>
                                              <td>{$usersdata[data].email}</td>
                                              
                                            <td>{$usersdata[data].entry_date|date_format:"%d- %m -%Y"}</td>
                                               <td>
                                               	<a href="{$site_url}/admin.php?page=user&action=add_users&edit_id={$usersdata[data].id}" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                                                {if $usersdata[data].status=='Y'}
                                               	<a href="{$site_url}/admin.php?page=user&action=add_users&status_id={$usersdata[data].id}" onclick="return confirm('Do You Want to Change it to Inactive?');" title="active"><span class="btn btn-success btn-sm" style="background-color:#F90;">Active&nbsp;</span></a>
                                                {else}
                                                	<a href="{$site_url}/admin.php?page=user&action=add_users&status_id={$usersdata[data].id}" onclick="return confirm('Do You Want to Change it to Active?');" title="inactive"><span class="btn btn-success btn-sm" style="background-color:#069;">Inactive</span></a>
                                                    {/if}
                                                <a href="{$site_url}/admin.php?page=user&action=add_users&del_cust={$usersdata[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><span class="btn btn-success btn-sm" style="background-color:#F00;">Delete</span></a>
                                               </td>
                                        </tr>
                                        {/section}
                                        
                                         <tr>
            <td colspan="8"><form name="frm_pagi" action="#" method="post">
                <input type="hidden"  name="pageval" id="pagevalid" value="{$page_val}" />
                {$show_pagi}
              </form></td>
          </tr>
          {else}
          <tr>
            <td colspan="8"><span style="color:red;">Record Not Found</span></td>
          </tr>
          {/if}
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>

