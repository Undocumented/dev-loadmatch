<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Testimonial List</h2>
                 {if $sucess=='Y'} <div class="notibar msgsuccess">
                        
                        <div class="alert alert-success" >Added Successfully !</div>
                    </div>{/if}
                     {if $update=='Y'} <div class="notibar msgsuccess">
                        
                        <div class="alert alert-success" >Update Successfully !</div>
                    </div>{/if}          
                       {if $del=='Y'} <div class="notibar msgsuccess">
                        
                        <div class="alert alert-success" >Deleted Successfully !</div>
                    </div>{/if}    
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <b> Testimonial List </b>
                      <div  style="text-align:right;">    
                 
                        </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="7%">Sr. No.</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Image</th>
                                             <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if $userdata}
                                        {section name=data loop=$userdata}
                                        <tr class="gradeU">
                                            <td align="center">{$smarty.section.data.index+$row_no+1}</td>
                                            <td>{$userdata[data].name}</td>
                                             <td>{$userdata[data].description}</td>
                                              <td><img src="{$site_url}/location_flage/{$userdata[data].img}" width="50PX;" height="50PX;" /></td>
                                               <td>
                                               	<a href="{$site_url}/admin.php?page=user&action=add_testimonial&edit_id={$userdata[data].id}" title="Update"><span class="btn btn-success btn-sm">Update &nbsp;&nbsp;&nbsp;</span></a>
                                                
                                                <a href="{$site_url}/admin.php?page=user&action=show_testimonial_list&del_id={$userdata[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><span class="btn btn-success btn-sm" style="background-color:#F00;">Delete</span></a>
                                               </td>
                                        </tr>
                                        {/section}
                                        
                                         <tr>
            <td colspan="8"><form name="frm_pagi" action="#" method="post">
                <input type="hidden"  name="pageval" id="pagevalid" value="{$page_val}" />
                {$show_pagi}
              </form></td>
          </tr>
          {else}
          <tr>
            <td colspan="8"><span style="color:red;">Record Not Found</span></td>
          </tr>
          {/if}
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>

