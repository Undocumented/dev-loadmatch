<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>View User Detail</h2> 
                     
                    </div>
                </div>
              
                 <hr />  <a href="" style="float:right; margin-bottom:8px;">  <button class="btn btn-default btn" onclick="goBack()">Go Back</button></a>  
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             {$userdetail[0].user_name} User Detail
                           
                            
                        </div>
                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>
                                        <tr>
                                        	<th>User Name</th>
                                            <td>{$userdetail[0].user_name}</td>
                                          <th>Email Address</th>
                                            <td>{$userdetail[0].email}</td>
                                        </tr>
                                         <tr >
                                            
                                            <th>Mobile</th>
                                            <td>{$userdetail[0].mobile_no}</td>
                                            
                                             <th>Enter Date</th>
                                            <td>{$userdetail[0].entry_date|date_format:"%d- %m -%Y"}</td>
                                        </tr>
                                        <tr>
                                            <th>Zip Code</th>
                                            <td>{$userdetail[0].zipcode}</td>
                                           <th>Address</th>
                                            <td colspan="">{$userdetail[0].address}</td>
                                        </tr>
                                        <tr>
                                           
                                        </tr>
                                    </tbody>
                                    
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->
            
                <!-- /. ROW  -->
            
             
        </div>
               
    </div>