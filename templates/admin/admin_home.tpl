<div id="wrapper"> 
  <div id="page-wrapper" >
    <div id="page-inner">
      <div class="row">
        <div class="col-md-12">
          <h2>Admin Dashboard</h2>
         <!-- <h5>Welcome Jhon Deo , Love to see you back. </h5>-->
        </div>
      </div>
      <!-- /. ROW  -->
      <hr />
      
      
      <div class="row">
      
        
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=user&action=show_user_list"><span class="icon-box bg-color-green set-icon"> <i class="fa fa-user" style="margin-top:10px;"></i> </span></a>
            <div class="text-box" >
             <!-- <p class="main-text">30 Tasks</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=user&action=show_user_list">User</a></p>
            </div>
          </div>
        </div>
    
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=payment&action=show_payment"> <span class="icon-box bg-color-brown set-icon"> <i class="fa fa-money" style="margin-top:10px;"></i> </span></a>
            <div class="text-box" >
              <!--<p class="main-text">3 Orders</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=payment&action=show_payment">Payment</a></p>
            </div>
          </div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment"><span class="icon-box bg-color-green set-icon"> <i class="fa fa-truck" style="margin-top:10px;"></i> </span></a>
            <div class="text-box" >
             
              <p class="text-muted"><a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment">Shipment Active</a></p>
            </div>
          </div>
        </div>
          <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment&type=B"><span class="icon-box bg-color-1 set-icon"> <i class="fa fa-truck" style="margin-top:10px;"></i> </span></a>
            <div class="text-box" >
             
              <p class="text-muted"><a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment&type=B">Shipment Undelivered</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment&type=CT"><span class="icon-box bg-color-2 set-icon"> <i class="fa fa-truck" style="margin-top:10px;"></i> </span></a>
            <div class="text-box" >
             <!-- <p class="main-text">30 Tasks</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment&type=CT">Shipment Delivered</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment&type=C"><span class="icon-box bg-color-red set-icon"> <i class="fa fa-truck" style="margin-top:10px;"></i> </span></a>
            <div class="text-box" >
             <!-- <p class="main-text">30 Tasks</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment&type=C">Shipment Cancel</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=admin_home&action=approve_requirement"> <span class="icon-box bg-color-3 set-icon"> <i class="fa fa-thumbs-o-up" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
           
              <p class="text-muted"><a href="{$site_url}/admin.php?page=admin_home&action=approve_requirement">Approve Shipment</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=admin_home&action=approve_review"> <span class="icon-box bg-color-4 set-icon"> <i class="fa fa-thumbs-o-up" style="margin-top:10px;"></i> </span></a>
            <div class="text-box" >
              <!--<p class="main-text">3 Orders</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=admin_home&action=approve_review"> Approve Review</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=admin_home&action=show_message"><span class="icon-box bg-color-10 set-icon"> <i class="fa fa-envelope-o" style="margin-top:10px;"></i> </span></a>
            <div class="text-box" >
              <p class="text-muted"><a href="{$site_url}/admin.php?page=admin_home&action=show_message">Web Enquiries</a></p>
            </div>
          </div>
        </div>
          <div class="col-md-3 col-sm-6 col-xs-6" >
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=payment&action=show_quote"><span class="icon-box bg-color-green set-icon" > <i class="fa fa-check" style="margin-top:10px;"></i> </span> </a>
            <div class="text-box" >
            
              <p class="text-muted"><a href="{$site_url}/admin.php?page=payment&action=show_quote">Active Quote</a></p>
            </div>
          </div>
        </div>
<div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=payment&action=show_quote&type=inactive"><span class="icon-box bg-color-red set-icon"> <i class="fa fa-ban" style="margin-top:10px;"></i> </span> </a>
            <div class="text-box" >
            
              <p class="text-muted"><a href="{$site_url}/admin.php?page=payment&action=show_quote&type=inactive">Inactive Quote</a></p>
            </div>
          </div>
        </div>
          
      </div>
      <br>
        <div class="row">
        <div class="col-md-12">
          <h2>Master Control</h2>
        </div>
      </div>
      <hr />
      <div class="row">
      
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=category&action=show_cat_list&type=M"><span class="icon-box bg-color-blue set-icon"> <i class="fa fa-sitemap" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=category&action=show_cat_list&type=M">Category List</a></p>
            </div>
          </div>
        </div>
              <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=category&action=add_cat&type=M"><span class="icon-box bg-color-red set-icon"> <i class="fa fa-plus" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=category&action=add_cat&type=M">Add Category</a></p>
            </div>
          </div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=category&action=add_cat&type=S"><span class="icon-box bg-color-green set-icon"> <i class="fa fa-plus-circle" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=category&action=add_cat&type=S">Add Sub Category</a></p>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=payment&action=Package"> <span class="icon-box bg-color-5 set-icon"> <i class="fa fa-credit-card" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
             <!-- <p class="main-text">30 Tasks</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=payment&action=Package">Service Charge</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=add_banner&action=add_slider"> <span class="icon-box bg-color-7 set-icon"> <i class="fa fa-spinner" style="margin-top:10px;"></i> </span></a>
            <div class="text-box" >
              <!--<p class="main-text">3 Orders</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=add_banner&action=add_slider">Slider Setting</a></p>
            </div>
          </div>
        </div>
          <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=admin_home&action=edit_company"><span class="icon-box bg-color-12 set-icon"> <i class="fa fa-cog" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
              <p class="text-muted"><a href="{$site_url}/admin.php?page=admin_home&action=edit_company">General Setting</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=admin_home&action=payment_gatway_setting"> <span class="icon-box bg-color-8 set-icon"> <i class="fa fa-money" style="margin-top:10px;"></i> </span></a>
            <div class="text-box" >
             <!-- <p class="main-text">30 Tasks</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=admin_home&action=payment_gatway_setting">Payment Gateway</a></p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=admin_home&action=social_setting"><span class="icon-box bg-color-9 set-icon"> <i class="fa fa-share-square-o" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=admin_home&action=social_setting">Social Setting</a></p>
            </div>
          </div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=category&action=add_vehicle"><span class="icon-box bg-color-1 set-icon"> <i class="fa fa-bus" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=category&action=add_vehicle">Add Vehicle</a></p>
            </div>
          </div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=category&action=show_vehicle"><span class="icon-box bg-color-9 set-icon"> <i class="fa fa-bus" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=category&action=show_vehicle">Show Vehicle</a></p>
            </div>
          </div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=mails&action=add_notice"><span class="icon-box bg-color-2 set-icon"> <i class="fa fa-bell-o" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=mails&action=add_notice">Add Notice Board</a></p>
            </div>
          </div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=mails&action=show_notice"><span class="icon-box bg-color-3 set-icon"> <i class="fa fa-bell-o" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=mails&action=show_notice">Show Notice Board</a></p>
            </div>
          </div>
        </div>
        
        
        
        
        
         <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=location_mange&action=add_country"><span class="icon-box bg-color-4 set-icon"> <i class="fa fa-globe" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=location_mange&action=add_country">Add Country</a></p>
            </div>
          </div>
        </div>
        
         <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=location_mange&action=add_state"><span class="icon-box bg-color-5 set-icon"> <i class="fa fa-globe" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=location_mange&action=add_state">Add Province</a></p>
            </div>
          </div>
        </div>
        
         <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=location_mange&action=add_city"><span class="icon-box bg-color-green set-icon"> <i class="fa fa-globe" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=location_mange&action=add_city">Add City</a></p>
            </div>
          </div>
        </div>
        
                 <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=location_mange&action=add_city"><span class="icon-box bg-color-12 set-icon"> <i class="fa fa-globe" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=location_mange&action=add_city">Add Town</a></p>
            </div>
          </div>
        </div>
        
         <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=location_mange&action=show_location_list"><span class="icon-box bg-color-7 set-icon"> <i class="fa fa-globe" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=location_mange&action=show_location_list">Country List</a></p>
            </div>
          </div>
        </div>
  
        
        
         <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"> <a href="{$site_url}/admin.php?page=mails&action=show_mails"><span class="icon-box bg-color-10 set-icon"> <i class="fa fa-edit" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=mails&action=show_mails">Mails Templates</a></p>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-6">
          <div class="panel panel-back noti-box"><a href="{$site_url}/admin.php?page=mails&action=show_news"> <span class="icon-box bg-color-6 set-icon"> <i class="fa fa-newspaper-o" style="margin-top:10px;"></i></span></a>
            <div class="text-box" >
            <!--  <p class="main-text">240 New</p>-->
              <p class="text-muted"><a href="{$site_url}/admin.php?page=mails&action=show_news">News List</a></p>
            </div>
          </div>
        </div>
      </div>
      
      <hr />
    </div>
  </div>
</div>
