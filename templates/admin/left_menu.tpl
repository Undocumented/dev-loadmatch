<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li class="text-center">
                <img src="{$site_url}/templates/admin/assets/img/find_user.png" class="user-image img-responsive"/>
            </li>
            <li>
                <a {if $action==admin_home} class="active-menu" {/if} href="{$site_url}/admin.php"><i
                        class="fa fa-dashboard fa-2x" style="width:30px;"></i>Dashboard</a>
            </li>
            <li>
                <a href="#" {if $action=="edit_company" || $action=="payment_gatway_setting" ||
                   $action=="social_setting"}class="active-menu" {/if}><i class="fa fa-cog fa-2x" style="width:30px;"
                                                                          style="width:50px;"></i> General Setting<span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level{if $action==" edit_company
                " || $action=="payment_gatway_setting" || $action=="social_setting"} collapse in {/if}" >
                    <li>
                        <a href="{$site_url}/admin.php?page=admin_home&action=edit_company" {if $action=="edit_company"}
                           class="active-menu-sub" {/if}>General Setting </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=admin_home&action=payment_gatway_setting" {if
                           $action=="payment_gatway_setting"} class="active-menu-sub" {/if}>Payment Gateway Setting </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=admin_home&action=social_setting" {if $action=="social_setting"}
                           class="active-menu-sub" {/if}>Social Setting</a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="#" {if $action=="show_sub_cat_list" || $action=="add_cat" || $action=="show_cat_list"}class="active-menu" {/if}><i class="fa fa-sitemap fa-2x" style="width:30px;"></i> Category <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level {if $action==" add_cat
                " || $action=="show_cat_list" || $action=="show_sub_cat_list" } collapse in {/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=industry_types" {if $action=="add_cat" && $type=="industry_types"}
                           class="active-menu-sub" {/if}>Add Industry Type </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=show_cat_list&type=industry_types" {if $action=="show_type_list" && $type=="industry_types"}
                           class="active-menu-sub" {/if}>View Industry Types </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=load_types" {if $action=="add_cat" && $type=="load_types"}
                           class="active-menu-sub" {/if}>Add Load Type </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=show_cat_list&type=load_types" {if $action=="show_type_list" && $type=="load_types"}
                           class="active-menu-sub" {/if}>View Load Types </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=transport_types" {if $action=="add_cat" && $type=="transport_types"}
                           class="active-menu-sub" {/if}>Add Transport Type </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=show_cat_list&type=transport_types" {if $action=="show_type_list" && $type=="transport_types"}
                           class="active-menu-sub" {/if}>View Transport Types </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=body_bulk_types" {if $action=="add_cat" && $type=="body_bulk_types"}
                           class="active-menu-sub" {/if}>Add Bulk Body Type </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=show_cat_list&type=bulk_body_types" {if $action=="show_type_list" && $type=="bulk_body_types"}
                           class="active-menu-sub" {/if}>View Bulk Body Types </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=size_types" {if $action=="add_cat" && $type=="size_types"}
                           class="active-menu-sub" {/if}>Add Size Type </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=show_cat_list&type=size_types" {if $action=="show_type_list" && $type=="size_types"}
                           class="active-menu-sub" {/if}>View Size Types </a>
                    </li>
                </ul>
            </li>
        <!--
            <li>
                <a href="#" {if $action=="show_sub_cat_list" || $action=="add_cat" ||
                   $action=="show_cat_list"}class="active-menu" {/if}><i class="fa fa-sitemap fa-2x"
                                                                         style="width:30px;"></i> Category <span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level {if $action==" add_cat
                " || $action=="show_cat_list" || $action=="show_sub_cat_list" } collapse in {/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=M" {if $action=="add_cat" && $type=="M"}
                           class="active-menu-sub" {/if}>Load Industry </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=S" {if $action=="add_cat" && $type=="S"}
                           class="active-menu-sub" {/if}>Load Type </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=SN" {if $action=="add_cat" && $type=="SN"}
                           class="active-menu-sub" {/if}>Transport Type </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=FC" {if $action=="add_cat" && $type=="FC"}
                           class="active-menu-sub" {/if}>Body & Bulk Type </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=FFC" {if $action=="add_cat" &&
                           $type=="FFC"} class="active-menu-sub" {/if}>Size Classification </a>
                    </li>

                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=SC" {if $action=="add_cat" && $type=="SC"}
                           class="active-menu-sub" {/if}>Payload KG </a>
                    </li>

                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=SVC" {if $action=="add_cat" &&
                           $type=="SVC"} class="active-menu-sub" {/if}>Length Metres </a>
                    </li>

                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=EC" {if $action=="add_cat" && $type=="EC"}
                           class="active-menu-sub" {/if}>Width Metres </a>
                    </li>

                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=NC" {if $action=="add_cat" && $type=="NC"}
                           class="active-menu-sub" {/if}>Preffered Distance </a>
                    </li>

                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_cat&type=TC" {if $action=="add_cat" && $type=="TC"}
                           class="active-menu-sub" {/if}>Other </a>
                    </li>


                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=show_cat_list&type=M" {if $action=="show_cat_list" ||
                           $action=="show_sub_cat_list" } class="active-menu-sub" {/if}>View Categories </a>
                    </li>

                </ul>

            </li>-->
            <li>
                <a href="#" {if $action=="add_users" || $action=="show_user_list"}class="active-menu" {/if}><i
                        class="fa fa-user fa-2x" style="width:30px;"></i>User <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level {if $action==" add_users
                " || $action=="show_user_list"} collapse in{/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=user&action=add_users" {if $action=="add_users"} class="active-menu-sub"
                           {/if}>Add User </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=user&action=show_user_list" {if $action=="show_user_list"}
                           class="active-menu-sub" {/if}>View User </a>
                    </li>


                </ul>

            </li>

            </li>
            <li>
                <a href="#" {if $action=="show_cust_review"|| $action=="show_trans_review" }class="active-menu" {/if}><i
                        class="fa fa-user fa-2x" style="width:30px;"></i>Review<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level {if $action==" show_cust_review
                " || $action=="show_trans_review"} collapse in{/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=user&action=show_cust_review" {if $action=="show_cust_review"}
                           class="active-menu-sub" {/if}>Customer </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=user&action=show_trans_review" {if $action=="show_trans_review"}
                           class="active-menu-sub" {/if}>Transporter</a>
                    </li>


                </ul>

            </li>


            <li>
                <a href="#" {if $action=="add_testimonial" || $action=="show_testimonial_list"}class="active-menu" {/if}><i
                        class="fa fa-user fa-2x" style="width:30px;"></i>Testimonial<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level {if $action==" add_imonial
                " || $action=="show_imonial_list"} collapse in{/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=user&action=add_testimonial" {if $action=="add_testimonial"}
                           class="active-menu-sub" {/if}>Add Testimonial </a>
                    </li>

                    <li>
                        <a href="{$site_url}/admin.php?page=user&action=show_testimonial_list" {if $action=="show_testimonial_list"}
                           class="active-menu-sub" {/if}>View Testimonial </a>
                    </li>


                </ul>

            </li>


            <li>
                <a href="#" {if $action=="add_service" || $action=="show_service_list"}class="active-menu" {/if}><i
                        class="fa fa-user fa-2x" style="width:30px;"></i>Service <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level {if $action==" add_service
                " || $action=="show_service_list"} collapse in{/if}">
                <!--<li>
                   <a href="{$site_url}/admin.php?page=user&action=add_service" {if $action=="add_service"} class="active-menu-sub" {/if}>Add Service </a>
               </li>-->

                    <li>
                        <a href="{$site_url}/admin.php?page=user&action=show_service_list" {if $action=="show_service_list"}
                           class="active-menu-sub" {/if}>View Service </a>
                    </li>


                </ul>

            </li>


            <li>
                <a href="#" {if $action=="booked_shipment" || $action=="detail_requirement"}class="active-menu" {/if}><i
                        class="fa fa-truck fa-2x" style="width:30px;"></i>Shipment <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level{if $action==booked_shipment || $action==detail_requirement} collapse in {/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment" {if
                           $action=="booked_shipment" && $type==""} class="active-menu-sub" {/if}>Active </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment&type=B" {if
                           $action=="booked_shipment" && $type=="B"} class="active-menu-sub" {/if}>Undelivered </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment&type=CT" {if
                           $action=="booked_shipment" && $type=="CT"} class="active-menu-sub" {/if}>Delivered </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=admin_home&action=booked_shipment&type=C" {if
                           $action=="booked_shipment" && $type=="C"} class="active-menu-sub" {/if}>Cancel </a>
                    </li>

                </ul>
            </li>


            <li>
                <a href="#" {if $action=="approve_requirement" || $action=="approve_review"}class="active-menu" {/if}><i
                        class="fa fa-thumbs-o-up fa-2x" style="width:30px;"></i>Approve Center <span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level {if $action==" approve_requirement
                " || $action=="approve_review"} collapse in{/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=admin_home&action=approve_requirement" {if
                           $action=="approve_requirement" } class="active-menu-sub" {/if}>New Shipment </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=admin_home&action=approve_review" {if $action=="approve_review" }
                           class="active-menu-sub" {/if}>User Review </a>
                    </li>

                </ul>
            </li>

            <li>
                <a href="{$site_url}/admin.php?page=payment&action=show_payment" {if $action=="show_payment" }
                   class="active-menu" {/if}><i class="fa fa-money fa-2x" style="width:30px;"></i> Payment</a>
            </li>
            <li>
                <a href="#" {if $action=="show_quote" || $action=="show_quote_agent" || $action=="detail_quote"}
                   class="active-menu" {/if}><i class="fa fa-bar-chart-o fa-2x" style="width:30px;"></i>Transporter Placed
                Quote <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level {if $action==" show_quote
                " || $inactive=="inactive"} collapse in{/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=payment&action=show_quote" {if $action=="show_quote" &&
                           $inactive!="inactive" } class="active-menu-sub" {/if}>Active Quote</a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=payment&action=show_quote&type=inactive" {if $inactive=="inactive" }
                           class="active-menu-sub" {/if}>Inactive Quote </a>
                    </li>

                </ul>
            </li>
            {*
            <li>
                <a href="{$site_url}/admin.php?page=payment&action=show_quote" {if $action=="show_quote" ||
                   $action=="show_quote_agent" || $action=="detail_quote"} class="active-menu" {/if}><i
                        class="fa fa-table fa-2x" style="width:30px;"></i>Transporter Placed Quote</a>
            </li>
            *}
            <li>
                <a href="{$site_url}/admin.php?page=payment&action=Package" {if $action=="Package" } class="active-menu" {/if}><i
                        class="fa fa-credit-card fa-2x" style="width:30px;"></i>Service Charge</a>
            </li>

            <li>
                <a href="{$site_url}/admin.php?page=mails&action=show_mails" {if $action=="show_mails" } class="active-menu"
                   {/if}><i class="fa fa-edit fa-2x" style="width:30px;"></i>Mails Templates </a>
            </li>

            <li>
                <a href="#" {if $action=="add_news" || $action=="show_news"}class="active-menu" {/if}><i
                        class="fa fa-newspaper-o fa-2x" style="width:30px;"></i>News <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level {if $action==" add_news
                " || $action=="show_news"} collapse in{/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=mails&action=add_news" {if $action=="add_news" } class="active-menu-sub"
                           {/if}>Add News </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=mails&action=show_news" {if $action=="show_news" }
                           class="active-menu-sub" {/if}>Show News </a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="#" {if $action=="add_vehicle" || $action=="show_vehicle"}class="active-menu" {/if}><i
                        class="fa fa-bus fa-2x" style="width:30px;"></i>Vehicle Management <span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level {if $action==" add_vehicle
                " || $action=="show_vehicle"} collapse in{/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=add_vehicle" {if $action=="add_vehicle" }
                           class="active-menu-sub" {/if}>Add Vehicle </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=category&action=show_vehicle" {if $action=="show_vehicle" }
                           class="active-menu-sub" {/if}>Show Vehicle</a>
                    </li>

                </ul>
            </li>

            <li>
                <a href="{$site_url}/admin.php?page=add_banner&action=add_slider" {if $action=="add_slider" }
                   class="active-menu" {/if}><i class="fa fa-spinner fa-2x" style="width:30px;"></i> Slider Setting</a>
            </li>

            <li>
                <a href="{$site_url}/admin.php?page=admin_home&action=show_message" {if $action=="show_message" }
                   class="active-menu" {/if}><i class="fa fa-envelope-o fa-2x" style="width:30px;"></i>Web Inquries</a>
            </li>
            <li>


            <li>
                <a href="#" {if $action=="add_notice" || $action=="show_notice"}class="active-menu" {/if}><i
                        class="fa fa-bell-o fa-2x" style="width:30px;"></i>Notice Board<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level {if $action==" add_notice
                " || $action=="show_notice"} collapse in{/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=mails&action=add_notice" {if $action=="add_notice" }
                           class="active-menu-sub" {/if}>Add Notice Board </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=mails&action=show_notice" {if $action=="show_notice" }
                           class="active-menu-sub" {/if}>Show Notice Board </a>
                    </li>

                </ul>
            </li>


            <li>
                <a href="#" {if $page=="location_mange"}class="active-menu" {/if}><i class="fa fa-globe fa-2x"
                                                                                     style="width:30px;"></i>Location
                Management <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level{if $page==location_mange} collapse in {/if}">
                    <li>
                        <a href="{$site_url}/admin.php?page=location_mange&action=add_country" {if $action=="add_country"}
                           class="active-menu-sub" {/if}> Add Country </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=location_mange&action=add_state" {if $action=="add_state"}
                           class="active-menu-sub" {/if}> Add Province </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=location_mange&action=add_city" {if $action=="add_city" }
                           class="active-menu-sub" {/if}> Add City </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=location_mange&action=add_town" {if $action=="add_town" }
                           class="active-menu-sub" {/if}> Add Town </a>
                    </li>
                    <li>
                        <a href="{$site_url}/admin.php?page=location_mange&action=show_location_list" {if
                           $action=="show_location_list"} class="active-menu-sub" {/if}> Country List</a>
                    </li>

                </ul>
            </li>


            <li>
                <a href="{$site_url}/admin.php?page=user&action=logout"><i class="fa fa-lock fa-2x" style="width:30px;"></i>Logout</a>
            </li>
        </ul>

    </div>

</nav>