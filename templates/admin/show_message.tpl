<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Message List</h2>
                    
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <h3>Message List</h3>
                           
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
              
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					 <thead align="left" >
					<tr>
						<th width="50">Sr.No.</th>
                        <th  width="600">Name</th>
						<th  width="600">Subject</th>
                        <th  width="600">Email</th>
						<th width="80">Action</th>
					</tr>
				 </thead>
				  <tbody>
					{if $userdata}
						{section name=data loop=$userdata}
							<tr>
								<td align="center">{$smarty.section.data.index+$row_no+1}</td>
                                <td>{$userdata[data].subject}</td>
								<td>{$userdata[data].subject}</td>
                                <td>{$userdata[data].email}</td>
								<td >
								
								  <a href="{$site_url}/admin.php?page=admin_home&action=detail_message&view_id={$userdata[data].id}" title="view"><i class="fa fa-pencil-square-o"></i></a>
							      <a href="{$site_url}/admin.php?page=admin_home&action=show_message&del_id={$userdata[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
						{/section}
                        <tr>
				 <td colspan="8"><form name="frm_pagi" action="#" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />
                {$show_pagi}
              </form></td>
          </tr>
				
					{else}
						<tr><td colspan="7" align="center">Record Not Found</td></tr>
					{/if}
					 <tbody>			
				</table>
					
					
 </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>