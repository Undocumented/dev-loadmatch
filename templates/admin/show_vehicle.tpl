<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Vehicle List</h2>
                     {if $show_message!=''}
                       <h5 class=" btn-success btn-lg" align="center">{$show_message}</h5>  
                       {/if} 
                      
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Vehicle List
                           
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="8%">Sr. No.</th>
                                            <th>Vehicle Name</th>
                                                                                  
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if $userdetail}
                                        {section name=data loop=$userdetail}
                                        <tr class="gradeU">
                                            <td align="center">{$smarty.section.data.index+$row_no+1}</td>
                                            <td>{$userdetail[data].vehicle_type}</td>
                                              <td>
                                               	<a href="{$site_url}/admin.php?page=category&action=add_vehicle&edit_id={$userdetail[data].id}" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                                              <a href="{$site_url}/admin.php?page=category&action=show_vehicle&del_id={$userdetail[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><span class="btn btn-success btn-sm" style="background-color:#F00;">Delete</span></a>
                                               </td>
                                        </tr>
                                        {/section}
                                        
                                         <tr>
            <td colspan="6"><form name="frm_pagi" action="" method="post">
                                        <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />
                                        {$show_pagi}
                       				 </form></td>
          </tr>
          {else}
          <tr>
            <td colspan="8"><span style="color:red;">Record Not Found</span></td>
          </tr>
          {/if}
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>

