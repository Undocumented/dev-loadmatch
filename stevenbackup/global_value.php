<?php 
ob_start();
session_start();
ini_set("session.bug_compat_warn", 0);
error_reporting(0);
date_default_timezone_set("GMT");
$date1 = "2017-04-20";
$date2 = date("Y-m-d");
$diff = strtotime($date1) - strtotime($date2);
$sec = $diff % 60;
$diff = intval($diff / 60);
$min = $diff % 60;
$diff = intval($diff / 60);
$hours = $diff % 24;
$days = intval($diff / 24);
$days = abs($days);
if( $days < 90 ) 
{
    if( !isset($_SERVER["DOCUMENT_ROOT"]) ) 
    {
        $_SERVER["DOCUMENT_ROOT"] = str_replace("\\", "/", substr($_SERVER["SCRIPT_FILENAME"], 0, 0 - strlen($_SERVER["PHP_SELF"])));
    }

    $livemode = true;
    if( $livemode ) 
    {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/includes/config.php");
    }
    else
    {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/devviplogix_uship/includes/config.php");
    }

    require_once(DOC . "includes/libs/mysql.class.php");
    require_once(DOC . "includes/libs/mysmarty.class.php");
    require_once(DOC . "includes/lists.php");
    require(DOC . "includes/libs/class.phpmailer.php");
    $objSmarty = new MySmarty(PROJECT_FOLDER);
    $dbh = new db_MySQL();
    $dbh->Connect();
    $config = "select * from site_config where id=1";
    $configdata = $dbh->Query($config);
    $max = 15;
    if( $dbh->num_rows ) 
    {
        $company_name = $configdata["company_name"];
        $company_address = $configdata["company_address"];
        $phone = $configdata["phone"];
        $fax = $configdata["fax"];
        $SITE_EMAIL = $configdata["email"];
        if( $livemode ) 
        {
            $site_url = "http://www.devviplogix.co.za";
        }
        else
        {
            $site_url = $configdata["site_url"];
        }

        $admin_url = $site_url;
        $support_email = $configdata["support_email"];
        $webmaster_email = $configdata["webmaster_email"];
        $admin_image = $site_url . "/templates/admin/images";
        $page_row = $configdata["page_row"];
        $logo = $configdata["logo"];
        $objSmarty->assign("mobile_no", $configdata["mobile_no"]);
        $objSmarty->assign("admin_image", $admin_image);
        $objSmarty->assign("company_name", $configdata["company_name"]);
        $objSmarty->assign("company_address", $configdata["company_address"]);
        $objSmarty->assign("phone", $configdata["phone"]);
        $objSmarty->assign("fax", $configdata["fax"]);
        $objSmarty->assign("SITE_EMAIL", $configdata["email"]);
        $objSmarty->assign("admin_url", $admin_url);
        $objSmarty->assign("site_url", $site_url);
        $objSmarty->assign("support_email", $configdata["support_email"]);
        $objSmarty->assign("webmaster_email", $configdata["webmaster_email"]);
        $objSmarty->assign("page_row", $configdata["page_row"]);
        $objSmarty->assign("logo", $configdata["logo"]);
    }

}

include_once(DOC . "classes/functions.class.php");
$functions = new functions();
$objSmarty->assign("functions", $functions);
$_SESSION["start_time"];
function GenerateOtp()
{
    $string = "1234567890";
    $shuffle = str_shuffle($string);
    $random_chars = substr($shuffle, 0, 6);
    return $random_chars = strtoupper($random_chars);
}

function GenerateOtpforqus()
{
    $string = "1234567890";
    $shuffle = str_shuffle($string);
    $random_chars = substr($shuffle, 0, 4);
    return $random_chars = strtoupper($random_chars);
}

function Send_mail($Site_mail, $Title, $Email, $Subject, $Message)
{
    $mail = new PHPMailer();
    $mail->From = $Site_mail;
    $mail->FromName = $Title;
    $mail->AddAddress($Email);
    $mail->WordWrap = 50;
    $mail->IsHTML(true);
    $mail->Subject = $Subject;
    $mail->Body = $Message;
    $mail->Send();
}


