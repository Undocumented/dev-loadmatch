<?php /* Smarty version 2.6.19, created on 2017-07-11 07:01:39
         compiled from admin/booked_shipment.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'admin/booked_shipment.tpl', 106, false),)), $this); ?>
<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2><?php if ($this->_tpl_vars['type'] == 'B'): ?> Undeliverd <?php elseif ($this->_tpl_vars['type'] == 'CT'): ?> Deliverd <?php elseif ($this->_tpl_vars['type'] == 'C'): ?> Cancel <?php else: ?> Active <?php endif; ?>Shipment List</h2>
                     <?php if ($this->_tpl_vars['show_message'] != ''): ?>
                      <h5 class=" btn-success btn-lg" align="center"><?php echo $this->_tpl_vars['show_message']; ?>
</h5>  
                       <?php endif; ?> 
                      
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <b><?php if ($this->_tpl_vars['type'] == 'B'): ?> Undeliverd <?php elseif ($this->_tpl_vars['type'] == 'CT'): ?> Deliverd <?php elseif ($this->_tpl_vars['type'] == 'C'): ?> Cancel <?php else: ?> Active <?php endif; ?> Shipment List</b>
                            <?php if ($this->_tpl_vars['show_message'] != ''): ?>
                          
                            <?php endif; ?>
                        </div>
                        <div class="panel-body">
                          <div class="container">
    <div class="row">
   <form name="search" id="search" method="post" action="#">
      <div class="col-sm-2">
         <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Category</label>
        <?php echo $this->_tpl_vars['functions']->select_category_admin_search($this->_tpl_vars['category12']); ?>

      
      </div>
  </div>
 
           <div class="col-sm-2">
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Date</label>
        
         
         <input type="date" value="<?php echo $this->_tpl_vars['search_date']; ?>
" id="search_date" name="search_date" class="form-control border-radius" onchange="document.search.submit()" />
     
  </div></div>
  
    <div class="col-sm-2">
     <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">From</label>
      
        
        <?php echo $this->_tpl_vars['functions']->select_city_form($this->_tpl_vars['from12']); ?>

      
      </div>
  </div>
  
  <div class="col-sm-2">
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">To</label>
        
         
       <?php echo $this->_tpl_vars['functions']->select_city_to($this->_tpl_vars['to12']); ?>

     
      </div>
      </div>
            
      </form>
  
 </div>
  </div>

          </section>
                            <div class="table-responsive">
                            
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                             <th>Entry Date</th>
                                            <th>Customer</th>
                                            <th>Category</th>
                                                                                        <th>From</th>
                                              <th>To</th>
                                            <th>Order Id</th>
                                           <?php if ($this->_tpl_vars['type'] == ''): ?>
                                           <th>Quote</th>
                                           <?php endif; ?>
                                            <th>Status</th>
                                             <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($this->_tpl_vars['usersdata']): ?>
                                        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['usersdata']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                                        <tr class="gradeU">
                                            <td align="center"><?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>
</td>
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['entery_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                            <td><?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['user_id']); ?>
</td>
                                             <td><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['category']); ?>
</td>
                                                                                           <td><?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['form_city']; ?>
</td>
                                               <td><?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['to_city']; ?>
</td>
                                            <td><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=detail_requirement&view_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['id']; ?>
&type=<?php echo $this->_tpl_vars['type']; ?>
" title="view"><?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['order_id']; ?>
</a></td>
                                            <?php if ($this->_tpl_vars['type'] == ''): ?>
                                             <td><?php echo $this->_tpl_vars['functions']->totel_quote_count($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['order_id']); ?>
</td>
                                             <?php endif; ?>
                                            <?php if ($this->_tpl_vars['type'] == ''): ?>
                                            <td> <span style="padding:5px; background-color:#0C3;border: 2px solid #0C3;
    border-radius: 25px;">Running </span></td>
                                            <?php else: ?>
                                            <td>
                                            <?php if ($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['transport_status'] != ''): ?>
                                            <?php if ($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['transport_status'] == 'Picked up'): ?> <span style="padding:5px; background-color:#0C0; border: 2px solid #0C0;
    border-radius: 25px;">Picked up</span><?php endif; ?>
                                            <?php if ($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['transport_status'] == 'In transit'): ?> <span style="padding:5px; background-color:#0FF;border: 2px solid #0FF;
    border-radius: 25px;">&nbsp;&nbsp;&nbsp;In transit</span><?php endif; ?>
                                            <?php if ($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['transport_status'] == 'Delivered'): ?> <span style="padding:5px; background-color:#0FF;border: 2px solid #0FF;
    border-radius: 25px;">&nbsp;&nbsp;&nbsp;Delivered</span><?php endif; ?>
     <?php if ($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['transport_status'] == 'Cancel'): ?> <span style="padding:5px; background-color:#0FF;border: 2px solid #0FF;
    border-radius: 25px;">Cancel</span><?php endif; ?>
                                            <?php else: ?> <span style="padding:5px; background-color:#F00; border: 2px solid #F00;
    border-radius: 25px;">&nbsp;&nbsp;&nbsp;Pending </span><?php endif; ?></td>
                                            <?php endif; ?>
                                               <td>
                                               	<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=detail_requirement&view_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['id']; ?>
&type=<?php echo $this->_tpl_vars['type']; ?>
" title="view"><span class="btn btn-success btn-sm">view</span></a>
                                                                                            </td>
                                        </tr>
                                        <?php endfor; endif; ?>
                                        
                                         <tr>
            <td colspan="10"><form name="frm_pagi" action="#" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" />
                <?php echo $this->_tpl_vars['show_pagi']; ?>

              </form></td>
          </tr>
          <?php else: ?>
          <tr>
            <td colspan="10"><span style="color:red;">Record Not Found</span></td>
          </tr>
          <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>
