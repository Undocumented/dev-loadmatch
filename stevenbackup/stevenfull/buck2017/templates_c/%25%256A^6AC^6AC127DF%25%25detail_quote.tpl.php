<?php /* Smarty version 2.6.19, created on 2017-04-26 17:52:20
         compiled from admin/detail_quote.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'admin/detail_quote.tpl', 46, false),)), $this); ?>
<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>View Quote Detail</h2> 
                     
                    </div>
                </div>
            
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                                    
                            
                        </div>
                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>
                                        <tr>
                                            <th width="150px">Transporter Name</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['userdata'][0]['agent_id']); ?>
</td>
                                        	
                                         
                                            </tr>
                                            <tr>
                                              <th>Order id</th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['order_id']; ?>
</td>
                                           
                                        </tr>
                                        <tr>
                                             <th>Price</th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['starting_price']; ?>
(<?php echo $this->_tpl_vars['functions']->get_currency(); ?>
)</td>
                                          
                                        </tr>
                                        </tbody>
                                        </table>
                                         <table class="table table-striped table-bordered table-hover">
                                          <tbody>
                                        <tr>
                                              <th width="150px">Collect Start Date</th>
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['userdata'][0]['collect_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                            <th width="150px">Collect End Date</th>
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['userdata'][0]['collect_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                        </tr>
                                        
                                         <tr>
                                              <th width="150px">Delivery Start Date</th>
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['userdata'][0]['delivery_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                            <th>Delivery End Date</th>
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['userdata'][0]['delivery_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                         <table class="table table-striped table-bordered table-hover">
                                          <tbody>
                                         <tr>
                                              <th>Payment Method</th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['payment_methods']; ?>
</td>
                                            <th>Payment Accepted</th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['payment_accepted']; ?>
</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                         <table class="table table-striped table-bordered table-hover">
                                          <tbody>
                                         <tr>
                                              <th width="150px">Payment Terms</th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['payment_terms']; ?>
</td>
                                            </tr>
                                            <tr>
                                            <th>Standerd Terms</th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['standerd_terms']; ?>
</td>
                                        </tr>
                                        
                                    </tbody>
                                    
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->
            
                <!-- /. ROW  -->
            
             
        </div>
               
    </div>