<?php /* Smarty version 2.6.19, created on 2017-06-19 13:13:47
         compiled from admin/add_cat.tpl */ ?>
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2><?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?> <?php if ($this->_tpl_vars['type'] == 'M'): ?>Category <?php else: ?> Category<?php endif; ?></h2>
      </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading"><b> <?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?> <?php if ($this->_tpl_vars['type'] == 'M'): ?>Category <?php else: ?> Category<?php endif; ?></b> <?php if ($this->_tpl_vars['add_sucess'] == 'Y'): ?>
            <div class="notibar msgsuccess">
              <div class="alert alert-success" >Category Added Successfully !</div>
            </div>
            <?php endif; ?> </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> <?php if ($this->_tpl_vars['type'] == 'M'): ?> 
                
                <!--===============customer form======================-->
                <form action="" method="post" name="add_new_cat"  enctype="multipart/form-data">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id']; ?>
" />
                  <div class="form-group">
                    <label>Load Industry</label>
                    <input class="form-control" type="text" name="cat_name" id="cat_name" value="<?php echo $this->_tpl_vars['useredit'][0]['category']; ?>
" placeholder="Enter Main Category Name " />
                  </div>
                  <div class="form-group">
                    <label>Keyword</label>
                    <input class="form-control" type="text" name="keyword" id="keyword" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_keyword']; ?>
" placeholder="Enter Keyword " />
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input class="form-control" type="text" name="description" id="description" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_description']; ?>
" placeholder="Enter Description " />
                  </div>
                  <div class="form-group">
                    <label>Logo</label>
                    <input type="file" name="logo"  id="logo"/>
                    <input type="hidden" name="old_main_img" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_image']; ?>
" />
                  </div>
                  <button type="submit" class="btn btn-primary" name="cat_btn" value="Add Category" onclick="return valid_cat_value(document.add_new_cat);"> <?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?> Category</button>
                </form>
                <!--==================================================--> 
                <?php endif; ?>
                <?php if ($this->_tpl_vars['type'] == 'S'): ?>
                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id_sub']; ?>
" />
                  <?php if ($this->_tpl_vars['add_suce'] == 'Y'): ?>
                  <div class="notibar msgsuccess">
                    <div class="alert alert-success" >Sub Category Added Successfully !</div>
                  </div>
                  <?php endif; ?>
                  <div class="form-group">
                    <label>Category Name</label>
                    <select name="category"  id="category" class="form-control" >
                      <option value="please Select" selected="selected" >please Select</option>
                      
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
                    
                      <option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id'] == $this->_tpl_vars['useredit'][0]['parent_id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
                      
                  <?php endfor; endif; ?>
                   
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Add Sub Category Name</label>
                    <input class="form-control" type="text" name="cat_name" id="cat_name" value="<?php echo $this->_tpl_vars['useredit'][0]['category']; ?>
" placeholder="Enter Sub Category name " />
                  </div>
                  <div class="form-group">
                    <label>Keyword</label>
                    <input class="form-control" type="text" name="keyword" id="keyword" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_keyword']; ?>
" placeholder="Enter Keyword " />
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input class="form-control" type="text" name="description" id="description" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_description']; ?>
" placeholder="Enter Description " />
                  </div>
                  <button type="submit" class="btn btn-primary" name="sub_cat_btn" id="sub_cat_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> <?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?>  Sub Category</button>
                </form>
                <?php endif; ?>
                
                <?php if ($this->_tpl_vars['type'] == 'SN'): ?>
                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id_sub']; ?>
" />
                  <?php if ($this->_tpl_vars['add_suce'] == 'Y'): ?>
                  <div class="notibar msgsuccess">
                    <div class="alert alert-success" >Sub Next Category Added Successfully !</div>
                  </div>
                  <?php endif; ?>
                  <div class="form-group">
                    <label>Category Name</label>
                    <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                      <option value="please Select" selected="selected" >please Select</option>
                      
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                       
                    
                      <option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id'] == $this->_tpl_vars['user_cat_edit'][0]['parent_id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
                      
                  <?php endfor; endif; ?>
                   
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Category Name</label>
                    <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Add Sub Next Category Name</label>
                    <input class="form-control" type="text" name="cat_name" id="cat_name" value="<?php echo $this->_tpl_vars['useredit'][0]['category']; ?>
" placeholder="Enter Sub Category name " />
                  </div>
                  <div class="form-group">
                    <label>Keyword</label>
                    <input class="form-control" type="text" name="keyword" id="keyword" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_keyword']; ?>
" placeholder="Enter Keyword " />
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input class="form-control" type="text" name="description" id="description" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_description']; ?>
" placeholder="Enter Description " />
                  </div>
                  <button type="submit" class="btn btn-primary" name="sub_nxt_cat_btn" id="sub_nxt_cat_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> <?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?>  Sub Category</button>
                </form>
                <?php endif; ?>
                
                <?php if ($this->_tpl_vars['type'] == 'FC'): ?>
                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id_sub']; ?>
" />
                  <?php if ($this->_tpl_vars['add_suce'] == 'Y'): ?>
                  <div class="notibar msgsuccess">
                    <div class="alert alert-success" >Sub Next Category Added Successfully !</div>
                  </div>
                  <?php endif; ?>
                  <div class="form-group">
                    <label>Category Name</label>
                    <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                      <option value="please Select" selected="selected" >please Select</option>
                      
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
                    
                      <option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id'] == $this->_tpl_vars['user_cat_edit'][0]['parent_id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
                      
                  <?php endfor; endif; ?>
                   
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Category Name</label>
                    <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Category Name</label>
                    <select name="last_sub_cat" id="sub_next_cat_dropdown" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Add Sub Next Category Name</label>
                    <input class="form-control" type="text" name="cat_name" id="cat_name" value="<?php echo $this->_tpl_vars['useredit'][0]['category']; ?>
" placeholder="Enter Sub Category name " />
                  </div>
                  <div class="form-group">
                    <label>Keyword</label>
                    <input class="form-control" type="text" name="keyword" id="keyword" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_keyword']; ?>
" placeholder="Enter Keyword " />
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input class="form-control" type="text" name="description" id="description" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_description']; ?>
" placeholder="Enter Description " />
                  </div>
                  <button type="submit" class="btn btn-primary" name="last_cat_btn" id="last_cat" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> <?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?>  Last Category</button>
                </form>
                <?php endif; ?>
                
                <?php if ($this->_tpl_vars['type'] == 'FFC'): ?>
                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id_sub']; ?>
" />
                  <?php if ($this->_tpl_vars['add_suce'] == 'Y'): ?>
                  <div class="notibar msgsuccess">
                    <div class="alert alert-success" >Fifth Category Added Successfully !</div>
                  </div>
                  <?php endif; ?>
                  <div class="form-group">
                    <label>Category Name</label>
                    <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                      <option value="please Select" selected="selected" >please Select</option>
                      
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
                    
                      <option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id'] == $this->_tpl_vars['user_cat_edit'][0]['parent_id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
                      
                  <?php endfor; endif; ?>
                   
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Category Name</label>
                    <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Next Category Name</label>
                    <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Fourth Category</label>
                    <select name="fourth_cat" id="fourth_cat" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Add Sub Next Category Name</label>
                    <input class="form-control" type="text" name="cat_name" id="cat_name" value="<?php echo $this->_tpl_vars['useredit'][0]['category']; ?>
" placeholder="Enter Sub Category name " />
                  </div>
                  <div class="form-group">
                    <label>Keyword</label>
                    <input class="form-control" type="text" name="keyword" id="keyword" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_keyword']; ?>
" placeholder="Enter Keyword " />
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input class="form-control" type="text" name="description" id="description" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_description']; ?>
" placeholder="Enter Description " />
                  </div>
                  <button type="submit" class="btn btn-primary" name="fifth_cat_btn" id="fifth_cat" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> <?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?>  Fifth Category</button>
                </form>
                <?php endif; ?>
                
                <?php if ($this->_tpl_vars['type'] == 'SC'): ?>
                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id_sub']; ?>
" />
                  <?php if ($this->_tpl_vars['add_suce'] == 'Y'): ?>
                  <div class="notibar msgsuccess">
                    <div class="alert alert-success" >Fifth Category Added Successfully !</div>
                  </div>
                  <?php endif; ?>
                  <div class="form-group">
                    <label>Category Name</label>
                    <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                      <option value="please Select" selected="selected" >please Select</option>
                      
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                    
                      <option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id'] == $this->_tpl_vars['user_cat_edit'][0]['parent_id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
                      
                  <?php endfor; endif; ?>
                   
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Category Name</label>
                    <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Next Category Name</label>
                    <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Fourth Category</label>
                    <select name="fourth_cat" id="fourth_cat" onchange="Get_fifth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Fifth  Category</label>
                    <select name="fifth_cat" id="fifth_id" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Add Sixth Category Name</label>
                    <input class="form-control" type="text" name="cat_name" id="cat_name" value="<?php echo $this->_tpl_vars['useredit'][0]['category']; ?>
" placeholder="Enter Sub Category name " />
                  </div>
                  <div class="form-group">
                    <label>Keyword</label>
                    <input class="form-control" type="text" name="keyword" id="keyword" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_keyword']; ?>
" placeholder="Enter Keyword " />
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input class="form-control" type="text" name="description" id="description" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_description']; ?>
" placeholder="Enter Description " />
                  </div>
                  <button type="submit" class="btn btn-primary" name="sixth_cat_btn" id="sixth_cat" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> <?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?>  Sixth Category</button>
                </form>
                <?php endif; ?>
                
                 <?php if ($this->_tpl_vars['type'] == 'SVC'): ?>
                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id_sub']; ?>
" />
                  <?php if ($this->_tpl_vars['add_suce'] == 'Y'): ?>
                  <div class="notibar msgsuccess">
                    <div class="alert alert-success" >Seventh Category Added Successfully !</div>
                  </div>
                  <?php endif; ?>
                  <div class="form-group">
                    <label>Category Name</label>
                    <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                      <option value="please Select" selected="selected" >please Select</option>
                      
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                    
                      <option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id'] == $this->_tpl_vars['user_cat_edit'][0]['parent_id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
                      
                  <?php endfor; endif; ?>
                   
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Category Name</label>
                    <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Next Category Name</label>
                    <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Fourth Category</label>
                    <select name="fourth_cat" id="fourth_cat" onchange="Get_fifth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Fifth  Category</label>
                    <select name="fifth_cat" id="fifth_id" onchange="Get_sixth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    <div class="form-group">
                    <label>Sixth  Category</label>
                    <select name="sixth_cat1" id="sixth_cat" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    
                  <div class="form-group">
                    <label>Add Seventh Category Name</label>
                    <input class="form-control" type="text" name="cat_name" id="cat_name" value="<?php echo $this->_tpl_vars['useredit'][0]['category']; ?>
" placeholder="Enter Sub Category name " />
                  </div>
                  <div class="form-group">
                    <label>Keyword</label>
                    <input class="form-control" type="text" name="keyword" id="keyword" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_keyword']; ?>
" placeholder="Enter Keyword " />
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input class="form-control" type="text" name="description" id="description" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_description']; ?>
" placeholder="Enter Description " />
                  </div>
                  <button type="submit" class="btn btn-primary" name="seveth_cat_btn" id="seveth_cat" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> <?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?>  Seventh Category</button>
                </form>
                <?php endif; ?>
                
                <?php if ($this->_tpl_vars['type'] == 'EC'): ?>
                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id_sub']; ?>
" />
                  <?php if ($this->_tpl_vars['add_suce'] == 'Y'): ?>
                  <div class="notibar msgsuccess">
                    <div class="alert alert-success" >Eigth Category Added Successfully !</div>
                  </div>
                  <?php endif; ?>
                  <div class="form-group">
                    <label>Category Name</label>
                    <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                      <option value="please Select" selected="selected" >please Select</option>
                      
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                    
                      <option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id'] == $this->_tpl_vars['user_cat_edit'][0]['parent_id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
                      
                  <?php endfor; endif; ?>
                   
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Category Name</label>
                    <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Next Category Name</label>
                    <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Fourth Category</label>
                    <select name="fourth_cat" id="fourth_cat" onchange="Get_fifth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Fifth  Category</label>
                    <select name="fifth_cat" id="fifth_id" onchange="Get_sixth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    <div class="form-group">
                    <label>Sixth  Category</label>
                    <select name="sixth_cat1" id="sixth_cat" onchange="Get_seventh_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    
                     <div class="form-group">
                    <label>Sevnth  Category</label>
                    <select name="seventh_cat" id="seventh_cat" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    
                  <div class="form-group">
                    <label>Add Eight Category Name</label>
                    <input class="form-control" type="text" name="cat_name" id="cat_name" value="<?php echo $this->_tpl_vars['useredit'][0]['category']; ?>
" placeholder="Enter Sub Category name " />
                  </div>
                  <div class="form-group">
                    <label>Keyword</label>
                    <input class="form-control" type="text" name="keyword" id="keyword" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_keyword']; ?>
" placeholder="Enter Keyword " />
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input class="form-control" type="text" name="description" id="description" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_description']; ?>
" placeholder="Enter Description " />
                  </div>
                  <button type="submit" class="btn btn-primary" name="eigth_cat_btn" id="eigth_cat_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> <?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?>  Seventh Category</button>
                </form>
                <?php endif; ?>
                
                <?php if ($this->_tpl_vars['type'] == 'NC'): ?>
                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id_sub']; ?>
" />
                  <?php if ($this->_tpl_vars['add_suce'] == 'Y'): ?>
                  <div class="notibar msgsuccess">
                    <div class="alert alert-success" >Ninth Category Added Successfully !</div>
                  </div>
                  <?php endif; ?>
                  <div class="form-group">
                    <label>Category Name</label>
                    <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                      <option value="please Select" selected="selected" >please Select</option>
                      
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                    
                      <option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id'] == $this->_tpl_vars['user_cat_edit'][0]['parent_id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
                      
                  <?php endfor; endif; ?>
                   
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Category Name</label>
                    <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Next Category Name</label>
                    <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Fourth Category</label>
                    <select name="fourth_cat" id="fourth_cat" onchange="Get_fifth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Fifth  Category</label>
                    <select name="fifth_cat" id="fifth_id" onchange="Get_sixth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    <div class="form-group">
                    <label>Sixth  Category</label>
                    <select name="sixth_cat1" id="sixth_cat" onchange="Get_seventh_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    
                     <div class="form-group">
                    <label>Sevnth  Category</label>
                    <select name="seventh_cat" id="seventh_cat"  onchange="Get_eigth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    
                    </div>
                    <div class="form-group">
                    <label>Eigth  Category</label>
                    <select name="eigth_cat" id="eigth_cat" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    
                  <div class="form-group">
                    <label>Add Ninth Category Name</label>
                    <input class="form-control" type="text" name="cat_name" id="cat_name" value="<?php echo $this->_tpl_vars['useredit'][0]['category']; ?>
" placeholder="Enter Sub Category name " />
                  </div>
                  <div class="form-group">
                    <label>Keyword</label>
                    <input class="form-control" type="text" name="keyword" id="keyword" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_keyword']; ?>
" placeholder="Enter Keyword " />
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input class="form-control" type="text" name="description" id="description" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_description']; ?>
" placeholder="Enter Description " />
                  </div>
                  <button type="submit" class="btn btn-primary" name="ninth_cat_btn" id="ninth_cat_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> <?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?>  Ninth Category</button>
                </form>
                <?php endif; ?>
                
                <?php if ($this->_tpl_vars['type'] == 'TC'): ?>
                <form action="" method="post" name="add_cat_sub"  enctype="multipart/form-data">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id_sub']; ?>
" />
                  <?php if ($this->_tpl_vars['add_suce'] == 'Y'): ?>
                  <div class="notibar msgsuccess">
                    <div class="alert alert-success" >Tenth Category Added Successfully !</div>
                  </div>
                  <?php endif; ?>
                  <div class="form-group">
                    <label>Category Name</label>
                    <select name="category"  id="category" class="form-control"  onchange="select_sub_cat(this.value);">
                      <option value="please Select" selected="selected" >please Select</option>
                      
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                    
                      <option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" <?php if ($this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id'] == $this->_tpl_vars['user_cat_edit'][0]['parent_id']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
                      
                  <?php endfor; endif; ?>
                   
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Category Name</label>
                    <select name="sub_next_cat" id="sub_cat_dropdown" class="form-control"  onchange="select_sub_next_cat(this.value);" >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Sub Next Category Name</label>
                    <select name="last_sub_cat" id="sub_next_cat_dropdown" onchange="Get_fourth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Fourth Category</label>
                    <select name="fourth_cat" id="fourth_cat" onchange="Get_fifth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                  <div class="form-group">
                    <label>Fifth  Category</label>
                    <select name="fifth_cat" id="fifth_id" onchange="Get_sixth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    <div class="form-group">
                    <label>Sixth  Category</label>
                    <select name="sixth_cat1" id="sixth_cat" onchange="Get_seventh_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    
                     <div class="form-group">
                    <label>Sevnth  Category</label>
                    <select name="seventh_cat" id="seventh_cat"  onchange="Get_eigth_cat(this.value);" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    
                    </div>
                    <div class="form-group">
                    <label>Eigth  Category</label>
                    <select name="eigth_cat" id="eigth_cat" onchange="Get_ninth_cat(this.value);"  class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    
                    <div class="form-group">
                    <label>Ninth  Category</label>
                    <select name="ninth_cat" id="ninth_cat" class="form-control"   >
                      <option value="please Select" selected="selected" >please Select</option>
                    </select>
                    <span class="alert" id="alert_cat_id"></span> </div>
                    
                  <div class="form-group">
                    <label>Add Tenth Category Name</label>
                    <input class="form-control" type="text" name="cat_name" id="cat_name" value="<?php echo $this->_tpl_vars['useredit'][0]['category']; ?>
" placeholder="Enter Sub Category name " />
                  </div>
                  <div class="form-group">
                    <label>Keyword</label>
                    <input class="form-control" type="text" name="keyword" id="keyword" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_keyword']; ?>
" placeholder="Enter Keyword " />
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input class="form-control" type="text" name="description" id="description" value="<?php echo $this->_tpl_vars['useredit'][0]['cat_description']; ?>
" placeholder="Enter Description " />
                  </div>
                  <button type="submit" class="btn btn-primary" name="tenth_cat_btn" id="tenth_cat_btn" value="AddCategory" onclick="return valid_sub_add_cat(document.add_cat_sub);"> <?php if ($this->_tpl_vars['edit_id'] == '' && $this->_tpl_vars['edit_id_sub'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?>  Tenth Category</button>
                </form>
                <?php endif; ?>
                
                 </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>