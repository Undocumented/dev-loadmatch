<?php /* Smarty version 2.6.19, created on 2017-07-03 11:54:57
         compiled from booked_cancel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'booked_cancel.tpl', 263, false),)), $this); ?>
<?php if ($_SESSION['user_type'] == 'C'): ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Cancel Shipment </h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My Cancel Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          My Cancel Shipment
          </h4>
           <table align="right">
          <tr>
          <td align="left">
          
           <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
          <p><b style="color:#F00;">Active&nbsp;:</b>&nbsp;&nbsp;Change Shipment Status to Active</p>
          <p><b style="color:#F00;">Delete&nbsp;:</b>&nbsp;&nbsp;Delete Shipment </p>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
            <br/>
                  
             </td>
             </tr>
             </table>
             <br/><br/><br/>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/edit_post_shipping" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                      Collection
                    </th>
                    <th>
                      Delivery
                    </th>
                    <th>
                     Category
                    </th>
                    <th>
                     Under Category
                    </th>
                     <th>
                     Status
                    </th>
                   <th>
                     Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['search']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>

                    </td>
                    <td>
                     <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['form_state']; ?>
, <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['form_city']; ?>

                    </td>
                    <td>
                     <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['to_state']; ?>
, <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['to_city']; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][$this->_sections['data']['index']]['category']); ?>

                    </td>
                    <td>
                       <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][$this->_sections['data']['index']]['sub_category']); ?>

                    </td>
                     <td>
                        <div class="btn btn-default btn-sm"><?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['transport_status']; ?>
 </div>
                    </td>
                  <td>
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                    <a style="cursor:pointer;" onclick="submitbuy_book_cancel_complet('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
','c',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/eagleushp.php?page=myaccount&action=booked_cancel&active=<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" class="btn btn-success btn-sm">Avtive</a>
                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/eagleushp.php?page=myaccount&action=booked_cancel&delete=<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                  </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>
</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    <?php endif; ?>
    
    <?php if ($_SESSION['user_type'] == 'T'): ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Cancel Bookings</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My Cancel Bookings</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
           My Cancel Bookings
          </h4>
          <table align="right">
          <tr>
          <td align="left">
           <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
          <p><b style="color:#F00;">Active&nbsp;:</b>&nbsp;&nbsp;Re-Active Shipment Quote </p>
          <p><b style="color:#F00;">Not Available&nbsp;:</b>&nbsp;&nbsp;Shipment is Booked by Other Transporter</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
            <br/>
              
                      </td>
             </tr>
             </table>
             <br/><br/><br/>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/detail_shiping_booked" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                      Customer
                    </th>
                    <th>
                     Pick up Time
                    </th>
                    <th>
                     Delivery Time
                    </th>
                     <th>
                      Delivery Location
                    </th>
                     <th>
                     Status
                    </th>
                     <th>
                     Action
                    </th> 
                  </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['search']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>

                    </td>
                     <td>
                       <?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][$this->_sections['data']['index']]['user_id']); ?>

                    </td>
                    <td>
                     <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['collect_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
, <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['collect_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                    </td>
                    <td>
                     <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['delivery_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
, <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['delivery_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                    </td>
                     <td>
                     <?php echo $this->_tpl_vars['functions']->get_destnation_state_city($this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']); ?>

                    </td>
                    <td>
                      <div class="btn btn-default btn-sm"> Cancled</div>
                    </td>
                    <td>
                      <?php if ($this->_tpl_vars['search'][$this->_sections['data']['index']]['agent_quata_cencle'] == 'Y'): ?> <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/eagleushp.php?page=myaccount&action=booked_cancel&active=<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" class="btn btn-success btn-sm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Avtive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><?php else: ?> <a href="" class="btn btn-success btn-sm">Not Available</a><?php endif; ?>
                    <a style="cursor:pointer;" onclick="submitbuy_agent_cencle_complet('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
','<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['order_id']; ?>
','c',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                  
                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/eagleushp.php?page=myaccount&action=booked_cancel&delete=<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                  </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>
</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    <?php endif; ?>