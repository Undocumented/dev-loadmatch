<?php /* Smarty version 2.6.19, created on 2017-07-17 14:57:36
         compiled from give_quote.tpl */ ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Give Quote</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Give Quote</a></li>
                        <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="component-bg">
        <div class="container">
            <!-- Forms
================================================== -->
<div class="bs-docs-section mar-b-30">
  <h1 id="forms" class="page-header">Give Quote</h1>
 <span style="color:red;"><?php echo $this->_tpl_vars['err']; ?>
</span>
    <form class="form-horizontal" name="post_shipment"  method="post" action="#" role="form">
    <label for="exampleInputEmail1">Transporter Details</label>
     <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="<?php echo $_SESSION['name']; ?>
" id="s_user_name" name="s_user_name" placeholder="User Name" readonly="readonly">
          <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>
" />
          
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" value="<?php echo $_SESSION['email']; ?>
" id="s_user_email" name="s_user_email" placeholder="Email" readonly="readonly">
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="mobile" value="<?php echo $this->_tpl_vars['functions']->get_mobile($_SESSION['user_id']); ?>
"  name="mobile" placeholder="Mobile no" readonly="readonly">
        </div>
      </div>
   <hr />
     <label for="exampleInputEmail1">Transport Timeframe</label>
     
     
     
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Pickup Timing</label>
        <div class="col-xs-12 text-left">
        &nbsp;&nbsp;&nbsp;&nbsp;<INPUT NAME="pickup_start_date" id="pickup_start_date" TYPE="date"   value="<?php echo $this->_tpl_vars['search1'][0]['collect_start_date']; ?>
" />
        
        
       <label for="inputEmail3" class=" control-label">&nbsp;&nbsp;Between&nbsp;&nbsp;</label>
         
        <INPUT NAME="pickup_end_date" id="pickup_end_date" TYPE="date"   value="<?php echo $this->_tpl_vars['search1'][0]['collect_end_date']; ?>
" />
        
       
        </div>
      
  </div>
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery Timing</label>
        <div class="col-xs-12 text-left">
         &nbsp;&nbsp;&nbsp;&nbsp;  <INPUT NAME="delv_start_date" id="delv_start_date" TYPE="date"   value="<?php echo $this->_tpl_vars['search1'][0]['delivery_start_date']; ?>
" />
         <label for="inputEmail3" class=" control-label">&nbsp;&nbsp;Between&nbsp;&nbsp;</label>
            <INPUT NAME="delv_end_date" id="delv_end_date" TYPE="date"   value="<?php echo $this->_tpl_vars['search1'][0]['delivery_end_date']; ?>
" />
        </div>
      
  </div>
   <hr />
        <label for="exampleInputEmail1">Quoteion Price</label>
      <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Price</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="<?php echo $this->_tpl_vars['search1'][0]['starting_price']; ?>
" id="Price" name="Price" placeholder="Price">
         </div>
      </div>
     <hr />  
       <label for="exampleInputEmail1">Payment Terms</label>
      
        <div class="form-group">
         <label for="inputEmail3"  class="col-sm-2 control-label" >Payment Methods</label>
        <div class="col-sm-10">
         <table>
       <tr>
       <td>
        <label class="checkbox-inline">
        <input type="checkbox" id="pay" name="pay[]" value="Cash" <?php if ($this->_tpl_vars['cash'] == 'Y'): ?> checked <?php endif; ?>> Cash&nbsp;&nbsp;&nbsp;&nbsp;
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="pay" name="pay[]" value="Credit Card" <?php if ($this->_tpl_vars['Credit_Card'] == 'Y'): ?> checked <?php endif; ?>>Credit Card
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="pay" name="pay[]" value="Cheque" <?php if ($this->_tpl_vars['Cheque'] == 'Y'): ?> checked <?php endif; ?> > Cheque
      </label>
      </td>
      </tr>
      <tr>
      <td>
      <label class="checkbox-inline">
       <input type="checkbox" id="pay" name="pay[]" value="PayPal" <?php if ($this->_tpl_vars['PayPal'] == 'Y'): ?> checked <?php endif; ?>> PayPal
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="pay" name="pay[]" value="Money Order" <?php if ($this->_tpl_vars['Money_Order'] == 'Y'): ?> checked <?php endif; ?> >Money Order
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="pay" name="pay[]" value="Other" <?php if ($this->_tpl_vars['Other'] == 'Y'): ?> checked <?php endif; ?> >Other
      </label>
      </td>
      </tr>
      </table>
      </div>
       </div>
       <div class="form-group">
         <label for="inputEmail3"  class="col-sm-2 control-label" >Payment Accepted</label>
        <div class="col-sm-10">
         <table>
       <tr>
       <td>
        <label class="checkbox-inline">
        <input type="checkbox" id="inlineCheckbox1" name="accept[]" value="Before collection" <?php if ($this->_tpl_vars['Beforecollection'] == 'Y'): ?> checked="checked" <?php endif; ?>> Before collection
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="inlineCheckbox2" name="accept[]" value="At collection" <?php if ($this->_tpl_vars['Atcollection'] == 'Y'): ?> checked="checked" <?php endif; ?>>At collection
      </label>
      <label class="checkbox-inline">
        <input type="checkbox" id="inlineCheckbox3" name="accept[]" value="At Delivery" <?php if ($this->_tpl_vars['AtDelivery'] == 'Y'): ?> checked="checked" <?php endif; ?>> At Delivery
      </label>
      </td>
      </tr>
      
      </table>
      </div>
       </div>
 
 <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Additional Payment Terms</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="payment_terms" rows="3"><?php echo $this->_tpl_vars['search1'][0]['payment_terms']; ?>
</textarea>
     </div>
        
  </div>
     
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
        <input type="hidden" value="<?php echo $this->_tpl_vars['user_id']; ?>
" name="user_id" />
        <input type="hidden" value="<?php echo $this->_tpl_vars['order_id']; ?>
" name="order_id" />
          <button type="submit" name="submit1" class="btn btn-default" onclick="return Valid_add_quote(document.post_shipment);" >Submit Quote	</button>
        </div>
      </div>
    </form>
  </div><!-- /.bs-example -->

  
     
   

</div>
        </div>
    </div>
    <!--container end-->