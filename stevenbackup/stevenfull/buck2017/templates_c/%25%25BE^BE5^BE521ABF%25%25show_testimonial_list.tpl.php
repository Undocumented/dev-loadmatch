<?php /* Smarty version 2.6.19, created on 2017-06-13 09:50:59
         compiled from admin/show_testimonial_list.tpl */ ?>
<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Testimonial List</h2>
                 <?php if ($this->_tpl_vars['sucess'] == 'Y'): ?> <div class="notibar msgsuccess">
                        
                        <div class="alert alert-success" >Added Successfully !</div>
                    </div><?php endif; ?>
                     <?php if ($this->_tpl_vars['update'] == 'Y'): ?> <div class="notibar msgsuccess">
                        
                        <div class="alert alert-success" >Update Successfully !</div>
                    </div><?php endif; ?>          
                       <?php if ($this->_tpl_vars['del'] == 'Y'): ?> <div class="notibar msgsuccess">
                        
                        <div class="alert alert-success" >Deleted Successfully !</div>
                    </div><?php endif; ?>    
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <b> Testimonial List </b>
                      <div  style="text-align:right;">    
                 
                        </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="7%">Sr. No.</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Image</th>
                                             <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($this->_tpl_vars['userdata']): ?>
                                        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['userdata']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                                        <tr class="gradeU">
                                            <td align="center"><?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>
</td>
                                            <td><?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['name']; ?>
</td>
                                             <td><?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['description']; ?>
</td>
                                              <td><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/location_flage/<?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['img']; ?>
" width="50PX;" height="50PX;" /></td>
                                               <td>
                                               	<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=add_testimonial&edit_id=<?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['id']; ?>
" title="Update"><span class="btn btn-success btn-sm">Update &nbsp;&nbsp;&nbsp;</span></a>
                                                
                                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=show_testimonial_list&del_id=<?php echo $this->_tpl_vars['userdata'][$this->_sections['data']['index']]['id']; ?>
" onclick="return confirm('Do You  want to delete ?');" title="Delete"><span class="btn btn-success btn-sm" style="background-color:#F00;">Delete</span></a>
                                               </td>
                                        </tr>
                                        <?php endfor; endif; ?>
                                        
                                         <tr>
            <td colspan="8"><form name="frm_pagi" action="#" method="post">
                <input type="hidden"  name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" />
                <?php echo $this->_tpl_vars['show_pagi']; ?>

              </form></td>
          </tr>
          <?php else: ?>
          <tr>
            <td colspan="8"><span style="color:red;">Record Not Found</span></td>
          </tr>
          <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>
