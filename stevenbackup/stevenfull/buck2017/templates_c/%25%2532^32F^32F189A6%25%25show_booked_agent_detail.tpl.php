<?php /* Smarty version 2.6.19, created on 2017-07-10 13:40:11
         compiled from show_booked_agent_detail.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'show_booked_agent_detail.tpl', 53, false),)), $this); ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Job Detail </h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Job Detail </a></li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
 <div class="white-bg">

        <!-- career -->
    <div class="container career-inner">
        <div class="row">
            <div class="col-md-12 career-head">
               

            </div>
        </div>
        <hr>
       
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="candidate wow fadeInLeft">
                    <h1>Transporter Detail</h1>
                    <p class="align-left"></p>
                    <h4></h4>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-angle-right pr-10"></i>Transporter Name : <?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][0]['user_id']); ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Transporter State : <?php echo $this->_tpl_vars['search'][0]['form_state']; ?>
 </li>
                        <li><i class="fa fa-angle-right pr-10"></i>Transporter City : <?php echo $this->_tpl_vars['search'][0]['form_city']; ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>location : <?php echo $this->_tpl_vars['search'][0]['sender_loc']; ?>
</li>
                       
                    </ul>
                   
                </div>
                <hr>
                <div class="candidate wow fadeInLeft">
                    <h1>Status</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                         <li><i class="fa fa-angle-right pr-10"></i>Start Date : <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['pickup_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</li>
                       
                       
                    </ul>
                    
                </div>
                <hr>
            </div>
            <div class="col-md-6">
                <div class="candidate wow fadeInRight">
                    <h1>Route Detail</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                         <li><i class="fa fa-angle-right pr-10"></i>Form : <?php echo $this->_tpl_vars['search'][0]['reciver_name']; ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>To : <?php echo $this->_tpl_vars['search'][0]['to_state']; ?>
 </li>
                      
                    </ul>
                  
                </div>
                <hr>
              
        </div>
        
    <!-- career -->
       </div>
    </div>
  