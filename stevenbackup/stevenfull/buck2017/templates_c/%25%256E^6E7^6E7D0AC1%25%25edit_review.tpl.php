<?php /* Smarty version 2.6.19, created on 2017-04-25 13:26:14
         compiled from admin/edit_review.tpl */ ?>

<?php if ($this->_tpl_vars['type'] == 'C'): ?>

<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2></h2>
      </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading"><b> <?php if ($this->_tpl_vars['edit_id'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?> Customer</b>
           
       
         </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
                
                <!--===============customer form======================-->
                <form role="form" name="add_new_customer" id="add_new_customer" action="" method="post">
                  <input type="text" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id']; ?>
" />
                  <input type="text" name="type" id="type"  value="<?php echo $this->_tpl_vars['type']; ?>
" />
                     <h4><?php echo $this->_tpl_vars['show_message']; ?>
</h4> 
              
                  <div class="form-group">
                    <label>Customer/Transporter Name</label>
                     <?php echo $this->_tpl_vars['functions']->get_customer_detail($this->_tpl_vars['usersdata'][0]['user_id']); ?>

                    <input class="form-control" type="text" name="customer_name" readonly="readonly" id="user_name" value="<?php echo $this->_tpl_vars['customer_info'][0]['name']; ?>
" placeholder="Enter Customer Name " />
                   </div>
                  <div class="form-group">
                    <label>Communication Rating</label>
                    <input class="form-control" type="text" name="com_rating" id="Communication" value="<?php echo $this->_tpl_vars['usersdata'][0]['com_rating']; ?>
" placeholder="Enter communication rating " />
                    </div>
                  <div class="form-group">
                    <label>Punctuality Rating</label>
                    <input class="form-control" type="text" name="pun_rating" id="punctuality" value="<?php echo $this->_tpl_vars['usersdata'][0]['pun_rating']; ?>
" placeholder="Enter punctuality rating " />
                   </div>
                  
                  <!--==========password=========-->
                  
                  <div class="form-group">
                    <label>Overall rating</label>
                    <input class="form-control" type="text" name="over_rating" id="rating" value="<?php echo $this->_tpl_vars['usersdata'][0]['rating']; ?>
" placeholder="Enter over all rating " />
                    </div>
                 <!-- <div class="form-group">
                    <label>Confirm Password</label>
                    <input class="form-control" type="password" name="confirm_password" id="alert_confirm_password" value="" placeholder="Enter Confirm Password " />
                    </div>-->
                  
                  <!--==========password finish====-->
                  
                  <div class="form-group">
                    <label>Comments</label>
                       <input class="form-control" type="text" name="comment" id="Comment" value="<?php echo $this->_tpl_vars['usersdata'][0]['review']; ?>
" placeholder="Enter comment " />
                  </div>
                  <div class="form-group">
                    
                
                
                  <button type="submit" class="btn btn-primary" name="user_btn" value="add_new_customer" > <?php if ($this->_tpl_vars['edit_id'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?> Customer</button>
                </form>
                <!--==================================================--> 
                
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>

<?php endif; ?>
<?php if ($this->_tpl_vars['type'] == 'T'): ?>
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2><?php if ($this->_tpl_vars['edit_id'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?> Transporter</h2>
      </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading"><b> <?php if ($this->_tpl_vars['edit_id'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?> Customer/Transporter</b>
       
         </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
                
                <!--===============customer form======================-->
                <form role="form" name="add_new_customer" id="add_new_customer" action="" method="post">
                  <input type="text" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id']; ?>
" />
                  <input type="text" name="type" id="type" value="<?php echo $this->_tpl_vars['type']; ?>
" />
                     <h4><?php echo $this->_tpl_vars['show_message']; ?>
</h4> 
              
                  <div class="form-group">
                    <label>Customer/Transporter Name</label>
                    <?php echo $this->_tpl_vars['functions']->get_Transpoter_detail($this->_tpl_vars['userdata'][0]['transpoter_id']); ?>

                    <input class="form-control" type="text" name="transporter_name" readonly="readonly" id="user_name" value="<?php echo $this->_tpl_vars['Transpoter_info'][0]['name']; ?>
" placeholder="Enter Customer Name " />
                   </div>
                  <div class="form-group">
                    <label>Communication Rating</label>
                    <input class="form-control" type="text" name="cum_rating" id="email" value="<?php echo $this->_tpl_vars['userdata'][0]['com_rating']; ?>
" placeholder="Enter Communication Rating " />
                    </div>
                  <div class="form-group">
                    <label>Service Rating</label>
                    <input class="form-control" type="text" name="ser_rating" id="mobile_no" value="<?php echo $this->_tpl_vars['userdata'][0]['ser_rating']; ?>
" placeholder="Enter Serice Rating" />
                   </div>
                  
                  <!--==========password=========-->
                  
                  <div class="form-group">
                    <label>Punctuality Rating</label>
                    <input class="form-control" type="text" name="pun_rating" id="password" value="<?php echo $this->_tpl_vars['userdata'][0]['pun_rating']; ?>
" placeholder="Enter Punctuality Rating " />
                    </div>
                    <div class="form-group">
                    <label>Care of Goods Rating</label>
                    <input class="form-control" type="text" name="car_rating" id="password" value="<?php echo $this->_tpl_vars['userdata'][0]['care_rating']; ?>
" placeholder="Enter Care of Goods Rating " />
                    </div>
                    <div class="form-group">
                    <label>Overall Rating</label>
                    <input class="form-control" type="text" name="rating" id="password" value="<?php echo $this->_tpl_vars['userdata'][0]['rating']; ?>
" placeholder="Enter Overall Rating " />
                    </div>
                    <div class="form-group">
                    <label>Date</label>
                    <input class="form-control" type="text" name="date" id="password" value="<?php echo $this->_tpl_vars['userdata'][0]['date']; ?>
" placeholder="Enter date " />
                    </div>
                 <!-- <div class="form-group">
                    <label>Confirm Password</label>
                    <input class="form-control" type="password" name="confirm_password" id="alert_confirm_password" value="" placeholder="Enter Confirm Password " />
                    </div>-->
                  
                  <!--==========password finish====-->
                  
                  <div class="form-group">
                    <label>Comments</label>
                     <input class="form-control" type="text" name="comment" id="password" value="<?php echo $this->_tpl_vars['userdata'][0]['review']; ?>
" placeholder="Enter comments" />
                  </div>
                  <div class="form-group">
                    
                
                
                  <button type="submit" class="btn btn-primary" name="user_btn1" value="add_new_customer" > <?php if ($this->_tpl_vars['edit_id'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?> Transporter</button>
                </form>
                <!--==================================================--> 
                
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>
<?php endif; ?>