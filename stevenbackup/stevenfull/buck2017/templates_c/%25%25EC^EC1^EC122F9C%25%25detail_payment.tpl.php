<?php /* Smarty version 2.6.19, created on 2017-07-11 07:38:35
         compiled from admin/detail_payment.tpl */ ?>
<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>View Payment Detail</h2> 
                     
                    </div>
                </div>
              
                 <hr />  <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=show_payment" style="float:right; margin-bottom:8px;">  <button class="btn btn-default btn" onclick="goBack()">Go Back</button></a>  
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Customer <?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['usersdata'][0]['user_id']); ?>
 Payment Detail
                           
                            
                        </div>
                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered ">
                                    <tbody>
                                        <tr>
                                                      
                                        	<th>User Name</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['usersdata'][0]['user_id']); ?>
</td>
                                          <th>Email Address</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_email($this->_tpl_vars['usersdata'][0]['user_id']); ?>
</td>
                                            </tr>
                                            <tr>
                                              <th>Mobile</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_mobile($this->_tpl_vars['usersdata'][0]['user_id']); ?>
</td>
                                            <th>Entery Date</th>
                                            <td><?php echo $this->_tpl_vars['usersdata'][0]['entrydate']; ?>
</td>
                                        </tr>
                                        <tr>
                                              <th>State</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_State($this->_tpl_vars['usersdata'][0]['user_id']); ?>
</td>
                                            <th>City</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_city($this->_tpl_vars['usersdata'][0]['user_id']); ?>
</td>
                                        </tr>
                                        <tr>
                                             <th>Payment Option</th>
                                            <td><?php echo $this->_tpl_vars['usersdata'][0]['payment_opt']; ?>
</td>
                                            <th>Amount</th>
                                            <td><?php echo $this->_tpl_vars['usersdata'][0]['amount']; ?>
</td>
                                        </tr>
                                         <tr>
                                              <th>Pay Status</th>
                                            <td><?php echo $this->_tpl_vars['usersdata'][0]['paystatus']; ?>
</td>
                                            <th></th>
                                            <td></td>
                                        </tr>
                                        
                                    </tbody>
                                    
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->
            
                <!-- /. ROW  -->
            
             
        </div>
               
    </div>