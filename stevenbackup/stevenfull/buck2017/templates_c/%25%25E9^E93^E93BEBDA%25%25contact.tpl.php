<?php /* Smarty version 2.6.19, created on 2017-06-18 13:39:39
         compiled from contact.tpl */ ?>
<?php echo '<?php'; ?>
 /* Smarty version 2.6.19, created on 2017-05-13 08:34:05
         compiled from contact.tpl */ <?php echo '?>'; ?>

 <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-sm-4">
            <h1>
              Contact
            </h1>
          </div>
          <div class="col-lg-8 col-sm-8">
            <ol class="breadcrumb pull-right">
              <li>
                <a href="#">
                  Home
                </a>
              </li>
              
              <li class="active">
                Contact
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->


    <!--container start-->
    <div class="container">

<?php echo $this->_tpl_vars['functions']->get_footer_address_detail(); ?>

      <div class="row">
        <div class="col-lg-5 col-sm-5 address">
          <section class="contact-infos">
            <h4 class="title custom-font text-black" style="text-align:left;">
              <?php echo $this->_tpl_vars['cont_info'][0]['company_name']; ?>

            </h4>
            
            <address style="text-align:left;">
             <?php echo $this->_tpl_vars['cont_info'][0]['company_address']; ?>

            </address>
          </section>
          <section class="contact-infos">
            <h4 class="title custom-font text-black" style="text-align:left;">
              business hours
            </h4>
            <p>
              Monday - Friday 08:00 to 17:00
              <br>
              Saturday - 09:00 to 14:00
              <br>
               Sunday - 10:00 to 12:00
              <br>
             Public Holidays - 10:00 to 12:00
              <br>
            </p>
          </section>

          <section class="contact-infos">
            <h4 class="title custom-font text-black" style="text-align:left;">
              TELEPHONE
            </h4>
            <p>
              <i class="icon-phone">
              </i>
              <?php echo $this->_tpl_vars['cont_info'][0]['mobile_no']; ?>

            </p>

<p>
              <i class="icon-phone">
              </i>
              <?php echo $this->_tpl_vars['cont_info'][0]['phone']; ?>

            </p>

          </section>

          <section class="google map">
      <!--google map start-->
    <div class="contact-map">
      <div id="map-canvas" style="height="42" width="42"">
      
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3580.8112732150403!2d28.13332205009413!3d-26.170275183371697!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e95121877ac0b51%3A0xd525305e7f99f86d!2sLoadMatch+(Pty)+Ltd!5e0!3m2!1sen!2sza!4v1497711050754" width="450" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
    <br />
    <!--google map end-->

    </section>

        </div>
        <div class="col-lg-7 col-sm-7 address">
          <h4>
            Contact us for information, or if you have any queries:
          </h4>
          <label style="color:red;"><?php echo $this->_tpl_vars['err']; ?>
</label>
          <div class="contact-form" style="text-align:left;">
            <form role="form" name="contect" action="" method="post">
              <div class="form-group" >
                <label for="name" >
                  Name
                </label>
                <input type="text" placeholder="" name="name" id="name" class="form-control">
              </div>
              <div class="form-group">
                <label for="email">
                  Email
                </label>
                <input type="text" placeholder="" name="email" id="email" class="form-control">
              </div>
              <div class="form-group">
                <label for="phone">
                  Phone
                </label>
                <input type="text" id="phone" name="phone" class="form-control">
              </div>
              <div class="form-group">
                <label for="phone">
                  Subject
                </label>
                <input type="text" id="Subject" name="Subject" class="form-control">
              </div>
              <div class="form-group">
                <label for="phone">
                  Message
                </label>
                <textarea name="massage" id="massage" rows="5" class="form-control"></textarea>
              </div>
               <div class="form-group">
                <label for="phone">
                 Captcha
                </label>
                <input class="form-control" type="text" name="norobot" id="norobot" /> <br />
		        <img src="captcha.php" />
               
              </div>
              
              
              
              <button class="btn btn-info" type="submit" name="submit" onclick="return Valid_contect_us(document.contect);">
                Submit
              </button>
            </form>

          </div>
        </div>
      </div>

    </div>
    <!--container end-->
    
    <!-- St3v0_  this is a form for pers file file called pers_file.php-->


    <!--google map start-->
    <div class="contact-map">
      <div id="map-canvas" style="width: 100%; height: 400px">


   <!-- <div class="container">
      <div class="row">
        <div class='col-md-offset-2 col-md-8 text-center'>
          <h2>
            Responsive Quote Carousel BS3
          </h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-offset-2 col-md-8 mar-b-30">
          <div class="carousel slide" data-ride="carousel" id="quote-carousel">
            <!-- Bottom Carousel Indicators -->
           <!-- <ol class="carousel-indicators">
              <li data-target="#quote-carousel" data-slide-to="0" class="active">
              </li>
              <li data-target="#quote-carousel" data-slide-to="1">
              </li>
              <li data-target="#quote-carousel" data-slide-to="2">
              </li>
            </ol>-->

            <!-- Carousel Slides / Quotes -->
          <!--  <div class="carousel-inner">-->

              <!-- Quote 1 -->
             <!-- <div class="item active">
                <blockquote>
                  <div class="row">
                    <div class="col-sm-3 text-center">
                      <img class="img-circle" src="img/person_1.png" style="width: 100px;height:100px;" alt="">
                    </div>
                    <div class="col-sm-9">
                      <p>
                        Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!
                      </p>
                      <small>
                        Someone famous
                      </small>
                    </div>
                  </div>
                </blockquote>
              </div>-->
              <!-- Quote 2 -->
             <!-- <div class="item">
                <blockquote>
                  <div class="row">
                    <div class="col-sm-3 text-center">
                      <img class="img-circle" src="img/person_2.png" style="width: 100px;height:100px;" alt="">
                    </div>
                    <div class="col-sm-9">
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.
                      </p>
                      <small>
                        Someone famous
                      </small>
                    </div>
                  </div>
                </blockquote>
              </div>-->
              <!-- Quote 3 -->
            <!--  <div class="item">
                <blockquote>
                  <div class="row">
                    <div class="col-sm-3 text-center">
                      <img class="img-circle" src="img/person_3.png" style="width: 100px;height:100px;" alt="">
                    </div>
                    <div class="col-sm-9">
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.
                      </p>
                      <small>
                        Someone famous
                      </small>
                    </div>
                  </div>
                </blockquote>
              </div>
            </div>
-->

          </div>

        </div>
      </div>
    </div>