<?php /* Smarty version 2.6.19, created on 2017-07-17 15:26:53
         compiled from header.tpl */ ?>
<div class="header-top">
	<div class="container">
    	<div class="top-bar">
        	
        </div>
    </div>
</div>
<div class="inner-header">
    <div class="container-fluid">
    <div class="row set_head_row">
        <nav class="navbar navbar-default top-nav">
                <div class="container-fluid-1">
                  <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                   <!-- <div class="logo">
                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/index.html"><img class="img-responsive" src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/<?php echo $this->_tpl_vars['logo']; ?>
" alt="" /></a>
                    </div>-->
                  </div>
                  <div class="navbar-collapse collapse" id="navbar">
                     <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/index.html"><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/<?php echo $this->_tpl_vars['logo']; ?>
" alt="" /></a>
                    <ul class="nav navbar-nav navbar-right new-right">
                        
                     <li><span class="sr-only">(current)</span></li>
                     <!-- <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/index.html">Home<span class="sr-only">(current)</span></a></li>-->
                      <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/about.html"> About Us</a></li>
                      <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/search_shipping.html"> Subscriptions</a></li>
                        <?php if ($_SESSION['user_id'] == ''): ?>
                         <!--<?php echo $this->_tpl_vars['page']; ?>
//<?php echo $this->_tpl_vars['action']; ?>
//-->
                      <li  class="dropdown"><a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle"  href="#/">Login <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                      
                          <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/login.html">Login</a></li>
                          <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/registration.html">Registration</a></li>
                        </ul>
                      </li>
                      <?php else: ?>
                      <!--<?php echo $this->_tpl_vars['page']; ?>
//<?php echo $this->_tpl_vars['action']; ?>
-->
                      <li  class="dropdown"><a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle"  href="#/">My Account <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="profile.html">Profile</a></li>
                          <li><a href="inbox.html">Inbox</a></li>
                          <?php if ($_SESSION['user_type'] == 'C'): ?>
                            <li><a href="my_post_job.html">My Shipments</a></li><?php endif; ?>
                          <?php if ($_SESSION['user_type'] == 'T'): ?> <li><a href="my_post_job.html">Active Shipment</a>  <?php endif; ?></li>
                          <li><a href="booked.html">Booked Shipment</a></li>
                          <li class="divider" role="separator"></li>
                          <li><a href="booked_complete.html">Completed Shipment</a></li>
                          <li><a href="canceled.html">Canceled Shipment</a></li>
                          <li><a href="logout.html">Logout</a></li>
                        </ul>

                      </li>
                      <?php endif; ?>
                      <li>
                      <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/post_shipping.html">New Shipment</a>
                      </li>
                      <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/search_shipping.html"> Available Loads</a></li>
                    
                      <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/search_shipping.html"> Classifieds</a></li>
                      <li  class="dropdown"><a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle"  href="#/">Services <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/search_shipping.html"> Loadmatch Partners</a></li>
                        <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/search_shipping.html"> Insurance</a></li>
                        <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/search_shipping.html"> Group Benefits</a></li>
                        <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/search_shipping.html"> Jobs</a></li>
                       </ul>
                       
                      <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/search_shipping.html"> Tools</a></li>
                      <li><a href="contact.html">Contact </a></li>
                      <!--<?php if ($_SESSION['user_type'] != 'T'): ?>
                      <?php endif; ?>-->
                      <?php if ($_SESSION['user_id'] != ''): ?>
                  <li><a href="#"><?php if ($_SESSION['user_type'] == 'C'): ?> <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/user.png" width="20" height="20" /> <?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][0]['user_id']); ?>
 <?php endif; ?> <?php if ($_SESSION['user_type'] == 'T'): ?><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/agent.png" width="20" height="20" /> <?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][0]['agent_id']); ?>
 <?php endif; ?></a></li>
              <?php endif; ?>
                    </ul>
                  </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
              </nav>
              </div>
    </div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV7Xhe7eWHw-z-e2zJcnSc4QiCcfYabHA&libraries=places"></script>
<!-- Navbar End -->

 