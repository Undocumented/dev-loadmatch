<?php /* Smarty version 2.6.19, created on 2017-06-19 12:10:19
         compiled from view_agent_review.tpl */ ?>

  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Transporter Review</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Transporter Review</a></li>
                         <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
 <div class="component-bg">
    <div class="container">
            <!-- Forms
================================================== -->
    <div class="bs-docs-section mar-b-30">
      <h1 id="forms" class="page-header">Transporter Detail</h1>
      <div class="col-lg-8" style="margin-left:15%; width:70%;">
     <div class="row user_proflie1">
              <div class="col-lg-2  bg-navy1" align="center">
      <div class="form-group"> <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/profile_pic/<?php echo $this->_tpl_vars['agent_detail_search'][0]['image']; ?>
" width="100px" height="100px" style="margin-top:15px;" /> </div>
       <div class="form-group"> <label for="inputEmail3" class="col-sm-30 control-label" ><?php echo $this->_tpl_vars['agent_detail_search'][0]['name']; ?>
</label></div>
      </div>
    
         <div class="col-lg-6" align="center">
       <h4 align="left">  Total Shipment:<?php echo $this->_tpl_vars['functions']->get_Agent_totel_job($this->_tpl_vars['agent_detail_search'][0]['id']); ?>
 </h4>
       <h4 align="left"> Completed: <?php echo $this->_tpl_vars['functions']->get_Agent_totel_job_complet($this->_tpl_vars['agent_detail_search'][0]['id']); ?>
</h4> 
      <h4 align="left"> User Rating:<?php echo $this->_tpl_vars['functions']->get_Agent_rating_average($this->_tpl_vars['agent_detail_search'][0]['id']); ?>
</h4>
     </div>
      <div class="col-lg-4" align="left">
      <label for="inputEmail3" class="col-sm-30 control-label" >Available Vehicle</label>
      <?php echo $this->_tpl_vars['agent_detail_search'][0]['agent_id']; ?>

        <?php echo $this->_tpl_vars['functions']->get_agent_vehicle_front($this->_tpl_vars['agent_detail_search'][0]['id']); ?>
 
        
      </div>
    </div> 
     </div>
    
       
        
        <table align="center" style="border:2px solid #999; padding:10px;" width="70%">
       
         <?php if ($this->_tpl_vars['search']): ?>
                    <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <tr style="border:2px solid #999;">
        <td align="left" style="color:#00F; background:#999;" >Posted By Customer:<?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][$this->_sections['data']['index']]['user_id']); ?>
</td><td>Date<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['date']; ?>
</td>
        </tr>
        <td colspan="2" align="left" style=" padding:15px;"><?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['review']; ?>
</td>
        </tr>
        <tr>
        <td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <?php endfor; endif; ?>
        <?php else: ?>
        <tr><td colspan="2" align="center">Review Not Found</td></tr>
        <?php endif; ?>
        </hr>
        
        </table>
    <br />
        <div class="col-lg-12 ">
    <form action="" method="post">
                    <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>

                    </form>
      </div><!-- /.bs-example -->
    
      
         
       
    
    </div>
    </div>
</div>
    <!--container end-->