<?php /* Smarty version 2.6.19, created on 2017-07-16 14:16:10
         compiled from registration.tpl */ ?>
 
<!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Registration</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Pages</a></li>
                        <li class="active">Registration</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="registration-bg">
        <div class="container">

            <form class="form-signin wow fadeInUp" action="" method="post" name="reg">
                <h2 class="form-signin-heading">Register now</h2>
                <div class="login-wrap">
                    <p>Enter personal details</p>
                    <input type="text" class="form-control" name="user_name" id="user_name" placeholder="Full Name" autofocus="">
                    <input type="text" class="form-control"  name="address" id="address" placeholder="Address" autofocus="">
                    <input type="text" class="form-control" name="email" id="email" placeholder="UserID" autofocus="">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" >
                     <input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Mobile" autofocus="">
                     
                     <div class="controls">                  
                    <select name="state" id="state" class="form-control input-sm border-radius" style="font-size:13px;"  onchange="get_state_city(this.value);">
        
        <option value="" selected="selected" >Please Select State</option>
      
						<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
  <option value="<?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['state'] == $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
 </option>
   <?php endfor; endif; ?>
               
                 
</select></br>
</div>
 <span id="city_change">
                     <select  name="form_change" id="form_change" class="form-control input-sm border-radius" style="font-size:13px;">
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="" selected="selected" >Please Select City</option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select></span></br></br>
                     <p> Select User Type</p>
                     <div class="radios">
                        <label class="label_radio col-lg-6 col-sm-6" for="radio">
                            <input name="user_type" id="radio-01" value="C" type="radio" <?php if ($this->_tpl_vars['type'] == '1'): ?> checked="" <?php endif; ?>> Customer
                        </label>
                        <label class="label_radio col-lg-6 col-sm-6" for="radio">
                            <input name="user_type" id="radio-02" value="T" type="radio" <?php if ($this->_tpl_vars['type'] != '1'): ?> checked="" <?php endif; ?>> Transporter
                        </label>
                         <label class="label_radio col-lg-6 col-sm-6" for="radio">
                            <input name="user_type" id="radio-03" value="B" type="radio" <?php if ($this->_tpl_vars['type'] == '1'): ?> checked="" <?php endif; ?>> Broker
                        </label>
                        <label class="label_radio col-lg-6 col-sm-6" for="radio">
                            <input name="user_type" id="radio-04" value="A" type="radio" <?php if ($this->_tpl_vars['type'] != '1'): ?> checked="" <?php endif; ?>> Admin
                        </label>
                    </div>
                    
                    <label class="checkbox">
                        <input type="checkbox" value="1" id="checkbox1" name="checkbox1" checked="checked"> I agree to the <a href="terms.html" target="_new">Terms of Service</a> and <a href="privacy.html" target="_new">Privacy Policy</a>
                    </label>
                    <button class="btn btn-lg btn-login btn-block" type="submit" name="submit"  onclick="return Valid_add_customerfront(document.reg);">Submit</button>

                    <div class="registration">
                        Already Registered ?
                        <a class="" href="login.html">
                            Login
                        </a>
                    </div>
                </div>
            </form>

        </div>
     </div>