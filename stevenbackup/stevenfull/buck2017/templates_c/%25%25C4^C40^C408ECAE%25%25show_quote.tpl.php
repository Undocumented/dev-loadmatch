<?php /* Smarty version 2.6.19, created on 2017-04-26 17:51:58
         compiled from admin/show_quote.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'admin/show_quote.tpl', 106, false),)), $this); ?>
<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Placed Quote</h2>
                     <?php if ($this->_tpl_vars['show_message'] != ''): ?>
                       <h5 class=" btn-success btn-lg" align="center"><?php echo $this->_tpl_vars['show_message']; ?>
</h5>  
                       <?php endif; ?> 
                      
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Placed Quote
                            <?php if ($this->_tpl_vars['show_message'] != ''): ?>
                          
                            <?php endif; ?>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                            <?php if ($this->_tpl_vars['inactive'] == ''): ?>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="8%">Sr. No.</th>
                                            <th>Order ID</th>
                                            <th>Customer Name</th>
                                            <th>Email</th>
                                            <th>New Quote</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($this->_tpl_vars['usersdata']): ?>
                                        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['usersdata']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                                        <tr class="gradeU">
                                            <td align="center"><?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>
</td>
                                            <td><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=show_quote_agent&order_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['order_id']; ?>
"><?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['order_id']; ?>
</a></td>
                                            <td><?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['user_id']); ?>
</td>
                                            <td><?php echo $this->_tpl_vars['functions']->get_email($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['user_id']); ?>
</td>
                                             <td>New(<?php echo $this->_tpl_vars['functions']->new_quote_count($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['order_id']); ?>
) Total(<?php echo $this->_tpl_vars['functions']->totel_quote_count($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['order_id']); ?>
)</td>
                                            
                                               <td>
<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=show_quote_agent&order_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['order_id']; ?>
" title="view"><span class="btn btn-success btn-sm">view</span></a>
                                               
                                                                                              </td>
                                        </tr>
                                        <?php endfor; endif; ?>
                                        
                                         <tr>
            <td colspan="9"><form name="frm_pagi" action="" method="post">
         <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" />
            <?php echo $this->_tpl_vars['show_pagi']; ?>

         </form>
              </td>
          </tr>
          <?php else: ?>
          <tr>
            <td colspan="9"><span style="color:red;">Record Not Found</span></td>
          </tr>
          <?php endif; ?>
                                    </tbody>
                                </table>
                                
                                
                                <?php else: ?>
                                
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="8%">Sr. No.</th>
                                            <th>Order ID</th>
                                            <th>Transporter Name</th>
                                            <th>Email</th>
                                            <th>Price</th>
                                            <th>Payment Methods</th>
                                            <th>Payment Accepted</th>
                                            <th>Entry Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($this->_tpl_vars['usersdata']): ?>
                                        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['usersdata']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                                        <tr class="gradeU">
                                            <td align="center"><?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>
</td>
                                            <td><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=detail_quote&view_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['id']; ?>
" title="view"><?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['order_id']; ?>
</a></td>
                                             <td><?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['agent_id']); ?>
</td>
                                              <td><?php echo $this->_tpl_vars['functions']->get_email($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['agent_id']); ?>
</td>
                                               <td><?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['starting_price']; ?>
</td>
                                               <td><?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['payment_methods']; ?>
</td>
                                               <td><?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['payment_accepted']; ?>
</td>
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['entery_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                            
                                               <td>
<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=detail_quote&view_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['id']; ?>
" title="view"><span class="btn btn-success btn-sm">view &nbsp;&nbsp;&nbsp;</span></a>
                                               
                                               <?php if ($this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['status'] == 'Y'): ?>
                                               	<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=show_quote_agent&status_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['id']; ?>
&active=N&order_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['order_id']; ?>
" title="active" onclick="return confirm('Do You Want to Change it to Inactive?');"><span class="btn btn-success btn-sm" style="background-color:#F90;">Active&nbsp;</span></a>
                                                <?php else: ?>
                                                	<a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=show_quote_agent&status_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['id']; ?>
&inactive=Y&order_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['order_id']; ?>
" onclick="return confirm('Do You Want to Change it to Active?');" title="inactive"><span class="btn btn-success btn-sm" style="background-color:#069;">Inactive</span></a>
                                                    <?php endif; ?>
                                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=show_quote_agent&del_id=<?php echo $this->_tpl_vars['usersdata'][$this->_sections['data']['index']]['id']; ?>
" onclick="return confirm('Do You  want to delete ?');" title="Delete"><span class="btn btn-success btn-sm" style="background-color:#F00;">Delete</span></a>
                                               </td>
                                        </tr>
                                        <?php endfor; endif; ?>
                                        
                                         <tr>
            <td colspan="9"><form name="frm_pagi" action="" method="post">
         <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" />
            <?php echo $this->_tpl_vars['show_pagi']; ?>

         </form>
              </td>
          </tr>
          <?php else: ?>
          <tr>
            <td colspan="9"><span style="color:red;">Record Not Found</span></td>
          </tr>
          <?php endif; ?>
                                    </tbody>
                                </table>
                                <?php endif; ?>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>
