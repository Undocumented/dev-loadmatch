  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Edit Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Edit Shipment</a></li>
                        <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="component-bg">
        <div class="container">
            <!-- Forms
================================================== -->
<div class="bs-docs-section mar-b-30">
  <h1 id="forms" class="page-header">Edit Shipment</h1>
 
    <form class="form-horizontal" name="post_shipment"  method="post" action="#" role="form" enctype="multipart/form-data">
      <h3 align="left">Sender Details</h3>
     <hr>
     <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Order Id</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" readonly="readonly" value="{$search[0].order_id}" id="Order" name="Order" >
         
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Title</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="{$search[0].title}" id="Title" name="Title" placeholder="Enter Title Here">
          
        </div>
        </div>
     <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Full Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" readonly="readonly" value="{$functions->get_name($search[0].user_id)}" id="s_user_name" name="s_user_name" placeholder="User Name">
          <input type="hidden"  name="user_id" value="{$smarty.session.user_id}" />
          
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" readonly="readonly" value="{$functions->get_email($search[0].user_id)}" id="s_user_email" name="s_user_email" placeholder="Email">
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
        <div class="col-sm-10">
          <input type="text" readonly="readonly" class="form-control" id="mobile"  name="mobile" value="{$functions->get_mobile($search[0].user_id)}" placeholder="Mobile no">
        </div>
      </div>
   
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Collection Province</label>
        <div class="col-sm-10">
         
         <select name="form_state" id="form_state" class="form-control border-radius"  onchange="get_state_city(this.value);">
        
         <option value="please Select" selected="selected" >Please Select Collection Province</option>
      
						{section name=data loop=$categories}
                        
  <option value="{$categories[data].state}" {if $search[0].form_state==$categories[data].state} selected="selected" {/if} >{$categories[data].state} </option>
   {/section}
               
                 
</select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Collection City</label>
        <div class="col-sm-10">
         
          <span id="city_change">
                     <select class="form-control"  name="form_change" id="form_change" >
                     <option value="please Select" selected="selected" >Please Select  City</option>
        {section name=data loop=$city_cat}
        
        <option value="{$city_cat[data].city}" {if $search[0].form_city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
        {/section}
   
</select>
     
      </div>
  </div>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Location/Address</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="loc_sen" id="loc_sen" rows="3">{$search[0].sender_loc}</textarea>
     </div>
        
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Pickup Dates</label>
        <div class="col-xs-2">
        <INPUT NAME="pickup_start_date" id="pickup_start_date" TYPE="date"   value="{$search[0].pickup_start_date}" />
       <label for="inputEmail3" class="col-sm-2 control-label">Between</label>
         
        <INPUT NAME="pickup_end_date" id="pickup_end_date" TYPE="date"   value="{$search[0].pickup_end_date}" />
        
       
        </div>
      
  </div>
  <hr />
   <h3 align="left">Receiver Details</h3>
    <hr />
   <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Receiver Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="{$search[0].receiver_name}" id="r_user_name" name="r_user_name" placeholder="User Name">
          
          
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label"> Receiver Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" value="{$search[0].receiver_email}" id="r_user_email" name="r_user_email" placeholder="Email">
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Receiver Mobile</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="r_mobile" value="{$search[0].receiver_mobile}"  name="r_mobile" placeholder="Mobile no">
        </div>
      </div>
   
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery Province</label>
        <div class="col-sm-10">
         
           <select name="to_state" id="to_state" class="form-control border-radius"  onchange="get_state_city_to(this.value);">
        
         <option value="please Select" selected="selected" >please Select</option>
      
						{section name=data loop=$categories}
                        
  <option value="{$categories[data].state}" {if $search[0].to_state==$categories[data].state} selected="selected" {/if} >{$categories[data].state} </option>
   {/section}
               
                 
</select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery City</label>
        <div class="col-sm-10">
         
          <span id="city_change1">
                     <select class="form-control"  name="to_city" id="to_city" >
                     
        <option value="please Select" selected="selected" >please Select</option>
       
        
        {section name=data loop=$city_cat1}
        <option value="{$city_cat1[data].city}" {if $search[0].to_city==$city_cat1[data].city} selected="selected" {/if} >{$city_cat1[data].city} </option>
        {/section}
     
</select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Location/Address</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="loc_rec" id="loc_rec" rows="3">{$search[0].receiver_loc}</textarea>
     </div>
     </div>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery Dates</label>
        <div class="col-xs-2">
           <INPUT NAME="delv_start_date" id="delv_start_date" TYPE="date"   value="{$search[0].delv_start_date}" />
         <label for="inputEmail3" class="col-sm-2 control-label">Between</label>
            <INPUT NAME="delv_end_date" id="delv_end_date" TYPE="date"   value="{$search[0].delv_end_date}" />
        </div>
      
  </div>
  
       <hr />
        <h3 align="left">Shipment Details</h3>
        <hr />
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Load Industry</label>
        <div class="col-sm-10">
         <select name="category"  id="category" class="form-control border-radius" onchange="get_sub_cat(this.value);" >
                    <option value="please Select" selected="selected" >Please Select Load Industry</option>
      				{$search[0].category}
                    {section name=data loop=$useredit1}
                       
                    <option value="{$useredit1[data].id}" {if $search[0].category==$useredit1[data].id} selected="selected" {/if}>{$useredit1[data].category} </option>
                  {/section}
                   </select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Load Type</label>
        <div class="col-sm-10">
         
         <span id="sub_cat">
                     <select class="form-control"  name="sub_cat_id" id="sub_cat_id"  >
                    
        <option value="please Select" selected="selected" >Please Select Load Type</option>
           
        {section name=data loop=$useredit11}
        <option value="{$useredit11[data].id}" {if $search[0].sub_category==$useredit11[data].id} selected="selected" {/if} >{$useredit11[data].category} </option>
        {/section}
     
</select>
      
      </div>
  </div>
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Transport Type</label>
        <div class="col-sm-10">
         
         <span id="sub_next_cat">
                     <select class="form-control"  name="sub_cat_id" id="sub_next_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Transport Type</option>
           
        {section name=data1 loop=$sub_cat_edit}
        <option value="{$sub_cat_edit[data1].id}" {if $search[0].parent_sub_id==$sub_cat_edit[data1].id} selected="selected" {/if} >{$sub_cat_edit[data1].category} </option>
        {/section}
     
</select>
      
      </div>
  </div>
 
 
 
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Body & Bulk Type</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Body & Bulk Type</option>
           
        {section name=last_data loop=$last_cat_edit}
        <option value="{$last_cat_edit[last_data].id}" {if $search[0].last_cat==$last_cat_edit[last_data].id} selected="selected" {/if} >{$last_cat_edit[last_data].category} </option>
        {/section}
     
</select>
      
      </div>
  </div>
  
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Size Classification</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Size Classification</option>
           
        {section name=fifth_data loop=$fifth_cat_edit}
        <option value="{$fifth_cat_edit[fifth_data].id}" {if $search[0].fifth_cat==$fifth_cat_edit[fifth_data].id} selected="selected" {/if} >{$fifth_cat_edit[fifth_data].category} </option>
        {/section}
     
</select>
      
      </div>
  </div>
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Payload KG</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Payload KG</option>
           
        {section name=sixth_data loop=$sixth_cat_edit}
        <option value="{$sixth_cat_edit[sixth_data].id}" {if $search[0].sixth_cat==$sixth_cat_edit[sixth_data].id} selected="selected" {/if} >{$sixth_cat_edit[sixth_data].category} </option>
        {/section}
     
</select>
      
      </div>
  </div>
  
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Length Meters</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Length Meters</option>
           
        {section name=seventh_data loop=$seventh_cat_edit}
        <option value="{$seventh_cat_edit[seventh_data].id}" {if $search[0].seventh_cat==$seventh_cat_edit[seventh_data].id} selected="selected" {/if} >{$seventh_cat_edit[seventh_data].category} </option>
        {/section}
     
</select>
      
      </div>
  </div>
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Width Meters</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Width Meters</option>
           
        {section name=eigth_data loop=$eight_cat_edit}
        <option value="{$eight_cat_edit[eigth_data].id}" {if $search[0].eight_cat==$eight_cat_edit[eigth_data].id} selected="selected" {/if} >{$eight_cat_edit[eigth_data].category} </option>
        {/section}
     
</select>
      
      </div>
  </div>
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Preferred Distance</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select Preferred Distance</option>
           
        {section name=ninth_data loop=$ninth_cat_edit}
        <option value="{$ninth_cat_edit[ninth_data].id}" {if $search[0].ninth_cat==$ninth_cat_edit[ninth_data].id} selected="selected" {/if} >{$ninth_cat_edit[ninth_data].category} </option>
        {/section}
     
</select>
      
      </div>
  </div>
  
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Other</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sub_cat_id" id="last_cat" >
                    
        <option value="please Select" selected="selected" >Please Select</option>
           
        {section name=tenth_data loop=$tenth_cat_edit}
        <option value="{$tenth_cat_edit[tenth_data].id}" {if $search[0].tenth_cat==$tenth_cat_edit[tenth_data].id} selected="selected" {/if} >{$tenth_cat_edit[tenth_data].category} </option>
        {/section}
     
</select>
      
      </div>
  </div>
 
  
 <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Dimensions</label>
        <div class="col-sm-10">
         <select name="Dimensions"  id="Dimensions" class="form-control border-radius" >
          <option value="please Select" selected="selected" >please Select</option>
                    <option value="Feet" {if $search[0].dimensions=='Feet' } selected="selected" {/if} >Feet</option>
      				 <option value="Inches" {if $search[0].dimensions=='Inches'} selected="selected" {/if} >Inches</option>
                      <option value="Centimeters" {if $search[0].dimensions=='Centimeters'} selected="selected" {/if} >Centimeters</option>   
                     <option value="Meters" {if $search[0].dimensions=='Meters'} selected="selected" {/if} >Meters</option>   
                 
                   </select>
      
      </div>
  </div>
   <table >
  <tr><td width="20%;"></td><td>
  <table>
  <tr><td>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Width</label>
       <div class="col-xs-2">
         <input type="text" name="Width" id="Width" value="{$search[0].Width}" class="form-control" style="min-width:70px;" placeholder="Width">
        </div>
              
  </div>
  </td>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Height</label>
       <div class="col-xs-2">
          <input type="text" name="Height" id="Height" value="{$search[0].Height}" class="form-control" style="min-width:70px;" placeholder="Height">
        </div>
        
  </div>
  </td>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Length</label>
       <div class="col-xs-2">
         <input type="text" name="Length" id="Length" value="{$search[0].Length}" class="form-control" style="min-width:70px;" placeholder="Length">
        </div>
        
  </div>
  </td>
  </tr>
  <tr>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Weight</label>
       <div class="col-xs-2">
       <input type="text" name="Weight" id="Weight" value="{$search[0].Weight}" class="form-control" style="min-width:70px;" placeholder="Weight">
        </div>
        
  </div>
  </td>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Quantity</label>
       <div class="col-xs-2">
       <input type="text" name="Qty" id="Qty" value="{$search[0].Qty}" class="form-control" style="min-width:70px;" placeholder="Quantity">
        </div>
        
  </div>
 </td>
 <td>
 <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Value</label>
       <div class="col-xs-2">
       <input type="text" name="value" id="value" value="{$search[0].value}" class="form-control" style="min-width:70px;" placeholder="">
        </div>
        
  </div>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Shipment Image</label>
      <div class="col-sm-10">
      <input type="file" name="logo" class="btn btn-default" style="max-width:240px;"/>
       <input type="hidden" name="old_main_img" value="{$search[0].img}" />
         </div>
 
  </div>
   {if $search[0].img!=''}
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label"></label>
      <div class="col-sm-10" style="text-align:left;" >
     <img src="{$site_url}/images/shipment_image/{$search[0].img}" width="100" />
     </div>
    </div>
    {/if}
 <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Description</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="description" rows="3">{$search[0].description}</textarea>
     </div>
   
        
  </div>
     
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="submit" class="btn btn-default" onclick="return valid_post(document.post_shipment);">Submit</button>
        </div>
      </div>
    </form>
  </div><!-- /.bs-example -->

  
     
   

</div>
        </div>
    </div>
    <!--container end-->