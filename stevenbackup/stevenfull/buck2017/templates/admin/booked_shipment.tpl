<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>{if $type=='B'} Undeliverd {elseif $type=='CT'} Deliverd {elseif $type=='C'} Cancel {else} Active {/if}Shipment List</h2>
                     {if $show_message!=''}
                      <h5 class=" btn-success btn-lg" align="center">{$show_message}</h5>  
                       {/if} 
                      
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <b>{if $type=='B'} Undeliverd {elseif $type=='CT'} Deliverd {elseif $type=='C'} Cancel {else} Active {/if} Shipment List</b>
                            {if $show_message!=''}
                          
                            {/if}
                        </div>
                        <div class="panel-body">
                          <div class="container">
    <div class="row">
   <form name="search" id="search" method="post" action="#">
      <div class="col-sm-2">
         <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Category</label>
        {$functions->select_category_admin_search($category12)}
      
      </div>
  </div>
 
           <div class="col-sm-2">
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Date</label>
        
         
         <input type="date" value="{$search_date}" id="search_date" name="search_date" class="form-control border-radius" onchange="document.search.submit()" />
     
  </div></div>
  
    <div class="col-sm-2">
     <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">From</label>
      
        
        {$functions->select_city_form($from12)}
      
      </div>
  </div>
  
  <div class="col-sm-2">
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">To</label>
        
         
       {$functions->select_city_to($to12)}
     
      </div>
      </div>
    {*  <div class="col-sm-2">
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <input type="text" value="" id="search_email" name="search_email" class="form-control" onblur="document.search.submit()"/>
         
            
      </div>
      </div>*}
        
      </form>
  
 </div>
  </div>

          </section>
                            <div class="table-responsive">
                            
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                             <th>Entry Date</th>
                                            <th>Customer</th>
                                            <th>Category</th>
                                           {* <th>Email Address</th>*}
                                             <th>From</th>
                                              <th>To</th>
                                            <th>Order Id</th>
                                           {if $type==''}
                                           <th>Quote</th>
                                           {/if}
                                            <th>Status</th>
                                             <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if $usersdata}
                                        {section name=data loop=$usersdata}
                                        <tr class="gradeU">
                                            <td align="center">{$smarty.section.data.index+$row_no+1}</td>
                                            <td>{$usersdata[data].entery_date|date_format:"%d- %m -%Y"}</td>
                                            <td>{$functions->get_name($usersdata[data].user_id)}</td>
                                             <td>{$functions->get_category($usersdata[data].category)}</td>
                                             {*<td>{$functions->get_email($usersdata[data].user_id)}</td>*}
                                              <td>{$usersdata[data].form_city}</td>
                                               <td>{$usersdata[data].to_city}</td>
                                            <td><a href="{$site_url}/admin.php?page=admin_home&action=detail_requirement&view_id={$usersdata[data].id}&type={$type}" title="view">{$usersdata[data].order_id}</a></td>
                                            {if $type==''}
                                             <td>{$functions->totel_quote_count($usersdata[data].order_id)}</td>
                                             {/if}
                                            {if $type==''}
                                            <td> <span style="padding:5px; background-color:#0C3;border: 2px solid #0C3;
    border-radius: 25px;">Running </span></td>
                                            {else}
                                            <td>
                                            {if $usersdata[data].transport_status!=''}
                                            {if $usersdata[data].transport_status=='Picked up'} <span style="padding:5px; background-color:#0C0; border: 2px solid #0C0;
    border-radius: 25px;">Picked up</span>{/if}
                                            {if $usersdata[data].transport_status=='In transit'} <span style="padding:5px; background-color:#0FF;border: 2px solid #0FF;
    border-radius: 25px;">&nbsp;&nbsp;&nbsp;In transit</span>{/if}
                                            {if $usersdata[data].transport_status=='Delivered'} <span style="padding:5px; background-color:#0FF;border: 2px solid #0FF;
    border-radius: 25px;">&nbsp;&nbsp;&nbsp;Delivered</span>{/if}
     {if $usersdata[data].transport_status=='Cancel'} <span style="padding:5px; background-color:#0FF;border: 2px solid #0FF;
    border-radius: 25px;">Cancel</span>{/if}
                                            {else} <span style="padding:5px; background-color:#F00; border: 2px solid #F00;
    border-radius: 25px;">&nbsp;&nbsp;&nbsp;Pending </span>{/if}</td>
                                            {/if}
                                               <td>
                                               	<a href="{$site_url}/admin.php?page=admin_home&action=detail_requirement&view_id={$usersdata[data].id}&type={$type}" title="view"><span class="btn btn-success btn-sm">view</span></a>
                                             {*   {if $usersdata[data].status=='Y'}
                                               	<a href="{$site_url}/admin.php?page=admin_home&action=approve_requirement&status_id={$usersdata[data].id}&active=N" title="active"><img src="{$site_url}/templates/admin/images/active.gif" width="12" height="12" alt="active" title="active" /></a>
                                                {else}
                                                	<a href="{$site_url}/admin.php?page=admin_home&action=approve_requirement&status_id={$usersdata[data].id}&inactive=Y" title="inactive"><img src="{$site_url}/templates/admin/images/inactive.gif" width="12" height="12" alt="inactive" title="inactive" /></a>
                                                    {/if}
                                                <a href="{$site_url}/admin.php?page=admin_home&action=approve_requirement&del_id={$usersdata[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><i class="fa fa-trash-o"></i></a>*}
                                               </td>
                                        </tr>
                                        {/section}
                                        
                                         <tr>
            <td colspan="10"><form name="frm_pagi" action="#" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />
                {$show_pagi}
              </form></td>
          </tr>
          {else}
          <tr>
            <td colspan="10"><span style="color:red;">Record Not Found</span></td>
          </tr>
          {/if}
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>

