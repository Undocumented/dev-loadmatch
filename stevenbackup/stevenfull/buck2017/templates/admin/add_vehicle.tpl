<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2>{if $edit_id==''}Add {else}Edit{/if} Vehicle Type</h2>
      </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading"> {if $edit_id==''}Add{else}Edit{/if} Vehicle Type
          <h4>{$show_message}</h4> 
          
          
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
                
                <!--===============customer form======================-->
                <form role="form" name="add_new_customer" id="add_new_customer" action="" method="post">
                  <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id}" />
                     <div class="form-group">
                    <label>Vehicle Type</label>
                    <input class="form-control" type="text" name="vehicle_type_name" id="vehicle_type_name" value="{$useredit[0].vehicle_type}" placeholder="Enter Vehicle Type " />
                   </div>
                 
                  <button type="submit" class="btn btn-primary" name="add_new_vehicle" value="add_new_vehicle" onclick="return Valid_add_vehicel(document.add_new_customer);"> Submit</button>
                </form>
                <!--==================================================--> 
                
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>
