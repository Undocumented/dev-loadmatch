<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>View Shipment Detail &nbsp;{if $type=="B"} [Booked] {elseif $type=="CT"} [Complted] {elseif $type=="C"} [Cencle] {else} {/if}</h2> 
                     
                    </div>
                </div>
                      
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b> Shipment Detail</b>	
                           
                            
                        </div>
                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered ">
                                    <tbody>
                                        <tr>
                                                      
                                        	<th>Customer Name</th>
                                            <td>{$functions->get_name($userdata[0].user_id)}</td>
                                          <th>Email Address</th>
                                            <td>{$functions->get_email($userdata[0].user_id)}</td>
                                            </tr>
                                            <tr>
                                              <th>Mobile</th>
                                            <td>{$functions->get_mobile($userdata[0].user_id)}</td>
                                            <th>Enter Date</th>
                                            <td>{$userdata[0].entery_date|date_format:"%d- %m -%Y"}</td>
                                        </tr>
                                        <tr>
                                              <th>Province</th>
                                            <td>{$functions->get_State($userdata[0].user_id)}</td>
                                            <th>city</th>
                                            <td>{$functions->get_city($userdata[0].user_id)}</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                         <table class="table table-striped table-bordered table-hover">
                                          <tbody>
                                        <tr>
                                              <th>Category</th>
                                            <td>{$functions->get_category($userdata[0].category)}</td>
                                            <th>Sub Category</th>
                                            <td>{$functions->get_category($userdata[0].sub_category)}</td>
                                        </tr>
                                        <tr>
                                              <th>form</th>
                                            <td>{$userdata[0].form_city}</td>
                                            <th>destination</th>
                                            <td>{$userdata[0].to_city}</td>
                                        </tr>
                                         <tr>
                                              <th>dimension</th>
                                            <td>{$userdata[0].Length}*{$userdata[0].Width}*{$userdata[0].Height}</td>
                                            <th>Weight</th>
                                            <td>{$userdata[0].Weight} KG</td>
                                        </tr>
                                         <tr>
                                              
                                            <th>Qty</th>
                                            <td>{$userdata[0].Qty} Pice</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                         <table class="table table-striped table-bordered table-hover">
                                          <tbody>
                                         <tr>
                                              <th>Pickup Start Date</th>
                                            <td>{$userdata[0].pickup_start_date|date_format:"%d- %m -%Y"}</td>
                                            <th>pickup End Date</th>
                                            <td>{$userdata[0].pickup_end_date|date_format:"%d- %m -%Y"}</td>
                                        </tr>
                                         <tr>
                                              <th>Delivery Start Date</th>
                                            <td>{$userdata[0].delv_start_date|date_format:"%d- %m -%Y"}</td>
                                            <th>Delivery End_Date</th>
                                            <td>{$userdata[0].delv_end_date|date_format:"%d- %m -%Y"}</td>
                                        </tr>
                                                                               
                                        <tr>
                                        <td colspan="4">
                                        <table>
                                        <tr>
                                        <th>description: &nbsp; </th>
                                            <td>{$userdata[0].description}</td>
                                        </tr>
                                        </table>
                                        
                                        </td>
                                        </tr>
                                        
                                    </tbody>
                                    
                                </table>
                                
                            </div>
                            
                        </div>
                        
                        
                        
                        
                    </div>
                    
                    {if $type=="B" || $type=="CT"}
                     <div class="panel panel-default">
                        <div class="panel-heading">
                            <b> Transporter Detail</b>
                                                
                        </div>
                          <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>
                                        <tr>
                                                      
                                        	<th>Transporter Name</th>
                                            <td>{$functions->get_name($userdata12[0].agent_id)}</td>
                                          <th>Email Address</th>
                                            <td>{$functions->get_email($userdata12[0].agent_id)}</td>
                                            </tr>
                                            <tr>
                                              <th>Mobile</th>
                                            <td>{$functions->get_mobile($userdata12[0].agent_id)}</td>
                                            <th>Quotation Price</th>
                                            <td>{$userdata12[0].starting_price}</td>
                                        </tr>
                                            <th>Payment Methods</th>
                                            <td>{$userdata12[0].payment_methods}</td>
                                            <th>Payment Accepted</th>
                                            <td>{$userdata12[0].payment_accepted}</td>
                                        </tr>
                                        
                                    </tbody>
                                    
                                </table>
                            </div>
                            
                        </div>
                   
                    </div>
                    {/if}
                    <!--End Advanced Tables -->
                          <div class="panel panel-default">
                        <div class="panel-heading">
                            <b> Shipment Status Detail</b>
                                                
                        </div>
                          <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                     {$functions->get_history($userdata12[0].order_id)}
                  <div class="bg-lg5" style="width:102.5%; margin-left:-15px;">
            <table class="table table-bordered">
              
                <thead style="background-color:#CCC;">
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Status
                    </th>
                    <th>
                     Date/Time
                    </th>
                    <th>
                     Activites
                    </th>
                    <th>
                     Details
                    </th>
                   </tr>
                </thead>
                <tbody>
                {if $status_history}
                {section name=data loop=$status_history}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    
                    <td>
                      {$status_history[data].new_status}
                    </td>
                    <td>
                    {$status_history[data].pickup_date|date_format:"%d- %m -%Y"}-{$status_history[data].pickup_time}
                    </td>
                    <td>
                       {$status_history[data].Activites}
                    </td>
                    <td>
                       {$status_history[data].Details}
                    </td>
                   </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
                </div>
                            
                        </div>
                   
                    </div>
                </div>
            </div>
                <!-- /. ROW  -->
            
                <!-- /. ROW  -->
            
             
        </div>
               
    </div>