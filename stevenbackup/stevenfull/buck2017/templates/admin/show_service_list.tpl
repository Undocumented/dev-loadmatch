<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Service List</h2>
                 {if $sucess=='Y'} <div class="notibar msgsuccess">
                        
                        <div class="alert alert-success" >Service Added Successfully !</div>
                    </div>{/if}
                     {if $update=='Y'} <div class="notibar msgsuccess">
                        
                        <div class="alert alert-success" >Service Update Successfully !</div>
                    </div>{/if}          
                       {if $del=='Y'} <div class="notibar msgsuccess">
                        
                        <div class="alert alert-success" >Service Deleted Successfully !</div>
                    </div>{/if}    
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <b>  Service List </b>
                      <div  style="text-align:right;">    
                 
                        </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="7%">Sr. No.</th>
                                            <th>service Name</th>
                                            <th>Description</th>
                                            <th>Image</th>
                                             <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if $usersdata}
                                        {section name=data loop=$usersdata}
                                        <tr class="gradeU">
                                            <td align="center">{$smarty.section.data.index+$row_no+1}</td>
                                            <td>{$usersdata[data].name}</td>
                                             <td>{$usersdata[data].description}</td>
                                              <td><img src="{$site_url}/location_flage/{$usersdata[data].img}" width="50PX;" height="50PX;" /></td>
                                               <td>
                                               	<a href="{$site_url}/admin.php?page=user&action=add_service&edit_id={$usersdata[data].id}" title="Update"><span class="btn btn-success btn-sm">Update &nbsp;&nbsp;&nbsp;</span></a>
                                                
                                                <a href="{$site_url}/admin.php?page=user&action=show_service_list&del_id={$usersdata[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><span class="btn btn-success btn-sm" style="background-color:#F00;">Delete</span></a>
                                               </td>
                                        </tr>
                                        {/section}
                                        
                                         <tr>
            <td colspan="8"><form name="frm_pagi" action="#" method="post">
                <input type="hidden"  name="pageval" id="pagevalid" value="{$page_val}" />
                {$show_pagi}
              </form></td>
          </tr>
          {else}
          <tr>
            <td colspan="8"><span style="color:red;">Record Not Found</span></td>
          </tr>
          {/if}
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>

