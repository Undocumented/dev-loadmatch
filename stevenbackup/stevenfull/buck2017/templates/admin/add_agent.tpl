<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2>{if $edit_id==''}Add New{else}Edit{/if} Agent</h2>
      </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading"> {if $edit_id==''}Add{else}Edit{/if} Agent
          <h4>{$show_message}</h4> 
          
          
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
                
                <!--===============customer form======================-->
                <form role="form" name="add_new_customer" id="add_new_customer" action="" method="post">
                  <input type="hidden" name="edit_id" id="edit_id" value="{$edit_id}" />
                  <input type="hidden" name="edit_mobile" id="edit_mobile" value="{$edit_query_data[0].customer_mobile}" />
                <!--  <div class="form-group">
                    <label>User Type</label>
                     <select class="form-control"  name="user_type" id="user_type" >
                     <option value="please Select" selected="selected" >please Select</option>
                      <option value="C" {if $useredit[0].type=='C'} selected="selected" {/if}>User</option>
                      <option value="T"  {if $useredit[0].type=='T'} selected="selected" {/if}>Agent</option>
                     </select>
                   </div>-->
                  <div class="form-group">
                    <label>User Name</label>
                    <input class="form-control" type="text" name="user_name" id="user_name" value="{$useredit[0].name}" placeholder="Enter Customer Name " />
                   </div>
                  <div class="form-group">
                    <label>Email Address</label>
                    <input class="form-control" type="email" name="email" id="email" value="{$useredit[0].email}" placeholder="Enter Customer Email " />
                    </div>
                  <div class="form-group">
                    <label>Mobile Number</label>
                    <input class="form-control" type="text" name="mobile_no" id="mobile_no" value="{$useredit[0].mobile}" placeholder="Enter Customer Mobile Number " />
                   </div>
                  
                  <!--==========password=========-->
                  
                  <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" type="password" name="password" id="password" value="" placeholder="Enter Password " />
                    </div>
                 <!-- <div class="form-group">
                    <label>Confirm Password</label>
                    <input class="form-control" type="password" name="confirm_password" id="alert_confirm_password" value="" placeholder="Enter Confirm Password " />
                    </div>-->
                  
                  <!--==========password finish====-->
                  
                  <div class="form-group">
                    <label>State</label>
                     <select name="state" id="state"  class="form-control" onchange="get_state_city(this.value);">
        
        <option value="please Select" selected="selected" >please Select</option>
      
						{section name=data loop=$categories}
                        
  <option value="{$categories[data].state}" {if $useredit[0].state==$categories[data].state} selected="selected" {/if} >{$categories[data].state} </option>
   {/section}
               
                 
</select>
                  </div>
                  <div class="form-group">
                    <label>City</label>
                    
                    <span id="city_change">
                     <select class="form-control"  name="city" id="city" >
                     {if $edit_id==''}
        <option value="please Select" selected="selected" >please Select</option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
        {/section}
     {/if}
</select></span>
                  </div>
                  <div class="form-group">
                    <label>Zip Code</label>
                    <input class="form-control" type="text" name="zipcode" id="zipcode" value="{$useredit[0].zip_code}" placeholder="Enter Your  Zipcode " />
                
                
                  </div>
                  <div class="form-group">
                    <label>Address</label>
                    <input class="form-control" type="text" name="address" id="address" value="{$useredit[0].address}" placeholder="Enter Address Number "  />
                 
                  </div>
                  
                  
                  
                  
                  <button type="submit" class="btn btn-primary" name="user_btn" value="add_new_customer" onclick="return Valid_add_customer(document.add_new_customer);"> {if $edit_id==''}Add{else}Edit{/if} Customer</button>
                </form>
                <!--==================================================--> 
                
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>
