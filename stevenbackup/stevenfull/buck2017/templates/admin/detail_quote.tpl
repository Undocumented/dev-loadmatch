<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>View Quote Detail</h2> 
                     
                    </div>
                </div>
            
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                                    
                            
                        </div>
                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>
                                        <tr>
                                            <th width="150px">Transporter Name</th>
                                            <td>{$functions->get_name($userdata[0].agent_id)}</td>
                                        	
                                         
                                            </tr>
                                            <tr>
                                              <th>Order id</th>
                                            <td>{$userdata[0].order_id}</td>
                                           
                                        </tr>
                                        <tr>
                                             <th>Price</th>
                                            <td>{$userdata[0].starting_price}({$functions->get_currency()})</td>
                                          
                                        </tr>
                                        </tbody>
                                        </table>
                                         <table class="table table-striped table-bordered table-hover">
                                          <tbody>
                                        <tr>
                                              <th width="150px">Collect Start Date</th>
                                            <td>{$userdata[0].collect_start_date|date_format:"%d- %m -%Y"}</td>
                                            <th width="150px">Collect End Date</th>
                                            <td>{$userdata[0].collect_end_date|date_format:"%d- %m -%Y"}</td>
                                        </tr>
                                        
                                         <tr>
                                              <th width="150px">Delivery Start Date</th>
                                            <td>{$userdata[0].delivery_start_date|date_format:"%d- %m -%Y"}</td>
                                            <th>Delivery End Date</th>
                                            <td>{$userdata[0].delivery_end_date|date_format:"%d- %m -%Y"}</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                         <table class="table table-striped table-bordered table-hover">
                                          <tbody>
                                         <tr>
                                              <th>Payment Methods</th>
                                            <td>{$userdata[0].payment_methods}</td>
                                            <th>Payment Accepted</th>
                                            <td>{$userdata[0].payment_accepted}</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                         <table class="table table-striped table-bordered table-hover">
                                          <tbody>
                                         <tr>
                                              <th width="150px">Payment Terms</th>
                                            <td>{$userdata[0].payment_terms}</td>
                                            </tr>
                                            <tr>
                                            <th>Standerd Terms</th>
                                            <td>{$userdata[0].standerd_terms}</td>
                                        </tr>
                                        
                                    </tbody>
                                    
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
                <!-- /. ROW  -->
            
                <!-- /. ROW  -->
            
             
        </div>
               
    </div>