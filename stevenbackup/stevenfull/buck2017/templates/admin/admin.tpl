<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head>
	<title>Admin</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	 <link href="{$site_url}/templates/admin/assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="{$site_url}/templates/admin/assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="{$site_url}/templates/admin/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="{$site_url}/templates/admin/assets/css/custom.css" rel="stylesheet" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    {if $action=='loginnow'}
    <link rel="stylesheet" href="{$site_url}/templates/admin/css/loginstyle.css">
    
    {/if}
     

   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <script type="text/javascript">var site_url="{$site_url}";</script>
   <script type="text/javascript" src="{$site_url}/templates/admin/js/ajax.js"></script>
    <script type="text/javascript" src="{$site_url}/templates/admin/js/validation.js"></script>
     <script type="text/javascript" src="{$site_url}/ckeditor/ckeditor.js"></script>
</head>
<body onload="setFocus();">

<div id="wrapper">



{if $smarty.session.adminUserId=='1'}
	{include file='admin/header.tpl'}
	
	
		{include file='admin/left_menu.tpl'}
	{/if}
    
	{include file=$contentBody}
    {include file='admin/footer.tpl'}
</div>


    <script src="{$site_url}/templates/admin/assets/js/jquery-1.10.2.js"></script>
    
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="{$site_url}/templates/admin/assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{$site_url}/templates/admin/assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="{$site_url}/templates/admin/assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="{$site_url}/templates/admin/assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="{$site_url}/templates/admin/assets/js/custom.js"></script>
    

</body>
</html>