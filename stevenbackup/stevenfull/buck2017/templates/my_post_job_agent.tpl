  <!--breadcrumbs start-->
   {if $smarty.session.user_type=='C'}
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Post Job</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My Post Job</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
           My Post Job
          </h4>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="{$site_url}/edit_post_shipping" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                      From
                    </th>
                    <th>
                      To
                    </th>
                    <th>
                     Category
                    </th>
                    <th>
                     Under Category
                    </th>
                     <th>
                     Status
                    </th>
                     <th>
                     Action
                    </th>
                    <th>
                     Quote
                    </th>
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                      {$search[data].order_id}
                    </td>
                    <td>
                     {$search[data].form_state}, {$search[data].form_city}
                    </td>
                    <td>
                     {$search[data].to_state}, {$search[data].to_city}
                    </td>
                    <td>
                      {$functions->get_category($search[data].category)}
                    </td>
                    <td>
                       {$functions->get_category($search[data].sub_category)}
                    </td>
                     <td>
                       {if $search[data].status=='Y'} Active {else} Pending {/if}
                    </td>
                    <td>
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a style="cursor:pointer;" onclick="submitbuy1('{$search[data].id}',document.view_detail);" class="btn btn-primary btn-sm"/>Edit</a>
                    </td>
                    <td>
                    {if $search[data].status=='Y'}
                    <a style="cursor:pointer;" onclick="submitbuy12('{$search[data].id}',document.view_detail);" class="btn btn-primary btn-sm"/>View Quote </a>
                    {/if}
                    </td>
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    {/if}
     <!--breadcrumbs start-->
   {if $smarty.session.user_type=='T'}
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Job Quote</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                         <li><a href="#">My Account</a></li>
                        <li>My Job Quote</li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          My Job Quote
          </h4>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="{$site_url}/give_quote.html" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                      Pickup Date
                    </th>
                    <th>
                      Delivery Date
                    </th>
                    <th>
                     Status
                    </th>
                     <th>
                     Action
                    </th>
                   
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                      {$search[data].order_id}
                    </td>
                    <td>
                     {$search[data].collect_start_date|date_format:"%d- %m -%Y"} to {$search[data].collect_end_date|date_format:"%d- %m -%Y"}
                    </td>
                    <td>
                     {$search[data].delivery_start_date|date_format:"%d- %m -%Y"} to {$search[data].delivery_end_date|date_format:"%d- %m -%Y"}
                    </td>
                   
                     <td>
                       {if $search[data].status=='Y'} Active {else} Pending {/if}
                    </td>
                    <td>
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a style="cursor:pointer;" onclick="submitbuy1234('{$search[data].id}',document.view_detail);" class="btn btn-primary btn-sm"/>Edit</a>
                    </td>
                    <td>
                    {if $search[data].status=='Y'}
                    <a style="cursor:pointer;" onclick="submitbuy12('{$search[data].id}',document.view_detail);" class="btn btn-primary btn-sm"/>View Quote </a>
                    {/if}
                    </td>
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    {/if}
    <!--container end-->