 <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-sm-4">
            <h1>
             HOW IT WORKS ?
            </h1>
          </div>
          <div class="col-lg-8 col-sm-8">
            <ol class="breadcrumb pull-right">
              <li>
                <a href="index.html">
                  Home
                </a>
              </li>
             
              <li class="active">
              Term & condition
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="container" >
      <div class="row">
</div>


          <p>
Submit your delivery details to Uship. Receive bids on a reverse auction format from Uship courier services & parcel delivery Firms, Choose the best bid for your consignment and get low price and safe delivered.

          </p>
		 <p><h3>What is a Courier Auction?</h3>
You're not going to be quoting on drivers to take you out to dinner, rather they will be quoting to try and win your delivery. Unlike a conventional salesroom a delivery auction works backwards – meaning the lower quotes win.

          </p>
           <p><h3>Why Would Drivers do a Job for Less?</h3>
The obvious question is: If couriers can charge £1.20 per mile, why would they settle for less?The answer: Because they can't always drum up enough business to fill their vehicles.<br />
When couriers run an empty or partially full vehicle it's an opportunity cost, they'll never get back the opportunity to move something at that point in time. So it's always in a driver's best interest to keep their vehicle as full as possible. Winning business on routes that they are already travelling is one way that these couriers can increase their profit margins and efficiency. <br />
We operate as a transport marketplace running reverse auctions on your delivery jobs, it's dead simple to use and you could save a bundle. 

          </p>
           <p><h3>Step One: Upload Your Requirements</h3>
First things first, you need to create your listing (or lot, if we're talking auctions). It's easy to list the details of your delivery, we need to know what you're moving, where it's going and when you'd like it delivered.

          </p>
           <p><h3>Step Two: The Auction</h3>
This is the fun part: couriers who are able to complete your delivery quote against one another to try and offer you the best quote – meaning the more popular your consignment, the better deal you're likely to get.When you have a few quotes you'll be able to compare prices from local transporters to see which suits you best. You might want to check quoter's feedback scores to see how other Shiply users rate them.

          </p>
           <p><h3>Step Three: Accepting a Quote</h3>
You don't have to slam your proverbial gavel as soon as the cheapest quote comes in; take some time to evaluate your options, because different couriers might be available on different days.<br />
When you have decided which courier you want to complete your consignment it's simply a matter of accepting your preferred quote and arranging a delivery date. 
It can be easy to just "set and forget" your auction and pray that you get lucky with a fantastic quote, but being proactive and taking some simple steps can help your delivery attract more offers.

          </p>
		   <p><h3>Give Details</h3>
	Listing "a sofa" isn't likely to attract many quotes. Whereas listing a "3 Seat Leather Sofa" with details of weight and dimensions is much more likely to attract accurate quotes from delivery services.<br />
Being specific with your delivery locations is useful as well because drivers will want to know that a consignment is on their pre-planned route before quoting. 

          </p>
 		<p><h3>Use Pictures</h3>
A picture is worth a thousand words. One of the most important steps to getting the lowest courier rates for an auction is to include images. Dimensions and weight are useful, but peculiarly shaped items especially can benefit from having pictures as this will help drivers decide if it will fit in their vehicle.<br />
Pictures also attract attention when seen alongside other items that do not have images, imagine a real life auction where you didn't see what you were quoting on, you would be hesitant too!

          </p>
 <p><h3>Communication</h3>

Communication is the foundation of a strong relationship and is also important when trying to win quote from couriers. Transport providers on Shiply will often have questions about deliveries such as when you are around for the pick-up. Maintaining communication with quoting transporters will definitely improve your chances of finding a cheaper quote.<br />
With these tips you'll be much more likely to attract quotes on your transport auction.
We wish you the best of luck with your listing and hope you get a great quote. Happy auctioneering! 

          </p>
          
        <!-- End row -->

      </div>
      <!-- End container -->
    </div>


    <!--container end-->
