{if $smarty.session.user_type=='C'}
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My Booked Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My Booked Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          My Booked Shipment
          </h4>
           {if $err}
          {$err}
          {/if}
          <table align="right">
          <tr>
          <td align="left">
           <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
                        <p><b style="color:#F00;">Pending&nbsp;:</b>&nbsp;&nbsp;Still no Action Tacken by Transporter</p>
         				<p><b style="color:#F00;">Picked up&nbsp;:</b>&nbsp;&nbsp;Shipment Picked up by Transporter</p>
          				<p><b style="color:#F00;">In Transit&nbsp;:</b>&nbsp;&nbsp;Shipment is In Transit</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
            <br/>
          
         
             </td>
             </tr>
             </table>
             <br/><br/><br/>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="{$site_url}/edit_post_shipping" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    
                    <th>
                      Order ID
                    </th>
                    <th>
                     Transporter
                    </th>
                    <th>
                      Collection
                    </th>
                    <th>
                     Delivery
                    </th>
                    <th>
                     Category
                    </th>
                  <!--  <th>
                     Under Category
                    </th>-->
                     <th>
                     Status
                    </th>
                     <th>
                     Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                      {$search[data].order_id}
                    </td>
                    <td>
                       {$functions->get_agent_name($search[data].order_id)}
                    </td>
                    <td>
                     {$search[data].form_state}, {$search[data].form_city}
                    </td>
                    <td>
                     {$search[data].to_state}, {$search[data].to_city}
                    </td>
                    <td>
                      {$functions->get_category($search[data].category)}
                    </td>
                    
                     <td>
                        {if $search[data].transport_status=='Picked up'}<div class="btn btn-success btn-sm">Picked up</div>	
                        
                        
                         {elseif $search[data].transport_status=='In transit'} <div class="btn btn-info btn-sm">In transit </div>{else} <div class="btn btn-danger btn-sm">Pending</div> {/if}
                    </td>
                   <td>
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a style="cursor:pointer;" onclick="submitbuy_book_cancel_complet('{$search[data].id}','b',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                    </td>
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    {/if}
    
    {if $smarty.session.user_type=='T'}
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>My booked Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">My booked Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
           My Booked Shipment
          </h4>
           {if $suss}
          {$suss}
          {/if}
            <table align="right">
          <tr>
          <td align="left">
          
           <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
                         <p><b style="color:#F00;">Pending&nbsp;:</b>&nbsp;&nbsp;Still no Action Tacken by Transporter</p>
         				 <p><b style="color:#F00;">Picked up&nbsp;:</b>&nbsp;&nbsp;Shipment Picked up by Transporter</p>
       					 <p><b style="color:#F00;">In Transit&nbsp;:</b>&nbsp;&nbsp;Shipment is In Transit</p>
        				 <p><b style="color:#F00;">Update&nbsp;:</b>&nbsp;&nbsp;Change Shipment Status</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
            <br/>
             </td>
             </tr>
             
             </table>
             <br/><br/><br/>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="{$site_url}/detail_shiping_booked" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                   <th>
                      Customer
                    </th>
                    <th>
                     Pickup Date
                    </th>
                    <th>
                     Delivery Date
                    </th>
                    <th>
                      Delivery Location
                    </th>
                     <th>
                     Status
                    </th>
                     <th>
                     Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                      {$search[data].order_id}
                    </td>
                    <td>
                       {$functions->get_name($search[data].user_id)}
                    </td>
                    <td>
                     {$search[data].collect_start_date|date_format:"%d- %m -%Y"}, {$search[data].collect_end_date|date_format:"%d- %m -%Y"}
                    </td>
                    <td>
                     {$search[data].delivery_start_date|date_format:"%d- %m -%Y"}, {$search[data].delivery_end_date|date_format:"%d- %m -%Y"}
                    </td>
                     <td>
                     {$functions->get_destnation_state_city($search[data].order_id)}
                    </td>
                    <td id="status_change_{$search[data].id}">
                      
                       {if $search[data].transport_status=='Picked up'}<div class="btn btn-success btn-sm">Picked up</div>	
                        
                        
                         {elseif $search[data].transport_status=='In transit'} <div class="btn btn-info btn-sm">In transit</div>{elseif $search[data].transport_status=='Cancel'} <div class="btn btn-danger btn-sm">Canceled</div> {else}<div class="btn btn-danger btn-sm">Pending</div> {/if}
                    </td>
                    <td>
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a style="cursor:pointer;" onclick="submitbuy_agent('{$search[data].id}','{$search[data].order_id}',document.view_detail);" class="btn btn-primary btn-sm"/>Show Detail</a>
                  
                    <a onclick="reaison_update({$search[data].id})" class="btn btn-primary btn-sm"/>Update</a>
                    </td>
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->
  <div id="cancel_value" style="display:none; position:fixed;  top:37%; left:50%; margin-top:-220px; margin-left:-200px; z-index:99999" >

           <div  style="width:350px; background-color: #fff;border:2px solid #888;text-align:center;padding:0px;margin:0px" >
<div style="margin:0px; padding: 3px 0px 2px 0px;text-align:center;background: #485b79 url({$site_url}/images/admin/default/headerbg.png); min-height: 10px; overflow: hidden; border-bottom: 2px solid #fb9337; border:1px solid #888;width:102;">
<a title="Upgrade Package" class="VSlabel" style="text-weight:bold;text-decoration:none;color:#fff;"><b>Update Shipment</b></a>
<img src="{$site_url}/images/converter_close.png" class="hand" onclick="reaison_close(0);" align="right" style="cursor:pointer"  />


 </div>
 
            <input type="hidden" id="cencel_id" />    
      
                    
             <table cellpadding="10" cellspacing="5"  class="stdtable1"  >
                  
                    <tr><td>&nbsp;</td></tr>
                      <tr>
                      <td>
                      <span id="update_status"></span>
                    Change Shipment Status <select name="transport_status"  id="transport_status"   >
          <option value="" selected="selected" >Please Select</option>
                    <option value="Picked up" {if $search[0].transport_status=='Picked up' } selected="selected" {/if} >Picked Up</option>
      				 <option value="In transit" {if $search[0].transport_status=='In transit'} selected="selected" {/if} >In transit</option>
                      <option value="Delivered" {if $search[0].transport_status=='Delivered'} selected="selected" {/if} >Delivered</option>
                      <option value="Cancel" {if $search[0].transport_status=='Cancel'} selected="selected" {/if} >Cancel</option>   
                     
                 
                   </select>
      
     
                    </ul>
                  
                   
              
                   </td>
                       </tr>
                       <tr>
                     <td align="center">
                     <a onClick="update_status_booking();" class="btn btn-primary btn-sm" >Submit
                     </a>
                      <!-- onClick="return add_city_name_pincode(document.frm_addcity);"-->
                      <!-- <a class="btn btn-primary btn-sm"  onclick="this.submit();" >Submit</a>-->
                     
                           
                        <td></tr> 
                    
                        
                        
                        
                        </table>
                  


          </div>

        </div>
      </div>
    </div>
    <!--container end-->
    {/if}