  <!--breadcrumbs start-->
   {if $smarty.session.user_type=='C'}
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Active Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Active Shipment</a></li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
        Cancel Shipment
          </h4>
          {if $err}
          {$err}
          {/if}
          <table align="right" >
          <tr>
          <td align="left">
           <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
                       <p><b style="color:#F00;">Active&nbsp;:</b>&nbsp;&nbsp;Shipment is Approved By Admin</p>
                      <p><b style="color:#F00;">Pending&nbsp;:</b>&nbsp;&nbsp;Shipment is Still in Admin Rewiew</p>
                      <p><b style="color:#F00;">View Quote&nbsp;:</b>&nbsp;&nbsp;View Quote Given By Transporter</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
            <br/>
             </td>
             </tr>
             </table>
           
          <div class="contact-form">
            
              <div class="table-responsive">
              <form name="view_detail" method="post" action="{$site_url}/edit_post_shipping" id="view_detail">
             
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                    Collection
                    </th>
                    <th>
                      Delivery
                    </th>
                    <th>
                     Category
                    </th>
                    <th>
                     Under Category
                    </th>
                     <th>
                     Status
                    </th>
                 {*   <th width="12%">
                     Action
                    </th> *}
                    
                 {*   <th>
                     Quote
                    </th> *}
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                      {$search[data].order_id}
                    </td>
                    <td>
                     {$search[data].form_state}, {$search[data].form_city}
                    </td>
                    <td>
                     {$search[data].to_state}, {$search[data].to_city}
                    </td>
                    <td>
                      {$functions->get_category($search[data].category)}
                    </td>
                    <td>
                       {$functions->get_category($search[data].sub_category)}
                    </td>
                     <td>
                     
                       {if $search[0].transport_status=='Cancel'}
                       <div class="btn btn-danger btn-sm" style=" cursor:move;"> Cancel </div>
                       {/if}
                    </td>
                 {*   <td>
                    <input type="hidden" name="id"  value="{$search[data].id}" />
                    <a style="cursor:pointer;" onclick="submitbuy1('{$search[data].id}',document.view_detail);" class="btn btn-primary btn-sm"/>Edit</a>
                   <!--href="{$site_url}/eagleushp.php?page=myaccount&action=booked_cancel&cancel_user={$search[data].id}"-->
                     <a  class="btn btn-primary btn-sm" onclick="reaison({$search[data].id})">Cancel </a>
                    </td> *}
                  {*  <td>
                    {if $search[data].status=='Y'}
                    <a style="cursor:pointer;" onclick="submitbuy12('{$search[data].id}',document.view_detail);" class="btn btn-primary btn-sm"/>View Quote({$functions->get_total_quote($search[data].order_id)}) </a>
                    {else}
                    <a style="cursor:pointer;" class="btn btn-primary btn-sm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Null&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a>
                    {/if}
                    </td> *}
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="10" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
                <div id="cancel_value" style="display:none; position:fixed;  top:37%; left:50%; margin-top:-220px; margin-left:-200px; z-index:99999" >

           <div  style="width:500px; background-color: #fff;border:2px solid #888;text-align:center;padding:0px;margin:0px" >
<div style="margin:0px; padding: 3px 0px 2px 0px;text-align:center;background: #485b79 url({$site_url}/images/admin/default/headerbg.png); min-height: 10px; overflow: hidden; border-bottom: 2px solid #fb9337; border:1px solid #888;width:102;">
<a title="Upgrade Package" class="VSlabel" style="text-weight:bold;text-decoration:none;color:#fff;"><b>Cancel Shipment</b></a>
<img src="{$site_url}/images/converter_close.png" class="hand" onclick="reaison_close(0);" align="right" style="cursor:pointer"  />


 </div>
 
                 
        <form class="stdform " name="frm_addcity" method="post" action="{$site_url}/eagleushp.php?page=myaccount&action=booked_cancel" enctype="multipart/form-data">
        <input type="hidden" id="cencel_id" name="cancel_user" value="" />
                    
             <table cellpadding="10" cellspacing="5"  class="stdtable1"  >
                  
                    <tr><td>&nbsp;</td></tr>
                      <tr>
                      <td>
                     Reason For Cancel:<textarea name="resone" rows="5"  cols="30"  ></textarea>
                   </td>
                       </tr>
                       <tr>
                     <td>
                      <!-- onClick="return add_city_name_pincode(document.frm_addcity);"-->
                      <!-- <a class="btn btn-primary btn-sm"  onclick="this.submit();" >Submit</a>-->
                      <input type="submit" name="submit" class="btn btn-primary btn-sm" />
                           
                        <td></tr> 
                    
                        
                        
                        
                        </table>
                    </form>
 			
 

 
 
	
   
</div>
			  

</div>
          </div>
        </div>
     

    </div>
    
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    {/if}
     <!--breadcrumbs start-->
   {if $smarty.session.user_type=='T'}
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Active Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                         <li><a href="#">My Account</a></li>
                        <li>Active Shipment</li>
                       
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
  
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          Cancel Shipment Transporter
          </h4>
          {if $err}
          {$err}
          {/if}
            <table align="right">
          <tr>
          <td>
          
          <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Note</button>
            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
            
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Important Notes</h4>
                  </div>
                  <div class="modal-body">
                     <p><b style="color:#F00;">Pending&nbsp;:</b>&nbsp;&nbsp;Shipment Quote is Under Review of Admin</p>
                      <p><b style="color:#F00;">Active&nbsp;:</b>&nbsp;&nbsp;Shipment Quote is Active</p>
                      <p><b style="color:#F00;">Edit&nbsp;:</b>&nbsp;&nbsp;Edit Shipment Quote </p>
                      <p><b style="color:#F00;">Cancel&nbsp;:</b>&nbsp;&nbsp;Cancel Shipment Quote</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
            
              </div>
            </div>
                <br/><br/>
             </td>
             </tr>
             </table>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="{$site_url}/give_quote.html" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Order ID
                    </th>
                    <th>
                      Pickup Date
                    </th>
                    <th>
                      Delivery Date
                    </th>
                     <th>
                      Delivery Location
                    </th>
                    <th>
                     Status
                    </th>
                  
                   
                  </tr>
                </thead>
                <tbody>
                {if $search}
                {section name=data loop=$search}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    <td>
                      {$search[data].order_id}
                    </td>
                    <td>
                     {$search[data].collect_start_date|date_format:"%d- %m -%Y"} to {$search[data].collect_end_date|date_format:"%d- %m -%Y"}
                    </td>
                    <td>
                     {$search[data].delivery_start_date|date_format:"%d- %m -%Y"} to {$search[data].delivery_end_date|date_format:"%d- %m -%Y"}
                    </td>
                    <td>
                     {$functions->get_destnation_state_city($search[data].order_id)}
                    </td>
                   
                      <td>
                     
                       {if $search[0].transport_status=='Cancel'}
                       <div class="btn btn-danger btn-sm" style=" cursor:move;"> Cancel </div>
                       {/if}
                    </td>
                
                  </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
              
          
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />{$show_pagi}</td></tr>
                </form>
                
                
                <div id="cancel_value" style="display:none; position:fixed;  top:37%; left:50%; margin-top:-220px; margin-left:-200px; z-index:99999" >

           <div  style="width:500px; background-color: #fff;border:2px solid #888;text-align:center;padding:0px;margin:0px" >
<div style="margin:0px; padding: 3px 0px 2px 0px;text-align:center;background: #485b79 url({$site_url}/images/admin/default/headerbg.png); min-height: 10px; overflow: hidden; border-bottom: 2px solid #fb9337; border:1px solid #888;width:102;">
<a title="Upgrade Package" class="VSlabel" style="text-weight:bold;text-decoration:none;color:#fff;"><b>Cancel Shipment</b></a>
<img src="{$site_url}/images/converter_close.png" class="hand" onclick="reaison_close(0);" align="right" style="cursor:pointer"  />


 </div>
 
                 
        <form class="stdform " name="frm_addcity" method="post" action="{$site_url}/eagleushp.php?page=myaccount&action=booked_cancel" enctype="multipart/form-data">
        <input type="hidden" id="cencel_id" name="cancel_user" value="" />
                    
             <table cellpadding="10" cellspacing="5"  class="stdtable1"  >
                  
                    <tr><td>&nbsp;</td></tr>
                      <tr>
                      <td>
                     Reason For Cencle:<textarea name="resone" rows="5"  cols="30"  ></textarea>
                   </td>
                       </tr>
                       <tr>
                     <td>
                      <!-- onClick="return add_city_name_pincode(document.frm_addcity);"-->
                      <!-- <a class="btn btn-primary btn-sm"  onclick="this.submit();" >Submit</a>-->
                      <input type="submit" name="submit" class="btn btn-primary btn-sm" />
                           
                        <td></tr> 
                    
                        
                        
                        
                        </table>
                    </form>
 			
          </div>
          
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    {/if}
    <!--container end-->
    
    