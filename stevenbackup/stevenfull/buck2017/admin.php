<?php
# include manadatory files
include_once( 'includes/global.php' );


# Check if admin login

$page="admin_home"; $action="admin_home"; $defaultpage="comingsoon";
if(isset($_REQUEST['page'])) $page=$_REQUEST['page'];
if(isset($_REQUEST['action'])) $action=$_REQUEST['action'];
$objSmarty->assign("page", $page);
$objSmarty->assign("action", $action);
switch($page){

	case 'admin_home': 
		switch($action){
			case 'admin_home': $admin_home->admin_home(); $page="admin_home"; break;
			case 'edit_company': $admin_home->admin_home(); $page="edit_company"; break;
			case 'payment_gatway_setting': $admin_home->payment_gatway_setting(); $page="payment_gatway_setting"; break;
			case 'social_setting': $admin_home->social_setting(); $page="social_setting"; break;
			case 'approve_requirement': $admin_home->approve_requirement(); $page="approve_requirement"; break;
			case 'detail_requirement': $admin_home->detail_requirement(); $page="detail_requirement"; break;
			case 'approve_review': $admin_home->approve_review(); $page="approve_review"; break;
			case 'detail_review': $admin_home->detail_review(); $page="detail_review"; break;
			case 'booked_shipment': $admin_home->booked_shipment(); $page="booked_shipment"; break;
			case 'show_message': $admin_home->show_message(); $page="show_message"; break;
			case 'detail_message': $admin_home->detail_message(); $page="detail_message"; break;
			default: $page=$defaultpage;
		}
		break;
		
	case 'user': 
		switch($action){
			case 'loginnow':  $user->login(); $page="login"; break;
			case 'login': $user->login();$page="login"; break;
			case 'logout': $user->logout(); break;
			case 'add_users':  $user->add_users(); $page="add_users"; break;
			case 'show_user_list':  $user->show_user_list(); $page="show_user_list"; break;
			case 'add_service':  $user->add_service(); $page="add_service"; break;
			case 'show_service_list':  $user->show_service_list(); $page="show_service_list"; break;
			case 'add_testimonial':  $user->add_testimonial(); $page="add_testimonial"; break;
			case 'show_testimonial_list':  $user->show_testimonial_list(); $page="show_testimonial_list"; break;
			case 'show_cust_review':  $user->show_cust_review(); $page="show_cust_review"; break;
			case 'edit_review':  $user->edit_review(); $page="edit_review"; break;
			case 'show_trans_review':  $user->show_trans_review(); $page="show_trans_review"; break;
			default: $page=$defaultpage;
		}
		break;
		
		case 'location_mange': 
		switch($action){ 
			case 'add_country':  $location_mange->add_country(); $page="add_country"; break;
			case 'add_state': $location_mange->add_state();$page="add_state"; break;
			case 'add_city':  $location_mange->add_city(); $page="add_city"; break;
			case 'show_location_list':  $location_mange->show_location_list(); $page="show_location_list"; break;
			default: $page=$defaultpage;
		}
		break;
		case 'category': 
		switch($action){
			case 'add_cat': $category->add_cat(); $page="add_cat"; break;
			case 'show_cat_list': $category->show_cat_list(); $page="show_cat_list"; break;
			case 'show_sub_cat_list': $category->show_sub_cat_list(); $page="show_sub_cat_list"; break;
			case 'show_sub_next_cat_list': $category->show_sub_next_cat_list(); $page="show_sub_next_cat_list"; break;
			case 'show_last_cat': $category->show_last_cat(); $page="show_last_cat"; break;
			
			case 'add_vehicle': $category->add_vehicle(); $page="add_vehicle"; break;
			case 'show_vehicle': $category->show_vehicle(); $page="show_vehicle"; break;
			default: $page=$defaultpage;
		}
		break;
		
		case 'payment': 
		switch($action){
			case 'show_payment': $payment->show_payment(); $page="show_payment"; break;
			case 'detail_payment': $payment->detail_payment(); $page="detail_payment"; break;
			case 'show_quote': $payment->show_quote(); $page="show_quote"; break;
			case 'show_quote_agent': $payment->show_quote_agent(); $page="show_quote_agent"; break;
			case 'detail_quote': $payment->detail_quote(); $page="detail_quote"; break;
			case 'Package': $payment->Package(); $page="Package"; break;
			default: $page=$defaultpage;
		}
		break;
		case 'mails': 
		switch($action){
			case 'show_mails': $mails->show_mails(); $page="show_mails"; break;
			case 'add_mails': $mails->add_mails(); $page="add_mails"; break;
			case 'show_news': $mails->show_news(); $page="show_news"; break;
			case 'add_news': $mails->add_news(); $page="add_news"; break;
			case 'show_notice': $mails->show_notice(); $page="show_notice"; break;
			case 'add_notice': $mails->add_notice(); $page="add_notice"; break;
			
			default: $page=$defaultpage;
		}
		break;
		case 'add_banner': 
		switch($action){
			case 'add_slider': $add_banner->add_slider(); $page="add_slider"; break;
						
			default: $page=$defaultpage;
		}
		break;
		
		
}

isAdminLogin();

if(file_exists(DOC.'templates/admin/'.$page.".tpl"))     	         //Check for file existence
{
	$objSmarty->assign("contentBody", 'admin/'.$page.".tpl");	 //Assign current page name in smarty variable
}
else
{
	$objSmarty->assign("contentBody", "comingsoon.tpl");
}

# display template
$objSmarty->display( 'admin/admin.tpl' );

# clear cache
$objSmarty->clear_cache( 'admin/admin.tpl' );
$objSmarty->clear_cache('admin/'.$page.'.tpl');

# clear buffer
ob_end_flush();
?>