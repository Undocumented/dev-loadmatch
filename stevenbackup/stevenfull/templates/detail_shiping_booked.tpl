  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Shipment Detail </h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Shipment Detail</a></li>
                         <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
 <div class="white-bg">

        <!-- career -->
    <div class="container career-inner">
        <div class="row">
            <div class="col-md-12 career-head">
               

            </div>
        </div>
        <hr>
       
     <table align="center" width="100%" border="0">
     <tr>
     <td>
        <div class="row">
            <div class="col-md-4" style="background-color:#F7F7F7;min-height:800px;">
                <div class="candidate wow fadeInLeft" style="text-align:left;" >
                    <h1>Sender Detail</h1>
                    <p class="align-left"></p>
                    <h4></h4>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-angle-right pr-10"></i>Sender Name : {$functions->get_name($search[0].user_id)}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Sender State : {$search[0].form_state} </li>
                        <li><i class="fa fa-angle-right pr-10"></i>Sender City : {$search[0].form_city}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>location : {$search[0].sender_loc}</li>
                       
                    </ul>
                   
                </div>
                <hr>
                <div class="candidate wow fadeInLeft" style="text-align:left;">
                    <h1>Pickup Dates</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                         <li><i class="fa fa-angle-right pr-10"></i>Start Date : {$search[0].pickup_start_date|date_format:"%d- %m -%Y"}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>End Date : {$search[0].pickup_end_date|date_format:"%d- %m -%Y"} </li>
                       
                    </ul>
                    <hr>
                    <h1>Shiping Detail</h1>
                    <p class="align-left"></p>
                    <h4></h4>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-angle-right pr-10"></i>Category : {$functions->get_category($search[0].category)}</li>
                        <li><i class="fa fa-angle-right pr-10"></i> Under Category : {$functions->get_category($search[0].sub_category)} </li>
                         <li><i class="fa fa-angle-right pr-10"></i>Dimensions : {$search[0].dimensions}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Width : {$search[0].Width}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Height : {$search[0].Height}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Length : {$search[0].Length}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Weight : {$search[0].Weight}</li>
                         <li><i class="fa fa-angle-right pr-10"></i>Quantity : {$search[0].Qty}</li>
                         {if $search[0].img}
                          <li><img src="{$site_url}/images/shipment_image/{$search[0].img}" width="100px" height="100px" /></li>
                          {/if}
                       <li style="max-height:187px; overflow:scroll;"><i class="fa fa-angle-right pr-10"></i>Description : {$search[0].description}</li>
                       
                    </ul>
                </div>
                <hr>
            </div>
             <div class="col-md-4">
                <div class="candidate wow fadeIn"  >
                    <h2>Origin, Destination, & Route Information</h2>
               
                      {include_php file="gmap_tranport.php"}             
                </div>
                </div>
            <div class="col-md-4" style="background-color:#F7F7F7; min-height:800px;">
                <div class="candidate wow fadeInRight" style="text-align:left;">
                    <h1>Receiver Detail</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                         <li><i class="fa fa-angle-right pr-10"></i>Receiver Name : {$search[0].receiver_name}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Receiver State : {$search[0].to_state} </li>
                        <li><i class="fa fa-angle-right pr-10"></i>Receiver City : {$search[0].to_city}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>location : {$search[0].receiver_loc}</li>
                    </ul>
                  
                </div>
                <hr>
                <div class="candidate wow fadeInRight" style="text-align:left;">
                    <h1>Delivery Dates</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                       <li><i class="fa fa-angle-right pr-10"></i>Start Date : {$search[0].delv_start_date|date_format:"%d- %m -%Y"}</li>
                        <li><i class="fa fa-angle-right pr-10"></i>End Date : {$search[0].delv_end_date|date_format:"%d- %m -%Y"} </li>
                    </ul>
                    <hr>
                   {if $type=='ct'}
             
                    <h1>Payment Detail</h1>
                   
                    <h4></h4>
                    
                     <ul class="list-unstyled">
                        <li><i class="fa fa-angle-right pr-10"></i>Payment Recived : ({$functions->get_currency()}) {$search1[0].starting_price}</li>
                         <li><i class="fa fa-angle-right pr-10"></i>Payment Accepted; : &nbsp; {$search1[0].payment_accepted} </li>
                         <li><i class="fa fa-angle-right pr-10"></i>Payment Method&nbsp; : &nbsp; {$search1[0].payment_methods} </li>
                       
                    </ul>
                
              
         {/if}
         
         {if $type=='c'}
            
                    <h1>&nbsp;&nbsp;Cancel Reason</h1>
                   
                    <h4></h4>
                    
                     <ul class="list-unstyled">
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$search1[0].reaison_cancel}</li>
                       
                    </ul>
                
               
                <hr>
               
            
         {/if}
                </div>
              
                
            </div>
            
            </div>
          {*  {if $type!='c' && $type!='ct'}
             <div class="candidate wow fadeInRight" style="text-align:left;">
                    <h1>Action</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                    
                     <form method="post" action="#" name="book"> 
              Change Shipment Status <select name="transport_status"  id="transport_status"  >
          <option value="please Select" selected="selected" >please Select</option>
                    <option value="Picked up" {if $search[0].transport_status=='Picked up' } selected="selected" {/if} >Picked up</option>
      				 <option value="In transit" {if $search[0].transport_status=='In transit'} selected="selected" {/if} >In transit</option>
                      <option value="Delivered" {if $search[0].transport_status=='Delivered'} selected="selected" {/if} >Delivered</option>   
                     
                 
                   </select>
      
     
                    </ul>
                  
                    <input type="hidden" value="{$search[0].order_id}" name="order_id" />
              
                    <input type="submit" name="submit" value="Submit" class="btn btn-info" />
                    
                    </form>
                </div>
                <hr>
               
            </div>
        </div>
         {/if}*}
         
          
         </td>
        </tr>
        </table>
             {$functions->get_history($search[0].order_id)}
                  <div class="bg-lg5" style="width:102.5%; margin-left:-15px;">
            <table class="table table-bordered">
              
                <thead style="background-color:#CCC;">
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Status
                    </th>
                    <th>
                     Date/Time
                    </th>
                    <th>
                     Activites
                    </th>
                    <th>
                     Details
                    </th>
                   </tr>
                </thead>
                <tbody>
                {if $status_history}
                {section name=data loop=$status_history}
                  <tr>
                    <td>
                      {$smarty.section.data.index+$row_no+1}
                    </td>
                    
                    <td>
                      {$status_history[data].new_status}
                    </td>
                    <td>
                    {$status_history[data].pickup_date|date_format:"%d- %m -%Y"}-{$status_history[data].pickup_time}
                    </td>
                    <td>
                       {$status_history[data].Activites}
                    </td>
                    <td>
                       {$status_history[data].Details}
                    </td>
                   </tr>
                  {/section}
                  {else}
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					{/if}
                </tbody>
              </table>
                 </div>
    <!-- career -->
    
       </div>
    </div>
  