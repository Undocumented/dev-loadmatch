<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Approve Review</h2>
                     {if $show_message!=''}
                      <h5 class=" btn-success btn-lg" align="center">{$show_message}</h5>  
                       {/if} 
                      
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <b> Review List</b>
                            {if $show_message!=''}
                          
                            {/if}
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="8%">Sr. No.</th>
                                            <th>Customer Name</th>
                                            <th>Email Address</th>
                                            <th>Entry Date</th>
                                             <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if $usersdata}
                                        {section name=data loop=$usersdata}
                                        <tr class="gradeU">
                                            <td align="center">{$smarty.section.data.index+$row_no+1}</td>
                                            <td>{$functions->get_name($usersdata[data].user_id)}</td>
                                           
                                             <td>{$functions->get_email($usersdata[data].user_id)}</td>
                                             
                                            <td>{$usersdata[data].date}</td>
                                               <td>
                                               	<a href="{$site_url}/admin.php?page=admin_home&action=detail_review&view_id={$usersdata[data].id}" title="view"><span class="btn btn-success btn-sm">view</span></a>
                                                 {if $usersdata[data].status=='Y'}
                                               	<a href="{$site_url}/admin.php?page=admin_home&action=approve_review&status_id={$usersdata[data].id}&active=N" onclick="return confirm('Do You Want to Change it to Inactive?');" title="active"><span class="btn btn-success btn-sm" style="background-color:#F90;">Active</span></a>
                                                {else}
                                                	<a href="{$site_url}/admin.php?page=admin_home&action=approve_review&status_id={$usersdata[data].id}&inactive=Y" onclick="return confirm('Do You Want to Change it to Active?');" title="inactive"><span class="btn btn-success btn-sm" style="background-color:#069;">Inactive</span></a>
                                                    {/if}
 <a href="{$site_url}/admin.php?page=admin_home&action=approve_review&del_id={$usersdata[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><span class="btn btn-success btn-sm" style="background-color:#F00;">Delete</span></a>
                                               </td>
                                        </tr>
                                        {/section}
                                        
                                         <tr>
            <td colspan="9"><form name="frm_pagi" action="#" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />
                {$show_pagi}
              </form></td>
          </tr>
          {else}
          <tr>
            <td colspan="9"><span style="color:red;">Record Not Found</span></td>
          </tr>
          {/if}
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>

