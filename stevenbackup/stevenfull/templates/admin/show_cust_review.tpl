<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Customer Review List</h2>
                     {if $show_message!=''}
                       <h5 class=" btn-success btn-lg" align="center">{$show_message}</h5>  
                       {/if} 
                      
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <b> Customer Review List </b>
                      <div  style="text-align:right;">    
                  
                        </div>
                        </div>
                        
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="7%">Sr. No.</th>
                                            <th>Customer name</th>
                                            <th>Transpoter name</th>
                                            <th>Communication Rating</th>
                                            <th>punctuality Rating</th>
                                            <th>Over All rating</th>
                                            <th>Comment</th>
                                             <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {if $userdata}
                                        {section name=data loop=$userdata}
                                        <tr class="gradeU">
                                            <td align="center">{$smarty.section.data.index+$row_no+1}</td>
                                            <td>
                                            {$functions->get_customer_detail($userdata[data].user_id)}
                                            {$customer_info[0].name}
                                            </td>
                                             <td>
                                             {$functions->get_Transpoter_detail($userdata[data].transpoter_id)}
                                             {$Transpoter_info[0].name}
                                             </td>
                                             <td>{$userdata[data].com_rating }</td>
                                              <td>{$userdata[data].pun_rating}</td>
                                              <td>{$userdata[data].rating}</td>
                                         <td>{$userdata[data].review}</td>
                                              
                                    
                                               <td>
                                               {$functions->get_customer_detail($userdata[data].user_id)}
                                               	<a href="{$site_url}/admin.php?page=user&action=edit_review&edit_id={$userdata[data].user_id}&type={$customer_info[0].type}" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                                             {if $userdata[data].status!='Y'}
                                               	<a href="{$site_url}/admin.php?page=user&action=show_cust_review&active_id={$userdata[data].id}" onclick="return confirm('Do You Want to Change it to Inactive?');" title="active"><span class="btn btn-success btn-sm" style="background-color:#F90;">Active&nbsp;</span></a>
                                                {else}
                                                	<a href="{$site_url}/admin.php?page=user&action=show_cust_review&inactive_id={$userdata[data].id}" onclick="return confirm('Do You Want to Change it to Active?');" title="inactive"><span class="btn btn-success btn-sm" style="background-color:#069;">Inactive</span></a>
                                                    {/if}
                                                <a href="{$site_url}/admin.php?page=user&action=show_cust_review&del_id={$userdata[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><span class="btn btn-success btn-sm" style="background-color:#F00;">Delete</span></a>
                                               </td>
                                        </tr>
                                        {/section}
                                        
                                         <tr>
            <td colspan="8"><form name="frm_pagi" action="#" method="post">
                <input type="hidden"  name="pageval" id="pagevalid" value="{$page_val}" />
                {$show_pagi}
              </form></td>
          </tr>
          {else}
          <tr>
            <td colspan="8"><span style="color:red;">Record Not Found</span></td>
          </tr>
          {/if}
                                    </tbody>
                                </table>
                                
                                
                                
                                
                                
                            </div>
                            
                        </div>
                    </div>
                
                </div>
            </div>
              
        </div>
               
    </div>

