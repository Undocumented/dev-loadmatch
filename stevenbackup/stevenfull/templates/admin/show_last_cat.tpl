<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2> Category List</h2>
        {if $show_message!=''}
        <h5 class=" btn-success btn-lg" align="center">{$show_message}</h5>
        {/if} </div>
    </div>
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Advanced Tables -->
        <div class="panel panel-default">
          <div class="panel-heading"> Category List </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                  <tr>
                    <th width="8%">Sr. No.</th>
                    <th>Category Name</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                
                {if $cat_data}
                {section name=data loop=$cat_data}
                <tr class="gradeU">
                  <td align="center">{$smarty.section.data.index+$row_no+1}</td>
                  {if $type=='FC'}
                  <td><a href="{$site_url}/admin.php?page=category&action=show_last_cat&type=FFC&id={$cat_data[data].id}"> {$cat_data[data].category} </a></td>
                  {/if}
                  {if $type=='FFC'}
                  <td><a href="{$site_url}/admin.php?page=category&action=show_last_cat&type=SC&id={$cat_data[data].id}"> {$cat_data[data].category} </a></td>
                  {/if}
                  {if $type=='SC'}
                  <td><a href="{$site_url}/admin.php?page=category&action=show_last_cat&type=SVC&id={$cat_data[data].id}"> {$cat_data[data].category} </a></td>
                  {/if}
                  {if $type=='SVC'}
                  <td><a href="{$site_url}/admin.php?page=category&action=show_last_cat&type=EC&id={$cat_data[data].id}"> {$cat_data[data].category} </a></td>
                  {/if}
                  {if $type=='EC'}
                  <td><a href="{$site_url}/admin.php?page=category&action=show_last_cat&type=NC&id={$cat_data[data].id}"> {$cat_data[data].category} </a></td>
                  {/if}
                  {if $type=='NC'}
                  <td><a href="{$site_url}/admin.php?page=category&action=show_last_cat&type=TC&id={$cat_data[data].id}"> {$cat_data[data].category} </a></td>
                  {/if}
                  {if $type=='TC'}
                  <td>{$cat_data[data].category} </td>
                  {/if}
                 
                  <td>
                  
                  {if $type=='FC'}
                  <a href="{$site_url}/admin.php?page=category&action=add_cat&type=FC&edit_id_sub={$cat_data[data].id}" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                  {/if}
                  {if $type=='FFC'}
                  <a href="{$site_url}/admin.php?page=category&action=add_cat&type=FFC&edit_id_sub={$cat_data[data].id}" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                  {/if}
                  {if $type=='SC'}
                  <a href="{$site_url}/admin.php?page=category&action=add_cat&type=SC&edit_id_sub={$cat_data[data].id}" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                  {/if}
                  {if $type=='SVC'}
                  <a href="{$site_url}/admin.php?page=category&action=add_cat&type=SVC&edit_id_sub={$cat_data[data].id}" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                  {/if}
                  {if $type=='EC'}
                  <a href="{$site_url}/admin.php?page=category&action=add_cat&type=EC&edit_id_sub={$cat_data[data].id}" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                  {/if}
                  {if $type=='NC'}
                  <a href="{$site_url}/admin.php?page=category&action=add_cat&type=NC&edit_id_sub={$cat_data[data].id}" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                  {/if}
                   {if $type=='TC'}
                  <a href="{$site_url}/admin.php?page=category&action=add_cat&type=TC&edit_id_sub={$cat_data[data].id}" title="Edit"><span class="btn btn-success btn-sm">Edit &nbsp;&nbsp;&nbsp;</span></a>
                  {/if}
                  
                  
                  
                 
                  
                  
                  
                   <a href="{$site_url}/admin.php?page=category&action=show_sub_cat_list&type=FC&del_id={$cat_data[data].id}" onclick="return confirm('Do You  want to delete ?');" title="Delete"><span class="btn btn-success btn-sm" style="background-color:#F00;">Delete</span></a></td>
                </tr>
                {/section}
                <tr>
                  <td colspan="6"><form name="frm_pagi" action="#" method="post">
                      <input type="hidden" name="pageval" id="pagevalid" value="{$page_val}" />
                      {$show_pagi}
                    </form></td>
                </tr>
                {else}
                <tr>
                  <td colspan="8"><span style="color:red;">Record Not Found</span></td>
                </tr>
                {/if}
                </tbody>
                
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
