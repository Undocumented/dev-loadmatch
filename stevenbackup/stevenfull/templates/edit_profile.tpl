  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Edit Profile</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Edit Profile</a></li>
                        <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="component-bg">
        <div class="container">
            <!-- Forms
================================================== -->
<div class="bs-docs-section mar-b-30">
  <h1 id="forms" class="page-header">Edit Profile</h1>
 
    <form class="form-horizontal" name="edit_user"  method="post" action="{$site_url}/edit_profile.html" role="form" enctype="multipart/form-data">
        <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >User Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="{$search[0].name}" id="s_user_name" name="s_user_name" placeholder="User Name">
          <input type="hidden" name="user_id" value="{$smarty.session.user_id}" />
          
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" value="{$search[0].email}" id="s_user_email" name="s_user_email" placeholder="Email">
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="mobile"  name="mobile" value="{$search[0].mobile}" placeholder="Mobile no">
        </div>
      </div>
   
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">State</label>
        <div class="col-sm-10">
         
         <select name="form_state" id="form_state" class="form-control border-radius"  onchange="get_state_city(this.value);">
        
         <option value="please Select" selected="selected" >please Select</option>
      
						{section name=data loop=$categories}
                        
  <option value="{$categories[data].state}" {if $search[0].state==$categories[data].state} selected="selected" {/if} >{$categories[data].state} </option>
   {/section}
               
                 
</select>
     
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">City</label>
        <div class="col-sm-10">
         
          <span id="city_change">
                     <select class="form-control"  name="form_change" id="form_change" >
        <option value="{$search[0].city}" selected="selected" >{$search[0].city}</option>
       
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $search[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
        {/section}
     
</select>
     
      </div>
  </div>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Location/Address</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="loc_sen" id="loc_sen" rows="3">{$search[0].address}</textarea>
     </div>
        
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Image</label>
      <div class="col-sm-10" >
          <input type="file" name="logo"  id="logo" class="btn btn-default" style="max-width:240px;"/>
          <input type="hidden" name="old_main_img" value="{$search[0].image}" />
          {if $search[0].image!=''}
           <div class="col-sm-1" >
           <br />
          <img src="{$site_url}/images/profile_pic/{$search[0].image}" width="100" height="100" />
          </div>
          {/if}
     </div>
        
  </div>
  {if $search[0].type=='T'}
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Company Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="{$search[0].t_company_name}" id="company_name" name="company_name" placeholder="company name">
        </div>
      </div>
     
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Number of Drivers</label>
        <div class="col-sm-10">
         
         <select name="Drivers" id="Drivers" class="form-control border-radius" >
        
         <option value="0" selected="selected" >please Select</option>
      
						
                        
   <option value="0-1" {if $search[0].no_employ=="0-1"} selected="selected" {/if} >0-1 </option>
   <option value="1-3" {if $search[0].no_employ=="1-3"} selected="selected" {/if} >1-3 </option>
   <option value="3-5" {if $search[0].no_employ=="3-5"} selected="selected" {/if} >3-5 </option>
   <option value="5-10" {if $search[0].no_employ=="5-10"} selected="selected" {/if} >5-10 </option>
   <option value="10" {if $search[0].no_employ=="10"} selected="selected" {/if} >More then 10 </option>
               
                 
         </select>
     
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Experience</label>
        <div class="col-sm-10">
         
         <select name="experience" id="experience" class="form-control border-radius" >
        
         <option value="0" selected="selected" >please Select</option>
      
						
                        
   <option value="0-1" {if $search[0].experiance=="0-1"} selected="selected" {/if} >0-1 Years</option>
   <option value="1-3" {if $search[0].experiance=="1-3"} selected="selected" {/if} >1-3 Years</option>
   <option value="3-5" {if $search[0].experiance=="3-5"} selected="selected" {/if} >3-5 Years</option>
   <option value="5-10" {if $search[0].experiance=="5-10"} selected="selected" {/if} >5-10 Years</option>
   <option value="10" {if $search[0].experiance=="10"} selected="selected" {/if} >More then 10 Years</option>
               
                 
         </select>
     
      </div>
  </div>
   
  <div class="form-group" align="left" >
        <label for="inputEmail3" class="col-sm-2 control-label">Select your trailer types</label>
      <div class="col-sm-10" >
     
      
     
      {$functions->get_vehicle_type($search[0].vehicle_type)}
      
     
     </div>
        
  </div>
  {/if}
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="submit"  id="submit" value="Submit" class="btn btn-default" >Submit</button>
        </div>
      </div>
    </form>
  </div><!-- /.bs-example -->

  
     
   

</div>
        </div>
    </div>
    <!--container end-->