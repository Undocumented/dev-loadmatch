 <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-sm-4">
            <h1>
              Notice Board
            </h1>
          </div>
          <div class="col-lg-8 col-sm-8">
            <ol class="breadcrumb pull-right">
              <li>
                <a href="index.html">
                  Home
                </a>
              </li>
              
              <li class="active">
                Notice Board
              </li>
             <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="container" >
      <div class="row">
</div>

 {if $news_info}
          {section name=data loop=$news_info}
          <p style="background-color:#999; color:#FFF;">
          Title: {$news_info[data].heading} &nbsp;Date:{$news_info[data].entry_date}
          </p>
          <p>
           {$news_info[data].content}
          </p>
   {/section}
       {/if}
        <!-- End row -->

      </div>
      <!-- End container -->
    </div>


    <!--container end-->
