 
<!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Registration</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Pages</a></li>
                        <li class="active">Registration</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="registration-bg">
        <div class="container">

            <form class="form-signin wow fadeInUp" action="" method="post" name="reg">
                <h2 class="form-signin-heading">Register now</h2>
                <div class="login-wrap">
                    <p>Enter personal details</p>
                    <input type="text" class="form-control" name="user_name" id="user_name" placeholder="Full Name" autofocus="">
                    <input type="text" class="form-control"  name="address" id="address" placeholder="Address" autofocus="">
                    <input type="text" class="form-control" name="email" id="email" placeholder="UserID" autofocus="">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" >
                     <input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Mobile" autofocus="">
                     
                     <div class="controls">                  
                    <select name="state" id="state" class="form-control input-sm border-radius" style="font-size:13px;"  onchange="get_state_city(this.value);">
        
        <option value="" selected="selected" >Please Select State</option>
      
						{section name=data loop=$categories}
                        
  <option value="{$categories[data].state}" {if $useredit[0].state==$categories[data].state} selected="selected" {/if} >{$categories[data].state} </option>
   {/section}
               
                 
</select></br>
</div>
 <span id="city_change">
                     <select  name="form_change" id="form_change" class="form-control input-sm border-radius" style="font-size:13px;">
                     {if $edit_id==''}
        <option value="" selected="selected" >Please Select City</option>
        {else}
        
        {section name=data loop=$city_cat}
        <option value="{$city_cat[data].city}" {if $useredit[0].city==$city_cat[data].city} selected="selected" {/if} >{$city_cat[data].city} </option>
        {/section}
     {/if}
</select></span></br></br>
                     <p> Select User Type</p>
                     <div class="radios">
                        <label class="label_radio col-lg-6 col-sm-6" for="radio">
                            <input name="user_type" id="radio-01" value="C" type="radio" {if $type=="1"} checked="" {/if}> Customer
                        </label>
                        <label class="label_radio col-lg-6 col-sm-6" for="radio">
                            <input name="user_type" id="radio-02" value="T" type="radio" {if $type!="1"} checked="" {/if}> Transporter
                        </label>
                         <label class="label_radio col-lg-6 col-sm-6" for="radio">
                            <input name="user_type" id="radio-03" value="B" type="radio" {if $type=="1"} checked="" {/if}> Broker
                        </label>
                        <label class="label_radio col-lg-6 col-sm-6" for="radio">
                            <input name="user_type" id="radio-04" value="A" type="radio" {if $type!="1"} checked="" {/if}> Admin
                        </label>
                    </div>
                    
                    <label class="checkbox">
                        <input type="checkbox" value="1" id="checkbox1" name="checkbox1" checked="checked"> I agree to the <a href="terms.html" target="_new">Terms of Service</a> and <a href="privacy.html" target="_new">Privacy Policy</a>
                    </label>
                    <button class="btn btn-lg btn-login btn-block" type="submit" name="submit"  onclick="return Valid_add_customerfront(document.reg);">Submit</button>

                    <div class="registration">
                        Already Registered ?
                        <a class="" href="login.html">
                            Login
                        </a>
                    </div>
                </div>
            </form>

        </div>
     </div>