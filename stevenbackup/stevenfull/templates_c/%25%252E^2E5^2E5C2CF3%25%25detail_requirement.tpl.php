<?php /* Smarty version 2.6.19, created on 2017-07-16 09:00:38
         compiled from admin/detail_requirement.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'admin/detail_requirement.tpl', 36, false),)), $this); ?>
<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>View Shipment Detail &nbsp;<?php if ($this->_tpl_vars['type'] == 'B'): ?> [Booked] <?php elseif ($this->_tpl_vars['type'] == 'CT'): ?> [Complted] <?php elseif ($this->_tpl_vars['type'] == 'C'): ?> [Cencle] <?php else: ?> <?php endif; ?></h2> 
                     
                    </div>
                </div>
                      
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b> Shipment Detail</b>	
                           
                            
                        </div>
                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered ">
                                    <tbody>
                                        <tr>
                                                      
                                        	<th>Customer Name</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['userdata'][0]['user_id']); ?>
</td>
                                          <th>Email Address</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_email($this->_tpl_vars['userdata'][0]['user_id']); ?>
</td>
                                            </tr>
                                            <tr>
                                              <th>Mobile</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_mobile($this->_tpl_vars['userdata'][0]['user_id']); ?>
</td>
                                            <th>Enter Date</th>
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['userdata'][0]['entery_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                        </tr>
                                        <tr>
                                              <th>Province</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_State($this->_tpl_vars['userdata'][0]['user_id']); ?>
</td>
                                            <th>city</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_city($this->_tpl_vars['userdata'][0]['user_id']); ?>
</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                         <table class="table table-striped table-bordered table-hover">
                                          <tbody>
                                        <tr>
                                              <th>Category</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['userdata'][0]['category']); ?>
</td>
                                            <th>Sub Category</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['userdata'][0]['sub_category']); ?>
</td>
                                        </tr>
                                        <tr>
                                              <th>form</th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['form_city']; ?>
</td>
                                            <th>destination</th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['to_city']; ?>
</td>
                                        </tr>
                                         <tr>
                                              <th>dimension</th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['Length']; ?>
*<?php echo $this->_tpl_vars['userdata'][0]['Width']; ?>
*<?php echo $this->_tpl_vars['userdata'][0]['Height']; ?>
</td>
                                            <th>Weight</th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['Weight']; ?>
 KG</td>
                                        </tr>
                                         <tr>
                                              
                                            <th>Qty</th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['Qty']; ?>
 Pice</td>
                                        </tr>
                                        </tbody>
                                        </table>
                                         <table class="table table-striped table-bordered table-hover">
                                          <tbody>
                                         <tr>
                                              <th>Pickup Start Date</th>
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['userdata'][0]['pickup_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                            <th>pickup End Date</th>
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['userdata'][0]['pickup_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                        </tr>
                                         <tr>
                                              <th>Delivery Start Date</th>
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['userdata'][0]['delv_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                            <th>Delivery End_Date</th>
                                            <td><?php echo ((is_array($_tmp=$this->_tpl_vars['userdata'][0]['delv_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</td>
                                        </tr>
                                                                               
                                        <tr>
                                        <td colspan="4">
                                        <table>
                                        <tr>
                                        <th>description: &nbsp; </th>
                                            <td><?php echo $this->_tpl_vars['userdata'][0]['description']; ?>
</td>
                                        </tr>
                                        </table>
                                        
                                        </td>
                                        </tr>
                                        
                                    </tbody>
                                    
                                </table>
                                
                            </div>
                            
                        </div>
                        
                        
                        
                        
                    </div>
                    
                    <?php if ($this->_tpl_vars['type'] == 'B' || $this->_tpl_vars['type'] == 'CT'): ?>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                            <b> Transporter Detail</b>
                                                
                        </div>
                          <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>
                                        <tr>
                                                      
                                        	<th>Transporter Name</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['userdata12'][0]['agent_id']); ?>
</td>
                                          <th>Email Address</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_email($this->_tpl_vars['userdata12'][0]['agent_id']); ?>
</td>
                                            </tr>
                                            <tr>
                                              <th>Mobile</th>
                                            <td><?php echo $this->_tpl_vars['functions']->get_mobile($this->_tpl_vars['userdata12'][0]['agent_id']); ?>
</td>
                                            <th>Quotation Price</th>
                                            <td><?php echo $this->_tpl_vars['userdata12'][0]['starting_price']; ?>
</td>
                                        </tr>
                                            <th>Payment Methods</th>
                                            <td><?php echo $this->_tpl_vars['userdata12'][0]['payment_methods']; ?>
</td>
                                            <th>Payment Accepted</th>
                                            <td><?php echo $this->_tpl_vars['userdata12'][0]['payment_accepted']; ?>
</td>
                                        </tr>
                                        
                                    </tbody>
                                    
                                </table>
                            </div>
                            
                        </div>
                   
                    </div>
                    <?php endif; ?>
                    <!--End Advanced Tables -->
                          <div class="panel panel-default">
                        <div class="panel-heading">
                            <b> Shipment Status Detail</b>
                                                
                        </div>
                          <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                     <?php echo $this->_tpl_vars['functions']->get_history($this->_tpl_vars['userdata12'][0]['order_id']); ?>

                  <div class="bg-lg5" style="width:102.5%; margin-left:-15px;">
            <table class="table table-bordered">
              
                <thead style="background-color:#CCC;">
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Status
                    </th>
                    <th>
                     Date/Time
                    </th>
                    <th>
                     Activites
                    </th>
                    <th>
                     Details
                    </th>
                   </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['status_history']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['status_history']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    
                    <td>
                      <?php echo $this->_tpl_vars['status_history'][$this->_sections['data']['index']]['new_status']; ?>

                    </td>
                    <td>
                    <?php echo ((is_array($_tmp=$this->_tpl_vars['status_history'][$this->_sections['data']['index']]['pickup_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
-<?php echo $this->_tpl_vars['status_history'][$this->_sections['data']['index']]['pickup_time']; ?>

                    </td>
                    <td>
                       <?php echo $this->_tpl_vars['status_history'][$this->_sections['data']['index']]['Activites']; ?>

                    </td>
                    <td>
                       <?php echo $this->_tpl_vars['status_history'][$this->_sections['data']['index']]['Details']; ?>

                    </td>
                   </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
                </div>
                            
                        </div>
                   
                    </div>
                </div>
            </div>
                <!-- /. ROW  -->
            
                <!-- /. ROW  -->
            
             
        </div>
               
    </div>