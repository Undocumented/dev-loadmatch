<?php /* Smarty version 2.6.19, created on 2017-07-10 19:13:44
         compiled from detail_quote.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'detail_quote.tpl', 63, false),)), $this); ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1> Transporter Detail </h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#"> Transporter Detail</a></li>
                          <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
 <div class="white-bg">

        <!-- career -->
    <div class="container career-inner">
        <div class="row">
            <div class="col-md-12 career-head">
                <h1>Order id :<?php echo $this->_tpl_vars['search'][0]['order_id']; ?>

</h1> 

            </div>
            <form method="post" action="#" name="book" style="text-align:right;" > 
                    <input type="hidden" value="<?php echo $this->_tpl_vars['search'][0]['order_id']; ?>
" name="order_id" />
                    <input type="hidden" value="<?php echo $this->_tpl_vars['search'][0]['id']; ?>
" name="id" />
                    <input type="submit" name="submit" value="Book Now" class="btn btn-info" onclick="return confirm('Do You  want to book it ?');"/>
                    
                    </form>		
        </div>
        <hr>
       
         <table align="center" width="100%" border="0">
     <tr>
     <td>
        <div class="row" style="background-color:#CCC;">
            <div class="col-md-6">
                <div class="candidate wow fadeInLeft"  style="text-align:left;">
                    <h1> Transporter Detail</h1>
                    <p class="align-left"></p>
                    <h4></h4>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-angle-right pr-10"></i> Transporter Name :<?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][0]['agent_id']); ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i> Transporter Province :<?php echo $this->_tpl_vars['functions']->get_State($this->_tpl_vars['search'][0]['agent_id']); ?>
 </li>
                        <li><i class="fa fa-angle-right pr-10"></i> Transporter City :<?php echo $this->_tpl_vars['functions']->get_city($this->_tpl_vars['search'][0]['agent_id']); ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i> location :<?php echo $this->_tpl_vars['functions']->get_location($this->_tpl_vars['search'][0]['agent_id']); ?>
</li>
                                            
                    </ul>
                                 </div>
                
                <div class="candidate wow fadeInLeft"  style="text-align:left;">
                    <h1>Pickup Dates</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                         <li><i class="fa fa-angle-right pr-10"></i>Start Date :<?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['collect_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>End Date :<?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['collect_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
 </li>
                       
                    </ul>
                    
                </div>
               
            </div>
            <div class="col-md-6">
                <div class="candidate wow fadeInRight"  style="text-align:left;">
                    <h1>Payment Detail</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                     <li><i class="fa fa-angle-right pr-10"></i>Quote Price :(<?php echo $this->_tpl_vars['functions']->get_currency(); ?>
)<?php echo $this->_tpl_vars['search'][0]['starting_price']; ?>
</li>
                         <li><i class="fa fa-angle-right pr-10"></i>Payment Methods :<?php echo $this->_tpl_vars['search'][0]['payment_methods']; ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Payment Accepted :<?php echo $this->_tpl_vars['search'][0]['payment_accepted']; ?>
 </li>
                        <li><i class="fa fa-angle-right pr-10"></i>Payment Terms :<?php echo $this->_tpl_vars['search'][0]['payment_terms']; ?>
</li>
                        
                    </ul>
                  
                </div>
                
                <div class="candidate wow fadeInRight" style="text-align:left;">
                    <h1>Delivery Dates</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                       <li><i class="fa fa-angle-right pr-10"></i>Start Date :<?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['delivery_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>End Date :<?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['delivery_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
 </li>
                    </ul>
                   
                </div>
               
                
            </div>
             
        </div>
        <div class="row" style="background-color:#9FC">
         <div class="col-md-6">
                <div class="candidate wow fadeInRight"  style="text-align:left;">
                    <h1>Available Vehicle</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                     <?php echo $this->_tpl_vars['functions']->get_agent_vehicle_front($this->_tpl_vars['search'][0]['agent_id']); ?>

                        
                    </ul>
                  
                </div>
               
                
        </div>
         <div class="col-md-6">
                <div class="candidate wow fadeInRight"  style="text-align:left;">
                    <h1>Other Detail</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                      <li><i class="fa fa-angle-right pr-10"></i>Experience :<?php echo $this->_tpl_vars['functions']->get_agent_detail_experience($this->_tpl_vars['search'][0]['agent_id']); ?>
 Years</li>
                                          <li><i class="fa fa-angle-right pr-10"></i><a onclick="view_agent_review('<?php echo $this->_tpl_vars['search'][0]['agent_id']; ?>
',document.view_detail);" style="font-style:bold; color:#F00;cursor: pointer;">TRANSPORTER REVIEW</a></li>
                    </ul>
                  
                </div>
              
                
        </div>
        </div>
         </td>
        </tr>   
        </table>   
    <!-- career -->
       </div>
    </div>
  