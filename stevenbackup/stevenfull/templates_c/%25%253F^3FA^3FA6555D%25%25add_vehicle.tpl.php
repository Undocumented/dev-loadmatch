<?php /* Smarty version 2.6.19, created on 2017-06-13 12:01:02
         compiled from admin/add_vehicle.tpl */ ?>
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
        <h2><?php if ($this->_tpl_vars['edit_id'] == ''): ?>Add <?php else: ?>Edit<?php endif; ?> Vehicle Type</h2>
      </div>
    </div>
    <!-- /. ROW  -->
    <hr />
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading"> <?php if ($this->_tpl_vars['edit_id'] == ''): ?>Add<?php else: ?>Edit<?php endif; ?> Vehicle Type
          <h4><?php echo $this->_tpl_vars['show_message']; ?>
</h4> 
          
          
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
                
                <!--===============customer form======================-->
                <form role="form" name="add_new_customer" id="add_new_customer" action="" method="post">
                  <input type="hidden" name="edit_id" id="edit_id" value="<?php echo $this->_tpl_vars['edit_id']; ?>
" />
                     <div class="form-group">
                    <label>Vehicle Type</label>
                    <input class="form-control" type="text" name="vehicle_type_name" id="vehicle_type_name" value="<?php echo $this->_tpl_vars['useredit'][0]['vehicle_type']; ?>
" placeholder="Enter Vehicle Type " />
                   </div>
                 
                  <button type="submit" class="btn btn-primary" name="add_new_vehicle" value="add_new_vehicle" onclick="return Valid_add_vehicel(document.add_new_customer);"> Submit</button>
                </form>
                <!--==================================================--> 
                
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>