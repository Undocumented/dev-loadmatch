<?php /* Smarty version 2.6.19, created on 2017-06-15 11:16:13
         compiled from admin/add_testimonial.tpl */ ?>
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
                        
            <h2 class="page-header"><?php if ($this->_tpl_vars['edit_id'] != ''): ?>Update <?php else: ?>Add <?php endif; ?> Testimonial
                     </h2>
       </div>
    </div>
    <!-- /. ROW  -->
   
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading">
        <b><?php if ($this->_tpl_vars['edit_id'] != ''): ?>Update <?php else: ?>Add <?php endif; ?> Testimonial</b>
         
 </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
        <?php if ($this->_tpl_vars['sucess'] != ''): ?>
               
    				 <div class="notibar msgsuccess">
                        <a class="close"></a>
                        <p>Service Added Successfully</p>
                    </div><!-- notification msgsuccess -->
                  
                <?php endif; ?> 
        <form class="stdform stdform2" name="frm_addcountry" id="frm_addcountry" method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="edit_id" value="<?php echo $this->_tpl_vars['edit_id']; ?>
" />  
         <input type="hidden" name="old_country_name" value="<?php echo $this->_tpl_vars['edit_data'][0]['country']; ?>
" />  
                    
                    	 <div class="form-group">
                        	<label>Name</label> <span  class="error_alert"></span>
                              
                             <input type="text" name="Name" id="alert_name" value="<?php echo $this->_tpl_vars['userdata'][0]['name']; ?>
" class="form-control" placeholder="Enter the  Name" required />
            
                          </div> 
                    
                         
                         <div class="form-group">
                        	<label>Image</label><span class="error_alert"></span> 
                          
                              <input type="file" name="image" id="alert_image" class="form-control" value="<?php echo $this->_tpl_vars['userdata'][0]['img']; ?>
" placeholder="Enter the image"  />
                             <?php if ($this->_tpl_vars['useredit'][0]['img'] != ''): ?>
                          <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/location_flage/<?php echo $this->_tpl_vars['userdata'][0]['img']; ?>
" height="80" width="100" />
                            <?php endif; ?>
                           <input type="hidden" name="image_old"  value="<?php echo $this->_tpl_vars['userdata'][0]['img']; ?>
" />
                           
                      </div>
                           
                    
                        <div class="form-group">
                        	<label>Description</label> <span  class="error_alert"></span>
                           
                            <textarea name="description" id="alert_description" class="form-control" placeholder="Enter the description"  required /><?php echo $this->_tpl_vars['userdata'][0]['description']; ?>
</textarea>
                           
                       </div>
                       
                        
                        
                        
                     
                        
                            <button type="submit"  name="submit1" value="Submit"  class="btn btn-default"><?php if ($this->_tpl_vars['edit_id'] == ''): ?>Submit<?php else: ?>Update<?php endif; ?></button>
                           
                       
                    </form>
 			     
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>