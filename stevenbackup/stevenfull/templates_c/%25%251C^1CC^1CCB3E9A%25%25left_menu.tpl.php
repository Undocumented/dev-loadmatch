<?php /* Smarty version 2.6.19, created on 2017-07-10 17:42:10
         compiled from admin/left_menu.tpl */ ?>
<nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/assets/img/find_user.png" class="user-image img-responsive"/>
					</li>
				    <li>
                        <a <?php if ($this->_tpl_vars['action'] == admin_home): ?> class="active-menu" <?php endif; ?> href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php"><i class="fa fa-dashboard fa-2x" style="width:30px;"></i>Dashboard</a>
                    </li>
                    
                    		<li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'edit_company' || $this->_tpl_vars['action'] == 'payment_gatway_setting' || $this->_tpl_vars['action'] == 'social_setting'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-cog fa-2x" style="width:30px;" style="width:50px;"></i> General Setting<span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level<?php if ($this->_tpl_vars['action'] == 'edit_company' || $this->_tpl_vars['action'] == 'payment_gatway_setting' || $this->_tpl_vars['action'] == 'social_setting'): ?> collapse in <?php endif; ?>" >
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=edit_company" <?php if ($this->_tpl_vars['action'] == 'edit_company'): ?> class="active-menu-sub" <?php endif; ?>>General Setting </a>
                            </li>
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=payment_gatway_setting" <?php if ($this->_tpl_vars['action'] == 'payment_gatway_setting'): ?> class="active-menu-sub" <?php endif; ?>>Payment Gateway Setting </a>
                            </li>
                               <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=social_setting" <?php if ($this->_tpl_vars['action'] == 'social_setting'): ?> class="active-menu-sub" <?php endif; ?>>Social Setting</a>
                            </li>
                             
                            </ul>
                    </li>	
                     <li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'show_sub_cat_list' || $this->_tpl_vars['action'] == 'add_cat' || $this->_tpl_vars['action'] == 'show_cat_list'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-sitemap fa-2x" style="width:30px;"></i> Category <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if ($this->_tpl_vars['action'] == 'add_cat' || $this->_tpl_vars['action'] == 'show_cat_list' || $this->_tpl_vars['action'] == 'show_sub_cat_list'): ?> collapse in  <?php endif; ?>">
                            <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=add_cat&type=M" <?php if ($this->_tpl_vars['action'] == 'add_cat' && $this->_tpl_vars['type'] == 'M'): ?> class="active-menu-sub" <?php endif; ?>>Load Industry </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=add_cat&type=S" <?php if ($this->_tpl_vars['action'] == 'add_cat' && $this->_tpl_vars['type'] == 'S'): ?> class="active-menu-sub" <?php endif; ?>>Load Type </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=add_cat&type=SN" <?php if ($this->_tpl_vars['action'] == 'add_cat' && $this->_tpl_vars['type'] == 'SN'): ?> class="active-menu-sub" <?php endif; ?>>Transport Type </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=add_cat&type=FC" <?php if ($this->_tpl_vars['action'] == 'add_cat' && $this->_tpl_vars['type'] == 'FC'): ?> class="active-menu-sub" <?php endif; ?>>Body & Bulk Type </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=add_cat&type=FFC" <?php if ($this->_tpl_vars['action'] == 'add_cat' && $this->_tpl_vars['type'] == 'FFC'): ?> class="active-menu-sub" <?php endif; ?>>Size Classification </a>
                            </li>
                            
                            <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=add_cat&type=SC" <?php if ($this->_tpl_vars['action'] == 'add_cat' && $this->_tpl_vars['type'] == 'SC'): ?> class="active-menu-sub" <?php endif; ?>>Payload KG </a>
                            </li>
                            
                            <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=add_cat&type=SVC" <?php if ($this->_tpl_vars['action'] == 'add_cat' && $this->_tpl_vars['type'] == 'SVC'): ?> class="active-menu-sub" <?php endif; ?>>Length Metres </a>
                            </li>
                            
                              <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=add_cat&type=EC" <?php if ($this->_tpl_vars['action'] == 'add_cat' && $this->_tpl_vars['type'] == 'EC'): ?> class="active-menu-sub" <?php endif; ?>>Width Metres </a>
                            </li>
                            
                            <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=add_cat&type=NC" <?php if ($this->_tpl_vars['action'] == 'add_cat' && $this->_tpl_vars['type'] == 'NC'): ?> class="active-menu-sub" <?php endif; ?>>Preffered Distance </a>
                            </li>
                            
                            <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=add_cat&type=TC" <?php if ($this->_tpl_vars['action'] == 'add_cat' && $this->_tpl_vars['type'] == 'TC'): ?> class="active-menu-sub" <?php endif; ?>>Other </a>
                            </li>
                            
                            
                            
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=show_cat_list&type=M" <?php if ($this->_tpl_vars['action'] == 'show_cat_list' || $this->_tpl_vars['action'] == 'show_sub_cat_list'): ?> class="active-menu-sub" <?php endif; ?>>View Categories </a>
                            </li>
                             
                            </ul>
                                             
                      </li>  
                            <li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'add_users' || $this->_tpl_vars['action'] == 'show_user_list'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-user fa-2x" style="width:30px;"></i>User <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if ($this->_tpl_vars['action'] == 'add_users' || $this->_tpl_vars['action'] == 'show_user_list'): ?> collapse in<?php endif; ?>">
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=add_users" <?php if ($this->_tpl_vars['action'] == 'add_users'): ?> class="active-menu-sub" <?php endif; ?>>Add User </a>
                            </li>                                         
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=show_user_list" <?php if ($this->_tpl_vars['action'] == 'show_user_list'): ?> class="active-menu-sub" <?php endif; ?>>View User </a>
                            </li>
                          
                             
                            </ul>
                                             
                      </li> 
                                               
                      </li>  
                            <li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'show_cust_review' || $this->_tpl_vars['action'] == 'show_trans_review'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-user fa-2x" style="width:30px;"></i>Review<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if ($this->_tpl_vars['action'] == 'show_cust_review' || $this->_tpl_vars['action'] == 'show_trans_review'): ?> collapse in<?php endif; ?>">
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=show_cust_review" <?php if ($this->_tpl_vars['action'] == 'show_cust_review'): ?> class="active-menu-sub" <?php endif; ?>>Customer </a>
                            </li>                                         
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=show_trans_review" <?php if ($this->_tpl_vars['action'] == 'show_trans_review'): ?> class="active-menu-sub" <?php endif; ?>>Transporter</a>
                            </li>
                          
                             
                            </ul>
                                             
                      </li> 
                      
                      
                      
                      
                      
                      
                      
                      
                            <li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'add_testimonial' || $this->_tpl_vars['action'] == 'show_testimonial_list'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-user fa-2x" style="width:30px;"></i>Testimonial<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if ($this->_tpl_vars['action'] == 'add_imonial' || $this->_tpl_vars['action'] == 'show_imonial_list'): ?> collapse in<?php endif; ?>">
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=add_testimonial" <?php if ($this->_tpl_vars['action'] == 'add_testimonial'): ?> class="active-menu-sub" <?php endif; ?>>Add Testimonial </a>
                            </li>
                           
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=show_testimonial_list" <?php if ($this->_tpl_vars['action'] == 'show_testimonial_list'): ?> class="active-menu-sub" <?php endif; ?>>View Testimonial </a>
                            </li>
                          
                             
                            </ul>
                                             
                      </li> 
                      
                      
                      <li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'add_service' || $this->_tpl_vars['action'] == 'show_service_list'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-user fa-2x" style="width:30px;"></i>Service <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if ($this->_tpl_vars['action'] == 'add_service' || $this->_tpl_vars['action'] == 'show_service_list'): ?> collapse in<?php endif; ?>">
                             <!--<li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=add_service" <?php if ($this->_tpl_vars['action'] == 'add_service'): ?> class="active-menu-sub" <?php endif; ?>>Add Service </a>
                            </li>-->
                           
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=show_service_list" <?php if ($this->_tpl_vars['action'] == 'show_service_list'): ?> class="active-menu-sub" <?php endif; ?>>View Service </a>
                            </li>
                          
                             
                            </ul>
                                             
                      </li>
                       
                   
                   <li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'booked_shipment' || $this->_tpl_vars['action'] == 'detail_requirement'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-truck fa-2x" style="width:30px;"></i>Shipment <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level<?php if ($this->_tpl_vars['action'] == booked_shipment || $this->_tpl_vars['action'] == detail_requirement): ?> collapse in <?php endif; ?>">
                         <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=booked_shipment" <?php if ($this->_tpl_vars['action'] == 'booked_shipment' && $this->_tpl_vars['type'] == ""): ?> class="active-menu-sub" <?php endif; ?>>Active </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=booked_shipment&type=B" <?php if ($this->_tpl_vars['action'] == 'booked_shipment' && $this->_tpl_vars['type'] == 'B'): ?> class="active-menu-sub" <?php endif; ?>>Undelivered </a>
                            </li>
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=booked_shipment&type=CT" <?php if ($this->_tpl_vars['action'] == 'booked_shipment' && $this->_tpl_vars['type'] == 'CT'): ?> class="active-menu-sub" <?php endif; ?>>Delivered </a>
                            </li>
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=booked_shipment&type=C" <?php if ($this->_tpl_vars['action'] == 'booked_shipment' && $this->_tpl_vars['type'] == 'C'): ?> class="active-menu-sub" <?php endif; ?>>Cancel </a>
                            </li>
                             
                            </ul>
                    </li>	
                   
                   
                   
						<li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'approve_requirement' || $this->_tpl_vars['action'] == 'approve_review'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-thumbs-o-up fa-2x" style="width:30px;"></i>Approve Center <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level <?php if ($this->_tpl_vars['action'] == 'approve_requirement' || $this->_tpl_vars['action'] == 'approve_review'): ?> collapse in<?php endif; ?>">
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=approve_requirement" <?php if ($this->_tpl_vars['action'] == 'approve_requirement'): ?> class="active-menu-sub" <?php endif; ?>>New Shipment </a>
                            </li>
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=approve_review" <?php if ($this->_tpl_vars['action'] == 'approve_review'): ?> class="active-menu-sub" <?php endif; ?>>User Review </a>
                            </li>
                             
                            </ul>
                    </li>	
                    
                     <li>
                        <a  href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=show_payment" <?php if ($this->_tpl_vars['action'] == 'show_payment'): ?> class="active-menu" <?php endif; ?>><i class="fa fa-money fa-2x" style="width:30px;"></i> Payment</a>
                    </li>
                    <li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'show_quote' || $this->_tpl_vars['action'] == 'show_quote_agent' || $this->_tpl_vars['action'] == 'detail_quote'): ?> class="active-menu" <?php endif; ?>><i class="fa fa-bar-chart-o fa-2x" style="width:30px;"></i>Transporter Placed Quote <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level <?php if ($this->_tpl_vars['action'] == 'show_quote' || $this->_tpl_vars['inactive'] == 'inactive'): ?> collapse in<?php endif; ?>">
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=show_quote" <?php if ($this->_tpl_vars['action'] == 'show_quote' && $this->_tpl_vars['inactive'] != 'inactive'): ?> class="active-menu-sub" <?php endif; ?>>Active Quote</a>
                            </li>
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=show_quote&type=inactive" <?php if ($this->_tpl_vars['inactive'] == 'inactive'): ?> class="active-menu-sub" <?php endif; ?>>Inactive Quote </a>
                            </li>
                             
                            </ul>
                    </li>	
                                        <li>
                        <a  href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=payment&action=Package" <?php if ($this->_tpl_vars['action'] == 'Package'): ?> class="active-menu" <?php endif; ?>><i class="fa fa-credit-card fa-2x" style="width:30px;"></i>Service Charge</a>
                    </li>
                    
                    <li>
                        <a  href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=mails&action=show_mails" <?php if ($this->_tpl_vars['action'] == 'show_mails'): ?> class="active-menu" <?php endif; ?>><i class="fa fa-edit fa-2x" style="width:30px;"></i>Mails Templates </a>
                    </li>	
                            
                    <li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'add_news' || $this->_tpl_vars['action'] == 'show_news'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-newspaper-o fa-2x" style="width:30px;"></i>News <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level <?php if ($this->_tpl_vars['action'] == 'add_news' || $this->_tpl_vars['action'] == 'show_news'): ?> collapse in<?php endif; ?>">
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=mails&action=add_news" <?php if ($this->_tpl_vars['action'] == 'add_news'): ?> class="active-menu-sub" <?php endif; ?>>Add News </a>
                            </li>
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=mails&action=show_news" <?php if ($this->_tpl_vars['action'] == 'show_news'): ?> class="active-menu-sub" <?php endif; ?>>Show News </a>
                            </li>
                             
                            </ul>
                    </li>	
                      <li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'add_vehicle' || $this->_tpl_vars['action'] == 'show_vehicle'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-bus fa-2x" style="width:30px;"></i>Vehicle Management <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level <?php if ($this->_tpl_vars['action'] == 'add_vehicle' || $this->_tpl_vars['action'] == 'show_vehicle'): ?> collapse in<?php endif; ?>">
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=add_vehicle" <?php if ($this->_tpl_vars['action'] == 'add_vehicle'): ?> class="active-menu-sub" <?php endif; ?>>Add Vehicle </a>
                            </li>
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=category&action=show_vehicle" <?php if ($this->_tpl_vars['action'] == 'show_vehicle'): ?> class="active-menu-sub" <?php endif; ?>>Show Vehicle</a>
                            </li>
                             
                            </ul>
                    </li>                   
                  
                  <li>
                        <a  href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=add_banner&action=add_slider" <?php if ($this->_tpl_vars['action'] == 'add_slider'): ?> class="active-menu" <?php endif; ?>><i class="fa fa-spinner fa-2x" style="width:30px;"></i> Slider Setting</a>
                    </li>	
                    
                    <li>
                        <a  href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=admin_home&action=show_message" <?php if ($this->_tpl_vars['action'] == 'show_message'): ?> class="active-menu" <?php endif; ?>><i class="fa fa-envelope-o fa-2x" style="width:30px;"></i>Web Inquries</a>
                    </li>	
<li>
                       

                     <li>
                        <a href="#" <?php if ($this->_tpl_vars['action'] == 'add_notice' || $this->_tpl_vars['action'] == 'show_notice'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-bell-o fa-2x" style="width:30px;"></i>Notice Board<span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level <?php if ($this->_tpl_vars['action'] == 'add_notice' || $this->_tpl_vars['action'] == 'show_notice'): ?> collapse in<?php endif; ?>">
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=mails&action=add_notice" <?php if ($this->_tpl_vars['action'] == 'add_notice'): ?> class="active-menu-sub" <?php endif; ?>>Add Notice Board </a>
                            </li>
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=mails&action=show_notice" <?php if ($this->_tpl_vars['action'] == 'show_notice'): ?> class="active-menu-sub" <?php endif; ?>>Show Notice Board </a>
                            </li>
                             
                            </ul>
                    </li>	
                    
                    
                    
                    
                    
                    
                      <li>
                        <a href="#" <?php if ($this->_tpl_vars['page'] == 'location_mange'): ?>class="active-menu" <?php endif; ?>><i class="fa fa-globe fa-2x" style="width:30px;"></i>Location Management <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level<?php if ($this->_tpl_vars['page'] == location_mange): ?> collapse in <?php endif; ?>">
                         <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_country" <?php if ($this->_tpl_vars['action'] == 'add_country'): ?> class="active-menu-sub" <?php endif; ?>> Add Country </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_state" <?php if ($this->_tpl_vars['action'] == 'add_state'): ?> class="active-menu-sub" <?php endif; ?>> Add Province </a>
                            </li>
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_city" <?php if ($this->_tpl_vars['action'] == 'add_city'): ?> class="active-menu-sub" <?php endif; ?>> Add City </a>
                            </li>
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_town" <?php if ($this->_tpl_vars['action'] == 'add_town'): ?> class="active-menu-sub" <?php endif; ?>> Add Town </a>
                            </li>
                             <li>
                                <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=show_location_list" <?php if ($this->_tpl_vars['action'] == 'show_location_list'): ?> class="active-menu-sub" <?php endif; ?>> Country List</a>
                            </li>
                             
                            </ul>
                    </li>	
                   
                    
                    <li>
                        <a  href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=user&action=logout"><i class="fa fa-lock fa-2x" style="width:30px;"></i>Logout</a>
                    </li>	
                </ul>
               
            </div>
            
        </nav>