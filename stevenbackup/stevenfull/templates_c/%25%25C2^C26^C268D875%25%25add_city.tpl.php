<?php /* Smarty version 2.6.19, created on 2017-04-25 13:09:43
         compiled from admin/add_city.tpl */ ?>
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
                        
           <h1 class="page-header"><?php if ($this->_tpl_vars['edit_id'] != ''): ?>Edit<?php else: ?>Add<?php endif; ?> City
                     </h1>    
         </div>
    </div>
  
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading">
      <b><?php if ($this->_tpl_vars['edit_id'] != ''): ?>Edit<?php else: ?>Add<?php endif; ?> City  </b>
    </div>
          <div class="panel-body">
            <div class="row">
            
              <div class="col-md-8"> 
              <?php if ($this->_tpl_vars['sucess']): ?><span style="color:#F00; text-align:center">success....! <?php echo $this->_tpl_vars['sucess']; ?>
</span>
                    <?php endif; ?>
        <form class="stdform stdform2" name="frm_addcity" id="frm_addcity" method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="edit_id" value="<?php echo $this->_tpl_vars['edit_id']; ?>
" />  
        <input type="hidden" name="State_name" value="<?php echo $this->_tpl_vars['State_name']; ?>
" /> 
        <input type="hidden" name="Country_name" value="<?php echo $this->_tpl_vars['Country_name']; ?>
" /> 
        <input type="hidden" name="old_city_name" value="<?php echo $this->_tpl_vars['edit_data'][0]['city']; ?>
" /> 
      <!--  <input type="hidden" name="sel_country" value="India" /> -->
                    
                    	<div class="form-group">
                        	<label>Select Country</label><span id="alert_country" class="alert"></span>
                            
                            <?php if ($this->_tpl_vars['Country_name'] != ''): ?>
                            <?php echo $this->_tpl_vars['functions']->getCountryDropdownForAdmin($this->_tpl_vars['Country_name']); ?>

                            <?php else: ?>
						   <?php echo $this->_tpl_vars['functions']->getCountryDropdownForAdmin($this->_tpl_vars['edit_data'][0]['country']); ?>

                           <?php endif; ?>
                       </div>
                        <div class="form-group">
                        	<label>Select State</label><span id="alert_state_name" class="alert"></span>
                            <span class="field"><span id="state_dropdown_show">
                            <?php echo $this->_tpl_vars['functions']->getStateDropdownForAdmin($this->_tpl_vars['edit_data'][0]['state']); ?>
</span>
                       </span>
                       </div>
                        <div class="form-group">
                        	<label>City Name</label><span id="alert_city_name" class="alert"></span>
                           
                            <input type="text" name="city_name" id="city_name" class="form-control" value="<?php echo $this->_tpl_vars['edit_data'][0]['city']; ?>
" />
                        </div>  
                    
                        <div class="form-group">
                        	<label>Title</label> <span id="alert_title" class="alert"></span>
                           
                            <input type="text" name="title" id="title" class="form-control" value="<?php echo $this->_tpl_vars['edit_data'][0]['title']; ?>
" />
                       </div>   
                         <div class="form-group">
                           	<label>Description</label><span id="alert_description" class="alert"></span>
                            
                            <textarea name="description" id="description" class="form-control"><?php echo $this->_tpl_vars['edit_data'][0]['descen']; ?>
</textarea>
                            
                        </div>
                         <div class="form-group">
                        	<label>Keyword</label> <span id="alert_keyword" class="alert"></span>
                            
                            <textarea name="keyword" id="keyword" class="form-control"><?php echo $this->_tpl_vars['edit_data'][0]['keyword']; ?>
</textarea>
                           
                         </div>
                        
                            
                        <button type="submit" class="btn btn-default" name="city_submit" value="city_submit" onClick="return add_city_name(document.frm_addcity);">Submit</button>
                           
                           
                       
                    </form>
 			   </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>