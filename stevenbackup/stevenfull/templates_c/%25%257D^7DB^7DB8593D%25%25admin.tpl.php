<?php /* Smarty version 2.6.19, created on 2017-04-24 05:02:50
         compiled from admin/admin.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head>
	<title>Admin</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	 <link href="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/assets/css/custom.css" rel="stylesheet" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <?php if ($this->_tpl_vars['action'] == 'loginnow'): ?>
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/css/loginstyle.css">
    
    <?php endif; ?>
     

   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <script type="text/javascript">var site_url="<?php echo $this->_tpl_vars['site_url']; ?>
";</script>
   <script type="text/javascript" src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/js/ajax.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/js/validation.js"></script>
     <script type="text/javascript" src="<?php echo $this->_tpl_vars['site_url']; ?>
/ckeditor/ckeditor.js"></script>
</head>
<body onload="setFocus();">

<div id="wrapper">



<?php if ($_SESSION['adminUserId'] == '1'): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	
	
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/left_menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php endif; ?>
    
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['contentBody'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>


    <script src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/assets/js/jquery-1.10.2.js"></script>
    
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/assets/js/custom.js"></script>
    

</body>
</html>