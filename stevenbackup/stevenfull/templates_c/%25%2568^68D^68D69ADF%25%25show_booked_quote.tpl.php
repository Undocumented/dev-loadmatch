<?php /* Smarty version 2.6.19, created on 2017-07-10 13:42:31
         compiled from show_booked_quote.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'show_booked_quote.tpl', 80, false),)), $this); ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1> Shipment Quote</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#"> Shipment Quote </a></li>
                         <li><a href="<?php echo $this->_tpl_vars['site_url']; ?>
/my_post_job.html">Back</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

   <div class="container">
      
        <div class="container mar-b-30">
            <div class="row">
        
          <h4>
          Shipment Quote
          </h4>
          <div class="contact-form">
              <div class="table-responsive">
              <form name="view_detail" method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/detail_shiping.html" id="view_detail">
              <table class="table table-bordered">
              
                <thead>
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Transporter
                    </th>
                    <th>
                     Quote Price(<?php echo $this->_tpl_vars['functions']->get_currency(); ?>
)
                    </th>
                   
                    <th>
                     Payment Method
                    </th>
                    <th>
                     Payment Accepted
                    </th>
                    <th>
                     Enter Date
                    </th>
                     <th>
                     Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['search']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['search']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    <td><?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][$this->_sections['data']['index']]['agent_id']); ?>

                                       </td>
                    <td>
                     <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['starting_price']; ?>

                    </td>
                   
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['payment_methods']; ?>

                    </td>
                    <td>
                      <?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['payment_accepted']; ?>

                    </td>
                    <td>
                      <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][$this->_sections['data']['index']]['entery_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>

                    </td>
                    <td>
                    <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                     <a style="cursor:pointer;" onclick="submitbuy123('<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
',document.view_detail);" class="btn btn-primary btn-sm"/>Show</a>
                     <input type="hidden" name="id"  value="<?php echo $this->_tpl_vars['search'][$this->_sections['data']['index']]['id']; ?>
" />
                     <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/trans_fedback-<?php echo $this->_tpl_vars['search2'][0]['transpoter_id']; ?>
.html" class="btn btn-primary btn-sm"/>Show Transporter Feedback</a>
                    </td>
                  </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
           
            </form>
<form action="" method="post">
                <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" /><?php echo $this->_tpl_vars['show_pagi']; ?>
</td></tr>
                </form>
          </div>
        </div>
     

    </div>
    <!--container end-->



          </div>

        </div>
      </div>
    </div>
    <!--container end-->