<?php /* Smarty version 2.6.19, created on 2017-04-25 13:11:55
         compiled from admin/show_location_list.tpl */ ?>
<?php echo '
<style style="text/css">
	/* Define the hover highlight color for the table row */
    .hoverTable tr:hover {
          background-color: #ffff99;
    }
</style>
'; ?>

<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
                        <h1 class="page-header">Location List
                     </h1>
                         
         </div>
    </div>
    <!-- /. ROW  -->

    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading">
                     <table class="table table-striped table-bordered table-hover">
                   <tr>
                                <th class="head1" colspan="5">
                                <form method="POST" id="bar_code_Frm" name="bar_code_Frm" >
                             <?php if ($this->_tpl_vars['Country_name'] != ''): ?> <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_state&country_name=<?php echo $this->_tpl_vars['Country_name']; ?>
" title="Add New State"><button type="button"  class="btn btn-default" >Add State</button></a><?php endif; ?> 
                              <?php if ($this->_tpl_vars['State_name'] != ''): ?>
                                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_city&state_name=<?php echo $this->_tpl_vars['State_name']; ?>
" title="Add New City"><button type="button"  class="btn btn-default" >Add City</button></a><?php endif; ?>
                              
							 <?php if ($this->_tpl_vars['Country_name'] == '' && $this->_tpl_vars['State_name'] == ''): ?>  <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_country" title="Add New Country"> 	<button type="button"  class="btn btn-default" >Add Country </button></a><?php endif; ?>
		 						 </form>
                                </th>
                            </tr> 
                   </table>
                 <table class="table table-striped table-bordered table-hover">
                             
                            <thead>
                          
                                <tr>
                                    <th width="5%">Sr.no</th>
                                    <th width="20%">Country Name</th>
                                   <?php if ($this->_tpl_vars['Country_name'] == '' || $this->_tpl_vars['State_name'] == ''): ?>    <th width="20%">Flag</th><?php endif; ?>
                                                                       <?php if ($this->_tpl_vars['Country_name'] != '' || $this->_tpl_vars['State_name'] != ''): ?>
                                    <th width="20%">State</th>
                                    <?php endif; ?>
                                    <?php if ($this->_tpl_vars['State_name'] != ''): ?>
                                    <th width="15%">City</th>
                                    <?php endif; ?>
                                    <th width="20%">Actions</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                            <?php if ($this->_tpl_vars['location_list']): ?>
                            	<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['location_list']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                                <tr>
                                    <td><?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>
</td>
                                    <td>
                                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=show_location_list&country_name=<?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['country']; ?>
">
                                    <?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['country']; ?>

                                    </a>
                                    </td>
                                  <?php if ($this->_tpl_vars['Country_name'] == '' || $this->_tpl_vars['State_name'] == ''): ?>   <td><?php if ($this->_tpl_vars['location_list'][$this->_sections['data']['index']]['flage'] != ''): ?><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/location_flage/<?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['flage']; ?>
" height="20px" width="30px" /><?php endif; ?> </td><?php endif; ?>
                                    
                                     <?php if ($this->_tpl_vars['Country_name'] != '' || $this->_tpl_vars['State_name'] != ''): ?>
                                    <td>
                                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=show_location_list&state_name=<?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['state']; ?>
">
                                    <?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['state']; ?>

                                    </a></td>
                                    <?php endif; ?>
                                    <?php if ($this->_tpl_vars['State_name'] != ''): ?>
                                    <td><?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['city']; ?>
</td>    
                                    <?php endif; ?>
                                    <td>
                                    <!--=================================active and inactive=============================--->
                             <!-- <?php if ($this->_tpl_vars['location_list'][$this->_sections['data']['index']]['status'] == 'Y'): ?> 
                                     <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_country&active_id=<?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['id']; ?>
&active=N" title="Active">
                                    <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/img/icons/bs/ic_unlock.png" />
                                    </a>
                                    <?php else: ?>
                                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_country&active_id=<?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['id']; ?>
&active=Y" title="Inactive">
                                    <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/img/icons/bs/ic_lock.png" />
                                    </a>
                                    <?php endif; ?>-->
                                    <!--================================================================================-->
                                   <?php if ($this->_tpl_vars['Country_name'] != ''): ?>
                                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_state&edit_id=<?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['id']; ?>
" class="fa fa-pencil-square-o" title="<?php echo $this->_tpl_vars['lang_edit']; ?>
"> <?php endif; ?>
                                   <?php if ($this->_tpl_vars['State_name'] != ''): ?>
                                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_city&edit_id=<?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['id']; ?>
"class="fa fa-pencil-square-o" title="<?php echo $this->_tpl_vars['lang_edit']; ?>
"> <?php endif; ?>
                                   <?php if ($this->_tpl_vars['Country_name'] == '' && $this->_tpl_vars['State_name'] == ''): ?> 
                                   <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=add_country&edit_id=<?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['id']; ?>
" class="fa fa-pencil-square-o" title="<?php echo $this->_tpl_vars['lang_edit']; ?>
">
                                   <?php endif; ?>
                                   
                                  
                                   <!-- <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/img/icons/bs/ic_edit.png" />-->
                                    </a>
                                      <?php if ($this->_tpl_vars['Country_name'] != ''): ?>
                                    <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=show_location_list&state_delete_id=<?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['state']; ?>
" title="<?php echo $this->_tpl_vars['lang_delete']; ?>
" onclick="return confirm('<?php echo $this->_tpl_vars['lang_do_you_want_delete']; ?>
');" class="fa fa-trash-o"><?php endif; ?>
                                      <?php if ($this->_tpl_vars['State_name'] != ''): ?>
                                        <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=show_location_list&city_delete_id=<?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['city']; ?>
" title="<?php echo $this->_tpl_vars['lang_delete']; ?>
" onclick="return confirm('<?php echo $this->_tpl_vars['lang_do_you_want_delete']; ?>
');" class="fa fa-trash-o"><?php endif; ?>
                                      <?php if ($this->_tpl_vars['Country_name'] == '' && $this->_tpl_vars['State_name'] == ''): ?> 
                                          <a href="<?php echo $this->_tpl_vars['site_url']; ?>
/admin.php?page=location_mange&action=show_location_list&country_delete_id=<?php echo $this->_tpl_vars['location_list'][$this->_sections['data']['index']]['country']; ?>
" title="<?php echo $this->_tpl_vars['lang_delete']; ?>
" onclick="return confirm('<?php echo $this->_tpl_vars['lang_do_you_want_delete']; ?>
');" class="fa fa-trash-o"><?php endif; ?>
                                   <!-- <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/admin/img/icons/bs/ic_delete.png" />-->
                                    </a>
                                    </td>                            
                                </tr>
                                <?php endfor; endif; ?>
                                 <tr>
                                	<td colspan="6" align="center">
                                    	  <form name="frm_pagi" action="" method="post">
                   			
                           <input type="hidden" name="pageval" id="pagevalid" value="<?php echo $this->_tpl_vars['page_val']; ?>
" />
                           
                           
                           

                      	<?php echo $this->_tpl_vars['show_pagi']; ?>

                        </form>
                                    </td>
                                </tr>
                            <?php else: ?>
                            	<tr><td colspan="6"><?php echo $this->_tpl_vars['lang_record_not_found']; ?>
</td></tr>
                            <?php endif; ?>    
                            </tbody>
                        </table>
                    </table>
    </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>