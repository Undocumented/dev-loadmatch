<?php /* Smarty version 2.6.19, created on 2017-06-01 07:02:25
         compiled from edit_profile.tpl */ ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Edit Profile</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Edit Profile</a></li>
                        <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="component-bg">
        <div class="container">
            <!-- Forms
================================================== -->
<div class="bs-docs-section mar-b-30">
  <h1 id="forms" class="page-header">Edit Profile</h1>
 
    <form class="form-horizontal" name="edit_user"  method="post" action="<?php echo $this->_tpl_vars['site_url']; ?>
/edit_profile.html" role="form" enctype="multipart/form-data">
        <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >User Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="<?php echo $this->_tpl_vars['search'][0]['name']; ?>
" id="s_user_name" name="s_user_name" placeholder="User Name">
          <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>
" />
          
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" value="<?php echo $this->_tpl_vars['search'][0]['email']; ?>
" id="s_user_email" name="s_user_email" placeholder="Email">
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="mobile"  name="mobile" value="<?php echo $this->_tpl_vars['search'][0]['mobile']; ?>
" placeholder="Mobile no">
        </div>
      </div>
   
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">State</label>
        <div class="col-sm-10">
         
         <select name="form_state" id="form_state" class="form-control border-radius"  onchange="get_state_city(this.value);">
        
         <option value="please Select" selected="selected" >please Select</option>
      
						<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
  <option value="<?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
" <?php if ($this->_tpl_vars['search'][0]['state'] == $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
 </option>
   <?php endfor; endif; ?>
               
                 
</select>
     
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">City</label>
        <div class="col-sm-10">
         
          <span id="city_change">
                     <select class="form-control"  name="form_change" id="form_change" >
        <option value="<?php echo $this->_tpl_vars['search'][0]['city']; ?>
" selected="selected" ><?php echo $this->_tpl_vars['search'][0]['city']; ?>
</option>
       
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['search'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     
</select>
     
      </div>
  </div>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Location/Address</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="loc_sen" id="loc_sen" rows="3"><?php echo $this->_tpl_vars['search'][0]['address']; ?>
</textarea>
     </div>
        
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Image</label>
      <div class="col-sm-10" >
          <input type="file" name="logo"  id="logo" class="btn btn-default" style="max-width:240px;"/>
          <input type="hidden" name="old_main_img" value="<?php echo $this->_tpl_vars['search'][0]['image']; ?>
" />
          <?php if ($this->_tpl_vars['search'][0]['image'] != ''): ?>
           <div class="col-sm-1" >
           <br />
          <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/profile_pic/<?php echo $this->_tpl_vars['search'][0]['image']; ?>
" width="100" height="100" />
          </div>
          <?php endif; ?>
     </div>
        
  </div>
  <?php if ($this->_tpl_vars['search'][0]['type'] == 'T'): ?>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Company Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="<?php echo $this->_tpl_vars['search'][0]['t_company_name']; ?>
" id="company_name" name="company_name" placeholder="company name">
        </div>
      </div>
     
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Number of Drivers</label>
        <div class="col-sm-10">
         
         <select name="Drivers" id="Drivers" class="form-control border-radius" >
        
         <option value="0" selected="selected" >please Select</option>
      
						
                        
   <option value="0-1" <?php if ($this->_tpl_vars['search'][0]['no_employ'] == "0-1"): ?> selected="selected" <?php endif; ?> >0-1 </option>
   <option value="1-3" <?php if ($this->_tpl_vars['search'][0]['no_employ'] == "1-3"): ?> selected="selected" <?php endif; ?> >1-3 </option>
   <option value="3-5" <?php if ($this->_tpl_vars['search'][0]['no_employ'] == "3-5"): ?> selected="selected" <?php endif; ?> >3-5 </option>
   <option value="5-10" <?php if ($this->_tpl_vars['search'][0]['no_employ'] == "5-10"): ?> selected="selected" <?php endif; ?> >5-10 </option>
   <option value="10" <?php if ($this->_tpl_vars['search'][0]['no_employ'] == '10'): ?> selected="selected" <?php endif; ?> >More then 10 </option>
               
                 
         </select>
     
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Experience</label>
        <div class="col-sm-10">
         
         <select name="experience" id="experience" class="form-control border-radius" >
        
         <option value="0" selected="selected" >please Select</option>
      
						
                        
   <option value="0-1" <?php if ($this->_tpl_vars['search'][0]['experiance'] == "0-1"): ?> selected="selected" <?php endif; ?> >0-1 Years</option>
   <option value="1-3" <?php if ($this->_tpl_vars['search'][0]['experiance'] == "1-3"): ?> selected="selected" <?php endif; ?> >1-3 Years</option>
   <option value="3-5" <?php if ($this->_tpl_vars['search'][0]['experiance'] == "3-5"): ?> selected="selected" <?php endif; ?> >3-5 Years</option>
   <option value="5-10" <?php if ($this->_tpl_vars['search'][0]['experiance'] == "5-10"): ?> selected="selected" <?php endif; ?> >5-10 Years</option>
   <option value="10" <?php if ($this->_tpl_vars['search'][0]['experiance'] == '10'): ?> selected="selected" <?php endif; ?> >More then 10 Years</option>
               
                 
         </select>
     
      </div>
  </div>
   
  <div class="form-group" align="left" >
        <label for="inputEmail3" class="col-sm-2 control-label">Select your trailer types</label>
      <div class="col-sm-10" >
     
      
     
      <?php echo $this->_tpl_vars['functions']->get_vehicle_type($this->_tpl_vars['search'][0]['vehicle_type']); ?>

      
     
     </div>
        
  </div>
  <?php endif; ?>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="submit"  id="submit" value="Submit" class="btn btn-default" >Submit</button>
        </div>
      </div>
    </form>
  </div><!-- /.bs-example -->

  
     
   

</div>
        </div>
    </div>
    <!--container end-->