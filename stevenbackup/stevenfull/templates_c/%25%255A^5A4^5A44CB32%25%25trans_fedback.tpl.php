<?php /* Smarty version 2.6.19, created on 2017-06-13 15:51:44
         compiled from trans_fedback.tpl */ ?>
 <div class="main-2 clearfix">
    	<div class="container">
        	<div class="latest-titel">
      	<div class="container">
        	<h4> Transporter Name : <span style="color:red"><?php echo $this->_tpl_vars['transcust'][0]['name']; ?>
</span> </h4>
        </div>
      </div>
        </div>
    </div>
    
   <!-- <div class="container">
    	<div class="col-md-2"><a class="btn btn-warning top-y bb-btn" href="#/">Request Quote</a></div><div class="col-md-10"><div class="row"><h4 class="widget-title   top-lab youck"><span class="star-img"><img src="images/feedback_16x16.png"></span><a class="uo" href="#/">Read Feedback</a><span><img src="images/flag_white_12x17.png"></span> <a class="uo" href="#/">Flag Violation</a> <span><img src="images/info-blue-12x12.png"></span></h4></div></div>
    </div>-->
    
    <div class="container"><h4 class="widget-title   top-lab">Company Snapshot*  </h4></div>
    </div>
	
    
    
    <div class="container">
    	<div class="uko-con clearfix">
        	<div class="col-md-6 ">
            	<p> Company: <?php if ($this->_tpl_vars['transcust'][0]['t_company_name'] != ''): ?><?php echo $this->_tpl_vars['transcust'][0]['t_company_name']; ?>
<?php else: ?> Company Not Available<?php endif; ?></p>
                <p>Number of empolyee:<?php if ($this->_tpl_vars['transcust'][0]['no_employ'] != ''): ?><?php echo $this->_tpl_vars['transcust'][0]['no_employ']; ?>
 <?php else: ?> not found<?php endif; ?> </p>
                <p>Experience:<?php if ($this->_tpl_vars['transcust'][0]['experiance'] != ''): ?><?php echo $this->_tpl_vars['transcust'][0]['experiance']; ?>
 <?php else: ?> not found <?php endif; ?></p>
                <p><!--Image:--><?php if ($this->_tpl_vars['transcust'][0]['image'] != ''): ?><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/profile_pic/<?php echo $this->_tpl_vars['transcust'][0]['image']; ?>
" style="max-height:100px;" <?php else: ?><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/profile_pic/notfound.png" style="max-height:100px;" <?php endif; ?> </p>
                           </div>
            <div class="col-md-6 yo-border">
                            <p>Locations Served: India: Nationwide, International: Worldwide </p>
                <p>Categories Served: Boats, Business & Industrial Goods, Food & Agriculture, Full Load (FTL Freight), Plant & Heavy Equipment, Horses & Livestock, Household Goods, Household Moves, Part Load, Motorcycles, Other Goods, Pets & Livestock, Special Care, Vehicles </p>
                <p>Trailer Type: Dry Van, Flatbed, Low Loader, Step Deck, Power Only, Refrigerated vehicle, Air Ride Van, Auto Carrier, Dump, Flatbed Double, Removable Gooseneck, Stretch Flatbed, Tanker, Van Double, Other </p>
            </div>
        </div>
    </div>
 
 
<div class="container hello">
	<div class="col-md-4">
    <h4 class="widget-title   top-lab">Member Information</h4>
    	<table class="profile-table profile-table-memberinfo">
                                    
                <tbody><tr>
                    <td class="profile-table-label">Member Since:</td>
                    <td>08-07-15</td>
                </tr>
                <tr>
                    <td class="profile-table-label">Feedback Score:</td>
                    <td class="votes"><?php echo $this->_tpl_vars['total_fedback_store']; ?>
</td>
                </tr>
                <tr>
                    <td class="profile-table-label">Positive Feedback:</td>
                    <td><?php echo $this->_tpl_vars['positive_fedback']; ?>
%</td>
                </tr>
                <tr>
                    <td class="profile-table-label">Star Rating:</td>
                    <td>
                        <div class="layout">
                        <div class="layout-unit layout3of4">
                            <span class="star-rating rating">
                                <span id="ContentBody_spanGoogleReviewFeedbackRating" class="value-title" style="width:91%;" title="4.6"></span>
                            </span>
                        </div>
                        <div class="layout-unit layout1of4">
                            <span style="margin-left:4px; font-weight:bold"><?php echo $this->_tpl_vars['star_rating']; ?>
/5</span>
                        </div>
                        </div>
                        <a href="#Feedback"><span class="count">based on <?php echo $this->_tpl_vars['positive_row']; ?>
 reviews</span></a>
                   </td>
                </tr>
              
                <tr id="ContentBody_trExcessiveCancellations" style="display:none;">
	<td class="profile-table-label">Excessive Cancellations:</td>
	<td></td>
</tr>

                        </tbody></table>
    </div>
    
    <div class="col-md-4">
    	<h4 class="widget-title   top-lab">Feedback History</h4>
        <table class="profile-table-feedback-history">
                    <tbody><tr class="profile-table-feedback-header">
                        <td style="width:10px;">&nbsp;</td>
                        <td class="layout-1of5"></td>
                        <td class="layout-1of5">Total</td>
                        <td class="layout-1of5">1 mo</td>
                        <td class="layout-1of5">6 mo</td>
                        <td class="layout-1of5">12 mo</td>
                    </tr>
                    <tr>
                        <td style="width:10px;">&nbsp;</td>
                        <td class="layout-1of5 profile-positive">Positive:</td>
                        <td class="layout-1of5 profile-center">
                            <div class="profile-table-feedback-greyCol">
                                
                                       <?php echo $this->_tpl_vars['trans_rating']; ?>

                                
                            </div>
                        </td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_ratingonemonth_status']; ?>
</td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_ratingsixmonth_status']; ?>
</td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_ratingoneyearpositive_status']; ?>
</td>
                    </tr>
                    <tr>
                        <td style="width:10px;">&nbsp;</td>
                        <td class="layout-1of5 profile-neutral">Neutral:</td>
                        <td class="layout-1of5 profile-center">
                            <div class="profile-table-feedback-greyCol">
                                
                                   <?php echo $this->_tpl_vars['trans_ratingnatural']; ?>

                                
                            </div>
                        </td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_ratingnetural_status']; ?>
</td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_ratingsixmonthnetural_status']; ?>
</td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_ratingoneyearnatural_status']; ?>
</td>
                    </tr>
                    <tr>
                        <td style="width:10px;">&nbsp;</td>
                        <td class="layout-1of5 profile-negative">Negative:</td>
                        <td class="layout-1of5 profile-center">
                            <div class="profile-table-feedback-greyCol">
                                
                                   <?php echo $this->_tpl_vars['trans_ratingngative']; ?>

                                
                            </div>
                        </td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_ratingnegative_status']; ?>
</td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_ratingsixmonthnegative_status']; ?>
</td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_ratingoneyearnegative_status']; ?>
</td>
                    </tr>
                    <tr>
                        <td style="width:10px;">&nbsp;</td>
                        <td class="layout-1of5">Total:</td>
                        <td class="layout-1of5 profile-center">
                            <div class="profile-table-feedback-greyCol"><?php echo $this->_tpl_vars['total_rating']; ?>
</div>
                        </td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_onemonthrating']; ?>
</td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_sixmonthrating']; ?>
</td>
                        <td class="layout-1of5 profile-center"><?php echo $this->_tpl_vars['total_oneyearrating']; ?>
</td>
                    </tr>
                                  </tbody></table>
    </div>
    
    <div class="col-md-4">
    	<h4 class="widget-title   top-lab">Feedback History</h4>
        <div id="detailed">
            <table class="profile-table-feedback">
                <tbody><tr class="profile-table-feedback-header">
                    <td class="profile-table-feedback-label"></td>
                    <td class="profile-table-feedback-content" style="text-align:left;">Average Rating (last yr)</td>
                </tr>
                <tr>
                    <td class="profile-table-feedback-label">Communication</td>
                    <td class="profile-table-feedback-content">
                        <div class="layout profile-table-feedback-StarContainer">
                            
                            
                             <?php if ($this->_tpl_vars['comunication_per1'] == ''): ?>
                            
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                            <?php if ($this->_tpl_vars['comunication_per1'] == '1'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             
                            <?php if ($this->_tpl_vars['comunication_per1'] == '2'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                            <?php if ($this->_tpl_vars['comunication_per1'] == '3'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             <?php if ($this->_tpl_vars['comunication_per1'] == '4'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             <?php if ($this->_tpl_vars['comunication_per1'] == '5'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i> 
                            <?php endif; ?>
                            
                                
                                
                                
                            <span class="str_set"><?php echo $this->_tpl_vars['comunication_per1']; ?>
/5</span>    
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="profile-table-feedback-label">Care of Goods</td>
                    <td class="profile-table-feedback-content">
                        <div class="layout profile-table-feedback-StarContainer">
                            
                            
                             <?php if ($this->_tpl_vars['caregood_per1'] == ''): ?>
                            
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                            <?php if ($this->_tpl_vars['caregood_per1'] == '1'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             
                            <?php if ($this->_tpl_vars['caregood_per1'] == '2'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                            <?php if ($this->_tpl_vars['caregood_per1'] == '3'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             <?php if ($this->_tpl_vars['caregood_per1'] == '4'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             <?php if ($this->_tpl_vars['caregood_per1'] == '5'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i> 
                            <?php endif; ?>
                            
                                
                                
                                
                            <span class="str_set"><?php echo $this->_tpl_vars['caregood_per1']; ?>
/5</span>    
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="profile-table-feedback-label">Punctuality</td>
                    <td class="profile-table-feedback-content">
                        <div class="layout profile-table-feedback-StarContainer">
                            
                            
                             <?php if ($this->_tpl_vars['punctuality_per1'] == ''): ?>
                            
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                            <?php if ($this->_tpl_vars['punctuality_per1'] == '1'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             
                            <?php if ($this->_tpl_vars['punctuality_per1'] == '2'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                            <?php if ($this->_tpl_vars['punctuality_per1'] == '3'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             <?php if ($this->_tpl_vars['punctuality_per1'] == '4'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             <?php if ($this->_tpl_vars['punctuality_per1'] == '5'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i> 
                            <?php endif; ?>
                            
                                
                                
                                
                            <span class="str_set"><?php echo $this->_tpl_vars['punctuality_per1']; ?>
/5</span>    
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="profile-table-feedback-label">Services as Described</td>
                    <td class="profile-table-feedback-content">
                        <div class="layout profile-table-feedback-StarContainer">
                            
                            
                             <?php if ($this->_tpl_vars['service_per1'] == ''): ?>
                            
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                            <?php if ($this->_tpl_vars['service_per1'] == '1'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             
                            <?php if ($this->_tpl_vars['service_per1'] == '2'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                            <?php if ($this->_tpl_vars['service_per1'] == '3'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             <?php if ($this->_tpl_vars['service_per1'] == '4'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#d5d5d5" ></i> 
                            <?php endif; ?>
                             <?php if ($this->_tpl_vars['service_per1'] == '5'): ?>
                            
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i>
                            <i class="fa fa-star" style="color:#FF0" ></i> 
                            <?php endif; ?>
                            
                                
                                
                                
                            <span class="str_set"><?php echo $this->_tpl_vars['service_per1']; ?>
/5</span>    
                        </div>
                    </td>
                </tr>
            </tbody></table>
        </div>
    </div>
</div>

<div class="container">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Feedback Received (<?php echo $this->_tpl_vars['total_fedback_store']; ?>
) </a></li>
   <!-- <li><a data-toggle="tab" href="#menu1"> Feedback Left (153)</a></li>
    <li><a data-toggle="tab" href="#kk"> Cancellations (10)</a></li>
    -->
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>Items: 1 - 1 of 1 </h3>
      <div class="border-2 clearfix">
<table class="table default footable-loaded footable">
              <thead>
                
              </thead>
              <tbody>
              <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['transpoterrating']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                <tr>
                  
                  <td>
                      <ul class="uya-list pull-left">
                      	<li>Rating:</li>
                       
                        <li>Left by:</li>
                        <li>On:</li>
                      
                      </ul>
                      
                      <ul class="uya-list pull-left">
                      	<li><?php echo $this->_tpl_vars['transpoterrating'][$this->_sections['data']['index']]['rating']; ?>
</li>
                       
                        <li><?php echo $this->_tpl_vars['username_data'][$this->_sections['data']['index']]['name']; ?>
</li>
                        <li><?php echo $this->_tpl_vars['transpoterrating'][$this->_sections['data']['index']]['date']; ?>
</li>
                       
                       
                      </ul>
                  </td>
                  <td>
                  	<p class="pok">Communication <span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                    <p class="pok">Care of Goods<span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                    <p class="pok">Punctuality<span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                    <p class="pok">Services as Described<span class="star-rating" title="5 / 5"><span style="width: 100%;"></span></span></p>
                                      	
                     
                      
                              

                  </td>
                  <td>
                  	<p>	<?php echo $this->_tpl_vars['transpoterrating'][$this->_sections['data']['index']]['review']; ?>
 </p>
                  <td></td>
                  
                </tr>
                <?php endfor; endif; ?>
               
               
               
                
              </tbody>
            </table>             
       </div>            
                
                

    </div>
    

    
                      
                      
    
  </div>
</div>


        