<?php /* Smarty version 2.6.19, created on 2017-07-17 21:33:16
         compiled from post_shipping.tpl */ ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>New Shipment</h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">New Shipment</a></li>
                        <li class="active">Add</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="component-bg">
        <div class="container">
            <!-- Forms
================================================== -->
<div class="bs-docs-section mar-b-5">
  <h2 id="forms" class="page-header">New Shipment</h2>
 
    <form class="form-horizontal" name="post_shipment"  method="post" action="#" role="form" enctype="multipart/form-data">
    <h3 align="left">Sender Details</h3>
     <hr>	
     <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Title</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="" id="Title" name="Title" placeholder="Enter Title Here">
          
        </div>
      </div>
     <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Full Name</label>
        <div class="col-sm-10">
          <input type="text" readonly="readonly" class="form-control" value="<?php echo $_SESSION['name']; ?>
" id="s_user_name" name="s_user_name" placeholder="User Name">
          <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>
" />
          
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
          <input type="email" readonly="readonly" class="form-control" value="<?php echo $_SESSION['email']; ?>
" id="s_user_email" name="s_user_email" placeholder="Email">
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
        <div class="col-sm-10">
          <input type="text" readonly="readonly" class="form-control" id="mobile"  name="mobile" placeholder="Mobile no" value="<?php echo $this->_tpl_vars['functions']->get_mobile($_SESSION['user_id']); ?>
">
        </div>
      </div>
   
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Collection Province</label>
        <div class="col-sm-10">
         
         <select name="form_state" id="form_state" class="form-control border-radius"  onchange="get_state_city(this.value);">
        
         <option value="please Select" selected="selected" >Please Select Collection Province</option>
      
						<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
  <option value="<?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['state'] == $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
 </option>
   <?php endfor; endif; ?>
               
                 
</select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Collection City</label>
        <div class="col-sm-10">
         
          <span id="city_change">
                     <select class="form-control"  name="form_change" id="form_change" >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >Please Select Collection City</option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select>
     
      </div>
  </div>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Collection Location/Address</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="loc_sen" id="loc_sen" rows="3"></textarea>
     </div>
        
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Pickup Dates</label>
        <div class="col-xs-5" align="left">
               
        <INPUT NAME="pickup_start_date" id="pickup_start_date" TYPE="date" value="<?php echo $this->_tpl_vars['functions']->get_current_date(); ?>
" />--Between--
       
        <INPUT NAME="pickup_end_date" id="pickup_end_date" TYPE="date"   value="<?php echo $this->_tpl_vars['functions']->get_current_date('1'); ?>
" />
        </div>
       </div>
  <hr />
   <h3 align="left">Receiver Details</h3>
    <hr />
   <div class="form-group">
        <label for="inputEmail3"  class="col-sm-2 control-label" >Receiver Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="" id="r_user_name" name="r_user_name" placeholder="Full Name">
          
          
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Receiver Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" value="" id="r_user_email" name="r_user_email" placeholder="Email">
        </div>
      </div>
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Receiver Mobile</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="r_mobile"  name="r_mobile" placeholder="Mobile no">
        </div>
      </div>
   
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery Province</label>
        <div class="col-sm-10">
         
           <select name="to_state" id="to_state" class="form-control border-radius"  onchange="get_state_city_to(this.value);">
        
         <option value="please Select" selected="selected" >Please Select Delivery Province</option>
      
						<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
  <option value="<?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['state'] == $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['categories'][$this->_sections['data']['index']]['state']; ?>
 </option>
   <?php endfor; endif; ?>
               
                 
</select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery City</label>
        <div class="col-sm-10">
         
          <span id="city_change1">
                     <select class="form-control"  name="to_city" id="to_city" >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >Please Select Delivery City</option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery Location/Address</label>
      <div class="col-sm-10">
      <textarea class="form-control" name="loc_rec" id="loc_rec" rows="3"></textarea>
     </div>
     </div>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Delivery Dates</label>
        <div class="col-xs-6" align="left">
           <INPUT NAME="delv_start_date" id="delv_start_date" TYPE="date"   value="<?php echo $this->_tpl_vars['functions']->get_current_date('3'); ?>
" />--Between--
        
            <INPUT NAME="delv_end_date" id="delv_end_date" TYPE="date"   value="<?php echo $this->_tpl_vars['functions']->get_current_date('5'); ?>
" />
        </div>
      
  </div>
  <hr />
        <h3 align="left">Shipment Details</h3>
        <hr />
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Load Industry</label>
        <div class="col-sm-10">
         <select name="category"  id="category" class="form-control border-radius" onchange="get_sub_cat(this.value);" >
                    <option value="please Select" selected="selected" >Please Select Load Industry</option>
      				<?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['useredit1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                        
                    <option value="<?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['id']; ?>
" ><?php echo $this->_tpl_vars['useredit1'][$this->_sections['data']['index']]['category']; ?>
 </option>
                  <?php endfor; endif; ?>
                   </select>
      
      </div>
  </div>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Load Type</label>
        <div class="col-sm-10">
         
         <span id="sub_cat">
                     <select class="form-control"  name="sub_cat_id" id="sub_cat_id" onchange="get_sub_next_cat(this.value);" >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >Please Select Load Type</option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select>
      
      </div>
  </div>
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Transport Type</label>
        <div class="col-sm-10">
         
         <span id="sub_next_cat">
                     <select class="form-control"  name="sub_cat_id" id="sub_cat_id" onchange="get_last_cat(this.value);" >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >Please Select Transport Type</option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select>
      
      </div>
  </div>
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Body &amp Bulk Type</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="last_cat_id" onchange="get_fifth_cat(this.value);" id="sublast_cat_id_cat_id" >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >Please Select Body &amp Bulk Type</option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select>
      
      </div>
  </div>
  
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label" >Size Classification</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="fifth_cat_id" id="fifth_cat" onchange="get_sixth_cat(this.value);" >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >Please Select Size Classification </option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select>
      
      </div>
  </div>
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Payload KG</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="sixth_cat_id" id="sixth_cat" onchange="get_seventh_cat(this.value);" >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >Please Select Payload KG</option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select>
      
      </div>
  </div>
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Length Meters</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="seventh_cat_id" id="seventh_cat" onchange="get_eigth_cat(this.value);" >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >Please Select Length Meters </option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select>
      
      </div>
  </div>
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Width Meters </label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="eight_cat_id" id="eigth_cat" onchange="get_ninth_cat(this.value);" >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >Please Select Width Meters </option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select>
      
      </div>
  </div>
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Preferred Distance </label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="ninth_cat_id" id="ninth_cat"  onchange="get_tenth_cat_id(this.value);" >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >Please Select Preferred Distance </option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select>
      
      </div>
  </div>
  
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Other</label>
        <div class="col-sm-10">
         
         <span id="last_cat">
                     <select class="form-control"  name="tenth_cat_id" id="tenth_cat"   >
                     <?php if ($this->_tpl_vars['edit_id'] == ''): ?>
        <option value="please Select" selected="selected" >Please Select </option>
        <?php else: ?>
        
        <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['city_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
        <option value="<?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
" <?php if ($this->_tpl_vars['useredit'][0]['city'] == $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']): ?> selected="selected" <?php endif; ?> ><?php echo $this->_tpl_vars['city_cat'][$this->_sections['data']['index']]['city']; ?>
 </option>
        <?php endfor; endif; ?>
     <?php endif; ?>
</select>
      
      </div>
  </div>
  
 <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Dimensions</label>
        <div class="col-sm-10">
         <select name="Dimensions"  id="Dimensions" class="form-control border-radius" >
          <option value="please Select" selected="selected" >Please Select Dimensions</option>
                 <!--   <option value="Feet"  >Feet</option>-->
      				 <option value="Liters" >Liters</option> 
                      <option value="Centimeters"  >Centimeters</option>   
                     <option value="Meters"  >Meters</option>   
                 
                   </select>
      
      </div>
  </div>
  <table>
  <tr><td width="25%"></td><td><table>
  <tr><td>
   <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Width  </label>
       <div class="col-xs-2">
         <input type="text" name="Width" id="Width" class="form-control" style="min-width:70px;" placeholder="Width">
        </div>
              
  </div>
  </td>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Height  </label>
       <div class="col-xs-2">
          <input type="text" name="Height" id="Height" class="form-control" style="min-width:70px;" placeholder="Height">
        </div>
        
  </div>
  </td>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Length  </label>
       <div class="col-xs-2">
         <input type="text" name="Length" id="Length" class="form-control" style="min-width:70px;" placeholder="Length">
        </div>
        
  </div>
  </td>
  </tr>
  <tr>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Weight  </label>
       <div class="col-xs-2">
       <input type="text" name="Weight" id="Weight" class="form-control" style="min-width:70px;" placeholder="Weight">
        </div>
        
  </div>
  </td>
  <td>
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Quantity  </label>
       <div class="col-xs-2">
       <input type="text" name="Qty" id="Qty" class="form-control" style="min-width:75px;" placeholder="Quantity">
        </div>
        
  </div>
 </td>
 <td>
 <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Value (<?php echo $this->_tpl_vars['functions']->get_currency(); ?>
)</label>
       <div class="col-xs-2">
       <input type="text" name="value" id="value" class="form-control" style="min-width:70px;" placeholder="Value">
        </div>
        
  </div>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
      
      <!-- renamed from logo to shipmaent image-->
  <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Shipment Image </label>
      <div class="col-sm-10">
      <input type="file" name="Shipment_image" class="btn btn-default" style="max-width:240px;"/>
     </div>
        
  </div>
 <div class="form-group" >
        <label for="inputEmail3" class="col-sm-2 control-label">Description </label>
      <div class="col-sm-10">
         
      <textarea class="ckeditor" style="width:100%; height:200px;" name="description" > </textarea>
      	
     </div>
        
  </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="submit" class="btn btn-default" onclick="return valid_post(document.post_shipment);">Submit</button>
        </div>
      </div>
    </form>
  </div><!-- /.bs-example -->

  
     
   

</div>
        </div>
    </div>
    <!--container end-->