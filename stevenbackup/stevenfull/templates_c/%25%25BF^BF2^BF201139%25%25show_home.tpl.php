<?php /* Smarty version 2.6.19, created on 2017-07-17 14:38:06
         compiled from show_home.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'show_home.tpl', 224, false),)), $this); ?>
<!--slider-->


<div id="owl-demo" class="owl-carousel owl-theme"> 
 <?php echo $this->_tpl_vars['functions']->get_slider_home1(); ?>

		<div class="item">
			<img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/slider/<?php echo $this->_tpl_vars['slider_info'][0]['image_path']; ?>
" alt="">
		</div>
		<div class="item">
			<img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/slider/<?php echo $this->_tpl_vars['slider_info'][1]['image_path']; ?>
" alt="">
		</div>
		<div class="item">
			<img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/slider/<?php echo $this->_tpl_vars['slider_info'][2]['image_path']; ?>
" alt="">
		</div> 
        <div class="item">
			<img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/slider/<?php echo $this->_tpl_vars['slider_info'][3]['image_path']; ?>
" alt="">
		</div> 
	</div>

<section id="main">
    
    
    <div class="section-2">
  	<div class="container">
    	<div id="primary_add">
    	    <h3>add server</h3>
    	</div>
        
        
        
        <!--<div class="col-md-3 col-sm-6 col-xs-6">
        <span class="x-icon"><i class="fa fa-cogs" aria-hidden="true"></i></span>
        	<h5>Secure Payments</h5>
            <h6>Learn More</h6>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-6">
        <span class="x-icon"><i class="fa fa-compass" aria-hidden="true"></i></span>
        	<h5>uShip Guarantee</h5>
            <h6>Learn Morelista de su</h6>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-6">
        <span class="x-icon"><i class="fa fa-crosshairs" aria-hidden="true"></i></span>
        	<h5>Secure Payments</h5>
            <h6>lista de su</h6>
        </div>-->
    </div>
  </div>
  
  
  <div class="section-1">
  	<div class="container">
    <?php echo $this->_tpl_vars['functions']->get_home_shipment(); ?>

                <?php if ($this->_tpl_vars['shipment_info']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['shipment_info']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?> 
    	<div class="col-md-2 text-center">
        	<div class="home_top_box">
				<div class="img-box">
				 <?php if ($this->_tpl_vars['shipment_info'][$this->_sections['data']['index']]['img'] != ''): ?>
                 <a style="cursor:pointer; color:#337ab7;"  onclick="submitbuy('<?php echo $this->_tpl_vars['shipment_info'][$this->_sections['data']['index']]['id']; ?>
',document.view_detail);">
					<img class="img-responsive" src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/shipment_image/<?php echo $this->_tpl_vars['shipment_info'][$this->_sections['data']['index']]['img']; ?>
"/></a>
					<?php else: ?>
					 <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/shipment_image/default.jpg" class="img-responsive"  >
					 <?php endif; ?>
				</div>
				<div class="contanet">
					<h5><?php echo ((is_array($_tmp=$this->_tpl_vars['shipment_info'][$this->_sections['data']['index']]['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 18, '...') : smarty_modifier_truncate($_tmp, 18, '...')); ?>
</h5>
					<p><?php echo ((is_array($_tmp=$this->_tpl_vars['shipment_info'][$this->_sections['data']['index']]['description'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 25, '...') : smarty_modifier_truncate($_tmp, 25, '...')); ?>
 </p>
					
				</div>
			</div>
        </div>
        
        <?php endfor; endif; ?>
        <?php endif; ?>
    </div>
  </div>
  
  
  
  </section>
  
   
  
  
  <div class="container">
        <div class="col-sm-3">ADD SERVER SPACE 1</div>
            <div class="col-sm-6">
  
                <div class="shipper" id="shipper">
             <section class="homepage-section slanted slantedBackgroundLeft flex-container and-smallAlignCenter">
                     <div class="imageWrapper fix_left">
                        <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/location_flage/<?php echo $this->_tpl_vars['sarvis_info'][0]['img']; ?>
" />
                            </div>
                              <div class="flex-item flex-small-12 flex-large-5 sectionContent">
                                 <div class="contentWrapper">
                          <div class="side-box">
    	        
                        <h1><?php echo $this->_tpl_vars['sarvis_info'][0]['name']; ?>
</h1>
                        <p><?php echo $this->_tpl_vars['sarvis_info'][0]['description']; ?>
</p>
                        </div>
            
                    </div>
                </div>
             </section>
        </div>
     </div>
     <div class="col-sm-3">ADD SERVER SPACE 2</div>
</div>

<div class="carrier" id="carrier">
<div class="container">
    <div class="col-sm-3">ADD SERVER SPACE 3</div>
            <div class="col-sm-6">
        <section class="homepage-section slanted slantedBackgroundRight flex-container and-smallAlignCenter">
        <div class="imageWrapper">
           <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/location_flage/<?php echo $this->_tpl_vars['sarvis_info'][1]['img']; ?>
" />
        </div>
        <div class="flex-item flex-small-12 flex-large-5 sectionContent">
            <div class="contentWrapper qub-p">
          <div class="side-box">
    	
        <h1><?php echo $this->_tpl_vars['sarvis_info'][1]['name']; ?>
</h1>
        <p><?php echo $this->_tpl_vars['sarvis_info'][1]['description']; ?>
</p>
       
                 </div>
            </div>
        </div>
            </section>
        </div>  
    <div class="col-sm-3">ADD SERVER SPACE 4</div>
    </div>
</div>
  
  
   
 <div class="container">
 <div class="business" id="business">
     
        <div class="col-sm-3">ADD SERVER SPACE 5</div>
            <div class="col-sm-6">
    
    <section class="homepage-section slanted slantedBackgroundLeft flex-container and-smallAlignCenter">
        
        <div id="add1">
            <h1>this is space for add one</h1>
        </div>
        
        <div class="imageWrapper fix_left">
              <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/location_flage/<?php echo $this->_tpl_vars['sarvis_info'][2]['img']; ?>
" />
        </div>
        <div class="flex-item flex-small-12 flex-large-5 sectionContent">
            <div class="contentWrapper">
           <div class="side-box">
    	
        <h1><?php echo $this->_tpl_vars['sarvis_info'][2]['name']; ?>
</h1>
        <p><?php echo $this->_tpl_vars['sarvis_info'][2]['description']; ?>
</p>
       
                </div>
            </div>
        </div>
    </section>
    </div>
    <div class="col-sm-3">ADD SERVER SPACE 6</div>
</div>
  
 </div>     
      
<div class="section-2">
  	<div class="container">
    	<div class="col-md-4">
        <span class="numb">3.5M</span>
            <h6 class="nub-text">Customers</h6>
        </div>
        
        
        
        
        <div class="col-md-4">
        <span class="numb">788000</span>
            <h6 class="nub-text">Transporters</h6>
        </div>
        
        <div class="col-md-4">
        <span class="numb">5.7M</span>
            <h6 class="nub-text">Listings</h6>
        </div>
    </div>
  </div> 
  
  
  <div class="yo-section">
   <div class="container">
   	<div class="col-md-9">
    	<h2>South Africa’s only and largest all-inclusive transport services platform.</h2>
    </div>
    <div class="col-md-3">
    	<h2><a href="#/">Solutions for Africa. </a></h2>
    </div>
   </div>
  </div>
  
  <div class="bg-lg4">
   <div id="home-services">

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2>
              Leading the virtual market with vehicle and freight exchange services in South Africa.
            </h2>
          </div>
 <!-- service end -->
    <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['testimonial_info']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?> 
          <div class="col-md-3">
            <div class="h-service">
              <div class="icon-wrap ico-bg round-fifty wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;" data-wow-animation-name="fadeInDown">
                <i class="fa fa-users">
                </i>
              </div>
              <div class="h-service-content wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;" data-wow-animation-name="fadeInUp">
                <h3>
               <?php echo $this->_tpl_vars['testimonial_info'][$this->_sections['data']['index']]['name']; ?>

                  
                </h3>
                <p>
           <?php echo $this->_tpl_vars['testimonial_info'][$this->_sections['data']['index']]['description']; ?>

                  <br>
                 
                </p>
              </div>
            </div>
          </div>
       <?php endfor; endif; ?>   
        </div>
        <!-- /row -->
</div>
      </div>
      <!-- END CLIENTS -->
    </div>