<?php /* Smarty version 2.6.19, created on 2017-06-14 10:52:46
         compiled from admin/add_country.tpl */ ?>
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">
                        
            <h1 class="page-header"><?php if ($this->_tpl_vars['edit_id'] != ''): ?>Edit <?php else: ?>Add <?php endif; ?> Country
                     </h1>
       </div>
    </div>
    <!-- /. ROW  -->
   
    <div class="row">
      <div class="col-md-12"> 
        <!-- Form Elements -->
        <div class="panel panel-default">
          <div class="panel-heading">
        <b><?php if ($this->_tpl_vars['edit_id'] != ''): ?>Edit <?php else: ?>Add <?php endif; ?> Country</b>
         
 </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-8"> 
        <?php if ($this->_tpl_vars['sucess'] != ''): ?>
               
    				 <div class="notibar msgsuccess">
                        <a class="close"></a>
                        <p>Country Added Sucessfully</p>
                    </div><!-- notification msgsuccess -->
                  
                <?php endif; ?> 
        <form class="stdform stdform2" name="frm_addcountry" id="frm_addcountry" method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="edit_id" value="<?php echo $this->_tpl_vars['edit_id']; ?>
" />  
         <input type="hidden" name="old_country_name" value="<?php echo $this->_tpl_vars['edit_data'][0]['country']; ?>
" />  
                    
                    	 <div class="form-group">
                        	<label>Country Name</label> <span  class="error_alert"></span>
                               <?php if (edit_id != ''): ?>
                             <input type="hidden" name="old_country_name" id="old_country_name" value="<?php echo $this->_tpl_vars['edit_data'][0]['country']; ?>
" /><?php endif; ?>
                             <input type="text" name="country_name" id="alert_name" value="<?php echo $this->_tpl_vars['edit_data'][0]['country']; ?>
" class="form-control" />
                          </div> 
                    
                         <div class="form-group">
                        	<label>Image</label><span class="error_alert"></span> 
                          
                              <input type="file" name="country_flage" id="alert_image" class="form-control"  />
                             <?php if ($this->_tpl_vars['edit_data'][0]['flage'] != ''): ?>
                          <img src="<?php echo $this->_tpl_vars['site_url']; ?>
/location_flage/<?php echo $this->_tpl_vars['edit_data'][0]['flage']; ?>
" height="80" width="100" />
                            <?php endif; ?>
                           <input type="hidden" name="country_flage_old"  value="<?php echo $this->_tpl_vars['edit_data'][0]['flage']; ?>
" />
                           
                      </div>
                        
                       <div class="form-group">
                        	<label>Title</label> <span  class="error_alert"></span>
                            
                            <input type="text" name="title" id="alert_title" class="form-control"  value="<?php echo $this->_tpl_vars['edit_data'][0]['title']; ?>
" />
                       </div>    
                    
                        <div class="form-group">
                        	<label>Description</label> <span  class="error_alert"></span>
                           
                            <textarea name="description" id="alert_description" class="form-control" ><?php echo $this->_tpl_vars['edit_data'][0]['descen']; ?>
</textarea>
                           
                       </div>
                       
                        <div class="form-group">
                        	<label>keyword</label><span  class="error_alert"></span>
                            
                            <textarea name="keyword" id="alert_keyword" class="form-control" ><?php echo $this->_tpl_vars['edit_data'][0]['keyword']; ?>
</textarea>
                            
                        </div>
                        
                        
                     
                        
                            <button type="submit"  name="submit" value="Submit" onClick="return add_country(document.frm_addcountry);" class="btn btn-default">Submit</button>
                           
                       
                    </form>
 			     
              </div>
            </div>
          </div>
        </div>
        <!-- End Form Elements --> 
      </div>
    </div>
    <!-- /. ROW  --> 
    
    <!-- /. ROW  --> 
  </div>
  <!-- /. PAGE INNER  --> 
</div>