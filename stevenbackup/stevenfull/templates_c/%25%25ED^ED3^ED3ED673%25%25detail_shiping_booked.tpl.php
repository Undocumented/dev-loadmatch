<?php /* Smarty version 2.6.19, created on 2017-06-22 15:02:49
         compiled from detail_shiping_booked.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'detail_shiping_booked.tpl', 55, false),)), $this); ?>
  <!--breadcrumbs start-->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h1>Shipment Detail </h1>
                </div>
                <div class="col-lg-8 col-sm-8">
                    <ol class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Shipment Detail</a></li>
                         <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!--breadcrumbs end-->
 <div class="white-bg">

        <!-- career -->
    <div class="container career-inner">
        <div class="row">
            <div class="col-md-12 career-head">
               

            </div>
        </div>
        <hr>
       
     <table align="center" width="100%" border="0">
     <tr>
     <td>
        <div class="row">
            <div class="col-md-4" style="background-color:#F7F7F7;min-height:800px;">
                <div class="candidate wow fadeInLeft" style="text-align:left;" >
                    <h1>Sender Detail</h1>
                    <p class="align-left"></p>
                    <h4></h4>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-angle-right pr-10"></i>Sender Name : <?php echo $this->_tpl_vars['functions']->get_name($this->_tpl_vars['search'][0]['user_id']); ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Sender State : <?php echo $this->_tpl_vars['search'][0]['form_state']; ?>
 </li>
                        <li><i class="fa fa-angle-right pr-10"></i>Sender City : <?php echo $this->_tpl_vars['search'][0]['form_city']; ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>location : <?php echo $this->_tpl_vars['search'][0]['sender_loc']; ?>
</li>
                       
                    </ul>
                   
                </div>
                <hr>
                <div class="candidate wow fadeInLeft" style="text-align:left;">
                    <h1>Pickup Dates</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                         <li><i class="fa fa-angle-right pr-10"></i>Start Date : <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['pickup_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>End Date : <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['pickup_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
 </li>
                       
                    </ul>
                    <hr>
                    <h1>Shiping Detail</h1>
                    <p class="align-left"></p>
                    <h4></h4>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-angle-right pr-10"></i>Category : <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['category']); ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i> Under Category : <?php echo $this->_tpl_vars['functions']->get_category($this->_tpl_vars['search'][0]['sub_category']); ?>
 </li>
                         <li><i class="fa fa-angle-right pr-10"></i>Dimensions : <?php echo $this->_tpl_vars['search'][0]['dimensions']; ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Width : <?php echo $this->_tpl_vars['search'][0]['Width']; ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Height : <?php echo $this->_tpl_vars['search'][0]['Height']; ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Length : <?php echo $this->_tpl_vars['search'][0]['Length']; ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Weight : <?php echo $this->_tpl_vars['search'][0]['Weight']; ?>
</li>
                         <li><i class="fa fa-angle-right pr-10"></i>Quntity : <?php echo $this->_tpl_vars['search'][0]['Qty']; ?>
</li>
                         <?php if ($this->_tpl_vars['search'][0]['img']): ?>
                          <li><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/images/shipment_image/<?php echo $this->_tpl_vars['search'][0]['img']; ?>
" width="100px" height="100px" /></li>
                          <?php endif; ?>
                       <li style="max-height:187px; overflow:scroll;"><i class="fa fa-angle-right pr-10"></i>Description : <?php echo $this->_tpl_vars['search'][0]['description']; ?>
</li>
                       
                    </ul>
                </div>
                <hr>
            </div>
             <div class="col-md-4">
                <div class="candidate wow fadeIn"  >
                    <h2>Origin, Destination, & Route Information</h2>
               
                      <?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "gmap_tranport.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>
             
                </div>
                </div>
            <div class="col-md-4" style="background-color:#F7F7F7; min-height:800px;">
                <div class="candidate wow fadeInRight" style="text-align:left;">
                    <h1>Reciver Detail</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                         <li><i class="fa fa-angle-right pr-10"></i>Reciver Name : <?php echo $this->_tpl_vars['search'][0]['reciver_name']; ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>Reciver State : <?php echo $this->_tpl_vars['search'][0]['to_state']; ?>
 </li>
                        <li><i class="fa fa-angle-right pr-10"></i>Reciver City : <?php echo $this->_tpl_vars['search'][0]['to_city']; ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>location : <?php echo $this->_tpl_vars['search'][0]['reciver_loc']; ?>
</li>
                    </ul>
                  
                </div>
                <hr>
                <div class="candidate wow fadeInRight" style="text-align:left;">
                    <h1>Delivery Dates</h1>
                   
                    <h4></h4>
                    <ul class="list-unstyled">
                       <li><i class="fa fa-angle-right pr-10"></i>Start Date : <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['delv_start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
</li>
                        <li><i class="fa fa-angle-right pr-10"></i>End Date : <?php echo ((is_array($_tmp=$this->_tpl_vars['search'][0]['delv_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
 </li>
                    </ul>
                    <hr>
                   <?php if ($this->_tpl_vars['type'] == 'ct'): ?>
             
                    <h1>Payment Detail</h1>
                   
                    <h4></h4>
                    
                     <ul class="list-unstyled">
                        <li><i class="fa fa-angle-right pr-10"></i>Payment Recived : (<?php echo $this->_tpl_vars['functions']->get_currency(); ?>
) <?php echo $this->_tpl_vars['search1'][0]['starting_price']; ?>
</li>
                         <li><i class="fa fa-angle-right pr-10"></i>Payment Accepted; : &nbsp; <?php echo $this->_tpl_vars['search1'][0]['payment_accepted']; ?>
 </li>
                         <li><i class="fa fa-angle-right pr-10"></i>Payment Method&nbsp; : &nbsp; <?php echo $this->_tpl_vars['search1'][0]['payment_methods']; ?>
 </li>
                       
                    </ul>
                
              
         <?php endif; ?>
         
         <?php if ($this->_tpl_vars['type'] == 'c'): ?>
            
                    <h1>&nbsp;&nbsp;Cancel Reason</h1>
                   
                    <h4></h4>
                    
                     <ul class="list-unstyled">
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['search1'][0]['reaison_cancel']; ?>
</li>
                       
                    </ul>
                
               
                <hr>
               
            
         <?php endif; ?>
                </div>
              
                
            </div>
            
            </div>
                   
          
         </td>
        </tr>
        </table>
             <?php echo $this->_tpl_vars['functions']->get_history($this->_tpl_vars['search'][0]['order_id']); ?>

                  <div class="bg-lg5" style="width:102.5%; margin-left:-15px;">
            <table class="table table-bordered">
              
                <thead style="background-color:#CCC;">
                  <tr>
                    <th>
                      Sr.no
                    </th>
                    <th>
                      Status
                    </th>
                    <th>
                     Date/Time
                    </th>
                    <th>
                     Activites
                    </th>
                    <th>
                     Details
                    </th>
                   </tr>
                </thead>
                <tbody>
                <?php if ($this->_tpl_vars['status_history']): ?>
                <?php unset($this->_sections['data']);
$this->_sections['data']['name'] = 'data';
$this->_sections['data']['loop'] = is_array($_loop=$this->_tpl_vars['status_history']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['data']['show'] = true;
$this->_sections['data']['max'] = $this->_sections['data']['loop'];
$this->_sections['data']['step'] = 1;
$this->_sections['data']['start'] = $this->_sections['data']['step'] > 0 ? 0 : $this->_sections['data']['loop']-1;
if ($this->_sections['data']['show']) {
    $this->_sections['data']['total'] = $this->_sections['data']['loop'];
    if ($this->_sections['data']['total'] == 0)
        $this->_sections['data']['show'] = false;
} else
    $this->_sections['data']['total'] = 0;
if ($this->_sections['data']['show']):

            for ($this->_sections['data']['index'] = $this->_sections['data']['start'], $this->_sections['data']['iteration'] = 1;
                 $this->_sections['data']['iteration'] <= $this->_sections['data']['total'];
                 $this->_sections['data']['index'] += $this->_sections['data']['step'], $this->_sections['data']['iteration']++):
$this->_sections['data']['rownum'] = $this->_sections['data']['iteration'];
$this->_sections['data']['index_prev'] = $this->_sections['data']['index'] - $this->_sections['data']['step'];
$this->_sections['data']['index_next'] = $this->_sections['data']['index'] + $this->_sections['data']['step'];
$this->_sections['data']['first']      = ($this->_sections['data']['iteration'] == 1);
$this->_sections['data']['last']       = ($this->_sections['data']['iteration'] == $this->_sections['data']['total']);
?>
                  <tr>
                    <td>
                      <?php echo $this->_sections['data']['index']+$this->_tpl_vars['row_no']+1; ?>

                    </td>
                    
                    <td>
                      <?php echo $this->_tpl_vars['status_history'][$this->_sections['data']['index']]['new_status']; ?>

                    </td>
                    <td>
                    <?php echo ((is_array($_tmp=$this->_tpl_vars['status_history'][$this->_sections['data']['index']]['pickup_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d- %m -%Y") : smarty_modifier_date_format($_tmp, "%d- %m -%Y")); ?>
-<?php echo $this->_tpl_vars['status_history'][$this->_sections['data']['index']]['pickup_time']; ?>

                    </td>
                    <td>
                       <?php echo $this->_tpl_vars['status_history'][$this->_sections['data']['index']]['Activites']; ?>

                    </td>
                    <td>
                       <?php echo $this->_tpl_vars['status_history'][$this->_sections['data']['index']]['Details']; ?>

                    </td>
                   </tr>
                  <?php endfor; endif; ?>
                  <?php else: ?>
						<tr><td colspan="8" align="center">Record Not Found</td></tr>
					<?php endif; ?>
                </tbody>
              </table>
                 </div>
    <!-- career -->
    
       </div>
    </div>
  