<?php /* Smarty version 2.6.19, created on 2017-06-08 14:04:21
         compiled from process.tpl */ ?>
<?php if ($this->_tpl_vars['data4'][0]['on_off'] == 'OFF'): ?>

<div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-sm-4">
            <h1>
         Thank You
            </h1>
          </div>
          <div class="col-lg-8 col-sm-8">
            <ol class="breadcrumb pull-right">
              <li>
                <a href="index.html">
                  Home
                </a>
              </li>
              
              <li class="active">
                Thank You
              </li>
                <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="container" >
      <div class="row">
</div>
     
  
                <div class="contact-form">
              <div class="table-responsive">
                    
                    <p><h3>Thank you for loading a new shipment.</h3></p>
                    
                    <p>You can view the detail of your listing under My Account.</p>
       

</div>
</div>

        <!-- End row -->

      </div>
      <!-- End container -->
    </div>


    <!--container end-->
<?php else: ?>


<div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-sm-4">
            <h1>
         Payment Option
            </h1>
          </div>
          <div class="col-lg-8 col-sm-8">
            <ol class="breadcrumb pull-right">
              <li>
                <a href="index.html">
                  Home
                </a>
              </li>
              
              <li class="active">
                Payment Option
              </li>
                <li class="active"><a href="" onclick="window.history.back();">Back</a></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <!--breadcrumbs end-->

    <!--container start-->
    <div class="container" >
      <div class="row">
</div>
     
  
                <div class="contact-form">
              <div class="table-responsive">
                    <form name="payment" method="post" action="pay_option.html" >
						<table border="0" cellspacing="1" cellpadding="2" align="center">
						
						  <input type="hidden" name="amount" value="<?php echo $this->_tpl_vars['amount']; ?>
" />
                          <input type="hidden" name="ord_id" value="<?php echo $this->_tpl_vars['oid']; ?>
" />
						    <tr>
								<td width="277" align="right" class="reg_align" valign="top">Service Charge&nbsp;&nbsp;&nbsp;:&nbsp;</td>
								<td width="278" align="left" 	class="form1" colspan="4">
								<table border="0" cellspacing="0" cellpadding="0">
								<?php echo $this->_tpl_vars['data4'][0]['price']; ?>
&nbsp;/ &nbsp;<?php echo $this->_tpl_vars['data4'][0]['currency']; ?>

								</table>	
								</td>
						  </tr>
                        
                          
                          <?php if ($this->_tpl_vars['data3'][0]['paypal_on'] == 'Y'): ?>
                          <tr><td colspan="4">&nbsp; </td></tr>
						  <tr>
								<td align="right" class="reg_align">Paypal Option&nbsp;&nbsp;&nbsp;:&nbsp;</td>
								<td class="contact" align="left"><input type="radio" name="payment_opt"  value="P" checked="checked">&nbsp;&nbsp;<td ><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/img/paypal.jpg" height="100" width="356" /></td> <!--<input type="radio" name="payment_opt" value="A" checked="checked">AUTHORISED.NET&nbsp;&nbsp;&nbsp; -->
								</td>
						  </tr>
                            <tr><td colspan="4">&nbsp;</td></tr>
                          <?php endif; ?>
                          
                          <?php if ($this->_tpl_vars['data3'][0]['checkout_on'] == 'Y'): ?>
						  <tr>
								<td align="right" class="reg_align">2CheckOut Option&nbsp;&nbsp;&nbsp;:&nbsp;</td>
								<td class="contact" align="left"><input type="radio" name="payment_opt"  value="C" >&nbsp;&nbsp;<td><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/img/checkout_logo.gif" height="70" width="200" /></td> 
								</td>
						  </tr>
						      <tr><td colspan="4">&nbsp;</td></tr>
                           <?php endif; ?>
                           
                          <?php if ($this->_tpl_vars['data3'][0]['wire_on'] == 'Y'): ?>
                          <tr>
								<td align="right" class="reg_align">Wire Transfer Option&nbsp;&nbsp;&nbsp;:&nbsp;</td>
								<td class="contact" align="left"><input type="radio" name="payment_opt"  value="W" >&nbsp;&nbsp;<td><img src="<?php echo $this->_tpl_vars['site_url']; ?>
/templates/img/wire_transfer_logo.gif" height="70" width="200" /></td> 
								</td>
						  </tr>
						      <tr><td colspan="4">&nbsp;</td></tr>
                           <?php endif; ?>
						<tr>
						<td colspan="4" align="center"  ><input name="submit"  class="sub_button" type="submit" value="SUBMIT"></td>
																  </tr>
						<tr><td colspan="4">&nbsp;</td></tr>									  
																</table></form>
       

</div>
</div>

        <!-- End row -->

      </div>
      <!-- End container -->
    </div>

<?php endif; ?>
    <!--container end-->
