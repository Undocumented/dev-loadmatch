<?php /* Smarty version 2.6.19, created on 2017-06-18 11:37:22
         compiled from login.tpl */ ?>

<!-- Navbar End -->

  </header>
  <div class="main-2 clearfix">
  <div class="container">
  	<div class="login clearfix well">
    	<div class="col-md-6 login-next ">
        	<h5>Sign In</h5>
             <?php if ($this->_tpl_vars['err'] != ''): ?> <div class="notibar msgsuccess">
                        
                       <?php echo $this->_tpl_vars['err']; ?>

                    </div><?php endif; ?> 
            
                  
                <?php if ($this->_tpl_vars['msg'] == 'Y'): ?> <div class="notibar msgsuccess">
                        
                        <div class="alert alert-success" >Please Enter Correct Login Detail!</div>
                    </div><?php endif; ?>     
                    
           
            <div class="well-1">
                          <form id="loginForm" method="POST" action="" novalidate="novalidate" name="login">
                              <div class="form-group">
                                  <label for="username" class="control-label">eMail address or LoadMatch username:</label>
                                  <input type="text" class="form-control" id="username" name="user_name" value="" required="" title="Please enter you username" placeholder="example@gmail.com">
                                  <span class="help-block"></span>
                              </div>
                              <div class="form-group">
                                  <label for="password" class="control-label">LoadMatch Password:</label>
                                  <input type="password" class="form-control" id="password" name="password" value="" required="" title="Please enter your password" placeholder="Enter password">
                                  <span class="help-block"></span>
                              </div>
                              <div id="loginErrorMsg" class="alert alert-error hide">Wrong Username or Password?</div>
                              <div class="checkbox">
                                 
                             
                              </div>
                              <button class="btn btn-lg btn-login btn-block" name="submit" type="submit" onclick="return Valid_add_login(document.login);">Sign in</button>
                              <div class="clearfix deatel">
                              
                              </div>
                              
 
 
                             
                          </form>
                      </div>
        </div>
<div class="col-xs-6 color-log">
<div class=" yoyo" >
                      <p class="lead">REGISTER now for free, <span class="text-success">or SUBSCRIBE to:</span></p>
                      <ul class="list-unstyled" style="line-height: 2">
                          <li><span class="fa fa-check text-success"></span> See all your shiprments and orders</li>
                          <li><span class="fa fa-check text-success"></span> See all available loads</li>
                          <li><span class="fa fa-check text-success"></span> Enjoy benefits and special offers</li>
                          <li><span class="fa fa-check text-success"></span> Be part of a virtual transport network</li>

                          
                      </ul>
                      <p><a href="registration.html" class="btn btn-info btn-block">Yes please, register me now!</a></p>
                      </div>
                  </div>  
                    </div>
   </div>
 </div>
 
 
        
                  
  <!--End Header-->

      <!-- END CLIENTS -->
    </div>
  <!--End Main-->
  
  <!--End Footer--> 
</div>
</body>
</html>