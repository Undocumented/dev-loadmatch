<?php include_once('includes/global.php'); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="{$site_url}/templates/images/icon.png">
<title><?php echo $site_title; ?></title> 
<meta name="description" content="<?php echo $site_title; ?>" /> 
<meta name="keywords" content="<?php echo $site_title; ?>" />

<meta name="google-site-verification" content="" />


<body>

<?php echo $under_construct; ?>

</body>

</html>
