<?php 
echo "\r\n";
include_once("includes/global.php");
$page = "home";
$action = "show_home";
$defaultpage = "comingsoon";
if( isset($_REQUEST["page"]) ) 
{
    $page = $_REQUEST["page"];
}

if( isset($_REQUEST["action"]) ) 
{
    $action = $_REQUEST["action"];
}

$objSmarty->assign("page", $page);
$objSmarty->assign("action", $action);
switch( $page ) 
{
    case "home":
        switch( $action ) 
        {
            case "show_home":
                $home->show_home();
                $page = "show_home";
                break;
            case "contact":
                $home->contact();
                $page = "contact";
                break;
            case "about":
                $home->about();
                $page = "about";
                break;
            case "privacy":
                $home->privacy();
                $page = "privacy";
                break;
            case "terms":
                $home->terms();
                $page = "terms";
                break;
            case "howitworks":
                $page = "howitworks";
                break;
            case "news":
                $home->news();
                $page = "news";
                break;
            case "notice_board":
                $home->notice_board();
                $page = "notice_board";
                break;
            default:
                $page = $defaultpage;
        }
        break;
    case "users":
        switch( $action ) 
        {
            case "login":
                $users->login();
                $page = "login";
                break;
            case "logout":
                $users->logout();
                break;
            default:
                $page = $defaultpage;
        }
        break;
    case "client":
        switch( $action ) 
        {
            case "registration":
                $client->registration();
                $page = "registration";
                break;
            case "profile":
                $client->profile();
                $page = "profile";
                break;
            case "edit_profile":
                $client->edit_profile();
                $page = "edit_profile";
                break;
            case "search_shipping":
                $client->search_shipping();
                $page = "search_shipping";
                break;
            case "detail_shiping":
                $client->detail_shiping();
                $page = "detail_shiping";
                break;
            case "post_shipping":
                is_User_Login();
                $client->post_shipping();
                $page = "post_shipping";
                break;
            case "forgot_password":
                $client->forgot_password();
                $page = "forgot_password";
                break;
            case "user_review":
                $client->user_review();
                break;
            case "view_agent_review":
                $client->view_agent_review();
                $page = "view_agent_review";
                break;
            case "cust_fedback":
                $client->cust_fedback();
                $page = "cust_fedback";
                break;
            case "trans_fedback":
                $client->trans_fedback();
                $page = "trans_fedback";
                break;
            default:
                $page = $defaultpage;
        }
        break;
    case "myaccount":
        switch( $action ) 
        {
            case "my_post_job":
                $myaccount->my_post_job();
                $page = "my_post_job";
                break;
            case "edit_post_shipping":
                $myaccount->edit_post_shipping();
                $page = "edit_post_shipping";
                break;
            case "booked":
                $myaccount->booked();
                $page = "booked";
                break;
            case "show_booked_quote":
                $myaccount->show_booked_quote();
                $page = "show_booked_quote";
                break;
            case "detail_quote":
                $myaccount->detail_quote();
                $page = "detail_quote";
                break;
            case "give_quote":
                $myaccount->give_quote();
                $page = "give_quote";
                break;
            case "detail_shiping_booked":
                $myaccount->detail_shiping_booked();
                $page = "detail_shiping_booked";
                break;
            case "show_booked_agent_detail":
                $myaccount->show_booked_agent_detail();
                $page = "show_booked_agent_detail";
                break;
            case "booked_cancel":
                $myaccount->booked_cancel();
                $page = "booked_cancel";
                break;
            case "booked_complete":
                $myaccount->booked_complete();
                $page = "booked_complete";
                break;
            case "inbox":
                $myaccount->inbox();
                $page = "inbox";
                break;
            case "process":
                $myaccount->process();
                $page = "process";
                break;
            default:
                $page = $defaultpage;
        }
        break;
}
if( file_exists(DOC . "templates/" . $page . ".tpl") ) 
{
    $objSmarty->assign("contentBody", $page . ".tpl");
}
else
{
    $objSmarty->assign("contentBody", "comingsoon.tpl");
}

$objSmarty->display("index.tpl");
$objSmarty->clear_cache("index.tpl");
$objSmarty->clear_cache($page . ".tpl");
ob_end_flush();

